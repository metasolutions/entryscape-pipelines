# EntryScape Pipelines

A collection of node scripts to manage basics tasks regarding pipelines in EntryScape.

## Installation

```
git clone https://bitbucket.org/metasolutions/entryscape-pipelines.git
cd entryscape-pipelines
git checkout develop

yarn
```

## Setup

Create a ``config.js``, a ``local.js`` and a ``mailconfig.js`` configuration file based on the available ``(config | local | mailconfig).js_example `` files. Then, create a ``output`` folder.

```
touch local.js
touch config.js
touch mailconfig.js

mkdir output
```

### Harvest example

To harvest with a minimal configuration, the config.js and mailconfig.js files can simply be stated as empty exports

```
module.exports = {};
```

The local.js file however, need info on repository, username and password.

It is then setup for a harvest. A pipeline can be created using the pipeline recipe located in the file ``input/reciptes/example_dcat.json``. This pipeline contains a fetch transform and a merge transform. It fetches the rdf/xml file ``input/sources/example_dcat.rdf``, and merge it as a DCAT catalog. Create such a pipeline from this recipe via

```
./pl create -t custom -d "Example;example@example.com;00000;www.example.com" -p input/recipes/example_dcat.json
```

Check the context id of this newly created pipeline via

```
./pl create -t custom -d "Example;example@example.com;00000;www.example.com" -p input/recipes/example_dcat.json
```

Finally, execute the pipeline using its context id, and make the harvest

```
./pl exec -c <contextID>
node src/daemon/run
```

## CLI interface

To check the commands available:

```
~/entryscape-pipelines$ ./pl

  Usage: pl [options] [command]

  A command line utility to manage EntryScape pipelines


  Options:

    -V, --version  output the version number
    -h, --help     output usage information


  Commands:

    ls [results]  List pipelines or pipeline results
    rm [results]  Remove pipeline results
    create        Create pipelines via file import or inline
    exec          Executes pipeline(s), create pipeline results in pending status, filter by context or resource URI
    help [cmd]    display help for [cmd]

```

To get help on a specific command:

```
~/entryscape-pipelines$ ./pl create --help

  Usage: pl-create [options]


  Options:

    -f, --file <file>  CSV file containing the pipeline information. Rows look like  `orgtype,name,email,web,orgid`
    -d, --data <data>  A CSV like row containing the pipeline information (orgtype, name, email, web, orgid). E.g "Kommun,Flens kommun,flenskommun@flen.se,www.flen.se,212000-0332"
    -h, --help         output usage information
```

## Running the harvester daemon

The harvester daemon runs with [forever](https://www.npmjs.com/package/forever).

To run the daemon, first, make sure you have a harvester configuration in ``src/daemon/daemon.json``. Make a copy and adapt the ``daemon.json_example`` where you set the ``workingDir`` and ``sourceDir`` appropriately.

```
{
  "uid": "entryscape-harvester",
  "append": true,
  "workingDir": "", // set accordingly for your environment (pipeline repo)
  "sourceDir": "", // set accordigly for your environment (pipeline_repo/src/daemon/)
  "script": "run.js",
  "killSignal": "SIGTERM",
  "killTree": true
}
```

Then, you can start the harvester by running the following in the root of the repository

```
yarn startHarvester
```

To stop all running daemons on the server (that use forever) run

```
forever stopall
```

Check the [forever](https://www.npmjs.com/package/forever) documentation for more.

