# syntax=docker/dockerfile:1

# FIXME does not work yet, execution of built image fails
FROM node:18-alpine
WORKDIR /pipelines
COPY . /pipelines
RUN apk add git
RUN yarn install --production
CMD [ "node" ]
