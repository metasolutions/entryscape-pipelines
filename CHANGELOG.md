# Changelog

All notable changes to the Pipelines project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.11.3]([]) - [2024-02-12]

### Fixed

- Ability to send more brief validation mail

## [1.11.2]([]) - [2024-02-04]

### Fixed

- GML
    - Add distribution as .zip if name contains it
    - Skip modified date for dataset if configurated

## [1.11.1]([]) - [2024-02-03]

### Fixed

- Tasks
    - Logging and pruning resuts for HVD and NSIP exports

## [1.11.0]([]) - [2024-01-29]

### Added

- Tasks
    - Export for HVD data items
    - Export for non-public data items / NSIP

## [1.10.0]([]) - [2024-01-22]

### Added

- Mapper: HVD category

## [1.9.0]([]) - [2024-01-22]

### Added

- Replaced class Entrysync and Dedupindex with Entrysync repository
- For deduplication, a PSI index and Dedup index with update function was implemented

## [1.8.3]([]) - [2024-01-09]

### Fixed

- Initialize datasetseriersURIs as array in stream mode in dcat harvest

- Check for configuration variable in convert/GML/distribution

## [1.8.2]([]) - [2024-12-19]

### Fixed

- Corrected namespace for owl in owl:sameAs

## [1.8.1]([]) - [2024-12-18]

### Added

- Mapper that can replace publisher with another publisher, using owl:sameAs

## [1.8.0]([]) - [2024-12-11]

### Added

- Support for Dataset Series

## [1.7.7]([]) - [2024-12-11]

### Changed

- DcatIndex: Fix previous change, so that a ignored property is not added (Exclude publishers from conformsTo to avoid wrong index)

## [1.7.6]([]) - [2024-12-10]

### Changed

- DcatIndex: Exclude publishers from dcterms:conformsTo to avoid wrong index 
- Task -> export: Include esterms:IndependentDataService

## [1.7.5]([]) - [2024-12-10]

### Changed

- Task -> sync: No need to load across context when organizations are loaded explicitly
- GML: add html distributions

## [1.7.4]([]) - [2024-11-26]

### Changed

- GML conversion:
    - disitribution: Search for additional languages for title and description

## [1.7.3]([]) - [2024-11-19]

### Changed

- PX conversion:
    - add landingpage and documentation, by configuration

## [1.7.2]([]) - [2024-11-13]

### Changed

- PX conversion:
    - add source as landingpage by config

- GML conversion:
    - contactpoint: replace name by config
    - publisher: replace name by config


## [1.7.1]([]) - [2024-11-12]

### Changed

- Miscellaneous:
    - initalize global variables as local for fetchPX transform

- GML conversion:
    - map format to pubeu vocab via configuration

## [1.7.0]([]) - [2024-11-7]

### Added

- Miscellaneous:
    - loadFromURL: possibility to add delay (sleep) between calls. Via configuration.

- GML conversion:
    - add HVD category by url (PI-124)

### Changed

- Px transform: 
    - generalized these transforms (fetch & convert) in order to harvest px web in general. (PI-120)

## [1.6.1]([]) - [2024-09-17]

### Changed

- Copy transform: 
    - only warn once when max limit is reached in saved source graph

## [1.6.0]([]) - [2024-09-09]

### Changed

- Config: 
    - denylist as an lias to blacklist (PI-114)
    - empty configuration possible for trivial pipelines such as dcat with fetch and merge transform (PI-115)

### Added

- Copy transform:
    - Make validation possible (PI-116)
    - Save source graph as dcterms:source (PI-118)

## [1.5.6]([]) - [2024-07-09]

### Changed

- Link extraction: use config.extractLinksEntryLabel to choose what metadata field to use as entry label

## [1.5.5]([]) - [2024-07-04]

### Fixed

- Link checker: use config.attempt when updating the pending checks file

## [1.5.4]([]) - [2024-07-04]

### Fixed

- GML contactpoint conversion: check if namespaces are configurated before initialization
- GML contactpoint conversion: rename namespace variable (since namespace variable seems reserved in javascript)
- Link checker: check if config is set
- Link extractor: check if config is set
- Link reporter: check if config is set

### Changed

- High-value dataset mark and categories in GML conversion

## [1.5.3]([]) - [2024-06-26]

### Added

- High-value dataset mark and categories in GML conversion

## [1.5.2]([]) - [2024-06-26]

### Fixed

- Generalize manespace use in gml conversion

## [1.5.1]([]) - [2024-06-13]

### Fixed

- Add fetchRDF metadata field to pipeline result, when performing a fetch transform.

## [1.5.0]([]) - [2024-06-11]

### Added

- Possibility to fetch JSON-LD

## [1.4.1]([]) - [2024-05-15]

### Fixed

- Update of rdfjson and entrystore-js dependencies to get rid of upstream issue with namespace declarations not being detected correctly everywhere in striped syntax in RDF/XML.


## [1.4.0]([]) - [2024-05-06]

### Added

- Improved loadFromURL with configurable retries and new package ([PI-108](https://metasolutions.atlassian.net/browse/PI-108))


## [1.3.0]([]) - [2024-04-19]

### Added

- Support catalog synchronization between instances via the sync task ([PI-110](https://metasolutions.atlassian.net/browse/PI-110))
- Improved copy transform for supporting copying of entries from many contexts ([PI-109](https://metasolutions.atlassian.net/browse/PI-109))

## [1.2.3]([]) - [2024-04-05]

### Fixed
- Use util function for detecting custom transforms, in fetch and convert steps

### Added
- Move functionality to detect custom transforms as a util function. Also remove need for path assignment in configuration file.

## [1.2.2]([]) - [2024-04-02]

### Fixed
- Extraction of merge profile types moved into util function

### Added
- Copy transform can now sync on specified rdftypes

## [1.2.1]([]) - [2024-03-21]

### Fixed
- Possibility to use a profile as a pipeline merge argument for setting merge types via local configuration

## [1.2.0]([]) - [2024-01-29]

### Fixed

### Added
- Add textual harvesting report from pipelines, in form of expandable report section with details ([PI-87](https://metasolutions.atlassian.net/browse/PI-87?atlOrigin=eyJpIjoiNzA0ODFiYTI4Mjg1NDFmYjlhYjczMDk2YjhhYzNlYWIiLCJwIjoiaiJ9))
- Introduction of log library, log4js ([PI-39](https://metasolutions.atlassian.net/browse/PI-39?atlOrigin=eyJpIjoiNjE4YTkyZDY1OTRiNGQwM2I5MDIxOGViMjkwNjk3OWMiLCJwIjoiaiJ9))
- Harvest from local file, when fetching rdf ([PI-94](https://metasolutions.atlassian.net/browse/PI-94?atlOrigin=eyJpIjoiZTNjOGFmNjY2ZDIyNDdjZjg4YjU4ZWZlNTFmMjVkODciLCJwIjoiaiJ9))


### Changed
- Possibility to use more generic dichotomy primary / supportive when creating custom pipelines ([PI-88](https://metasolutions.atlassian.net/browse/PI-88?atlOrigin=eyJpIjoiNDg3YTQwMWVhNmVjNGQ4OGIxYmYxMDU5MmQ5YWMyNjgiLCJwIjoiaiJ9))

## [1.1.3]([]) - [2023-11-9]

### Fixed-
- Update @entryscape/rdfjson to 2.7.5 for latest rdfxml-streaming-parser
### Added
### Changed

## [1.1.2]([]) - [2023-10-19]

### Fixed-
- Update @entryscape/rdfjson to 2.7.4 for replacing replaceBlankWithURI()
### Added
### Changed

## [1.1.1]([]) - [2023-10-16]

### Fixed
- Check safety limit in fetchCSW after filtering on datasets
- Check error before accessing error name when running a job
### Added
### Changed

### Removed
## [1.1.0]([]) - [2023-10-16]

### Fixed
### Added
- Safety limit rdf fetch transform ([PI-90](https://metasolutions.atlassian.net/browse/PI-90?atlOrigin=eyJpIjoiMzY2NDg5MjdhNGVhNDdhNjg2NWQ4Nzc4NGNlZmEwMTMiLCJwIjoiaiJ9))
- Safety limit ckan fetch transform ([PI-91](https://metasolutions.atlassian.net/browse/PI-91?atlOrigin=eyJpIjoiNmM0MzRkYmMzY2QxNGJmMDg5YTliZjA1MzY1NTA5ZDEiLCJwIjoiaiJ9))
- Safety limit stream fetch transform ([PI-92](https://metasolutions.atlassian.net/browse/PI-92?atlOrigin=eyJpIjoiYTdhZmE2ZjU0M2ZlNDE4ZTgxNmY5OTY4ODQzMWQ1ZGEiLCJwIjoiaiJ9))
### Changed

### Removed

## [1.0.1]([]) - [2023-10-05]

### Fixed
- CSW fetch operation should stop if response has exception text 
### Added

### Changed
- Replaced replaceBlankWithURI() with corresponding method in rdfjson
- Use more than one custom convert transform ([PI-76](https://metasolutions.atlassian.net/browse/PI-76?atlOrigin=eyJpIjoiOTJhZWE0NmExYjgwNGZiZDkxNDYyMzUxNDE5NjhkZjkiLCJwIjoiaiJ9))
### Removed

## [1.0.0]([https://bitbucket.org/metasolutions/entryscape-pipelines/branches/compare/release%2F1.0.0%0Dmaster]) - [2023-09-01]

### Fixed

### Added
- Possibility to set a lower limit on how many items to be found at data source in order to proceed with harvest
- Move custom transforms into a separate repository
### Changed
- Configure email smtp settings in a separate file ([PI-80](https://metasolutions.atlassian.net/browse/PI-80))

### Removed

## Version 0 (commit 9b9b523) - 2022-10-11

# - Changelog initiated - 2022-10-12
