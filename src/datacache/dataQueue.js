const fileSystem = require('fs');
const path = require('path');
const { dataCacheProps } = require('../config');
const logger = require('../utils/logger/logger');

const dir = path.join(__dirname, '..', '..', 'output', 'data', '/');

module.exports = {
  /**
   * Filters link checks that correspond to data to cache according to the 'dataCacheProps'
   * configuration and writes them on a JSON file.
   *
   * @param {Object[]} report - Non-filtered link checks
   */
  exportDataLinks(report) {
    if (!fileSystem.existsSync(dir)) {
      fileSystem.mkdirSync(dir);
    }

    // Filter out the checks according to the provided configuration
    const dataChecks = report.filter(
      ({ entryType, property, entryFormat }) => (entryFormat === 'text/csv' || entryFormat === 'http://publications.europa.eu/resource/authority/file-type/CSV')
    );

    const fileName = `links.json`;
    const filePath = `${dir}${fileName}`;

    if (dataChecks.length === 0) {
      logger.info(
        `Data Queue - No links to data found. Check 'dataCacheProps' in your configuration.`
      );
      return;
    }

    const fileDescriptor = fileSystem.openSync(filePath, 'w');
    fileSystem.writeSync(
      fileDescriptor,
      JSON.stringify(dataChecks, null, '  '),
      0,
      'utf8'
    );
    logger.info(
      `Data cache queue - Exported ${dataChecks.length} Data link(s) on file ${fileName}.`
    );
  },

  /**
   * Retrieves the checks stored in the api-links.json file,
   *
   * @return {Array}
   */
  importDataLinks() {
    try {
      const jsonData = fileSystem.readFileSync(`${dir}links.json`, 'utf8');
      return JSON.parse(jsonData);
    } catch (err) {
      if (err.code == 'ENOENT') {
        logger.error('No data links file found');
      } else {
        logger.error(err);
      }
    }
    return [];
  },
};
