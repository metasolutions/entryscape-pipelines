const path = require('path');
const fileSystem = require('fs');
const fetch = require('node-fetch');
const md5 = require('md5');
const config = require('../config');
const { importDataLinks } = require('./dataQueue');
const mail = require('../utils/mail');
const logger = require('../utils/logger/logger');

const dir = path.join(__dirname, '..', '..', 'output', 'data', 'cache', '/');

module.exports = async () => {
  if (!fileSystem.existsSync(dir)) {
    fileSystem.mkdirSync(dir);
  }

  const AbortController = globalThis.AbortController || await require('abort-controller');

  logger.info('-----------------------------------');
  const date = new Date();
  const startMessage = `Started data caching on the ${date}:`;
  logger.info(startMessage);
  const warningReport = [];
  const errorReport = [];
  const abortReport = [];
  let success = 0;
  let warning = 0;
  let errored = 0;
  const dataLinks = importDataLinks();
  logger.info(`Imported data links from data/links.json: ${dataLinks.length}`);

  const filteredDataLinks = dataLinks.filter((dl) => ((dl.status === 'success' || dl.status === 'excluded') && dl.property === 'dcat:accessURL'));
  const downloaded = new Set();
  let csvCounter = 0;
  for (let i = 0; i < filteredDataLinks.length; i++) {
    const check = filteredDataLinks[i];
    if (downloaded.has(check.uri)) {
      logger.info(`URI for ${check.entryLabel} already downloaded`);
      continue;
    }
    downloaded.add(check.uri);
    try {
      const controller = new AbortController();
      const timeLimit = 30000;
      const timeout = setTimeout(() => {
        logger.error(`Error: fetch operation exeeded time limit of ${timeLimit} ms - ${i} of ${filteredDataLinks.length} ${check.entryLabel} - ${check.uri}`);
        errorReport.push('\n' + `Error: fetch operation exeeded time limit of ${timeLimit} ms - ${i} of ${filteredDataLinks.length} ${check.entryLabel} - ${check.uri}`);
        errored += 1;
        controller.abort();
      }, timeLimit);
      const useragent = config.datacache.agent ? config.datacache.agent : 'Data cacher';
      const response = await fetch(check.uri, {
        signal: controller.signal,
        headers: {
          "User-Agent": useragent,
        }
      });
      clearTimeout(timeout);
      const { status } = response;
      if (status !== 200) {
        const abortMsg = `Aborted downloaded ${i} of ${filteredDataLinks.length} named: ${check.entryLabel} with md5 ${md5(check.uri)} due to status: ${status}`;
        logger.info(abortMsg);
        abortReport.push(`\n${abortMsg}`);
        continue;
      }
      const contentType = response.headers.get('Content-Type');
      if (contentType.indexOf('text/csv') > -1 || contentType.indexOf('text/comma-separated-value') > -1) {
        csvCounter += 1;
      } else {
        const abortMsg = `Aborted downloaded ${i} of ${filteredDataLinks.length} named: ${check.entryLabel} with md5 ${md5(check.uri)} due to content type: ${contentType}`;
        logger.info(abortMsg);
        abortReport.push(`\n${abortMsg}`);
        continue;
      }
      const filePath = path.join(dir, '/', md5(check.uri));
      const filePathTemporary = `${filePath}-tmp`;
      const dest = fileSystem.createWriteStream(filePathTemporary);
      let timer;
      let redDataSize = 0;
      const timeoutValue = 33000;
      const maxDataSize = 151155000;

      await new Promise((resolve, reject) => {
        response.body.pipe(dest);
        timer = setTimeout(() => {
          const streamError = new Error(`Error: stream operation exceeded time limit of ${timeoutValue} ms - ${i} of ${filteredDataLinks.length} ${check.entryLabel} - ${check.uri}`);
          dest.destroy(streamError); 
        }, timeoutValue);
        response.body.on('data', (chunk) => {
        redDataSize += chunk.length;
        if (redDataSize > maxDataSize) {
          const maxSizeError = new Error(`Error: max size exceeded ${redDataSize}/${maxDataSize} bytes - ${i} of ${filteredDataLinks.length} ${check.entryLabel} - ${check.uri}`);
          dest.destroy(maxSizeError);
        }
        });
        dest.on('error', (err) => {
          logger.error(err.message);
          errored += 1;
          errorReport.push('\n' + err.message);
          resolve();
        });
        dest.on("finish", () => {
          resolve();
        });
      })
      const stat = fileSystem.statSync(filePathTemporary);
      if (stat.size > 0) {
        logger.info(`Downloaded ${i} of ${filteredDataLinks.length} named: ${check.entryLabel} with size ${stat.size} with md5 ${md5(check.uri)}`);
        fileSystem.copyFileSync(filePathTemporary, filePath);
        success += 1;
      } else {
        const emptydate = new Date();
        logger.info(`Warning: empty download for ${i} of ${filteredDataLinks.length} named: ${check.entryLabel}, ${check.uri}, response code ${response.status} on ${emptydate}`);
        warning += 1;
        warningReport.push(`\n Warning: empty download for ${i} of ${filteredDataLinks.length} named: ${check.entryLabel}, ${check.uri}, response code ${response.status} on ${emptydate}`);
      }
      clearTimeout(timer);
      clearTimeout(timeout);
      fileSystem.unlinkSync(filePathTemporary);
    } catch (e) {
      logger.error(e);
    }
  }
  const enddate = new Date();
  const finishMessage = `Finished data caching on the ${enddate}:`;
  logger.info(finishMessage);
  const reportSummary = `Success: ${success}, warnings ${warning}, error: ${errored} of total ${filteredDataLinks.length}`;
  logger.info(reportSummary);
  let emptyFiles = 0;
  let existingFiles = 0;
  let missingFiles = 0;
  let missingFilesURLs = '';
  let emptyFilesURLs = '';
  for (let i = 0; i < filteredDataLinks.length; i++) {
    const check = filteredDataLinks[i];
    const filePath = path.join(dir, '/', md5(check.uri));
    try {
      const fileStat = fileSystem.statSync(filePath);
      if (fileStat.size === 0) {
        emptyFiles += 1;
        emptyFilesURLs += `\n ${check.entryLabel} - ${check.uri}`;
      } else if (fileStat.size > 0) {
        existingFiles += 1;
      }
    } catch (err) {
      missingFiles += 1;
      missingFilesURLs += `\n ${check.entryLabel} - ${check.uri}`;
    }
  }
  if (config.datacache.mail) {
    const fileCheck = `Existing files: ${existingFiles}, empty files: ${emptyFiles}, missingFiles: ${missingFiles} \n`;
    const missingURL = `Missing URLs: ${missingFilesURLs} \n`;
    const emptyURL = `Empty URLs: ${emptyFilesURLs} \n`;
    const fileCheckReport = fileCheck + missingURL + emptyURL;
    let finalreport = `${startMessage} \n Summary: ${reportSummary} \n Errors: \n ${errorReport}\n Warnings: \n${warningReport}\n${finishMessage}\n${fileCheckReport}\n Downloads with a csv content type: ${csvCounter}\n${abortReport}`;
    finalreport = finalreport.replace(/(\r\n|\n|\r)/gm, '<br>');
    mail.send(config.datacache.mail, 'Datacache report', finalreport, true);
  }
};
