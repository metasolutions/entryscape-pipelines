


const hvdCategoryTree = {

    // Companies and company ownership - http://data.europa.eu/bna/c_a9135398
    'http://data.europa.eu/bna/c_56a1bf47': 'http://data.europa.eu/bna/c_a9135398', // Basic company information: key attributes
    'http://data.europa.eu/bna/c_8f0fac04': 'http://data.europa.eu/bna/c_a9135398', // Company documents and accounts

    // Earth observation and environment - http://data.europa.eu/bna/c_dd313021
    'http://data.europa.eu/bna/c_63b37dd4': 'http://data.europa.eu/bna/c_dd313021', // Air
    'http://data.europa.eu/bna/c_af646f5b': 'http://data.europa.eu/bna/c_dd313021', // Area management / restriction / regulation zones & reporting units
    'http://data.europa.eu/bna/c_c873f344': 'http://data.europa.eu/bna/c_dd313021', // Bio-geographical regions
    'http://data.europa.eu/bna/c_59e64dd4': 'http://data.europa.eu/bna/c_dd313021', // Climate
    'http://data.europa.eu/bna/c_315692ad': 'http://data.europa.eu/bna/c_dd313021', // Elevation
    'http://data.europa.eu/bna/c_4ba9548e': 'http://data.europa.eu/bna/c_dd313021', // Emissions
    'http://data.europa.eu/bna/c_b7de66cd': 'http://data.europa.eu/bna/c_dd313021', // Energy resources
    'http://data.europa.eu/bna/c_7b8fbb64': 'http://data.europa.eu/bna/c_dd313021', // Environmental monitoring facilities
    'http://data.europa.eu/bna/c_e3f55603': 'http://data.europa.eu/bna/c_dd313021', // Geology
    'http://data.europa.eu/bna/c_c3919aec': 'http://data.europa.eu/bna/c_dd313021', // Habitats and biotopes
    'http://data.europa.eu/bna/c_4d63300b': 'http://data.europa.eu/bna/c_dd313021', // Horizontal legislation
    'http://data.europa.eu/bna/c_06b1eec4': 'http://data.europa.eu/bna/c_dd313021', // Hydrography
    'http://data.europa.eu/bna/c_b21e1296': 'http://data.europa.eu/bna/c_dd313021', // Land cover
    'http://data.europa.eu/bna/c_ad9ae929': 'http://data.europa.eu/bna/c_dd313021', // Land use
    'http://data.europa.eu/bna/c_4dd389c5': 'http://data.europa.eu/bna/c_dd313021', // Mineral resources
    'http://data.europa.eu/bna/c_63be22bd': 'http://data.europa.eu/bna/c_dd313021', // Natural risk zones  
    'http://data.europa.eu/bna/c_b7f6a4f3': 'http://data.europa.eu/bna/c_dd313021', // Nature preservation and biodiversity
    'http://data.europa.eu/bna/c_e4358335': 'http://data.europa.eu/bna/c_dd313021', // Noise
    'http://data.europa.eu/bna/c_b40e6d46': 'http://data.europa.eu/bna/c_dd313021', // Oceanographic geographical features
    'http://data.europa.eu/bna/c_91185a85': 'http://data.europa.eu/bna/c_dd313021', // Orthoimagery
    'http://data.europa.eu/bna/c_59c93ba5': 'http://data.europa.eu/bna/c_dd313021', // Production and industrial facilities
    'http://data.europa.eu/bna/c_83aa10a6': 'http://data.europa.eu/bna/c_dd313021', // Protected sites
    'http://data.europa.eu/bna/c_f399050e': 'http://data.europa.eu/bna/c_dd313021', // Sea regions
    'http://data.europa.eu/bna/c_87a129d9': 'http://data.europa.eu/bna/c_dd313021', // Soil
    'http://data.europa.eu/bna/c_793164b6': 'http://data.europa.eu/bna/c_dd313021', // Species distribution
    'http://data.europa.eu/bna/c_38933a65': 'http://data.europa.eu/bna/c_dd313021', // Waste
    'http://data.europa.eu/bna/c_43f88346': 'http://data.europa.eu/bna/c_dd313021', // Water

    // Geospatial - http://data.europa.eu/bna/c_ac64a52d
    'http://data.europa.eu/bna/c_c3de25e4': 'http://data.europa.eu/bna/c_ac64a52d', // Addresses
    'http://data.europa.eu/bna/c_9427236f': 'http://data.europa.eu/bna/c_ac64a52d', // Administrative units
    'http://data.europa.eu/bna/c_642643e6': 'http://data.europa.eu/bna/c_ac64a52d', // Agricultural parcels
    'http://data.europa.eu/bna/c_60182062': 'http://data.europa.eu/bna/c_ac64a52d', // Buildings
    'http://data.europa.eu/bna/c_6a3f6896': 'http://data.europa.eu/bna/c_ac64a52d', // Cadastral parcels
    'http://data.europa.eu/bna/c_6c2bb82d': 'http://data.europa.eu/bna/c_ac64a52d', // Geographical names
    'http://data.europa.eu/bna/c_fbd2fc3f': 'http://data.europa.eu/bna/c_ac64a52d', // Reference parcels


    // Meteorological - http://data.europa.eu/bna/c_164e0bf5
    'http://data.europa.eu/bna/c_36807466': 'http://data.europa.eu/bna/c_164e0bf5', // Climate data: validated observations
    'http://data.europa.eu/bna/c_13e3cf16': 'http://data.europa.eu/bna/c_164e0bf5', // NWP model data
    'http://data.europa.eu/bna/c_3af3368c': 'http://data.europa.eu/bna/c_164e0bf5', // Observations data measured by weather stations
    'http://data.europa.eu/bna/c_d13a4420': 'http://data.europa.eu/bna/c_164e0bf5', // Radar data
    'http://data.europa.eu/bna/c_be47b010': 'http://data.europa.eu/bna/c_164e0bf5', // Weather alerts

    // Mobility - http://data.europa.eu/bna/c_b79e35eb
    'http://data.europa.eu/bna/c_b151a0ba': 'http://data.europa.eu/bna/c_b79e35eb', // Inland waterways datasets
    'http://data.europa.eu/bna/c_1226dc1a': 'http://data.europa.eu/bna/c_b79e35eb', // Bank of waterway at mean water level
    'http://data.europa.eu/bna/c_664c9e5a': 'http://data.europa.eu/bna/c_b79e35eb', // Boundaries of the fairway/navigation channel
    'http://data.europa.eu/bna/c_883d0205': 'http://data.europa.eu/bna/c_b79e35eb', // Contours of locks and dams
    'http://data.europa.eu/bna/c_f76b01e6': 'http://data.europa.eu/bna/c_b79e35eb', // Fairway characteristics
    'http://data.europa.eu/bna/c_99bc517f': 'http://data.europa.eu/bna/c_b79e35eb', // Isolated dangers in the fairway/navigation channel under and above water
    'http://data.europa.eu/bna/c_298ffb73': 'http://data.europa.eu/bna/c_b79e35eb', // Links to the external xml-files with operation times of restricting structures
    'http://data.europa.eu/bna/c_9cbe4435': 'http://data.europa.eu/bna/c_b79e35eb', // List of navigation aids and traffic signs
    'http://data.europa.eu/bna/c_3e8e3bf7': 'http://data.europa.eu/bna/c_b79e35eb', // Location and characteristics of ports and transhipment sites
    'http://data.europa.eu/bna/c_407951ff': 'http://data.europa.eu/bna/c_b79e35eb', // Location of ports and transhipment sites
    'http://data.europa.eu/bna/c_fa2a1c3a': 'http://data.europa.eu/bna/c_b79e35eb', // Long-time obstructions in the fairway and reliability
    'http://data.europa.eu/bna/c_2037ada4': 'http://data.europa.eu/bna/c_b79e35eb', // Navigation rules and recommendations
    'http://data.europa.eu/bna/c_b24028d7': 'http://data.europa.eu/bna/c_b79e35eb', // Official aids-to-navigation (e.g. buoys, beacons, lights, notice marks)
    'http://data.europa.eu/bna/c_7e19ef26': 'http://data.europa.eu/bna/c_b79e35eb', // Other physical limitations on waterways
    'http://data.europa.eu/bna/c_e5f69a04': 'http://data.europa.eu/bna/c_b79e35eb', // Present and future water levels at gauges
    'http://data.europa.eu/bna/c_25f43866': 'http://data.europa.eu/bna/c_b79e35eb', // Rates of waterway infrastructure charges
    'http://data.europa.eu/bna/c_1e787364': 'http://data.europa.eu/bna/c_b79e35eb', // Reference data for water level gauges relevant to navigation
    'http://data.europa.eu/bna/c_03ba8d92': 'http://data.europa.eu/bna/c_b79e35eb', // Regular lock and bridge operating times
    'http://data.europa.eu/bna/c_f6886b00': 'http://data.europa.eu/bna/c_b79e35eb', // Restrictions caused by flood and ice
    'http://data.europa.eu/bna/c_bc8941d9': 'http://data.europa.eu/bna/c_b79e35eb', // Shoreline construction
    'http://data.europa.eu/bna/c_593bc53d': 'http://data.europa.eu/bna/c_b79e35eb', // Short term changes of aids to navigation
    'http://data.europa.eu/bna/c_66b946cb': 'http://data.europa.eu/bna/c_b79e35eb', // Short term changes of lock and bridge operating times
    'http://data.europa.eu/bna/c_e50004c6': 'http://data.europa.eu/bna/c_b79e35eb', // State of the rivers, canals, locks and bridges
    'http://data.europa.eu/bna/c_b121e2f6': 'http://data.europa.eu/bna/c_b79e35eb', // Temporary obstructions in the fairway
    'http://data.europa.eu/bna/c_fef208ab': 'http://data.europa.eu/bna/c_b79e35eb', // Water depths contours in the navigation channel
    'http://data.europa.eu/bna/c_c19af83a': 'http://data.europa.eu/bna/c_b79e35eb', // Waterway axis with kilometres indication
    'http://data.europa.eu/bna/c_4b74ea13': 'http://data.europa.eu/bna/c_b79e35eb', // Transport networks

    // Statistics - http://data.europa.eu/bna/c_e1da4e07
    'http://data.europa.eu/bna/c_dd8f4797': 'http://data.europa.eu/bna/c_e1da4e07', // Consolidated government gross debt
    'http://data.europa.eu/bna/c_424bb0b4': 'http://data.europa.eu/bna/c_e1da4e07', // Current healthcare expenditure
    'http://data.europa.eu/bna/c_a2c6dcd8': 'http://data.europa.eu/bna/c_e1da4e07', // Employment
    'http://data.europa.eu/bna/c_92874eb2': 'http://data.europa.eu/bna/c_e1da4e07', // Environmental accounts and statistics
    'http://data.europa.eu/bna/c_20cd11bb': 'http://data.europa.eu/bna/c_e1da4e07', // EU International trade in goods statistics 
    'http://data.europa.eu/bna/c_6a7250c1': 'http://data.europa.eu/bna/c_e1da4e07', // Fertility
    'http://data.europa.eu/bna/c_4ac557e7': 'http://data.europa.eu/bna/c_e1da4e07', // Government expenditure and revenue
    'http://data.europa.eu/bna/c_c0022235': 'http://data.europa.eu/bna/c_e1da4e07', // Harmonised Indices of consumer prices
    'http://data.europa.eu/bna/c_2aed31f9': 'http://data.europa.eu/bna/c_e1da4e07', // Industrial producer price index breakdowns by activity
    'http://data.europa.eu/bna/c_34abf8c1': 'http://data.europa.eu/bna/c_e1da4e07', // Industrial production
    'http://data.europa.eu/bna/c_a8b937c4': 'http://data.europa.eu/bna/c_e1da4e07', // Inequality
    'http://data.europa.eu/bna/c_4acb6bf3': 'http://data.europa.eu/bna/c_e1da4e07', // Mortality
    'http://data.europa.eu/bna/c_b72b721f': 'http://data.europa.eu/bna/c_e1da4e07', // National accounts – GDP main aggregates
    'http://data.europa.eu/bna/c_95da87c7': 'http://data.europa.eu/bna/c_e1da4e07', // National accounts – key indicators on corporations
    'http://data.europa.eu/bna/c_59627af3': 'http://data.europa.eu/bna/c_e1da4e07', // National accounts – key indicators on households
    'http://data.europa.eu/bna/c_f2b50efd': 'http://data.europa.eu/bna/c_e1da4e07', // Population
    'http://data.europa.eu/bna/c_317b9493': 'http://data.europa.eu/bna/c_e1da4e07', // Population, Fertility, Mortality
    'http://data.europa.eu/bna/c_23385471': 'http://data.europa.eu/bna/c_e1da4e07', // Potential labour force
    'http://data.europa.eu/bna/c_04bf94a3': 'http://data.europa.eu/bna/c_e1da4e07', // Poverty
    'http://data.europa.eu/bna/c_a3767648': 'http://data.europa.eu/bna/c_e1da4e07', // Tourism flows in Europe
    'http://data.europa.eu/bna/c_fd4e881c': 'http://data.europa.eu/bna/c_e1da4e07', // Unemployment
    'http://data.europa.eu/bna/c_a49ec591': 'http://data.europa.eu/bna/c_e1da4e07', // Volume of sales by activity
};

const addHVDCategory = (graph, hvdStatements) => {

    hvdStatements.forEach((statement) => {
        const category = statement.getValue();
        if (category in hvdCategoryTree)  {
            const parentCategory = hvdCategoryTree[category];
            graph.add(statement.getSubject(), 'http://data.europa.eu/r5r/hvdCategory', parentCategory);
        }
    })
};

/**
 * The hvdCategory mapper adds the top-level category for any provided non top-level category
 */
module.exports = {

  init(params) {
    return Promise.resolve();
  },

  async exec(passOnObj, pipeline, args) {

    let graph; 
    if (passOnObj.mainGraph) graph = passOnObj.mainGraph;
    else graph = passOnObj;
    
    const hvdStatements = graph.find(null, 'http://data.europa.eu/r5r/hvdCategory');
    if (hvdStatements.length > 0) {
        addHVDCategory(graph, hvdStatements);
    }
  },
};