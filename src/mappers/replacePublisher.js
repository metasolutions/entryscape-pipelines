const { namespaces } = require('@entryscape/rdfjson');
const { EntryStore } = require('@entryscape/entrystore-js');
const config = require('../config');

namespaces.add('esterms', 'http://entryscape.com/terms/');

let sameAsMap = new Map();
let blockedAgents = new Set();
let indexTime = 0;

/**
 * Update:
 * 1. a set of URIs of official publisher URIs.
 * 2. a map of URIs that should be replaced with official publisher URIs.
 *
 * @return {Promise<void>}
 */
const reIndex = async () => {
  if (config.replacePublisher)  {
    const thresholdTime = new Date().getTime() - (config.replacePublisher.threshold || 24*3600*1000);
    // If less time than threshold has passed, don't reindex.
    if (indexTime > thresholdTime ) {
      return;
    }
    indexTime = new Date().getTime();
    sameAsMap = new Map();
    blockedAgents = new Set();
    const repository = config.replacePublisher.repository;
    const context = config.replacePublisher.context;
    const entrystore = new EntryStore(repository);

    const query = entrystore.newSolrQuery().context(context)
      .publicRead(true).limit(50)
      .rdfType(['foaf:Agent', 'foaf:Person', 'foaf:Organization']);
    await query.forEach((e) => {
      const md = e.getMetadata();
      const ruri = e.getResourceURI();
      blockedAgents.add(ruri);
      const sameAs = md.find(ruri, 'owl:sameAs');
      sameAs.map((s) => {
        if (s.getType() === 'uri') {
          sameAsMap.set(s.getValue(), s.getSubject());
        }
      })
    });
  }
};

/**
 * Discovers a set of publishers in a given repository and does the following:
 * 1. Blocks so those publishers (based on URI) are not harvested as duplicates
 * 2. Changes to the official URIs for those publishers for all explicit sameAs URIs
 */
module.exports = {
  init(params) {
    return Promise.resolve();
  },

  async exec(passOnObj, pipeline, args) {
    await reIndex();
    const g = passOnObj._graph;
    if (config.replacePublisher)  {
      // Remove all metadata about URIs (publishers) maintained externally.
      for (const uri of blockedAgents) {
        if (g.hasOwnProperty(uri)) {
          passOnObj.findAndRemove(uri);
        }
      }
      // Loop over all uris that should be replaced according to sameAs info
      // Also remove the original triples of those URIs as they should not be
      // incorporated into the respository
      for (const item of sameAsMap) {
        if (g.hasOwnProperty(item[0])) {
          passOnObj.findAndRemove(item[0]);
          passOnObj.replaceURI(item[0], item[1]);
        }
      }
    }
  },
};