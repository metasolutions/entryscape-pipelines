const { namespaces } = require('@entryscape/rdfjson');
const select = require('../utils/cswSelect');
const { recordBase, createSubject } = require('../transforms/convert/GML/utils');

namespaces.add('esterms', 'http://entryscape.com/terms/');

/**
 * If API appears in title of dataset (not neccessarily served by a real dataservice)
 * @param {rdfjson/Graph} graph
 */
const apiInTitle = (graph, subject) => {
  const apiRegexp = /^((Api|API)|(Api|API)[\s.,:].*|.*\s(api|Api|API)|.*\s(api|Api|API)[\s.,:].*)$/;
  return graph.find(subject, 'dcterms:title').find((statement) => statement.getValue().match(apiRegexp)) !== undefined;
};

const getServiceSubject = (dataservice, args) => {
  const base = args.base ? args.base : 'http://example.com/';
  const serviceID = select('string(gmd:fileIdentifier/gco:CharacterString)', dataservice);
  const serviceURI = select(
    `string(${recordBase}gmd:citation/*/gmd:identifier/*/gmd:code/gco:CharacterString)`,
    dataservice,
  );
  const serviceSubject = createSubject(base, serviceURI, serviceID);
  return serviceSubject;
};

/**
 * The dataserviceMarks mapper mainly adds metadata to datasets and their
 * distributions, if the dataset in question is served by a dataservice.
 * The mapper then adds metadata to describe this, typing the dataset
 * as served by a dataservice and providing link to the dataservice itself.
 */
module.exports = {

  init(params) {
    return Promise.resolve();
  },

  async exec(passOnObj, pipeline, args) {
    if (passOnObj.mainGraph) {
      const graph = passOnObj.mainGraph;
      const datasetStatements = graph.find(null, 'rdf:type', 'dcat:Dataset');
      const dataserviceStatements = graph.find(null, 'rdf:type', 'dcat:DataService');

      if (dataserviceStatements.length > 0) {
        const subject = dataserviceStatements[0].getSubject();
        const servedDatasets = passOnObj.service2datasets[passOnObj.mainURI];
        if (servedDatasets && servedDatasets.length > 1) {
          graph.add(subject, 'rdf:type', 'esterms:IndependentDataService');
        }
      } else if (datasetStatements.length > 0 && passOnObj.dataset2service) {
        const identifier = graph.findFirstValue(null, 'dcterms:identifier');
        const subject = datasetStatements[0].getSubject();
        const dataservices = passOnObj.dataset2service[subject] || passOnObj.dataset2service[identifier];
        if (dataservices) {
          graph.add(subject, 'rdf:type', 'esterms:ServedByDataService');
          dataservices.forEach((dataservice) => {
            const serviceSubject = getServiceSubject(dataservice, args);
            graph.add(subject, 'esterms:servedByDataService', serviceSubject);
          });

          const { extraGraphs } = passOnObj;
          // eslint-disable-next-line no-restricted-syntax
          for (const [, extraGraph] of Object.entries(extraGraphs)) {
            const distributionStatement = extraGraph.find(null, 'rdf:type', 'dcat:Distribution');
            if (distributionStatement.length > 0) {
              const distributionSubject = distributionStatement[0].getSubject();
              const service = extraGraph.findFirstValue(distributionSubject, 'dcat:accessService');
              const sourceRelation = extraGraph.findFirstValue(distributionSubject, 'dcterms:source');
              if (service || sourceRelation || apiInTitle(extraGraph, distributionSubject)) {
                extraGraph.add(distributionSubject, 'rdf:type', 'esterms:ServiceDistribution');
              }
            }
          }
        }
      }
    } else {
      const graph = passOnObj;
      graph.find(null, 'rdf:type', 'dcat:Dataset').forEach((datasetStatement) => {
        const datasetURI = datasetStatement.getSubject();
        if (apiInTitle(graph, datasetURI)) graph.add(datasetURI, 'rdf:type', 'esterms:ServedByDataService');
        graph.find(datasetURI, 'dcat:distribution').forEach((distributionStatement) => {
          const distributionSubject = distributionStatement.getValue();
          const service = graph.findFirstValue(distributionSubject, 'dcat:accessService');
          // EntryScape hack, we know that distributions that have dcterms:source are API:s.
          const sourceRelation = graph.findFirstValue(distributionSubject, 'dcterms:source');
          // Hack that we check for sourceRelation or apiInTitle for the distribution.
          if (service || sourceRelation || apiInTitle(graph, distributionSubject)) {
            graph.add(distributionSubject, 'rdf:type', 'esterms:ServiceDistribution');
            graph.add(datasetURI, 'rdf:type', 'esterms:ServedByDataService');
            if (service) {
              graph.add(datasetURI, 'esterms:servedByDataService', service);
            }
          }
        });
      });
      const catalogStatement = graph.find(null, 'rdf:type', 'dcat:Catalog');
      if (catalogStatement.length === 0) {
        return;
      }
      const catalogURI = catalogStatement[0].getSubject();
      graph.find(null, 'rdf:type', 'dcat:DataService').forEach((dataserviceStatement) => {
        const dataserviceURI = dataserviceStatement.getSubject();
        // Service is pointed to via dcat:service, then consider this an independent service
        if (graph.find(catalogURI, 'dcat:service', dataserviceURI).length !== 0) {
          graph.add(dataserviceURI, 'rdf:type', 'esterms:IndependentDataService');
        }
        graph.find(dataserviceURI, 'dcat:servesDataset').forEach((statement) => {
          graph.add(statement.getValue(), 'esterms:servedByDataService', statement.getSubject());
        });
      });
    }
  },
};
