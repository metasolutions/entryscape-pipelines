const config = require('../config');
const { getMediaType } = require('../transforms/convert/formatUtil');

const addMediatype = (graph, subject) => {
  const accessURL = graph.findFirstValue(subject, 'dcat:accessURL');
  const downloadURL = graph.findFirstValue(subject, 'dcat:downloadURL');
  const format = graph.findFirstValue(subject, 'dcterms:format');
  const mediatype = getMediaType({}, downloadURL || accessURL, format);
  graph.findAndRemove(subject, 'dcterms:format');
  if (mediatype) {
    graph.addL(subject, 'dcterms:format', mediatype);
  }
};

/**
 * The addFormat mapper identifies any distribution in the provided object,
 * then replaces its dcterms:format with the identified mediatype.
 */
module.exports = {

  init(params) {
    return Promise.resolve();
  },

  async exec(passOnObj, pipeline, args) {
    const contextId = pipeline.getEntry(true).getContext().getId();
    if (config.addFormat && config.addFormat[contextId]) {
      if (passOnObj.mainGraph && passOnObj.extraGraphs) {
        const { extraGraphs } = passOnObj;
        const graphs = Object.values(extraGraphs);
        graphs.forEach((graph) => {
          const distributionStatements = graph.find(null, 'rdf:type', 'dcat:Distribution');
          if (distributionStatements.length > 0) {
            const distributionSubject = distributionStatements[0].getSubject();
            addMediatype(graph, distributionSubject);
          }
        });
      } else {
        const graph = passOnObj;
        graph.find(null, 'rdf:type', 'dcat:Distribution').forEach((statement) => {
          addMediatype(graph, statement.getSubject());
        });
      }
    }
  },
};
