const config = require('../config');

let mappers;
const initMappers = async () => {
  if (!mappers) {
    mappers = [];
    const cmappers = config.mappers || [];
    if (cmappers.length > 0) {
      for (let i = 0; i < cmappers.length; i++) {
        const mapper = require(`./${cmappers[i]}`);
        await mapper.init();
        mappers.push(mapper.exec);
      }
    }
  }
};

const runMappers = (passOnObj, pipeline, transformId, args) => {
  const relevantMappers = mappers.filter((mapper) => (mapper.test ? mapper.test(pipeline, transformId) : true));
  return Promise.all(relevantMappers.map((mapper) => mapper(passOnObj, pipeline, args)));
};

/**
 * The mappers initializes the provided mappers via the
 * configuration, then filtering the list of mappers via
 * each mappers test function. Finally, each mapper is
 * executed.
 * @param {rdfjson/Graph | Object} passOnObj - Either batch of all data items in a graph,
 *                                             or an object consisting of a streamed data item.
 * @param {Object} pipeline - Metadata of the Pipeline object.
 * @param {String} transformId - The id of the transform.
 * @param {Object} args - Any arguments provided with the transform.
 */
module.exports = {
  init: initMappers,
  async runAll(passOnObj, pipeline, transformId, args) {
    await initMappers(pipeline);
    return runMappers(passOnObj, pipeline, transformId, args);
  },
};
