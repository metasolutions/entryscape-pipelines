const config = require('../config');
const fs = require('fs');

let id2keyword;
const mapping = [
  { theme: 'http://publications.europa.eu/resource/authority/data-theme/AGRI', form: ['34.30', '34.25'] },
  { theme: 'http://publications.europa.eu/resource/authority/data-theme/ENER', form: ['56'] },
  { theme: 'http://publications.europa.eu/resource/authority/data-theme/REGI', form: ['24', '26'] },
  { theme: 'http://publications.europa.eu/resource/authority/data-theme/TRAN', form: ['58', '59'] },
  { theme: 'http://publications.europa.eu/resource/authority/data-theme/ECON', form: ['06', '30'] },
  { theme: 'http://publications.europa.eu/resource/authority/data-theme/INTR', form: ['02', '03'] },
  { theme: 'http://publications.europa.eu/resource/authority/data-theme/GOVE', form: ['34', '52', '54', '60', '62', '63', '65', '67', '68'] },
  { theme: 'http://publications.europa.eu/resource/authority/data-theme/JUST', form: ['40', '42', '44', '46', '47'] },
  { theme: 'http://publications.europa.eu/resource/authority/data-theme/ENVI', form: ['37', '38'] },
  { theme: 'http://publications.europa.eu/resource/authority/data-theme/EDUC', form: ['10', '16', '17', '18'] },
  { theme: 'http://publications.europa.eu/resource/authority/data-theme/HEAL', form: ['20'] },
  { theme: 'http://publications.europa.eu/resource/authority/data-theme/SOCI', form: ['05', '08', '14'] },
  { theme: 'http://publications.europa.eu/resource/authority/data-theme/TECH', form: ['12'] },
];

const formBase = 'http://www.form-online.dk/opgavenoegle/';
const getFormId = function (statement) {
  return statement.getValue().substr(formBase.length);
};

const mapToTheme = function (datasetURI, formId, graph) {
  let matchTheme;
  if (mapping.some((obj) => {
    matchTheme = obj.theme;
    return obj.form.some(matchId => formId.indexOf(matchId) === 0);
  })) {
    graph.add(datasetURI, 'dcat:theme', matchTheme);
  }
};

/**
 * @param {rdfjson/Graph} graph
 */
module.exports = {

  init(params) {
    return new Promise((resolve, reject) => {
      fs.readFile(`./../${params.keywordIndex}`, (str) => {
        id2keyword = JSON.parse(str);
        resolve();
      });
    });
  },

  exec(graph) {
    return new Promise((resolve, reject) => {
      const stmt = graph.find(null, 'dcterms:subject');
      stmt.forEach((st) => {
        const id = getFormId(st);
        const ds = st.getSubject();
        // Map to keyword
        const kw = id2keyword[st.getValue()];
        if (kw) {
          graph.addL(ds, 'dcat:keyword', kw, 'da');
        }
        // Map to theme
        mapToTheme(ds, id, graph);
      });

      resolve();
    });
  },

};
