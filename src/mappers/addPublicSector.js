const { namespaces } = require('@entryscape/rdfjson');

namespaces.add('esterms', 'http://entryscape.com/terms/');

/**
 * The addPublicSector checks the PipelineResult graph wether the metadata
 * contains a psi tag. If so, add metadata to the datasets and dataservices
 * in order to describe it as belonging to public sector.
 */
module.exports = {

  init(params) {
    return Promise.resolve();
  },

  async exec(passOnObj, pipeline, args) {
    const pipelineResultMetadata = pipeline.getEntry(true).getMetadata();
    const tags = pipelineResultMetadata.find(null, 'dcterms:subject').map((statement) => statement.getValue());

    if (passOnObj.mainGraph) {
      const graph = passOnObj.mainGraph;
      const subject = passOnObj.mainURI;
      if (tags.includes('psi')) {
        if (graph.find(null, 'rdf:type', 'dcat:Dataset').length !== 0 || graph.find(null, 'rdf:type', 'dcat:DataService').length !== 0) {
          graph.addD(subject, 'esterms:publicSector', 'true', 'xsd:boolean');
        }
      }
    } else {
      const graph = passOnObj;
      if (tags.includes('psi')) {
        graph.find(null, 'rdf:type', 'dcat:Dataset').forEach((stmt) => {
          graph.addD(stmt.getSubject(), 'esterms:publicSector', 'true', 'xsd:boolean');
        });
        graph.find(null, 'rdf:type', 'dcat:DataService').forEach((stmt) => {
          graph.addD(stmt.getSubject(), 'esterms:publicSector', 'true', 'xsd:boolean');
        });
      }
    }
  },
};
