const { converters } = require('@entryscape/rdfjson');
const loadFromURL = require('../utils/loadFromURL');

const locateCatalogs = (graph) => {
  const catalogResources = graph.find(null, 'rdf:type', 'dcat:Catalog').map((s) => s.getSubject());
  let top;
  const parts = [];
  catalogResources.forEach((subj) => {
    graph.find(subj, 'dcterms:hasPart').forEach((stmt) => {
      top = stmt.getSubject();
      if (stmt.getValue() !== top) { parts.push(stmt.getValue()); }
    });
  });
  return { top, parts };
};

const loadParts = (graph, parts) => Promise.all(parts.map(async (part) => {
  const isDefinedBy = graph.findFirstValue(part, 'rdfs:isDefinedBy');
  if (isDefinedBy) {
    return loadFromURL(isDefinedBy).then(async (body) => {
      const detect = await converters.detect(body);
      if (!detect.error) {
        graph.addAll(detect.graph);
      }
    });
  }
  return Promise.resolve();
}));

const mergeParts = (graph, top, parts) => {
  parts.forEach((subcatalog) => {
    graph.find(subcatalog, 'dcat:dataset').forEach((stmt) => {
      graph.add(top, 'dcat:dataset', stmt.getValue());
      graph.remove(stmt);
    });
    graph.find(subcatalog, 'dcat:service').forEach((stmt) => {
      graph.add(top, 'dcat:service', stmt.getValue());
      graph.remove(stmt);
    });
    graph.findAndRemove(top, 'dcterms:hasPart', subcatalog);
    graph.findAndRemove(subcatalog, 'rdf:type', 'dcat:Catalog');
  });
};

/**
 * The subCatalog mapper identifies if catalogs have sub-catalogs.
 * If so, the sub-catalogs content are loaded and included into
 * the main catalog graph.
 */
module.exports = {

  init(params) {
    return Promise.resolve();
  },
  test(pipeline, transformId) {
    const transformType = pipeline.getTransformType(transformId);
    const args = pipeline.getTransformArguments(transformId);
    return transformType === 'fetch' && (args.type === undefined || args.type === 'rdf');
  },
  async exec(passOnObj, pipeline, args) {
    if (passOnObj.mainGraph) {
      // TODO
    } else {
      const graph = passOnObj;
      const { top, parts } = locateCatalogs(graph);
      await loadParts(graph, parts);
      mergeParts(graph, top, parts);
    }
  },
};
