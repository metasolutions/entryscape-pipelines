const { Graph } = require('@entryscape/rdfjson');
const config = require('../config');

const addPublisher = (graph, uri) => {
  graph.find(null, 'rdf:type', 'dcat:Dataset').forEach((statement) => {
    graph.add(statement.getSubject(), 'dcterms:publisher', uri);
  });
  graph.find(null, 'rdf:type', 'dcat:Catalog').forEach((statement) => {
    graph.add(statement.getSubject(), 'dcterms:publisher', uri);
  });
};

/**
 * The addPublisher mapper adds a provided publisher uri into
 * the metadata of datasets and catalog.
 */
module.exports = {

  init(params) {
    return Promise.resolve();
  },

  async exec(passOnObj, pipeline, args) {
    const contextId = pipeline.getEntry(true).getContext().getId();
    if (config.addPublisher && config.addPublisher[contextId]) {
      const { uri } = config.addPublisher[contextId];
      if (passOnObj.mainGraph) {
        const graph = passOnObj.mainGraph;
        const datasetStatements = graph.find(null, 'rdf:type', 'dcat:Dataset');
        if (datasetStatements.length > 0) {
          addPublisher(graph, uri);
        }
      } else {
        const graph = passOnObj;
        addPublisher(graph, uri);
      }
    }
  },
};
