const cmd = process.argv[2] || 'daemon/harvester';
dojoConfig = { locale: 'en' };// to run validate, which requires locale
const requirejs = require('requirejs');

requirejs.config({
  nodeRequire: require,
  baseUrl: 'bower_components',
  packages: [
    {
      name: 'dcat',
      location: '../src',
    },
    {
      name: 'config',
      location: '..',
      main: 'config.js',
    },
    {
      name: 'md5',
      location: 'md5/js',
      main: 'md5.min',
    },
    {
      name: 'text',
      location: 'requirejs-text',
      main: 'text',
    },
  ],
  map: {
    '*': {
      has: 'dojo/has', // Use dojos has module since it is more clever.
      'dojo/text': 'text', // Use require.js text module
      // Make sure i18n, dojo/i18n and di18n/i18n are all treated as a SINGLE module named i18n.
      // (We have mapped i18n to be the module provided in di18n/i18n, see paths above.)
      'dojo/i18n': 'i18n',
      'di18n/i18n': 'i18n',
    },
    'store/rest': {
      'dojo/request': 'dojo/request/node', // Force using xhr since we know we are in the browser
      'dojo/request/iframe': 'dojo/request/iframe', // Override above line for iframe path.
    },
    'rdforms/template/bundleLoader': {
      'dojo/request': 'dojo/request/xhr', // Force using xhr since we know we are in the browser
    },
  },
  deps: ['dcat/fix', `dcat/${cmd}`],
});
