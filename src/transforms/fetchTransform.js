const RDFFetch = require('./fetch/RDFFetch');
const JSONFetch = require('./fetch/JSONFetch');
const Fetch = require('./fetch/fetch');
const { getCustomTransform } = require('../utils/transform');

const getFetchTransform = (args) => {
  const type = args.type ? args.type.toLowerCase() : '';
  if (type && !type.includes('json')) { 
    const transform = getCustomTransform('fetch', type);
    if (transform) return transform;
  }
  switch (type) {
    case 'json':
      return JSONFetch;
    case 'application/ld+json':
      return Fetch;
    default:
      return RDFFetch;
  }
};

const fetch = (jobEntry, pipeline, transformId, passOnObj, streamState = null) => {
  const args = pipeline.getTransformArguments(transformId) || {};
  return getFetchTransform(args)(jobEntry, pipeline, transformId, passOnObj, streamState);
};

module.exports = fetch;
