const noop = (jobEntry, pipeline, transformId, passOnObj) => Promise.resolve(passOnObj);

module.exports = noop;
