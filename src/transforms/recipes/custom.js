/**
 * Create a pipeline of transforms as defined in a json file
 *
 * @param {store/PrototypeEntry} context
 * @param {string} row a csw line in the form name;email;orgid
 */
const fs = require('fs');

module.exports = (protoPipeline, pipelineFile) => {
  const pipelineRes = protoPipeline.getResource();
  const data = fs.readFileSync(`../${pipelineFile}`, { encoding: 'utf8' });
  const pipelineStruct = JSON.parse(data);
  pipelineStruct.forEach(transform =>
    pipelineRes.addTransform(transform.type, transform.args || {}));
  return protoPipeline.commit();
};
