/**
 * Create a pipeline for importing datasets from a RDF file containing DCAT-AP.
 *
 * @param {store/PrototypeEntry} protoPipeline
 * @param {string} row csv line assumed to look like: name;email;orgid;web;
 */
const config = require('../../config');
const utils = require('./utils');

module.exports = (protoPipeline, row) => {
  const arr = row.split(';');
  const web = arr[3] === '' || arr[3] == null ? 'http://example.com' : arr[3];
  const psidataPath = config.psidataPath || 'psidata';
  const harvestPath = config.dcat_harvestPath || 'datasets/dcat';
  const prefix = web.indexOf('http') === 0 ? '' : config.httpprefix || 'http://';
  const psiUrl = `${prefix}${web}/${psidataPath}`;
  const dcatUrl = `${prefix}${web}/${harvestPath}`;

  protoPipeline.add('foaf:page', `${prefix}${web}`);

  const pipelineRes = protoPipeline.getResource();
  pipelineRes.addTransform('empty', {});
  pipelineRes.addTransform('check', { source: psiUrl });
  pipelineRes.addTransform('fetch', { source: dcatUrl });
  pipelineRes.addTransform('validate', { profile: 'dcat_ap_se' });
  pipelineRes.addTransform('merge', {});

  return utils.createCatalog(protoPipeline.getContext(), arr[0]).then(() => protoPipeline.commit());
};
