/**
 * Create a pipeline for inporting datasets from a Inspire registry using the CSW protocol.
 *
 * @param store/PrototypeEntry protoPipeline
 * @param {string} row assumed to be a csv row in the form name;email;orgid;CSWURL;resourcebaseURL
 */
const utils = require('./utils');

module.exports = (protoPipeline, row) => {
  const arr = row.split(';');
  const pipelineRes = protoPipeline.getResource();
  pipelineRes.addTransform('empty', {});
  pipelineRes.addTransform('fetchCSW', { source: arr[3] });
  pipelineRes.addTransform('gml2rdf', { base: arr[4] });
  pipelineRes.addTransform('merge', { name: arr[0] });

  return utils.createCatalog(protoPipeline.getContext(), arr[0]).then(() =>
    protoPipeline.commit());
};
