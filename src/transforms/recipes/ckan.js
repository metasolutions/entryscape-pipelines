/**
 * Create a pipeline to check psidata and fetch datasets from CKAN
 *
 * @param {store/PrototypeEntry} context
 * @param {string} row a csw line in the form name;email;orgid;web;ckanurl
 */
const config = require('../../config');
const utils = require('./utils');

module.exports = (protoPipeline, row) => {
  const arr = row.split(';');
  const name = arr[0];
  const web = arr[3] === '' || arr[3] == null ? 'http://example.com' : arr[3];
  const psidataPath = config.psidataPath || 'psidaten';
  const psiUrl = `http://${web}/${psidataPath}`;
  const ckanUrl = arr[4];

  protoPipeline.add('foaf:page', `http://${web}`);

  const pipelineRes = protoPipeline.getResource();
  pipelineRes.addTransform('empty', {});
  pipelineRes.addTransform('check', { source: psiUrl });
  pipelineRes.addTransform('fetchCKAN', { source: ckanUrl });
  pipelineRes.addTransform('ckan2rdf', { base: ckanUrl });
  pipelineRes.addTransform('validate', { profile: 'dcat_ap_se' });
  pipelineRes.addTransform('merge', { name });
  return utils.createCatalog(protoPipeline.getContext(), name).then(() => protoPipeline.commit());
};
