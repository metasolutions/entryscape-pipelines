module.exports = {
  /**
   * Create a new catalog
   * @param {store/Context} context
   * @return {Promise}
   */
  createCatalog(context, name) {
    return context.newEntry()
      .add('rdf:type', 'dcat:Catalog')
      .addL('dcterms:title', name)
      .commit();
  },
};
