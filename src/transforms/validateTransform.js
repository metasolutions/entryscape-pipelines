const { validate } = require('@entryscape/rdforms');
const { namespaces } = require('@entryscape/entrystore-js');
const globals = require('../utils/globals');
const reportPrinter = require('../utils/reportPrinter');
const config = require('../config');
const logger = require('../utils/logger/logger');

config.validate = config.validate || {};
// Be backward compatible, remove after updated all pipeline installations.
config.validate.mandatoryTypes = config.validate.mandatoryTypes || config.mandatoryTypes;
config.validate.type2template = config.validate.type2template || config.type2template;

namespaces.add('prov', 'http://www.w3.org/ns/prov#');
let type2template;
let mandatoryTypes;
const getType2template = () => {
  if (type2template) {
    return Promise.resolve();
  }
  return globals.getItemStore().then((itemstore) => {
    const t2t = config.validate.type2template;
    type2template = {};
    Object.keys(t2t).forEach((cls) => {
      type2template[namespaces.expand(cls)] = itemstore.getItem(t2t[cls]);
    });
    mandatoryTypes = config.validate.mandatoryTypes.map(mt => namespaces.expand(mt));
  });
};

let report;
const init = () => getType2template().then(() => {
  report = { errors: 0, warnings: 0, deprecated: 0, resources: [], blocked: [] };
});

const process = (graph, uri, jobEntry) => {
  const subreport = validate.graphReport(graph, type2template);
  report.errors += subreport.errors;
  report.warnings += subreport.warnings;
  report.deprecated += subreport.deprecated;
  report.resources.push(...subreport.resources);
  if (config.validate.mandatoryFieldsRequired && subreport.errors > 0) {
    jobEntry.add('storepr:validateBlocked', uri);
    report.blocked.push(uri);
    return false;
  }
  return true;
};

const checkMandatoryTypes = () => {
  const type2resourceCount = {};
  mandatoryTypes = mandatoryTypes || [];
  Object.keys(type2template).forEach((t) => {
    type2resourceCount[t] = 0;
  });

  report.resources.forEach((rr) => {
    type2resourceCount[rr.type] += 1;
  });

  const mandatoryError = [];
  mandatoryTypes.forEach((mt) => {
    if (type2resourceCount[mt] === 0) {
      mandatoryError.push(mt);
    }
  });
  if (mandatoryError.length > 0) {
    report.mandatoryError = mandatoryError;
    report.errors += mandatoryError.length;
  }
};

const removeIncompleteMandatory = (graph, report) => {
  if (config.validate.mandatoryFieldsRequired && report.errors > 0) {
    // Remove types of resources not passing mandatory check so they won't be discovered and merged in the next step.
    report.resources.forEach((res) => {
      if (res.errors.length > 0 && res.errors.filter(err => err.code === 'min').length > 0) {
        graph.findAndRemove(res.uri, 'rdf:type');
      }
    });
  }
};

const finish = (jobEntry, pipeline) => {
  if (report.mandatoryError) {
    jobEntry.addD('storepr:validateMandatoryMissing', `${report.mandatoryError.length}`,
      'xsd:integer');
  }
  if (report.errors > 0) {
    jobEntry.addD('storepr:validateErrors', `${report.errors}`, 'xsd:integer');
  }
  if (report.warnings > 0) {
    jobEntry.addD('storepr:validateWarnings', `${report.warnings}`, 'xsd:integer');
  }
  if (report.deprecated > 0) {
    jobEntry.addD('storepr:validateDeprecated', `${report.deprecated}`, 'xsd:integer');
  }
  if (mandatoryTypes && mandatoryTypes.length > 0) {
    report.mainResources = report.resources.filter(res => res.type === mandatoryTypes[0]);
    if (report.mainResources.length - report.blocked.length > 0) {
      jobEntry.addD('storepr:validateMainResourceCount', `${report.mainResources.length - report.blocked.length}`, 'xsd:integer');
    }
  } else {
    report.mainResources = [];
  }
  const md = jobEntry.getMetadata();

  logger.info(reportPrinter.summaryFromReport(report));
  if (config.validate.saveReport) {
    const name = pipeline.getGraph().findFirstValue(null, 'dcterms:title');
    const initialText = `Validation report for ${name}\n======================================\n\n`;
    const reportStr = reportPrinter.full(report);
    return jobEntry.getContext().newEntry().commit()
      .then((fileEntry) => {
        jobEntry.add('prov:generated', fileEntry.getURI());
        return fileEntry.getResource(true).putText(initialText + reportStr);
      })
  }
  report = {}; // Let GC clean up.
  return Promise.resolve(true);
};

const transform = function (jobEntry, pipeline, transformId, passOnObj, streamState) {
  switch (streamState) {
    case 'init':
      return init().then(() => passOnObj);
    case 'process':
      if (typeof passOnObj === 'object' && passOnObj != null) {
        if (passOnObj.mainGraph) {
          if (!process(passOnObj.mainGraph, passOnObj.mainURI, jobEntry)) {
            // Main resource did not pass
            // Delete the resources in the extraGraph from the extraIndex so they can be added at a later time
            if (passOnObj.extraGraphs) {
              Object.keys(passOnObj.extraGraphs).forEach((ruri) => {
                delete passOnObj.extraIndex[ruri];
              });
            }
            throw new Error("Main resource lacks mandatory fields, aborting.")
          } else {
            if (passOnObj.extraGraphs) {
              Object.keys(passOnObj.extraGraphs).forEach(uri => process(passOnObj.extraGraphs[uri], uri, jobEntry));
            }
          }
        }
      }
      return passOnObj;
    case 'finish':
      checkMandatoryTypes();
      return finish(jobEntry, pipeline).then(() => passOnObj);
    default:
      if (passOnObj) {
        return init().then(() => {
          // Assuming passOnObj is a single graph.

          report = { ...report, ...validate.graphReport(passOnObj, type2template, mandatoryTypes) };
          removeIncompleteMandatory(passOnObj, report);
          return finish(jobEntry, pipeline).then(() => passOnObj);
        }).catch((err) => {
          logger.error(`Error ind validation transform ${err}`);
          return Promise.reject('No truthy value for passOnObj in validate transform');
        });
      }
      return Promise.reject('No truthy value for passOnObj in validate transform');
  }
};

transform.stream = 'intermediate';

module.exports = transform;

