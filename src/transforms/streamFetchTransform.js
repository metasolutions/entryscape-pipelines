const JSONIndexFetch = require('./fetch/JSONIndexFetch');
const { getCustomTransform } = require('../utils/transform');

const getFetchTransform = (args) => {
  if (args.type && args.type.toLowerCase() !== 'jsonindex') {
    const transform = getCustomTransform('fetch', args.type);
    if (transform) return transform;
  }
  switch (args.type ? args.type.toLowerCase() : '') {
    case 'jsonindex':
    default:
      return JSONIndexFetch;
  }
};

const fetch = (jobEntry, pipeline, transformId, passOnObj, streamState = null) => {
  const args = pipeline.getTransformArguments(transformId) || {};
  return getFetchTransform(args)(jobEntry, pipeline, transformId, passOnObj, streamState);
};

module.exports = fetch;
module.exports.stream = 'start';
