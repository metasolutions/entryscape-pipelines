const loadFromURL = require('../utils/loadFromURL');
const logger = require('../utils/logger/logger');

module.exports = (jobEntry, pipeline, transformId) => {
  const args = pipeline.getTransformArguments(transformId);
  const checkurl = args && args.source ? args.source : '';

  return loadFromURL(checkurl, true).then(() => {
    jobEntry.addD('storepr:check', 'true', 'xsd:boolean');
    logger.info(`Success - checked source: ${checkurl}`);
  }, (err) => {
    logger.error(`Failure - checked source: ${checkurl}`);
    throw err;
  });
};
