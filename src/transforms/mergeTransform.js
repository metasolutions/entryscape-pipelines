const dcat = require('./merge/dcat');
const multitype = require('./merge/multitype');

let merge = dcat;
const detectMergeModule = (args) => {
  if (args.profile || args.masterType || args.primaryTypes) {
    merge = multitype;
  } else {
    merge = dcat;
  }
};

const transform = (jobEntry, pipeline, transformId, passOnObj, streamState = null) => {
  const args = pipeline.getTransformArguments(transformId) || {};
  const context = jobEntry.getContext();
  switch (streamState) {
    case 'init':
      detectMergeModule(args);
      return merge.streamInit(context, args);
    case 'process':
      return merge.streamProcess(passOnObj);
    case 'finish':
      return merge.streamFinish(context, args ? args.name : '', jobEntry).then(() => passOnObj);
    default:
      if (passOnObj) {
        detectMergeModule(args);
        return merge.process(context, passOnObj, jobEntry, args, pipeline);
      }

      return Promise.reject('No truthy value for passOnObj in merge transform');
  }
};

transform.stream = 'stop';
module.exports = transform;
