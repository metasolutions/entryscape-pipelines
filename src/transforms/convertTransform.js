const { Graph, converters } = require('@entryscape/rdfjson');
const logger = require('../utils/logger/logger');
const { getCustomTransform } = require('../utils/transform');

const config = require('../config');
const OGD2RDF = require('./convert/OGD2RDF');
const CKAN2RDF = require('./convert/CKAN2RDF');
const GML2RDF = require('./convert/GML2RDF');
const PX2RDF = require('./convert/PX2RDF');
const Nobel2RDF = require('./convert/Nobel2RDF');
const Fsk2RDF = require('./convert/Fsk2RDF');
const mappers = require('../mappers/mappers');


let checkDuplicates = {};
let args;
let firstRDFFound = false;
let converter;
let totalGraph;

const detectConverter = (pipeline, transformId) => {
  totalGraph = new Graph();
  checkDuplicates = {};
  args = pipeline.getTransformArguments(transformId);
  firstRDFFound = false;
  const name = pipeline.getTransformType(transformId);
  switch (args.type || name) {
    case 'ogd2rdf':
    case 'ogd':
      converter = OGD2RDF;
      break;
    case 'ckan2rdf':
    case 'ckan':
      converter = CKAN2RDF;
      break;
    case 'gml2rdf':
    case 'gml':
      GML2RDF.clear();
      converter = GML2RDF;
      break;
    case 'nmd':
      converter = NMD2RDF;
      break;
    case 'nobel':
      converter = Nobel2RDF;
      break;
    case 'fsk':
      converter = Fsk2RDF;
      break;
    case 'px':
        converter = PX2RDF;
        break;
    default:
      if (args.type) converter = getCustomTransform('convert', args.type);
      else converter = null;
  }
};

const addToTotal = (passOn) => {
  if (passOn.mainGraph) {
    totalGraph.addAll(passOn.mainGraph);
  }
  if (passOn.extraGraphs) {
    Object.values(passOn.extraGraphs).forEach(g => totalGraph.addAll(g));
  }
};

const storeRDF = (jobEntry) => {
  return jobEntry.getContext().newEntry().commit()
    .then((fileEntry) => {
      jobEntry.add('dcterms:source', fileEntry.getURI());
      try {
        const rdfGraph = converters.rdfjson2rdfxml(totalGraph);
        return fileEntry.getResource(true).put(rdfGraph, 'application/rdf+xml');
      } catch (err) {
        logger.error('Could not store RDF');
      } finally {
        return;
      }
    });
};

const transform = (jobEntry, pipeline, transformId, passOnObj, streamState) => {
  switch (streamState) {
    case 'init':
      detectConverter(pipeline, transformId);
      if (converter.init) {
        converter.init(args, passOnObj);
      }
      return passOnObj;
    case 'process':
      try {
        const val = converter.map(args, passOnObj, checkDuplicates);
        if (val !== null) {
          if (!firstRDFFound && val.mainGraph) {
            jobEntry.addD('storepr:fetchRDF', 'true', 'xsd:boolean');
            firstRDFFound = true;
          }
          val.extraIndex = checkDuplicates;
          addToTotal(val);
          return mappers.runAll(val, pipeline, transformId, args).then(() => val);
        }
        return val;
      } catch (err) {
        logger.error(`${err.message}`);
        return undefined;
      }
    case 'finish':
      if (converter && converter.clear) {
        // Potentially throw away saved state to minimize memory consumtion.
        converter.clear();
      }
      return storeRDF(jobEntry).then(() => passOnObj);
    default:
      if (passOnObj) {
        detectConverter(pipeline, transformId);
        const val = converter.map(args, passOnObj, jobEntry);
        return mappers.runAll(val, pipeline, transformId).then(() => val);
      }

      return Promise.reject('No truthy value for passOnObj in convert transform');
  }
};

transform.stream = 'intermediate';
module.exports = transform;
