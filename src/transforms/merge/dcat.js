const globals = require('../../utils/globals');
const { EntrySync, DuplicateIndex } = require('@entryscape/entrysync');
const DcatIndex = require('../../utils/DcatIndex');
const dcatFix = require('../../utils/dcatFix');
const config = require('../../config');
const logger = require('../../utils/logger/logger');
const { report, getDedup, hasType } = require('./utils');

const es = globals.getEntryStore();

let datasetURIs;
let dataserviceURIs;
let datasetSeriesURIs;
let esync;
let dsync;
let dssync;
let ssync;
let publisherSync;
let destinationContextId;
let dedup;

/*
 *
 *
 *
 */
const streamInit = async (context) => {
  datasetURIs = [];
  dataserviceURIs = [];
  datasetSeriesURIs = [];
  publisherURIs = [];

  dedup = await getDedup(es);

  esync = new EntrySync({context: context});
  dsync = new EntrySync({context: context, dedup});
  dssync = new EntrySync({context: context, dedup});
  ssync = new EntrySync({context: context});
  publisherSync = new EntrySync({context: context});
  destinationContextId = context.getId();
  const types = [
    'dcat:Distribution',
    'vcard:Individual',
    'vcard:Organization',
    'vcard:Kind'];
  const publisherTypes = [
    'foaf:Agent',
    'foaf:Person',
    'foaf:Organization',
  ];
  return dsync.calculateIndexFromList(es.newSolrQuery().rdfType('dcat:Dataset')
    .context(context).list())
    .then(() => esync.calculateIndexFromList(es.newSolrQuery().rdfType(types)
      .context(context).list()))
    .then(() => ssync.calculateIndexFromList(es.newSolrQuery().rdfType('dcat:DataService')
      .context(context).list()))
    .then(() => dssync.calculateIndexFromList(es.newSolrQuery().rdfType('dcat:DatasetSeries')
      .context(context).list()))
    .then(() => publisherSync.calculateIndexFromList(es.newSolrQuery().rdfType(publisherTypes)
      .context(context).list()));
};

/*
 *
 *
 *
 */
const streamProcess = (passOnObj) => {
  const promises = [];
  let forceUpdate = false;
  const publisherTypes = ['http://xmlns.com/foaf/0.1/Agent', 'http://xmlns.com/foaf/0.1/Person', 'http://xmlns.com/foaf/0.1/Organization'];

  let entrysync;
  if (hasType(passOnObj.mainGraph, passOnObj.mainURI, ['http://www.w3.org/ns/dcat#DataService'])) entrysync = ssync;
  if (hasType(passOnObj.mainGraph, passOnObj.mainURI, ['http://www.w3.org/ns/dcat#Dataset'])) entrysync = dsync;
  if (hasType(passOnObj.mainGraph, passOnObj.mainURI, ['http://www.w3.org/ns/dcat#DatasetSeries'])) entrysync = dssync;

  if (entrysync && dedup.isDuplica(passOnObj.mainURI, passOnObj.mainGraph, destinationContextId)) return;

  Object.keys(passOnObj.extraGraphs || {}).forEach((uri) => {
    const extraGraph = passOnObj.extraGraphs[uri];
    if (extraGraph && hasType(extraGraph, uri, publisherTypes)) {
      if (!(dedup.isDuplica(uri, extraGraph, destinationContextId))) {
        publisherURIs.push(uri);
        promises.push(publisherSync.harvestMetadata(uri, extraGraph));
      }
    } else {
      promises.push(esync.harvestMetadata(uri, extraGraph));
    }
    if (!esync.isUnchanged(uri) || !publisherSync.isUnchanged(uri)) {
      forceUpdate = true;
    }
  });

  if (hasType(passOnObj.mainGraph, passOnObj.mainURI, ['http://www.w3.org/ns/dcat#DataService'])) {
    dataserviceURIs.push(passOnObj.mainURI);
    promises.push(ssync.harvestMetadata(passOnObj.mainURI, passOnObj.mainGraph, undefined, forceUpdate));
  } else if (hasType(passOnObj.mainGraph, passOnObj.mainURI, ['http://www.w3.org/ns/dcat#Dataset'])) {
    if (config.copyLicenseToDataset) {
      dcatFix.licenseOnDataset(passOnObj.mainURI, passOnObj.mainGraph, passOnObj.extraGraphs);
    }
    if (config.copyFormatToDataset) {
      dcatFix.formatOnDataset(passOnObj.mainURI, passOnObj.mainGraph, passOnObj.extraGraphs);
    }
    datasetURIs.push(passOnObj.mainURI);
    promises.push(dsync.harvestMetadata(passOnObj.mainURI, passOnObj.mainGraph, undefined, forceUpdate));
  } else if (hasType(passOnObj.mainGraph, passOnObj.mainURI, ['http://www.w3.org/ns/dcat#DatasetSeries'])) {
    datasetSeriesURIs.push(passOnObj.mainURI);
    promises.push(dssync.harvestMetadata(passOnObj.mainURI, passOnObj.mainGraph, undefined, forceUpdate));
  }

  return Promise.all(promises).then(null, (err) => {
    logger.error(`Error in merge process ${err.message}`);
  });
};

/*
 *
 *
 *
 */
const streamFinish = async (context, catalogname, jobEntry) => {
  const catalogEntry = await globals.getEntryStoreUtil()
    .getEntryByType('dcat:Catalog', context)
    .then(null, () => context.newEntry().add('rdf:type', 'dcat:Catalog')
      .addL('dcterms:title', catalogname || 'New catalog').commit());
  await esync.removeMissing();
  await dsync.removeMissing();
  await ssync.removeMissing();
  await dssync.removeMissing();
  await publisherSync.removeMissing();
  catalogEntry.getMetadata().findAndRemove(catalogEntry.getResourceURI(), 'dcat:dataset');
  catalogEntry.getMetadata().findAndRemove(catalogEntry.getResourceURI(), 'dcat:service');
  catalogEntry.getMetadata().findAndRemove(catalogEntry.getResourceURI(), 'dcterms:publisher');
  datasetURIs.forEach((dURI) => catalogEntry.add('dcat:dataset', dURI));
  datasetSeriesURIs.forEach((dURI) => catalogEntry.add('dcat:dataset', dURI));
  dataserviceURIs.forEach((dsURI) => catalogEntry.add('dcat:service', dsURI));
  publisherURIs.forEach((publisherURI) => catalogEntry.add('dcterms:publisher', publisherURI));
  await catalogEntry.commitMetadata();
  const dataSetReport = { ...dsync.getReport(), type: 'dcat:Dataset', title: 'Dataset' };
  const datasetSeriesReport = { ...dssync.getReport(), type: 'dcat:DatasetSeries', title: 'DatasetSeries' };
  const dataServiceReport = { ...ssync.getReport(), type: 'dcat:Dataset', title: 'DataService' };
  const publisherReport = { ...publisherSync.getReport(), type: 'foaf:Agent', title: 'Publisher' };
  const reports = [dataSetReport, dataServiceReport, publisherReport, datasetSeriesReport];
  // Let GC clean up as early as possible.
  esync = undefined;
  dsync = undefined;
  dssync = undefined;
  ssync = undefined;
  publisherSync = undefined;
  dedup.save();
  report(jobEntry)(reports);
};

/*
 * Only used for rdf graph, treats rdf data as graph
 *
 *
 */
const process = async (context, passOnObj, jobEntry, args, pipeline) => {
  dedup = await getDedup(es);
  const didx = new DcatIndex(passOnObj, `${context.getResourceURI()}/resource/`, context, dedup);
  return didx.sync().then((reports) => {
    report(jobEntry)(reports);
    return reports;
  }, (err) => {
    logger.error(err.message);
    throw err;
  }).then(() => passOnObj);
};

module.exports = {
  streamInit,
  streamProcess,
  streamFinish,
  process,
};
