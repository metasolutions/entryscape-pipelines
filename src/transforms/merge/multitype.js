const { utils: rdfjsonUtils, Graph } = require('@entryscape/rdfjson');
const { promiseUtil } = require('@entryscape/entrystore-js');
const globals = require('../../utils/globals');
const utils = require('./utils');
const logger = require('../../utils/logger/logger');
const { profileTypes } = require('../../utils/transform');
const { EntrySync } = require('@entryscape/entrysync');

const es = globals.getEntryStore();
let masterURIs;
let ssync;
let msyncs;
let stypes;
let mtypes;

const hasEntrySync = (graph, uri, types, entrysyncs) => {
  // eslint-disable-next-line no-restricted-syntax
  for (const [i, type] of types.entries()) {
    const res = graph.find(uri, 'http://www.w3.org/1999/02/22-rdf-syntax-ns#type', type);
    if (res.length > 0) return entrysyncs[i];
  }
  logger.debug('Could not locate correct entry sync for the graph');
};

const streamInit = async (context, args) => {
  masterURIs = [];
  msyncs = [];
  [mtypes, stypes] = profileTypes(args);
  for await (const mtype of mtypes) {
    const msync = new EntrySync({context: context});
    msyncs.push(msync);
    await msync.calculateIndexFromList(es.newSolrQuery().rdfType(mtype).context(context).list());
  };
  if (stypes.length > 0) {
    ssync = new EntrySync({context: context});
    await ssync.calculateIndexFromList(es.newSolrQuery().rdfType(stypes)
      .context(context).list());
  } else {
    ssync = null;
  }
  return true;
};

const streamProcess = (passOnObj) => {
  const promises = [];
  let forceUpdate = false;
  if (ssync) {
    Object.keys(passOnObj.extraGraphs || {}).forEach((uri) => {
      promises.push(ssync.harvestMetadata(uri, passOnObj.extraGraphs[uri]));
      if (!ssync.isUnchanged(uri)) {
        forceUpdate = true;
      }
    });
  }
  masterURIs.push(passOnObj.mainURI);
  const msync = hasEntrySync(passOnObj.mainGraph, passOnObj.mainURI, mtypes, msyncs);
  promises.push(msync.harvestMetadata(passOnObj.mainURI, passOnObj.mainGraph,
    undefined, forceUpdate));
  return Promise.all(promises).then(null, (err) => {
    logger.error(`Error in merge process ${err.message}`);
  });
};

const streamFinish = async (context, catalogname, jobEntry) => {
  if (ssync) {
    await ssync.removeMissing();
  }
  logger.debug('Finishing merge transform: mulitype');
  const reports = [];
  for await (const [i, msync] of msyncs.entries()) {
    await msync.removeMissing();
    const report = { ...msync.getReport(), type: mtypes[i], title: mtypes[i]};
    reports.push(report);
  };
  return utils.report(jobEntry)(reports);
};

const harvestType = async (entrySync, type, bigGraph) => {
  const uris = bigGraph.find(null, 'rdf:type', type).map(stmt => stmt.getSubject());
  await promiseUtil.forEach(uris, async (uri) => {
    const graph = rdfjsonUtils.extract(bigGraph, new Graph(), uri);
    try {
      await entrySync.harvestMetadata(uri, graph);
    } catch (error) {
      logger.error(`Failed to harvest object with uri ${uri} due to: ${error.message}`);
    } 
  });
};

module.exports = {
  streamInit,
  streamProcess,
  streamFinish,
  async process(context, passOnObj, jobEntry, args) {
    await streamInit(context, args);
    [ mtypes, stypes ] = profileTypes(args);
    await promiseUtil.forEach(mtypes, async (type) => {
      const typeIndex = mtypes.indexOf(type);
      return harvestType(msyncs[typeIndex], type, passOnObj);
    });
    if (stypes) {
      await promiseUtil.forEach(stypes, async (type) => {
        return harvestType(ssync, type, passOnObj);
      });
    }
    return streamFinish(context, null, jobEntry);
  },
};
