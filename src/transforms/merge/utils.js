const { DuplicateIndex } = require('@entryscape/entrysync');
const { types } = require('@entryscape/entrystore-js');
const logger = require('../../utils/logger/logger');
const config = require('../../config');

const addMetadata = (subject, metadata, report) => {
  metadata.addD(subject, 'storepr:mergeAdded', `${report.added}`, 'xsd:integer');
  metadata.addD(subject, 'storepr:mergeUpdated', `${report.updated}`, 'xsd:integer');
  metadata.addD(subject, 'storepr:mergeUnchanged', `${report.unchanged}`, 'xsd:integer');
  metadata.addD(subject, 'storepr:mergeRemoved', `${report.removed}`, 'xsd:integer');
};

const log = (title, report) => {
  logger.info();
  logger.info(title);
  logger.info(`Success - ${report.count} resources found`);
  logger.info(`${report.added} resources added`);
  logger.info(`${report.updated} resources updated`);
  logger.info(`${report.unchanged} resources unchanged`);
  logger.info(`${report.removed} resources removed`);
};

/*
*  Reports for one main resources type
*
*/
const reportType = (jobEntry) => {
  return (report) => {
    const title = `Primary type resources of type ${report.title}:`;
    log(title, report);
    const resourceURI = jobEntry.getResourceURI();
    const metadata = jobEntry.getMetadata();
    const blankId = metadata.add(resourceURI, 'storepr:mergeEntityType').getValue();
    metadata.add(blankId, 'storepr:entityType', report.type);
    addMetadata(blankId, metadata, report);
  };
};

/*
* Combines reports
*
*/
const mergeReports = (reports) => {
  const totalReport = {
    updated: 0,
    unchanged: 0,
    added: 0,
    removed: 0,
  };
  reports.map((r) => {
    totalReport.updated += r.updated;
    totalReport.unchanged += r.unchanged;
    totalReport.added += r.added;
    totalReport.removed += r.removed;
  });
  totalReport.count = totalReport.updated + totalReport.added + totalReport.unchanged;
  return totalReport;
};

/*
* Reports for all main resources. Combination of all reports
*
*/
const reportTotal = (jobEntry) => {
  return (reports) => {
    const report = mergeReports(reports);
    const title = 'Primary type resources:';
    log(title, report);
    const resourceURI = jobEntry.getResourceURI();
    const metadata = jobEntry.getMetadata();
    metadata.addD(resourceURI, 'storepr:merge', 'true', 'xsd:boolean');
    metadata.addD(resourceURI, 'storepr:mergeResourceCount', `${report.count}`, 'xsd:integer');
    metadata.addD(resourceURI, 'storepr:mergeMainResourceCount', `${report.count}`, 'xsd:integer');
    addMetadata(resourceURI, metadata, report);
  };
};

const timeToUpdate = (now, index, updateFrequency) => {
  const timeNow = now.getTime();
  const timeThen = index.modified.getTime();
  const seconds = (timeNow - timeThen) / 1000;
  if (seconds > (60 * updateFrequency)) return true;
};

let psiIndex;
const updatePSIIndex = async (es) => {
  const now = new Date();
  const updateInterval = config.dedup.psiUpdateFrequency ? config.dedup.psiUpdateFrequency : 15;
  if (!psiIndex || timeToUpdate(now, psiIndex, updateInterval) )  {
    psiIndex = {};
    const query = es.newSolrQuery().graphType(types.GT_PIPELINE); // ev precisera query så att bara psi hämtas
    await query.forEach((pipeline) => {
      const psi = pipeline.getMetadata().find(null, 'dcterms:subject', { value: 'psi', type: 'literal' });
      const contextId = pipeline.getContext().getId();
      if (psi.length > 0) psiIndex[contextId] = true;
    })
    logger.info('PSI index updated');
    psiIndex.modified = now;
  }
};

let configPriority = (context) => 1;
if (config?.dedup?.priority || config?.dedup?.primary) {
  configPriority = config.dedup.priority ? config.dedup.priority : config.dedup.primary;
  if (typeof configPriority === 'function') {
    // Nothing to do
  } else if (typeof configPriority === 'string') {
    const origVal = configPriority;
    configPriority = (context) => context === origVal ? 10 : 1;
  } else if (Array.isArray(configPriority)) {
    const origVal = configPriority;
    configPriority = (context) => origVal.includes(context) ? 10 : 1;
  }
}

let existingContextsIndex;
const updateDedupIndex = async (es, dedup) => {
  const now = new Date();
  const updateInterval = config?.dedup?.dedupUpdateFrequency ? config.dedup.dedupUpdateFrequency : 15;
  if (!existingContextsIndex || timeToUpdate(now, existingContextsIndex, updateInterval) )  {
    existingContextsIndex = {};
    existingContextsIndex.contexts = [];
    const ctxtsSet = dedup.contextsInIndex();
    const ctxts = Array.from(ctxtsSet);
    const esContextsQuery = es.getContextList();
    await esContextsQuery.forEach((c) => {
      const cID = c.getId();
      existingContextsIndex.contexts.push(cID);
    });
    const removeCtxts = ctxts.filter(x => !existingContextsIndex.contexts.includes(x));
    dedup.removeContextsFromIndex(removeCtxts);
    logger.info('Dedup index updated');
    existingContextsIndex.modified = now;
  }
};

/**
 * If config.dedup.priority is a context or array of contexts (string or array of strings):
 *   1 if neither psi or priority
 *   2 if psi
 *   10 if priority
 *   11 if priority AND psi
 *
 * If config.dedup.priority is a function, psi contexts just adds 1.
 *
 * @param context
 * @return {*}
 */
const psiPriority = (context) =>
  configPriority(context) + ((context in psiIndex) ? 1 : 0);

let dedup;
const getDedup = async (es) => {

  if (config?.dedup?.psi) {
    await updatePSIIndex(es);
  }
  if (dedup) {
    if (!dedup.disabled) await updateDedupIndex(es, dedup);
    return dedup;
  }
  if (!config.dedup || config.dedup.disabled || !config.dedup.key)  {
    logger.warn('Dedup is disabled');
    dedup = new DuplicateIndex({disabled: true});
  } else {
    dedup = new DuplicateIndex({
      priority: config.dedup.psi ? psiPriority : configPriority,
      key: config.dedup.key
    }, (msg) => logger.info(msg));
  }
  if (!dedup.disabled) await updateDedupIndex(es, dedup);
  return dedup;
};

const hasType = (graph, uri, types) => {
  // eslint-disable-next-line no-restricted-syntax
  for (const type of types) {
    const res = graph.find(uri, 'http://www.w3.org/1999/02/22-rdf-syntax-ns#type', type);
    if (res.length > 0) return true;
  }
  return false;
};

module.exports = {
  report(jobEntry) {
    return (reports) => {
      reportTotal(jobEntry)(reports);
      reports.map((r) => {
        if (r.title && r.type) reportType(jobEntry)(r);
        else logger.info('No specific entity types to report');
      });
      logger.info();
    };
  },
  getDedup,
  hasType,
};
