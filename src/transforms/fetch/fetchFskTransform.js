const config = require('../../config');
const loadFromURL = require('../../utils/loadFromURL');

const reportBaseURL = 'https://www.forsakringskassan.se/fk_apps/MEKAREST/public/v1/';
const metadataURL = 'https://www.forsakringskassan.se/fk_apps/GenerelltREST/public/v2/wcmcontent/meka?lang=sv';
let reports;
const init = async () => {
  const reportsBody = await loadFromURL(`${reportBaseURL}reports`);
  const reportsArr = JSON.parse(reportsBody);
  reportsArr.forEach(rArr => {
    rArr.reports.forEach(r => reports.push(r));
  });
  const metadataBody = await loadFromURL(metadataURL);
  return JSON.parse(metadataBody);
};

let counter;
const fetch = (jobEntry, pipeline, transformId, passOnObj, streamState) => {
  switch (streamState) {
    case 'init':
      reports = [];
      counter = 0;
      return init();
    case 'process':
      const report = reports[counter];
      counter += 1;
      return loadFromURL(`${reportBaseURL}${report}/meta.json`)
        .then(JSON.parse);
    default:
      return passOnObj;
  }
};

module.exports = fetch;
module.exports.stream = 'start';