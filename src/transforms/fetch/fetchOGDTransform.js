const fetchFactory = require('./fetchFactory');

const urlFactory = (args /* ,start ,limit */) => {
  const source = args ? args.source : '';
  return {
    url: source,
    method: 'GET',
  };
};
const contentTransform = (body) => {
  const doc = JSON.parse(body);
  if (doc.dataset && doc.dataset.length > 0) {
    return doc.dataset;
  }
  return null;
};

module.exports = fetchFactory(urlFactory, contentTransform);
module.exports.stream = 'start';