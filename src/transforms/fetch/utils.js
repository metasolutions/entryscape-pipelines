
class TooFewResultsError extends Error {
  constructor(args) {
    super(args);
    this.name = 'TooFewResultsError';
    this.message = `Error: only found ${args} records`;
  }
}
class LoadSourceError extends Error {
  constructor(args) {
    super(args);
    this.name = 'LoadFromSourceError';
    this.message = `Error: Could not load URL ${args}`;
  }
}

module.exports = {
  TooFewResultsError,
  LoadSourceError,
};
