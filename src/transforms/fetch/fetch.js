const { load } = require('../../utils/load');
const logger = require('../../utils/logger/logger');
const rdfParser = require("rdf-parse").default;
const { Graph } = require('@entryscape/rdfjson');

const fetch = async function (jobEntry, pipeline, transformId) {
  const args = pipeline.getTransformArguments(transformId);
  let source = args ? args.source : '';
  let type = args ? args.type : '';
  const urlParams = [undefined, undefined, undefined];
  const body = await load(source, urlParams);
  jobEntry.addD('storepr:fetchSource', 'true', 'xsd:boolean');
  jobEntry.addD('storepr:fetchRDF', 'true', 'xsd:boolean');
  logger.info('Success - fetched source');    
  const graph = await new Promise((resolve, reject) => {
    const g = new Graph();
    const textStream = require('streamify-string')(body);
    const quads = [];
    rdfParser.parse(textStream, {contentType: type})
      .on('data', (quad) => {
        logger.debug(quad);
        quads.push(quad);
      })
      .on('error', (error) => {
        logger.error(error);
        reject(error);
      })
      .on('end', () => {
        g.addQuads(quads);
        resolve(g)
      })
  })
  const fileEntry = await jobEntry.getContext().newEntry().commit();
  jobEntry.add('dcterms:source', fileEntry.getURI());
  try {
    await fileEntry.getResource(true).putJSON(graph.exportRDFJSON());
  } catch (error) {
    logger.error(error);
    logger.error('Graph could not be saved as resource');
  }
  return graph;
};

module.exports = fetch;
