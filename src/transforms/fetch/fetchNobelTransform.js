const fetchFactory = require('./fetchFactory');

// https://masterdataapi.nobelprize.org/2.0/laureates

const urlFactory = (args, start /* ,limit */) => {
  const source = args ? args.source : '';
  return {
    url: `${source}?extended=yes&offset=${start}&limit=25`,
    method: 'GET',
  };
};

let count = 0;
const contentTransform = (body) => {
  const doc = JSON.parse(body);
/*  if (count > 10) {
    return null;
  }*/
  count += 1;
  if (doc.laureates && doc.laureates.length > 0) {
    return doc.laureates;
  }
  return null;
};
module.exports = fetchFactory(urlFactory, contentTransform, 25);
module.exports.stream = 'start';