const loadFromURL = require('../../utils/loadFromURL');
const logger = require('../../utils/logger/logger');
const { px } = require('../../config')
const config = px;

let stackCursor;


const init = async (args) => {
  const source = config.primarySource ? config.primarySource : args.source;
  const source_en = config.secondarySource ? config.secondarySource : args.source;

  stackCursor = [];
  try {
    const body = await loadFromURL(source, null, 'GET');
    const body_en = await loadFromURL(source_en, null, 'GET');
    logger.info(`Loaded base from: ${source}`);
    stackCursor.push({
      url: source,
      url_en: source_en,
      content: JSON.parse(body),
      content_en: JSON.parse(body_en),
      cursor: 0,
    });
  } catch (err) {
    logger.error(`Failed loading records from: ${source} or ${source_en}`);
    logger.error(`Error is: ${err.message}`);
    throw err;
  }
};

const stack = [];

const pushIn = async () => {
  const current = stackCursor[stackCursor.length-1];
  const contentElement = Array.isArray(current.content) ? current.content[current.cursor] : current.content;
  if (!contentElement) {
    throw new Error('No content element found');
  }
  let id = contentElement.id || contentElement.dbid;
  let url = current.url
  if (config.urlEncode) {
    url = url + `/${id.replaceAll(' ', '%20')}`;
  } else {
    url = url + `/${id}`;
  }

  try {
    const body = await loadFromURL(url, null,'GET');
    const obj = {
      url,
      content: JSON.parse(body),
      cursor: stack.pop() || 0,
    };
    if (current.url_en && current.content_en) {
      let matchedId;
      const en_contentElement = current.content_en.find((element) => {
        let enid = element.id || element.dbid;
        if (enid.includes('.px') && config.idLangSpecific)  {
          enid = enid.replace(config.idLangSpecific.secondary, config.idLangSpecific.primary);
        }
        if(id === enid) {
          matchedId = enid;
          return true;
        }
      });
      if (en_contentElement) {
        let url_en = current.url_en
        if (config.urlEncode) matchedId = matchedId.replaceAll(' ', '%20');
        if (matchedId.includes('.px') && config.idLangSpecific) matchedId = matchedId.replace(config.idLangSpecific.primary, config.idLangSpecific.secondary);
        url_en = url_en + `/${matchedId}`;
        try {
          const body_en = await loadFromURL(url_en, null, 'GET');
          obj.url_en = url_en;
          obj.content_en = JSON.parse(body_en);
        } catch (err) {
          logger.error(`Failed loading english although parent list indicates it exists: ${url_en}`);
        }
      }
    }
    stackCursor.push(obj);
  } catch(err) {
    logger.error(`Failed loading records from: ${url}`);
    // if (url_en) logger.error(`or ${url_en}`);
    logger.error(`Error is: ${err && err.message}`);
    logger.info("Ignoring this level and moving to next");
    current.cursor = current.cursor + 1;
//    throw err;
  }
};

const getData = async () => {
  const current = stackCursor[stackCursor.length-1];
  const reqBody = {
    query: current.content.variables.map(v => ({
      code: v.code,
      selection: {
        filter: 'top',
        values: [1]
      }
    }))
  };
  try {
    current.data = await loadFromURL(current.url, null, 'POST', JSON.stringify(reqBody));
  } catch (err) {
    logger.error(`Failed post request to: ${current.url}`);
    logger.error(`Error is: ${err}`);
    logger.info('Proceding anyway');
    current.data = '';
  }
};

const popOut = () => {
  stackCursor.pop();
  const current = stackCursor[stackCursor.length-1];
  current.cursor = current.cursor + 1;
};

const isLeafLevel = () => {
  const current = stackCursor[stackCursor.length-1];
  return !Array.isArray(current.content);
};

const pushInToLeaf = async () => {
  if (!isLeafLevel()) {
    await pushIn();
    return await pushInToLeaf();
  }
  await getData();
};

const popOutToNextBranch = () => {
  while (true) {
    popOut();
    const current = stackCursor[stackCursor.length-1];
    if (current.content.length > current.cursor) {
      return true;
    } else if (stackCursor.length === 1 && current.content.length === current.cursor) {
      return false;
    }
  }
};

const constructValue = () => ({
  url: stackCursor[stackCursor.length - 1].url,
  url_en: stackCursor[stackCursor.length - 1].url_en, // May not exist
  sv: stackCursor[stackCursor.length - 1].content,
  en: stackCursor[stackCursor.length - 1].content_en, // May not exist
  data: stackCursor[stackCursor.length - 1].data, // May not exist
  path: stackCursor.slice(0, stackCursor.length - 1).map((obj) => {
    const sv = obj.content[obj.cursor];
    const ret = { sv };

    // Search for the element, may be in another order then the swedish order since not everything is translated,
    // hence search for it based on the id rather than the cursor.
    if (obj.content_en) {
      let id = sv.id || sv.dbid;
      if (id.includes('.px') && config.idLangSpecific) id = id.replace(config.idLangSpecific.primary, config.idLangSpecific.secondary);
      ret.en = obj.content_en.find(el => el.id === id || el.dbid === id );
    }
    return ret;
  }),
});

let counter = 0;
let startTime;

const next = async () => {
  if (isLeafLevel()) {
    counter += 1;
    if (counter%10 === 0) {
      logger.info(`${counter} has been fetched`);
      const now = new Date().getTime();
      const timeInMinutes = Math.floor((now-startTime)/60000);
      logger.info(`Running time is ${timeInMinutes} minutes`);
    }
    const hasNext = await popOutToNextBranch();
    if (!hasNext) {
      return null;
    }


    await pushInToLeaf();
    return constructValue();
  } else {
    // This is the case after initialization.
    await pushInToLeaf();
    return constructValue();
  }
};

let firstContent = true;
const firstContentCheck = (jobEntry) => {
  if (firstContent) {
    firstContent = false;
    jobEntry.addD('storepr:fetchSource', 'true', 'xsd:boolean');
  }
};

const fetch = (jobEntry, pipeline, transformId, passOnObj, streamState) => {
  switch (streamState) {
    case 'init':
      counter = 0;
      startTime = new Date().getTime();
      const args = pipeline.getTransformArguments(transformId);
      firstContent = true;
      return init(args);
    case 'process':
      return next().then((data) => {
        if (data === null) {
          return null;
        }
        firstContentCheck(jobEntry);
        return data;
      });
    default:
      return passOnObj;
  }
};

module.exports = fetch;
module.exports.stream = 'start';
