const fetchFactory = require('./fetchFactory');
const cswXML = require('../../utils/cswXML');
const select = require('../../utils/cswSelect');
const config = require('../../config');
const FewResultsError = require('./utils').TooFewResultsError;
const logger = require('../../utils/logger/logger');

/*
  http://ied.metagis.se/geonetwork/srv/swe/csw?request=GetRecords&service=CSW&version=2.0.2&typeNames=gmd:MD_Metadata&maxRecords=2&startPosition=1&ElementSetName=full&outputSchema=http://data.naturvardsverket.se/document&outputFormat=application/xml&resultType=results&outputSchema=http://data.naturvardsverket.se/document
  */

const csw = config.csw || config.fetchCSW || {};
const cswDefault = csw.default || csw;
config.csw = cswDefault;

const oneOrTwoPass = () => {
  const limit = config.csw.maxRecords || 10; // TODO make sure it doesn't exceed what the max is for CSW server
  const urlParams = {
    request: 'GetRecords',
    outputFormat: 'application/xml',
    version: '2.0.2',
    service: 'CSW',
    constraintLanguage: 'CQL_TEXT',
    constraint_language_version: '1.1.0',
    // Some documentation on expressions here: https://docs.geotools.org/latest/userguide/library/cql/cql.html
    elementSetName: 'full',
    resultType: 'results',
    typeNames: config.csw.typeNames || 'gmd:MD_Metadata',
    outputSchema: config.csw.outputSchema || 'http://www.isotc211.org/2005/gmd',
    distributedsearch: config.csw.distributedsearch || false,
    maxRecords: limit,
  };
  if (config.csw.constraint) {
    urlParams.constraint = config.csw.constraint;
  }
  const params = Object.assign({}, urlParams, {
    namespaces: Object.assign({
      gmd: 'http://www.isotc211.org/2005/gmd',
      ns1: 'http://www.isotc211.org/2005/gmd',
      gco: 'http://www.isotc211.org/2005/gco',
      csw: 'http://www.opengis.net/cat/csw/2.0.2',
      ows: 'http://www.opengis.net/ows',
      srv: 'http://www.isotc211.org/2005/srv',
      xlink: 'http://www.w3.org/1999/xlink'
    }, config.csw.namespaces || {}),
    /**
     * Filters to apply to check if CSW records should be included or not
     */
    filters: config.csw.filters,
    recordXPath: config.csw.recordXPath || '//gmd:MD_Metadata',
  });

  const urlFactory = (args, start) => {
    params.startPosition = start + 1;

    const xml = cswXML.getRecordsQueryXML(params);
    const xmlString = cswXML.xmlToString(xml);

    if (config.csw.method === 'GET') {
      const mergeParams = Object.assign({ startPosition: params.startPosition }, urlParams);
      const query = Object.keys(mergeParams).map(key => `${key}=${mergeParams[key]}`).join('&');
      return {
        url: `${args ? args.source : ''}?${query}`,
        method: 'GET',
      };
    }
    return {
      url: args ? args.source : '',
      method: config.csw.method || 'POST',
      params: xmlString,
    };
  };

  /**
   *
   * @param body
   * @return {*}
   */
  const contentTransform = (body) => {
    const { records, matched, returned } = cswXML.parseRecords(body, params);

    logger.info(`Fetched ${parseInt(params.startPosition, 10) - 1 + parseInt(returned, 10)} / ${matched} records`);
    logger.info(`Passed filters : ${parseInt(records.length, 10)} records`);

    if (parseInt(params.startPosition, 10) > parseInt(matched, 10)) {
      return null;
    }

    return records;
  };

  if (config.csw.datasetFilters || config.csw.serviceFilters) {
    const cswtransform = fetchFactory(urlFactory, contentTransform, limit);
    let services = [];
    let datasets = [];

    const loadAll = async (jobEntry, pipeline, transformId, passOnObj) => {
      const args = pipeline.getTransformArguments(transformId);
      const afterInit = await cswtransform(jobEntry, pipeline, transformId, passOnObj, 'init');
      let item = await cswtransform(jobEntry, pipeline, transformId, afterInit, 'process');
      while (item != null) {
        if (config.csw.serviceFilters && cswXML.recordPassesFilters(item, params.namespaces, config.csw.serviceFilters)) {
          // The filter says it is a service
          const relatedDatasets = config.csw.getServicedDatasets(item, select);
          // The service has more than zero related datasets
          if (relatedDatasets.length > 0) {
            services.push(cswXML.xmlToString(item));
          }
        } else if (!config.csw.datasetFilters ||
          (config.csw.datasetFilters && cswXML.recordPassesFilters(item, params.namespaces, config.csw.datasetFilters))) {
          // Either no filter or there is a filter and it passes
          // Store all non-services as strings to minimize memory footprint when doing two-pass

          datasets.push(cswXML.xmlToString(item));
        }
        item = await cswtransform(jobEntry, pipeline, transformId, afterInit, 'process');
      }
      logger.info(`Found ${services.length} dataservices and ${datasets.length} datasets in harvesting phase`);
      if (args.limit && (Number(datasets.length) < Number(args.limit))) throw new FewResultsError(`${datasets.length}/${args.limit}`);
      return passOnObj;
    };

    let cursor = 0;
    let result;
    return (jobEntry, pipeline, transformId, passOnObj, streamState) => {
      switch (streamState) {
        case 'init':
          cursor = 0;
          if (services) {
            services = [];
          }
          if (datasets) {
            datasets = [];
          }
          return loadAll(jobEntry, pipeline, transformId, passOnObj);
        case 'process':
          if (cursor < services.length) {
            result = select('gmd:MD_Metadata', cswXML.parseRecord(services[cursor]))[0];
            result.__service = true;
          } else if ((cursor - services.length) < datasets.length) {
            result = select('gmd:MD_Metadata', cswXML.parseRecord(datasets[cursor - services.length]))[0];
          } else {
            result = undefined;
          }
          cursor += 1;
          return result;
        default:
          results = [];
          return passOnObj;
      }
    };
  } else {
    return fetchFactory(urlFactory, contentTransform, limit);
  }
};

let fetchTransform;
module.exports = (jobEntry, pipeline, transformId, passOnObj, streamState) => {
  if (streamState === 'init') {
    const args = pipeline.getTransformArguments(transformId);
    if (args.profile && csw[args.profile]) {
      config.csw = csw[args.profile];
    } else {
      config.csw = cswDefault;
    }
    config.csw.filters = config.csw.filters || config.fetchCSWFilters || [];
    fetchTransform = oneOrTwoPass();
  }
  return fetchTransform(jobEntry, pipeline, transformId, passOnObj, streamState);
}

module.exports.stream = 'start';
