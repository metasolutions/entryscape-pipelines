const loadFromURL = require('../../utils/loadFromURL');
const logger = require('../../utils/logger/logger');

module.exports = (urlFactory, contentTransform, limit = 0) => {
  let data = [];
  let cursor = 0;
  let start;
  let args;
  let first = true;
  const load = () => {
    if (!first && limit === 0) {
      data = null;
      first = true;
      return Promise.resolve();
    }
    first = false;
    start += limit;
    const { url, method, params, mimeType } = urlFactory(args, start, limit);
    return loadFromURL(url, null, method, params, mimeType).then((body) => {
      logger.info(`Loaded records from: ${url}`);
      data = contentTransform(body, args);
    }, (err) => {
      logger.error(`Failed loading records from: ${url}`);
      logger.error(`Error is: ${err.message}`);
      throw err;
    });
  };

  // Above the fetch method:
  const loadRecursively = () => load().then(() => {
    if (data === null) {
      return null;
    } else if (data.length === 0) {
      return loadRecursively();
    }
    cursor = 0; // runs only once for every non empty batch (loadRecursively)
    return data[cursor];
  });

  let firstContent = true;
  const firstContentCheck = (jobEntry) => {
    if (firstContent && Array.isArray(data) && data.length > 0) {
      firstContent = false;
      jobEntry.addD('storepr:fetchSource', 'true', 'xsd:boolean');
    }
  };

  /**
   * Return a single passOnObj
   * @param jobEntry
   * @param pipeline
   * @param transformId
   * @param passOnObj
   * @param streamState
   * @return {*}
   */
  const fetch = (jobEntry, pipeline, transformId, passOnObj, streamState) => {
    switch (streamState) {
      case 'process':
        if (data === null) {
          return null;
        }
        if (data.length > 0) {
          cursor += 1;
        }
        if (data.length === cursor) {
          return loadRecursively().then((val) => {
            firstContentCheck(jobEntry);
            return val;
          });
        }
        firstContentCheck(jobEntry);
        return data[cursor];
      default:
        args = pipeline.getTransformArguments(transformId);
        data = []; // init or finish
        start = -limit;
        cursor = 0;
        firstContent = true;
        return passOnObj;
    }
  };

  fetch.stream = 'start';
  return fetch;
};
