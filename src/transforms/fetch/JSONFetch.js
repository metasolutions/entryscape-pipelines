const loadFromURL = require('../../utils/loadFromURL');
const logger = require('../../utils/logger/logger');

const fetch = function (jobEntry, pipeline, transformId) {
  const args = pipeline.getTransformArguments(transformId);
  const fetchURL = args ? args.source : '';
  return loadFromURL(fetchURL)
    .then((body) => {
      jobEntry.addD('storepr:fetchSource', 'true', 'xsd:boolean');
      logger.info('Success - fetched source');
      try {
        return JSON.parse(body);
      } catch (e) {
        jobEntry.addD('storepr:fetchJSON', 'false', 'xsd:boolean');
        throw new Error(`Failure - could not parse JSON: ${e}`);
      }
    }, (err) => {
      jobEntry.addD('storepr:fetchSource', 'false', 'xsd:boolean');
      logger.info('Failed - fetching source');
      throw err;
    });
};

module.exports = fetch;
