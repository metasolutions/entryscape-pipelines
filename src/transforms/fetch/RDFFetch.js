const { converters } = require('@entryscape/rdfjson');
const { load } = require('../../utils/load');
const mappers = require('../../mappers/mappers');
const FewResultsError = require('./utils').TooFewResultsError;
const logger = require('../../utils/logger/logger');

const fetch = function (jobEntry, pipeline, transformId) {
  const args = pipeline.getTransformArguments(transformId);
  const source = args ? args.source : '';
  const urlParams = [undefined, undefined, undefined, 'application/rdf+xml'];
  return load(source, urlParams)
    .then(async (body) => {
    jobEntry.addD('storepr:fetchSource', 'true', 'xsd:boolean');
    logger.info('Success - fetched source');
    // Assume RDF
    converters.convertSpacesInURIs = true;
    const detect = await converters.detect(body);
    if (!detect.error) {
      jobEntry.addD('storepr:fetchRDF', 'true', 'xsd:boolean');
      logger.info('Success - fetched RDF');
      const datasets = detect.graph.find(null, 'rdf:type', 'dcat:Dataset');
      if (args.limit && (datasets.length < Number(args.limit))) throw new FewResultsError(`${datasets.length}/${args.limit}`);

      return mappers.runAll(detect.graph, pipeline, transformId, args).then(() => jobEntry.getContext().newEntry().commit()
        .then(async (fileEntry) => {
          jobEntry.add('dcterms:source', fileEntry.getURI());
          try {
            await fileEntry.getResource(true).putJSON(detect.graph.exportRDFJSON());
          } catch (error) {
            logger.error(error);
            logger.error('Graph could not be saved as resource');
          }
        })).then(() => detect.graph);
    }

    jobEntry.addD('storepr:fetchRDF', 'false', 'xsd:boolean');
    if (detect.errorCode === 1 || detect.errorCode === 2) {
      jobEntry.addL('storepr:fetchRDFError', detect.error);
    }
    throw new Error(`Failure - fetch RDF: ${detect.error}`);
  });
};

module.exports = fetch;
