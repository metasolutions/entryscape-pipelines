const loadFromURL = require('../../utils/loadFromURL');
const FewResultsError = require('./utils').TooFewResultsError;

//const filesURL = 'https://opendata.dresden.de/duva2ckan/files';
let datasetFiles;
let counter;

const fetch = (jobEntry, pipeline, transformId, passOnObj, streamState) => {
  const args = pipeline.getTransformArguments(transformId) || {};

  switch (streamState) {
    case 'init':
      counter = 0;
      return loadFromURL(args.source).then(body => {
        datasetFiles = JSON.parse(body).map(o => {
          const uri = args.path ? o[args.path] : o.uri;
          // If not absolute, use source as base
          if (uri && !uri.startsWith('http')) {
            const separator = args.source[args.source.length - 1] === '/' ? '' : '/';
            return `${args.source}${separator}${uri}`;
          }
          return uri;
        }).filter(uri => uri !== undefined);
        if (args.limit && datasetFiles.length && datasetFiles.length < args.limit) throw new FewResultsError(`${datasetFiles.length}/${args.limit}`);
      });
    case 'process':
      const uri = datasetFiles[counter];
      counter += 1;
      return loadFromURL(uri).then(JSON.parse);
    default:
      return passOnObj;
  }
};

module.exports = fetch;
module.exports.stream = 'start';