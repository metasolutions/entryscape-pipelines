const fetchFactory = require('./fetchFactory');
const config = require('../../config');
const FewResultsError = require('./utils').TooFewResultsError;

const urlFactory = (args, start /* ,limit */) => {
  const source = args ? args.source : '';
  return {
    url: `${source}/api/3/action/package_search?start=${start}`,
    method: 'GET',
  };
};
const contentTransform = (body, args) => {
  const doc = JSON.parse(body);
  const ckan = config.ckan.default || config.ckan;
  if (doc.result.count && args.limit && doc.result.count < args.limit) throw new FewResultsError(`${doc.result.count}/${args.limit}`);
  if (doc.result && doc.result.results && doc.result.results.length > 0) {
    if (ckan && ckan.filter) {
      return ckan.filter(doc);
    }
    return doc.result.results;
  }
  return null;
};
module.exports = fetchFactory(urlFactory, contentTransform, 10);
module.exports.stream = 'start';