const example = require('./example');
const { converters } = require('@entryscape/rdfjson');

const ex = (jobEntry, pipeline, transformId, passOnObj) =>
  converters.rdfxml2graph(example);
ex.intermediateTransform = true;

module.exports = ex;
