const { EntryStore, types } = require('@entryscape/entrystore-js');
const { Graph, converters } = require('@entryscape/rdfjson');
const globals = require('../utils/globals');
const logger = require('../utils/logger/logger');
const config = require('../config');
const { report } = require('../transforms/merge/utils');
const { profileTypes } = require('../utils/transform');
const validateTransform = require('./validateTransform');
const { EntrySync, ContextSync } = require('@entryscape/entrysync');

/**
 * This transform corresponds to the "copy" transform type and copies information from one
 * context to another. The source context may reside on another server.
 * The arguments are:
 *
 * * context - mandatory and indicates the source context via a context id.
 * * repository - optional, indicates which repository the source context resides in, if it is
 * not provided it is assumed that the source context is in the same repository as the
 * destination context
 *
 * The destination context is given by the location of the pipeline.
 * The copy algorithm uses the EntrySync approach, see {@link dcat/utils/EntrySync}.
 *
 * @param {store/Entry} jobEntry
 * @param {store/Pipeline} pipeline
 * @param {string} transformId
 * @return {Promise}
 */
const transform = async (jobEntry, pipeline, transformId) => {
  const args = pipeline.getTransformArguments(transformId);
  const toContext = jobEntry.getContext();
  const fromEs = args.repository ? new EntryStore(args.repository) :
    globals.getEntryStore();
  const fromContext = args.context ? fromEs.getContextById(args.context) : undefined;
  if (args.context) {
    logger.info(`Copying entries from context ${fromContext.getResourceURI()} to context ${toContext.getResourceURI()}`);
  } else {
    logger.info(`Copying entries from entrystore ${fromEs.getBaseURI()} to context ${toContext.getResourceURI()}`);
  }

  // If a module is given it corresponds to a restriction to specific contexts.
  // For simplicity we calculate them in advance
  let contextIncludeList;
  if (args.module) {
    contextIncludeList = new Set();
    const moduleType = `http://entryscape.com/terms/${args.module[0].toUpperCase()}${args.module.substring(1)}Context`
    await fromEs.newSolrQuery().publicRead().limit(100).rdfType(moduleType)
      .graphType(types.GT_CONTEXT).forEach(contextEntry => contextIncludeList.add(contextEntry.getId()));
  }

  if (args.profile && config.profiles && config.profiles[args.profile]) {
    const toEntryStore = globals.getEntryStore();
    const source = {
      graph: new Graph(),
      currentEntries: 0,
      maxEntries: config.validate && config.validate.maxEntries ? config.validate.maxEntries : 1000,
    };
    const [ primaryTypes, supportiveTypes ] = profileTypes(args);
    const reports = [];
    const primaryContexts = new Set();
    const extraIndex = {};
    const extraGraphs = {};
    const doValidation = args.validate ? (entry) => {
      const passOnObject = {
        mainGraph: entry.getAllMetadata(),
        mainURI: entry.getResourceURI(),
        extraIndex,
        extraGraphs
      };
      validateTransform(jobEntry, pipeline, transformId, passOnObject, 'process');
    } : () => {};
    const addToSource = (entry) => {
      if (source.currentEntries < source.maxEntries) {
        source.graph.addAll(entry.getMetadata());
        source.currentEntries++;
        if (source.currentEntries === source.maxEntries) logger.warn('Max entries exceeded for source graph, ignoring remaining entries!');
      }
    };
    const saveSource = async () => {
      const rdfGraph = source.graph.exportRDFJSON();
      const fileEntry = await jobEntry.getContext().newEntry().commit();
      await fileEntry.getResource(true).putJSON(rdfGraph);
      return fileEntry;
    };
    await validateTransform(jobEntry, pipeline, transformId, {}, 'init');

    await Promise.all(primaryTypes.map(async(mtype) => {
      const msync = new EntrySync({context: toContext})
      await msync.loadInitialIndexFromDisk();
      await msync.calculateIndexFromList(toEntryStore.newSolrQuery().rdfType(mtype).context(toContext).list());
      if (fromContext) {
        let query = fromEs.newSolrQuery()
          .context(fromContext).graphType(types.GT_NONE);
        if (mtype) query = query.rdfType(mtype);
        await query.forEach((entry) => {
          doValidation(entry);
          addToSource(entry);
          return msync.harvestEntry(entry)
        });
      } else {
        let query = fromEs.newSolrQuery().graphType(types.GT_NONE);
        if (mtype) query = query.rdfType(mtype);
        await query.forEach((entry) => {
          // Ignore all entities found that are not in a context belonging to the right module (if any is given)
          if (contextIncludeList && !contextIncludeList.has(entry.getContext().getId())) {
            return;
          }
          doValidation(entry);
          addToSource(entry);
          // If synchronizing a whole repository, keep track of which contexts we found primary instances
          primaryContexts.add(entry.getContext().getId());
          return msync.harvestEntry(entry)
        });
      }
      await msync.removeMissing();
      await msync.saveSynchronizedIndexToDisk();

      // One report per primary type
      reports.push({ ...msync.getReport(), type: mtype, title: mtype});
    }));
    if (supportiveTypes.length > 0) {
      // Since we don't report supportive types, we can do them all in one EntrySync.
      const ssync = new EntrySync({context: toContext});

      await ssync.loadInitialIndexFromDisk()
      await ssync.calculateIndexFromList(toEntryStore.newSolrQuery().rdfType(supportiveTypes)
        .context(toContext).list());

      if (fromContext) {
        let query = fromEs.newSolrQuery()
          .context(fromContext).graphType(types.GT_NONE);
        if (supportiveTypes) query = query.rdfType(supportiveTypes);
        await query.forEach((entry) => {
          doValidation(entry);
          addToSource(entry);
          return ssync.harvestEntry(entry)
        });
      } else {
        // If synchronizing a whole repository, restrict to check for supportive instances to the contexts
        // where primary instances where found
        const arr = Array.from(primaryContexts);
        for (let idx = 0; idx < arr.length; idx++) {
          let query = fromEs.newSolrQuery()
            .context(fromEs.getContextById(arr[idx])).graphType(types.GT_NONE);
          if (supportiveTypes) query = query.rdfType(supportiveTypes);
          await query.forEach((entry) => {
            doValidation(entry);
            addToSource(entry);
            return ssync.harvestEntry(entry)
          });
        }
      }
      await validateTransform(jobEntry, pipeline, transformId, {}, 'finish');
      await ssync.removeMissing();
      await ssync.saveSynchronizedIndexToDisk();
    }
    const fileEntry = await saveSource();
    report(jobEntry)(reports);
    jobEntry.add('dcterms:source', fileEntry.getURI());
    jobEntry.addD('storepr:merge', 'true', 'xsd:boolean');
    jobEntry.addD('storepr:fetchSource', 'true', 'xsd:boolean');
    jobEntry.addD('storepr:fetchRDF', 'true', 'xsd:boolean');
  } else {
    const idx = new ContextSync({sourceContext: fromContext, destinationContext: toContext});
    return idx.sync().then(async () => {
      let report = idx.getReport();
      jobEntry.addD('storepr:mergeResourceCount', `${report.count}`, 'xsd:integer');
      logger.info(`Synchronized a total of ${report.count} entries`);
      jobEntry.addD('storepr:mergeMainResourceCount', `${report.count}`, 'xsd:integer');
      jobEntry.addD('storepr:mergeAdded', `${report.added}`, 'xsd:integer');
      logger.info(`  ${report.added} added`);
      jobEntry.addD('storepr:mergeRemoved', `${report.removed}`, 'xsd:integer');
      logger.info(`  ${report.removed} removed`);
      jobEntry.addD('storepr:mergeUpdated', `${report.updated}`, 'xsd:integer');
      logger.info(`  ${report.updated} updated`);
      jobEntry.addD('storepr:mergeUnchanged', `${report.unchanged}`, 'xsd:integer');
      logger.info(`  ${report.unchanged} unchanged`);
      jobEntry.addD('storepr:merge', 'true', 'xsd:boolean');
      jobEntry.addD('storepr:fetchSource', 'true', 'xsd:boolean');
      jobEntry.addD('storepr:fetchRDF', 'true', 'xsd:boolean');
    });
  }
};

module.exports = transform;
