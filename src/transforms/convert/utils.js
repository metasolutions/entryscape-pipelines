module.exports = {
  /**
   * Uses the publisherMapping metod or array to find a prospective publisher.
   * In case the publisherMapping is an array each value is an object with 6 different ways of matching a publisher:
   * name - exact match of a name (lowercase and trimmed)
   * org - exact match of a organization (lowercase and trimmed)
   * email - exact match of an email (lowercase and trimmed)
   * nameRegexp - test via regexp of name
   * orgRegexp - test via regexp of organization
   * emailRegexp - test via regexp of email
   *
   * For each matching object there must be a uri-attribute corresponding to publisher to connect to.
   * The following is an example of a publisherMapping array:
   * [
   *       {
   *         org: 'hallands län',
   *         uri: 'https://catalog.lansstyrelsen.se/store/16/resource/1'
   *       },
   *       {
   *         orgRegexp: 'västra',
   *         uri: 'https://catalog.lansstyrelsen.se/store/15/resource/1'
   *       },
   *       {
   *         orgRegexp: 'gemensamt',
   *         uri: 'https://catalog.lansstyrelsen.se/store/2/resource/1'
   *       }
   * ]
   *
   * @params an object with name, org and email
   * @mapping is the mapping configuration to use, e.g. drawn from config.csw.publisherMapping,
   * may be an array or a method. If a method is used it is calles with the params parameter and are expected to return
   * a URI or undefined if there is no match.
   */
  publisherMap: (params, mapping) => {
    if (Array.isArray(mapping)) {
      const p = mapping.find((publisher) => {
        return ['name', 'org', 'email'].find((field) => {
          if (publisher[field] && publisher[field] === (params[field] || '').toLocaleLowerCase().trim()) {
            return true;
          } else if (publisher[`${field}Regexp`] && new RegExp(publisher[`${field}Regexp`]).test((params[field] || '')
            .toLocaleLowerCase())) {
            return true;
          }
          return false;
        });
      });
      if (p) {
        return p.uri;
      }
    } else if (typeof mapping === 'function') {
      return mapping(params);
    }
  }
};