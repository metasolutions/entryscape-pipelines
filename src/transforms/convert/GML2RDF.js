const { Graph, namespaces } = require('@entryscape/rdfjson');
const iso639 = require('../../utils/iso639');
const config = require('../../config');
const XML2RDFUtil = require('../../utils/XML2RDFUtil');
const select = require('../../utils/cswSelect');
const GMLCodes = require('./GML/Codes');
const date = require('./GML/date');
const contactpoint = require('./GML/contactpoint');
const publisher = require('./GML/publisher');
const distribution = require('./GML/distribution');
const theme = require('./GML/theme');
const { addDate, recordBase, createSubject } = require('./GML/utils');
const logger = require('../../utils/logger/logger');

// Add some namespaces that we will need for the mapping
namespaces.add('spdx', 'http://spdx.org/rdf/terms#');
namespaces.add('dcatde', 'http://dcat-ap.de/def/dcatde/');
namespaces.add('locn', 'http://www.w3.org/ns/locn#');
namespaces.add('gsp', 'http://www.opengis.net/ont/geosparql#');

const extractLang = (record, args) => {
  let lang = select('string(gmd:language/gmd:LanguageCode/@codeListValue)', record);
  if (lang !== '') {
    lang = iso639.to2(lang);
  } else {
    lang = args.defaultLanguage;
  }
  return lang;
};

// Map dataset to the data services which serves it.
let dataset2service = {};

// Map data service to served datasets.
let service2datasets = {};


const keywordBanCheck = XML2RDFUtil.createBanCheck(config?.csw?.bannedKeywords);
const mapKeyword = (keyword) => {
  if (config.csw.mappedKeywords) {
    return config.csw.mappedKeywords[keyword] || keyword;
  }
  return keyword;
};


const basic = (graph, subject, language, record) => {
  const addL = XML2RDFUtil.addLFactory({
    graph, subject, language, record, select,
  });
  graph.add(subject, 'rdf:type', 'dcat:Dataset');
  addL({ property: 'dcterms:title', path: `${recordBase}gmd:citation/*/gmd:title/gco:CharacterString` });
  addL({ property: 'dcterms:description', path: `${recordBase}gmd:abstract/gco:CharacterString` });
  addL({ property: 'dcterms:description', path: `${recordBase}gmd:purpose/gco:CharacterString` });
  select(`${recordBase}gmd:descriptiveKeywords`, record).forEach((descKeys) => {
    const thesaurus = select('string(.//gmd:thesaurusName//gmd:title/*)', descKeys);

    if (config.csw.allowedKeywordThesaurus && config.csw.allowedKeywordThesaurus.indexOf(thesaurus || '') === -1) {
      return;
    }

    let hvd;
    if (thesaurus.includes('High-value dataset categories') || thesaurus.includes('Kategori för värdefulla datamängder')) {
      graph.add(subject, 'http://data.europa.eu/r5r/applicableLegislation', 'http://data.europa.eu/eli/reg_impl/2023/138/oj');
      hvd = true;
    }
    
    select(`.//gmd:keyword`, descKeys).forEach((key) => {
      const keyword = select('string(./*)', key);
      const keywordURI = select('string(./gmx:Anchor/@xlink:href)', key);

      if (hvd && keywordURI && keywordURI.includes('http://data.europa.eu/bna/'))  {
        graph.add(subject, 'http://data.europa.eu/r5r/hvdCategory', keywordURI);
      } else if (hvd && keyword) {
        if (keyword.includes('Erdbeobachtung und Umwelt')) {
          graph.add(subject, 'http://data.europa.eu/r5r/hvdCategory', 'http://data.europa.eu/bna/c_dd313021');
        } else if (keyword.includes('Georaumelse')) {
          graph.add(subject, 'http://data.europa.eu/r5r/hvdCategory', 'http://data.europa.eu/bna/c_ac64a52d');
        } else if (keyword.includes('Mobilität')) {
          graph.add(subject, 'http://data.europa.eu/r5r/hvdCategory', 'http://data.europa.eu/bna/c_b79e35eb');
        } else if (keyword.includes('Meteorologie')) {
          graph.add(subject, 'http://data.europa.eu/r5r/hvdCategory', 'http://data.europa.eu/bna/c_164e0bf5');
        } else if (keyword.includes('Statistik')) {
          graph.add(subject, 'http://data.europa.eu/r5r/hvdCategory', 'http://data.europa.eu/bna/c_e1da4e07');
        } else if (keyword.includes('Unternehmen und Eigentümerschaft von Unternehmen')) {
          graph.add(subject, 'http://data.europa.eu/r5r/hvdCategory', 'http://data.europa.eu/bna/c_a9135398');
        }
      } else if (keyword && keywordBanCheck(keyword)) {
        graph.addL(subject, 'dcat:keyword', mapKeyword(keyword.trim()), language);
      }
    });
  });
};

const basicDataService = (graph, subject, language, record) => {
  const addL = XML2RDFUtil.addLFactory({
    graph, subject, language, record, select,
  });
  graph.add(subject, 'rdf:type', 'dcat:DataService');
  addL({ property: 'dcterms:title', path: `${recordBase}gmd:citation/*/gmd:title/gco:CharacterString` });
  addL({ property: 'dcterms:description', path: `${recordBase}gmd:abstract/gco:CharacterString` });
  addL({ property: 'dcterms:description', path: `${recordBase}gmd:purpose/gco:CharacterString` });
  select(`${recordBase}gmd:descriptiveKeywords`, record).forEach((descKeys) => {
    const thesaurus = select('string(.//gmd:thesaurusName//gmd:title/*)', descKeys);
    if (config.csw.allowedKeywordThesaurus && config.csw.allowedKeywordThesaurus.indexOf(thesaurus || '') === -1) {
      return;
    }
    select(`.//gmd:keyword`, descKeys).forEach((key) => {
      const keyword = select('string(./*)', key);
      if (keyword && keywordBanCheck(keyword)) {
        graph.addL(subject, 'dcat:keyword', mapKeyword(keyword.trim()), language);
      }
    });
  });
};


const links = (graph, subject, record) => {
  const distributorSelector = 'gmd:distributionInfo/gmd:MD_Distribution/gmd:distributor';
  try {
    const distributors = select(distributorSelector, record) || [];
    const linkSelect = 'string(gmd:MD_Distributor/gmd:distributorTransferOptions/gmd:MD_DigitalTransferOptions/gmd:onLine/gmd:CI_OnlineResource/gmd:linkage)';
    // const descSelect = 'string(gmd:MD_Distributor/gmd:distributorTransferOptions/gmd:MD_DigitalTransferOptions/gmd:onLine/gmd:CI_OnlineResource/gmd:description)';
    distributors.forEach((distributor) => {
      const link = select(linkSelect, distributor).trim();

      if (link !== '' && link.toUpperCase().includes('WMS')) {
        graph.add(subject, 'dcat:endpointURL', link);
        graph.add(subject, 'dcterms:conformsTo', 'https://resources.geodata.se/codelist/metadata/protokoll.xml#HTTP:OGC:WMS');
      } else if (link !== '') {
        graph.add(subject, 'dcat:endpointURL', link);
      }
    });
  } catch (err) {
    logger.error(err);
  }
  const transferOptionSelector = 'gmd:distributionInfo/gmd:MD_Distribution/gmd:transferOptions';
  const transfers = select(transferOptionSelector, record) || [];
  const transferSelect = 'string(gmd:MD_DigitalTransferOptions/gmd:onLine/gmd:CI_OnlineResource/gmd:linkage/gmd:URL)';
  transfers.forEach((transfer) => {
    const transferLink = select(transferSelect, transfer).trim();
    if (transferLink.includes('pdf')) {
      graph.add(subject, 'http://xmlns.com/foaf/0.1/page', transferLink);
    } else if (transferLink.includes('html')) {
      graph.add(subject, 'http://www.w3.org/ns/dcat#landingPage', transferLink);
    }
  });

  const containsOperations = select('gmd:identificationInfo/srv:SV_ServiceIdentification/srv:containsOperations', record) || [];
  containsOperations.forEach((operator) => {
    const operatorURL = select('string(srv:SV_OperationMetadata/srv:connectPoint/gmd:CI_OnlineResource/gmd:linkage/gmd:URL)', operator);
    if(operatorURL && operatorURL !== '') {
      graph.add(subject, 'dcat:endpointURL', operatorURL);
    }
  });
};


const lineage = (g, s, l, record) => {
  // Lineage as provenancestatement
  const lnodes = select('gmd:dataQualityInfo/*/gmd:lineage/*/gmd:statement/gco:CharacterString', record);
  if (lnodes.length > 0) {
    lnodes.forEach((lnode) => {
      if (lnode.firstChild !== null) {
        const stmt = g.add(s, 'dcterms:provenance', null);
        g.add(stmt.getValue(), 'rdf:type', 'dcterms:ProvenanceStatement');
        g.addL(stmt.getValue(), 'rdfs:value', lnode.firstChild.nodeValue, l);
      }
    });
  }
};

const resourceLanguage = (g, s, l, record) => {
  let lang = select(`string(${recordBase}gmd:language/gmd:LanguageCode/@codeListValue)`, record);
  if (config.csw.langMapping && config.csw.langMapping[lang]) {
    lang = config.csw.langMapping[lang];
  }
  if (lang !== '') {
    g.add(s, 'dcterms:language', iso639.toEU(lang));
  }
};

const maintainance = (g, s, l, record) => {
  const mcode = select(`string(${recordBase}gmd:resourceMaintenance//gmd:MD_MaintenanceFrequencyCode/@codeListValue)`, record);
  if (mcode !== '') {
    const freq = GMLCodes.mapMaintainance(mcode);
    if (freq) {
      g.add(s, 'dcterms:accrualPeriodicity', freq);
    } else {
      logger.info(`Could not map gmd:MD_MaintenanceFrequencyCode: ${mcode}`);
    }
  }
};


const spatial = (g, s, l, record) => {
  const extents = select(`${recordBase}gmd:extent//gmd:EX_GeographicBoundingBox`, record);
  extents.forEach((extent) => {
    const spatialstmt = g.add(s, 'dcterms:spatial');
    g.add(spatialstmt.getValue(), 'rdf:type', 'dcterms:Location');
    const W = select('string(gmd:westBoundLongitude/*)', extent);
    const E = select('string(gmd:eastBoundLongitude/*)', extent);
    const S = select('string(gmd:southBoundLatitude/*)', extent);
    const N = select('string(gmd:northBoundLatitude/*)', extent);
    // POLYGON((W N,E N,E S,W S,W N))
    const value = `POLYGON((${[`${W} ${N}`, `${E} ${N}`, `${E} ${S}`, `${W} ${S}`, `${W} ${N}`].join(',')}))`;
    g.addD(spatialstmt.getValue(), 'http://www.w3.org/ns/locn#geometry',
      value, 'http://www.opengis.net/ont/geosparql#wktLiteral');
  });
};

const temporal = (g, s, l, record) => {
  const extents = select(`${recordBase}gmd:extent//gmd:EX_TemporalExtent/gmd:extent/gml:TimePeriod`, record);
  extents.forEach((extent) => {
    const start = select('string(gml:beginPosition)', extent);
    const end = select('string(gml:endPosition)', extent);
    if (start || end) {
      const temporalstmt = g.add(s, 'dcterms:temporal');
      g.add(temporalstmt.getValue(), 'rdf:type', 'http://purl.org/dc/terms/PeriodOfTime');
      if (start) {
        addDate(start, g, temporalstmt.getValue(), config.dcat2 ? 'dcat:startDate' : 'http://schema.org/startDate');
      }
      if (end) {
        addDate(end, g, temporalstmt.getValue(), config.dcat2 ? 'dcat:endDate' : 'http://schema.org/endDate');
      }
    }
  });
};


/*
 * Adds a datasets and its corresponding
 * dataservices to an index
 *
 * @param {Object} dataset
 *
 */
const indexDataservice = (dataservice, s) => {
  if (config.csw.getServicedDatasets) {
    const datasets = config.csw.getServicedDatasets(dataservice, select);

    datasets.forEach((dataset) => {
      let services = dataset2service[dataset];
      if (!services) {
        services = [];
        dataset2service[dataset] = services;
      }
      services.push(dataservice);
    });
    if (datasets.length === 0) {
      logger.info('Service has no connection to a dataset');
    } else {
      service2datasets[s] = datasets;
    }
  }
};

const clear = () => {
  dataset2service = {};
  service2datasets = {};
};

/*
 * Map records into graph. Records are datasets and dataservices, filtered
 * during the fetch transform
 *
 * @param { Object, Object, Object}
 * @returns {}
 *
 */
const map = (args, record, checkDuplicates) => {
  const base = args.base ? args.base : 'http://example.com/';
  const id = select('string(gmd:fileIdentifier/*)', record);
  const l = extractLang(record, args) || config.defaultLanguage || 'en';
  const uri = select(
    `string(${recordBase}gmd:citation/*/gmd:identifier/*/gmd:code/gco:CharacterString)`,
    record,
  );
  const s = createSubject(base, uri, id);
  const g = new Graph();
  g.addL(s, 'dcterms:identifier', id);
  const extraGraphs = {};

  if (record.__service) {
    if (!(config.handleDataServices)) {
      return null;
    }
    indexDataservice(record, s);
    try {
      basicDataService(g, s, l, record);
      links(g, s, record);
      g.add(s, 'dcterms:type', 'http://www.wikidata.org/entity/Q749568');

      contactpoint({
        g, s, record, checkDuplicates, extraGraphs,
      });
      publisher({
        g, s, record, checkDuplicates, extraGraphs,
      });
      return {
        mainURI: s,
        mainGraph: g,
        extraGraphs,
        service2datasets,
      };
    } catch (err) {
      logger.error(err.stack);
    }
  } else {
    // Record is not a service, probably a dataset
    try {
      basic(g, s, l, record);
      date({ g, s, record });
      theme({
        g, s, l, record,
      });
      lineage(g, s, l, record);
      resourceLanguage(g, s, l, record);
      maintainance(g, s, l, record);
      spatial(g, s, l, record);
      temporal(g, s, l, record);
      const distCount = distribution({
        g,
        s,
        l,
        id,
        record,
        extraGraphs,
        dataset2service,
        base,
      });
      if (config.csw.excludeDatasetsWithoutDistributions && distCount === 0) {
        return null;
      }
      contactpoint({
        g, s, record, checkDuplicates, extraGraphs,
      });
      publisher({
        g, s, record, checkDuplicates, extraGraphs,
      });
      if (config.csw.addForDataset) {
        config.csw.addForDataset(g, s, record, select);
      }
      // gmd:supplementalInformation?
      // gmd:portrayalCatalogueInfo?
      // gmd:status - nope
      return {
        mainURI: s,
        mainGraph: g,
        extraGraphs,
        dataset2service: dataset2service,
      };
    } catch (err) {
      logger.error(err.stack);
    }
  }
};

module.exports = {
  clear,
  map,
};
