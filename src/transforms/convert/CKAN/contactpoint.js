const { Graph } = require('@entryscape/rdfjson');
const md5 = require('md5');
const JSON2RDFUtil = require('../../../utils/JSON2RDFUtil');
const config = require('../../../config');

const fr = (field, type, list) => ({
  field: typeof field === 'string' ? [field] : field,
  type,
  list,
});

const contactMapping = {
  'vcard:fn': { field: ['extra:contact_name'], type: 'literal', lang: false },
  'vcard:hasEmail': { field: ['extra:contact_email'], type: 'uri', toURI:
    JSON2RDFUtil.toMailURI },
  'foaf:homepage': fr('extra:publisher_url', 'uri'),
  'dcterms:type': fr('extra:publisher_type'),
};

const addContactPoint = (base, author, email) => {
  const graph = new Graph();
  const uri = base + md5((author + email).replace(' ', '').toLowerCase());
  if (author !== '') {
    graph.addL(uri, 'vcard:fn', author);
  }
  if (email !== '') {
    graph.add(uri, 'vcard:hasEmail', JSON2RDFUtil.toMailURI(null, email));
  }
  return {graph, uri};
}

module.exports = (params) => {
  const { checkDuplicates, extraGraphs, dsGraph, dsURI, language, record, extras, base, ckan } = params;

  const addContactPointToDataset = (graph, uri) => {
    if (!graph.isEmpty()) {
      dsGraph.add(dsURI, 'dcat:contactPoint', uri);
      if (!checkDuplicates[uri]) {
        graph.add(uri, 'rdf:type', ckan.defaultContactType || 'vcard:Individual'); // Adding this first here to avoid
        extraGraphs[uri] = graph;
        checkDuplicates[uri] = true;
      }
    }
  };

  if (ckan.extractContacts) {
    ckan.extractContacts(params).forEach(contact => {
      const { graph, uri } = addContactPoint(base, contact.name, contact.email);
      addContactPointToDataset(graph, uri);
    });
  } else if (extras.contact_uri) {
    const cURI = extras.contact_uri;
    const graph = new Graph();
    JSON2RDFUtil.mapToRDF({
      graph: graph,
      subject: cURI,
      language,
      record,
      extras,
      base,
      mapping: Object.assign({}, contactMapping, ckan.contactMapping),
    });
    addContactPointToDataset(graph, uri);
  } else {
    const { graph, uri } = addContactPoint(base,record.author || '', record.author_email || '');
    addContactPointToDataset(graph, uri);
  }
};