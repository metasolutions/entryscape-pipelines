const JSON2RDFUtil = require('../../../utils/JSON2RDFUtil');

const fr = (field, type, list) => ({
  field: typeof field === 'string' ? [field] : field,
  type,
  list,
});

const datasetMapping = {
  'dcterms:title': fr('title'),
  'dcterms:description': fr('notes'),
  'dcat:keyword': { field: ['tags'], objectAttribute: 'display_name' },
  'dcat:theme': fr('extra:theme', 'uri', true),
  'dcterms:identifier': { field: ['extra:guid', 'extra:identifier', 'id'], singleValued: true },
  'adms:identifier': fr('extra:alternate_identifier'),
  'adms:versionNotes': fr('extra:version_notes'),
  'dcterms:language': fr('extra:language', 'uri', true),
  'dcat:landingPage': fr('url', 'uri'),
  'dcterms:accrualPeriodicity': fr('extra:frequency', 'uri'),
  'dcterms:conformsTo': fr('extra:conforms_to', 'uri', true),
  'dcterms:accessRights': fr('extra:access_rights', 'uri'),
  'foaf:page': fr('extra:documentation', 'uri', true),
  'dcterms:spatial': fr('extra:spatial_uri', 'uri'),
  'dcterms:issued': { dtype: 'xsd:date', field: ['extra:issued', 'metadata_created'] },
  'dcterms:modified': { dtype: 'xsd:date', field: ['extra:modified', 'metadata_modified'] },
  'owl:versionInfo': { type: 'literal', lang: false, field: ['version', 'extra:dcat_version'] },
  'dcterms:provenance': {
    type: 'blank',
    field: ['extra:provenance'],
    property: 'rdfs:value',
    blankType: 'dcterms:ProvenanceStatement',
  },
  //    'dcterms:type': ['extra:dcat_type'],
};

module.exports = (params) => {
  const { checkDuplicates, extraGraphs, dsGraph, dsURI, language, record, extras, base, ckan } = params;

  dsGraph.add(dsURI, 'rdf:type', 'dcat:Dataset');
  JSON2RDFUtil.mapToRDF({
    graph: dsGraph,
    subject: dsURI,
    language,
    record,
    extras,
    base,
    mapping: Object.assign({}, datasetMapping, ckan.datasetMapping),
  });
}