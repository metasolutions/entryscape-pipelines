const { Graph } = require('@entryscape/rdfjson');
const JSON2RDFUtil = require('../../../utils/JSON2RDFUtil');

const fr = (field, type, list) => ({
  field: typeof field === 'string' ? [field] : field,
  type,
  list,
});

const publisherMapping = {
  'foaf:name': { field: ['title', 'extra:publisher_name'], type: 'literal', lang: false },
  'foaf:mbox': { field: ['extra:publisher_email'], type: 'uri', toURI:
    JSON2RDFUtil.toMailURI },
  'foaf:homepage': fr('extra:publisher_url', 'uri'),
  'dcterms:type': fr('extra:publisher_type'),
};

module.exports = (params) => {
  const { checkDuplicates, extraGraphs, dsGraph, dsURI, language, record, extras, base, ckan } = params;

  let pURI;
  const pGraph = new Graph();
  if (ckan.extractPublisher) {
    const { uri, name, homepage, organization } = ckan.extractPublisher(params);
    pURI = uri;
    pGraph.add(pURI, 'rdf:type', organization ? 'foaf:Organization' : 'foaf:Agent');
    pGraph.addL(pURI, 'foaf:name', name);
    pGraph.add(pURI, 'foaf:homepage', homepage);
  } else {
    if (extras.publisher_uri) {
      pURI = extras.publisher_uri;
      pGraph.add(pURI, 'rdf:type', 'foaf:Agent');
    } else {
      pURI = `${base}/publisher/${record.organization.id}`;
      pGraph.add(pURI, 'rdf:type', 'foaf:Organization');
    }
    JSON2RDFUtil.mapToRDF({
      graph: pGraph,
      subject: pURI,
      language,
      record: record.organization,
      extras,
      base,
      mapping: Object.assign({}, publisherMapping, ckan.publisherMapping),
    });
  }

  dsGraph.add(dsURI, 'dcterms:publisher', pURI);
  if (!checkDuplicates[pURI]) {
    extraGraphs[pURI] = pGraph;
    checkDuplicates[pURI] = true;
  }
};
