const JSON2RDFUtil = require('../../../utils/JSON2RDFUtil');
const { Graph } = require('@entryscape/rdfjson');
const { getMediaType } = require('../formatUtil');
const { namespaces } = require('@entryscape/entrystore-js');
const toURN = require('../GML/toURN');

namespaces.add('spdx', 'http://spdx.org/rdf/terms#');

const addHash = (rsGraph, rsURI, res) => {
  if (res.hash && res.hash_alghorithm) {
    const cBlank = rsGraph.add(rsURI, 'spdx:checksum').getValue();
    rsGraph.add(cBlank, 'rdf:type', 'spdx:Checksum');
    rsGraph.addL(cBlank, 'spdx:checksumValue', res.hash);
    let ha = res.hash_algorithm;
    if (JSON2RDFUtil.notURI(ha)) {
      ha = JSON2RDFUtil.toURI('http://spdx.org/rdf/terms#checksumAlgorithm_', ha);
    }
    rsGraph.addL(cBlank, 'spdx:algorithm', ha);
  }
};

const fr = (field, type, list) => ({
  field: typeof field === 'string' ? [field] : field,
  type,
  list,
});

const distributionMapping = {
  'dcterms:title': fr('name'),
  'dcat:accessURL': fr('url', 'uri'),
  'dcat:downloadURL': fr('download_url', 'uri'),
  'dcterms:description': fr('description'),
  'dcat:mediaType': fr('mimetype'),
  //'dcterms:format': fr('format'),
  'dcterms:license': fr('license', 'uri'),
  'adms:status': fr('status', 'uri'),
  //'dcat:byteSize': { dtype: 'xsd:decimal', field: ['size'] },
  'dcterms:issued': { dtype: 'xsd:date', field: ['issued'] },
  'dcterms:modified': { dtype: 'xsd:date', field: ['modified'] },
  'foaf:page': fr('documentation', 'uri', true),
  'dcterms:language': { field: ['language'], type: 'uri', list: true, toURI: JSON2RDFUtil.toLangURI },
  'dcterms:conformsTo': fr('conforms_to', 'uri', true),
  // 'dcterms:rights': fr('rights', 'uri'),
};

module.exports = (params) => {
  const { checkDuplicates, extraGraphs, dsGraph, dsURI, language, record, extras, base, ckan } = params;

  // Distributions (CKAN resources)
  (record.resources || []).forEach((res) => {
    let rsURI;
    if (res.uri || res.id) {
      const rsId = res.id;
      rsURI = res.uri || `${base}/resource/${res.id}`;
    } else {
      rsURI = toURN(`${dsURI}_${res.url}_${res.format}`);
    }
    const rsGraph = new Graph();
    extraGraphs[rsURI] = rsGraph;
    rsGraph.add(rsURI, 'rdf:type', 'dcat:Distribution');
    dsGraph.add(dsURI, 'dcat:distribution', rsURI);
    JSON2RDFUtil.mapToRDF({
      graph: rsGraph,
      subject: rsURI,
      language,
      parent: record,
      record: res,
      extras: {},
      base,
      mapping: Object.assign({}, distributionMapping, ckan.distributionMapping),
    });
    const mediatype = getMediaType(ckan, res.download_url || res.url, res.format);
    if (mediatype) {
      rsGraph.addL(rsURI, 'dcterms:format', mediatype);
    }
    addHash(rsGraph, rsURI, res);
  });
};
