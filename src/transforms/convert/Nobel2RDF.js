const { Graph, namespaces } = require('@entryscape/rdfjson');
const fs = require('fs');

namespaces.add('nobel', 'http://data.nobelprize.org/terms/');
namespaces.add('dbpo', 'http://dbpedia.org/ontology/');
namespaces.add('dbp', 'http://dbpedia.org/property/');
namespaces.add('schema', 'http://schema.org/');

const cat2path = {
  'Physics': 'Physics',
  'Chemistry': 'Chemistry',
  'Physiology or Medicine': 'Physiology_or_Medicine',
  'Literature': 'Literature',
  'Economic Sciences': 'Economic_Sciences',
  'Peace': 'Peace'
};


const category2order = {
  "Physics": "1",
  "Chemistry": "2",
  "Physiology_or_Medicine": "3",
  "Literature": "4",
  "Peace": "5",
  "Economic_Sciences": "6"
};

let incompletePrizes = {};
let incompletePrizeShares = {};
let uri2pictures = {};

const wdfix = (uri) => {
  if (uri.startsWith('https://www.wikidata.org/wiki/')) {
    return uri.replace('https://www.wikidata.org/wiki/', 'http://www.wikidata.org/entity/');
  }
}

const extractEntity = (entity, entityNow, checkDuplicates, extraGraphs, extractorFn, entityCls) => {
  if (!entity) {
    return;
  }
  const entityURI = extractorFn(entity.en);
  if (!checkDuplicates[entityURI]) {
    // Copy over sameas from cityNow if missing in city and they have the same label
    if (!entity.sameas && entityNow && entityNow.sameas) {
      const entityNowURI = extractorFn(entityNow.en);
      if (entityNowURI === entityURI) {
        entity.sameas = entityNow.sameas;
      }
    }
    const entityGraph = new Graph();
    entityGraph.add(entityURI, 'rdf:type', entityCls);
    entityGraph.addL(entityURI, 'rdfs:label', entity.en, 'en');
    entity.se && entityGraph.addL(entityURI, 'rdfs:label', entity.se, 'sv');
    entity.no && entityGraph.addL(entityURI, 'rdfs:label', entity.no, 'no');
    let sameas = entity.sameas || [];
    sameas = Array.isArray(sameas) ? sameas : [sameas];
    sameas.forEach(sa => entityGraph.add(entityURI, 'owl:sameAs', wdfix(sa)));
    extraGraphs[entityURI] = entityGraph;
    checkDuplicates[entityURI] = true;
  }
  return entityURI;
};

const countryBase = 'http://data.nobelprize.org/resource/country/';
const extractCountryURI = (label) => `${countryBase}${encodeURIComponent(label.trim().replace(/\s/g, '_'))}`;
const extractCountry = (country, countryNow, checkDuplicates, extraGraphs) =>
  extractEntity(country, countryNow, checkDuplicates, extraGraphs, extractCountryURI, 'dbpo:Country');

const cityBase = 'http://data.nobelprize.org/resource/city/';
const extractCityURI = (label) => `${cityBase}${encodeURIComponent(label.trim().replace(/\s/g, '_'))}`;
const extractCity = (city, cityNow, checkDuplicates, extraGraphs) =>
  extractEntity(city, cityNow, checkDuplicates, extraGraphs, extractCityURI, 'dbpo:City');

const addPlace = (graph, uri, place, cityProp, countryProp, checkDuplicates, extraGraphs) => {
  const cityURI = extractCity(place.city, place.cityNow, checkDuplicates, extraGraphs);
  graph.add(uri, cityProp, cityURI);
  const cityNowURI = extractCity(place.cityNow, undefined, checkDuplicates, extraGraphs);
  if (cityURI !== cityNowURI) {
    graph.add(uri, cityProp, cityURI);
  }
  const countryURI = extractCountry(place.country, place.countryNow, checkDuplicates, extraGraphs);
  graph.add(uri, countryProp || cityProp, countryURI);
  const countryNowURI = extractCountry(place.countryNow, undefined, checkDuplicates, extraGraphs);
  if (countryURI !== countryNowURI) {
    graph.add(uri, countryProp || cityProp, countryNowURI);
  }
};

const universityBase = 'http://data.nobelprize.org/resource/university/';
const extractUniversityURI = (label) => `${universityBase}${encodeURIComponent(label.trim().replace(/\s/g, '_'))}`;
const extractAffiliation = (affiliation, checkDuplicates, extraGraphs) => {
  const universityURI = extractUniversityURI(affiliation.nameNow.en);
  if (!checkDuplicates[universityURI]) {
    const universityGraph = new Graph();
    universityGraph.add(universityURI, 'rdf:type', 'dbpo:University');
    universityGraph.addL(universityURI, 'rdfs:label', affiliation.name.en, 'en');
    affiliation.name.se && universityGraph.addL(universityURI, 'rdfs:label', affiliation.name.se, 'sv');
    affiliation.name.no && universityGraph.addL(universityURI, 'rdfs:label', affiliation.name.no, 'no');
    addPlace(universityGraph, universityURI, affiliation, 'dbpo:city', 'dbpo:country',
      checkDuplicates, extraGraphs);
    checkDuplicates[universityURI] = true;
    extraGraphs[universityURI] = universityGraph;
  }
  return universityURI;
};

const extractLaureate = (graph, uri, laureate, checkDuplicates, extraGraphs) => {
  graph.add(uri, 'rdf:type', 'nobel:Laureate');
  if (laureate.orgName) {
    graph.add(uri, 'rdf:type', 'foaf:Organization');
    const on = laureate.orgName;
    on.en && graph.addL(uri, 'foaf:name', on.en, 'en');
    on.se && graph.addL(uri, 'foaf:name', on.se, 'sv');
    on.no && graph.addL(uri, 'foaf:name', on.no, 'no');
    if (laureate.nativeName) {
      graph.addL(uri, 'rdfs:label', laureate.nativeName);
    }
    // Organizations are founded, they have not been birth
    if (laureate.founded) {
      graph.addD(uri, 'schema:foundingDate', laureate.founded.date, 'xsd:date');
      graph.addD(uri, 'dcterms:created', laureate.founded.date, 'xsd:date');
      addPlace(graph, uri, laureate.founded.place,'schema:foundingLocation', undefined,
        checkDuplicates, extraGraphs);
    }
  } else {
    graph.add(uri, 'rdf:type', 'foaf:Person');
    const known = laureate.knownName;
    const given = laureate.givenName;
    const family = laureate.familyName;
    const full = laureate.fullName;
    graph.addL(uri, 'foaf:name', known.en || known.se);
    graph.addL(uri, 'foaf:givenName', given.en || given.se);
    if (family && (family.en || family.se)) {
      graph.addL(uri, 'foaf:familyName', family.en || family.se);
    }
    graph.addL(uri, 'rdfs:label', full.en || full.se);
    graph.addL(uri, 'foaf:gender', laureate.gender);

    // Just in case, everyone should really have been born :-)
    if (laureate.birth) {
      graph.addD(uri, 'dbp:dateOfBirth', laureate.birth.date, 'xsd:date');
      graph.addD(uri, 'foaf:birthday', laureate.birth.date, 'xsd:date');
      addPlace(graph, uri, laureate.birth.place,'dbpo:birthPlace', undefined,
        checkDuplicates, extraGraphs);
    }

    // But not everyone have died yet...
    if (laureate.death) {
      graph.addD(uri, 'dbp:dateOfDeath', laureate.death.date, 'xsd:date');
      addPlace(graph, uri, laureate.death.place,'dbpo:deathPlace', undefined,
        checkDuplicates, extraGraphs);
    }
  }
  (laureate.sameas || []).forEach((sa) => graph.add(uri, 'owl:sameAs', wdfix(sa)));
  const laureateId = laureate.id;
  laureate.nobelPrizes.forEach((prize) => {
    const year = prize.awardYear;
    const categoryNameEn = prize.category.en;
    const category = cat2path[categoryNameEn];
    const prizeURI = `http://data.nobelprize.org/resource/nobelprize/${category}/${year}`;
    const awardURI = `http://data.nobelprize.org/resource/laureateaward/${category}/${year}/${laureateId}`;
    graph.add(uri, 'nobel:nobelPrize', prizeURI);
    graph.add(uri, 'nobel:laureateAward', awardURI);
  });
  checkDuplicates[uri] = true;
};

const prizeShareAsRatio = (val) => {
  switch (val) {
    case '1/2':
      return 1/2;
    case '1/3':
      return 1/3;
    case '1/4':
      return 1/4;
    default:
      return 1;
  }
};

const prizeShareAsXSDInteger = (val) => {
  switch (val) {
    case '1/2':
      return '2';
    case '1/3':
      return '3';
    case '1/4':
      return '4';
    default:
      return '1';
  }
}

const extractPrizeAndAward = (laureate, laureateURI, laureateGraph, prize, checkDuplicates, extraGraphs) => {
  const laureateId = laureate.id;
  const categoryNameEn = prize.category.en;
  const year = prize.awardYear;
  const category = cat2path[categoryNameEn];
  const prizeURI = `http://data.nobelprize.org/resource/nobelprize/${category}/${year}`;
  const awardURI = `http://data.nobelprize.org/resource/laureateaward/${category}/${year}/${laureateId}`;

  // Fix prize unless it has already been committed in full
  if (!checkDuplicates[prizeURI]) {
    const incompletePrizeGraph = incompletePrizes[prizeURI];
    let prizeGraph = incompletePrizeGraph || new Graph();
    let prizePortion = prizeShareAsRatio(prize.portion);
    const incompletePrizePortion = (incompletePrizeShares[prizeURI] || 0);
    if (!incompletePrizeGraph) {
      prizeGraph.add(prizeURI, 'rdf:type', 'nobel:NobelPrize');
      prizeGraph.add(prizeURI, 'rdf:type', 'dbpo:Award');
      prizeGraph.addD(prizeURI, 'nobel:year', year, 'xsd:integer');
      const cfn = prize.categoryFullName;
      cfn.en && prizeGraph.addL(prizeURI, 'rdfs:label', `${cfn.en} ${year}`, 'en');
      cfn.se && prizeGraph.addL(prizeURI, 'rdfs:label', `${cfn.se} ${year}`, 'sv');
      cfn.no && prizeGraph.addL(prizeURI, 'rdfs:label', `${cfn.no} ${year}`, 'no');
      prizeGraph.add(prizeURI, 'nobel:category', `http://data.nobelprize.org/terms/${category}`);
      prizeGraph.addD(prizeURI, 'nobel:categoryOrder', category2order[category], 'xsd:integer');
    }

    // Add connection to laureate and laureate award (both on first time and when appending to incomplete prize)
    prizeGraph.add(prizeURI, 'dcterms:hasPart', awardURI);
    prizeGraph.add(prizeURI, 'nobel:laureate', laureateURI);

    // Check if there is a top motivation
    if (prize.topMotivation) {
      const tm = prize.topMotivation;
      tm.en && prizeGraph.addL(prizeURI, 'nobel:motivation', tm.en, 'en');
      tm.se && prizeGraph.addL(prizeURI, 'nobel:motivation', tm.se, 'sv');
      tm.no && prizeGraph.addL(prizeURI, 'nobel:motivation', tm.no, 'no');
    }

    // Prize is done
    if ((incompletePrizePortion + prizePortion) === 1) {
      // Cleanup in incomplete index if there where any
      if (incompletePrizeGraph) {
        delete incompletePrizes[prizeURI];
        delete incompletePrizeShares[prizeURI];
      }

      checkDuplicates[prizeURI] = true;
      extraGraphs[prizeURI] = prizeGraph;
    } else {
      // Prize is not done yet (total portions is not yet up to 1)
      incompletePrizes[prizeURI] = prizeGraph;
      incompletePrizeShares[prizeURI] = incompletePrizePortion + prizePortion;
    }
  }
  // Fix award
  if (!checkDuplicates[awardURI]) {
    const awardGraph = new Graph();
    awardGraph.add(awardURI, 'rdf:type', 'nobel:LaureateAward');
    awardGraph.add(awardURI, 'rdf:type', 'dbpo:Award');
    const cfn = prize.categoryFullName;
    const name = laureate.knownName || laureate.orgName;
    cfn.en && awardGraph.addL(awardURI, 'rdfs:label', `${cfn.en} ${year}, ${name.en || name.se}`, 'en');
    cfn.no && awardGraph.addL(awardURI, 'rdfs:label', `${cfn.no} ${year}, ${name.se || name.en}`, 'no');
    cfn.se && awardGraph.addL(awardURI, 'rdfs:label', `${cfn.se} ${year}, ${name.se || name.en}`, 'sv');
    awardGraph.add(awardURI, 'nobel:category', `http://data.nobelprize.org/resource/category/${category}`);
    awardGraph.add(awardURI, 'nobel:laureate', laureateURI);
/*    prize.fields.forEach((f) => {
      f.en && awardGraph.addL(awardURI, 'nobel:field', f.en, 'en');
      f.se && awardGraph.addL(awardURI, 'nobel:field', f.se, 'sv');
      f.no && awardGraph.addL(awardURI, 'nobel:field', f.no, 'no');
    });*/
    const pm = prize.motivation;
    pm.en && awardGraph.addL(awardURI, 'nobel:motivation', pm.en, 'en');
    pm.se && awardGraph.addL(awardURI, 'nobel:motivation', pm.se, 'sv');
    pm.no && awardGraph.addL(awardURI, 'nobel:motivation', pm.no, 'no');
    awardGraph.addD(awardURI, 'nobel:share', prizeShareAsXSDInteger(prize.portion), 'xsd:integer');
    prize.sortOrder && awardGraph.addD(awardURI, 'nobel:sortOrder', `${prize.sortOrder}`, 'xsd:integer');
    awardGraph.addD(awardURI, 'nobel:year', year, 'xsd:integer');
    awardGraph.add(awardURI, 'dcterms:isPartOf', prizeURI);

    (prize.affiliations || []).forEach(aff => {
      const affiliationURI = extractAffiliation(aff, checkDuplicates, extraGraphs);
      laureateGraph.add(laureateURI, 'dbpo:affiliation', affiliationURI);
      awardGraph.add(awardURI, 'nobel:university', affiliationURI);
    });


    extraGraphs[awardURI] = awardGraph;
    checkDuplicates[awardURI] = true;
  }
};

module.exports = {
  clear() {
    incompletePrizes = {};
    incompletePrizeShares = {};
    fs.writeFileSync('laureate-images.js', `window.__entryscape_laureateImages = ${JSON.stringify(uri2pictures, null, '  ')};`);
    uri2pictures = {};
  },
  map(args, laureate, checkDuplicates) {
    const laureateId = laureate.id;
    const laureateURI = `http://data.nobelprize.org/resource/laureate/${laureateId}`;
    const laureateGraph = new Graph();
    const extraGraphs = {};
    // Just in case laureate is already treated, like Marie Curie that got the Nobel prize twice.
    if (checkDuplicates[laureateURI]) {
      return null;
    }

    extractLaureate(laureateGraph, laureateURI, laureate, checkDuplicates, extraGraphs);
    laureate.nobelPrizes.forEach((prize) => {
      extractPrizeAndAward(laureate, laureateURI, laureateGraph, prize, checkDuplicates, extraGraphs);
      uri2pictures[laureateURI] = prize.portraits;
    });

    return {
      mainURI: laureateURI,
      mainGraph: laureateGraph,
      extraGraphs,
    };
  }
};