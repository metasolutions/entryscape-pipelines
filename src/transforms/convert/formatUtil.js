const ext2mt = {
  htm: 'text/html',
  html: 'text/html',
  php: 'text/html',
  xls: 'application/vnd.ms-excel',
  xlsx: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
  ods: 'application/vnd.oasis.opendocument.spreadsheet',
  pdf: 'application/pdf',
  csv: 'text/csv',
  txt: 'text/plain',
  zip: 'application/zip',
  xml: 'application/xml',
  json: 'application/json',
  rdf: 'application/rdf+xml',
  ttl: 'text/turtle'
};

const extension = (url) => {
  const urll = url.toLowerCase();
  if ((urll.match(/\//g) || []).length < 3) {
    return;
  }
  const lastp = urll.lastIndexOf('.');
  const lasts = urll.lastIndexOf('/');
  if (lastp > lasts) {
    const ext = urll.substr(lastp+1);
    if (ext.length < 5 ) {
      return ext;
    }
  }
};

const getMediaType = (config, url, format, protocol) => {
  if (format) {
    if (config.format2mimetype && config.format2mimetype[format.toLowerCase()]) {
      return config.format2mimetype[format.toLowerCase()];
    } else if (/^\w+\/[-.\w]+(?:\+[-.\w]+)?(\s*;\s*\w+=\w+)?$/.exec(format) != null) {
      return format;
    }
  }

  if (protocol) {
    if (config.protocol2mimetype && config.protocol2mimetype[protocol.toLowerCase()]) {
      return config.protocol2mimetype[protocol.toLowerCase()];
    }
  }

  const urll = url.toLowerCase();
  if (urll.indexOf('service=wms') !== -1) {
    return 'application/vnd.ogc.wms_xml';
  } else if (urll.indexOf('service=wfs') !== -1 ) {
    return 'application/vnd.ogc.wfs_xml';
  }

  const ext = extension(url);
  if (ext && ext2mt[ext]) {
    return ext2mt[ext];
  } else if (!ext) {
    // Report as html if there is no explicit extension
    return 'text/html';
  }
};

const isDownloadable = (config, format, protocol, mediatype) => {
  if (mediatype === 'text/html' || mediatype === 'text/xhtml') {
    return false;
  }

  if (config.treatAsService &&
    ((mediatype && config.treatAsService.indexOf(mediatype))
      || (format && config.treatAsService.indexOf(format.toLowerCase()))
      || (protocol && config.treatAsService.indexOf(protocol.toLowerCase()))
    )) {
    return false;
  }

  return true;
};


module.exports = {
  getMediaType,
  isDownloadable
};