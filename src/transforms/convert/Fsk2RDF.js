const { Graph } = require('@entryscape/rdfjson');

const detectYears = (report) => {
  const yearDimension = report.filter.dimension.find(dim => dim.key === 'ar');
  if (yearDimension) {
    try {
      const years = yearDimension.values.map(v => parseInt(v.key));
      return years.sort((a,b) => a-b);
    } catch (e) {
    }
  }
};

const hasMonthVariable = report => !!report.filter.dimension.find(dim => dim.key === 'manad');

const extractKeywords = (ao, fo) => {
  const keywords = [];
  keywords.push(ao.knapp.replace('<br>', '').trim().toLowerCase());

  fo.rubrik.split(',').forEach(sent => {
    sent.split('och').forEach((word) => {
      keywords.push(word.trim().toLowerCase());
    });
  });
  return keywords;
}

const baseURI = 'https://www.forsakringskassan.se/fk_apps/MEKAREST/public/v1/';
let metadata;
module.exports = {
  init(args, passOnObj) {
    metadata = passOnObj;
  },
  map(args, report, checkDuplicates) {
    const key = report.key;
    const area = report.area;
    const forman = report.forman;
    const mo = metadata[`MEKA.report.${key}`];
    const ao = metadata[`MEKA.area.${area}`];
    const fo = metadata[`MEKA.forman.${forman}`];
    const g = new Graph();
    const extraGraphs = {};
    const s = `${baseURI}${key}/meta.json`;
    if (checkDuplicates[s]) {
      return null;
    }
    checkDuplicates[s] = true;
    g.add(s, 'rdf:type', 'dcat:Dataset');
    g.addL(s, 'dcterms:identifier', key);
    g.addL(s, 'dcterms:title', mo.name, 'sv');
    g.addL(s, 'dcterms:description', mo.ingress, 'sv');
    g.add(s, 'dcat:landingPage', `https://www.forsakringskassan.se/statistik/statistikdatabas/#!/${area}/${key}`);
    g.add(s, 'dcterms:language', 'http://publications.europa.eu/resource/authority/language/SWE');

    g.add(s, 'dcterms:publisher', 'http://dataportal.se/organisation/SE2021005521');
    // Statistikservice
    g.add(s, 'dcat:contactPoint', 'https://catalog.forsakringskassan.se/store/9/resource/46');

    // Keywords from area and forman
    extractKeywords(ao, fo).forEach(kw => {
      g.addL(s, 'dcat:keyword', kw, 'sv');
    });

    // Dokumentation?

    // Befolkning och samhälle
    g.add(s, 'dcat:theme', 'http://publications.europa.eu/resource/authority/data-theme/SOCI');
    // Ekonomi och finans
    g.add(s, 'dcat:theme', 'http://publications.europa.eu/resource/authority/data-theme/ECON');
    // Regeringen och den offentliga sektorn
    g.add(s, 'dcat:theme', 'http://publications.europa.eu/resource/authority/data-theme/GOVE');

    // Hälsa
    if (area !== 'bf' && area !=='asueee') {
      g.add(s, 'dcat:theme', 'http://publications.europa.eu/resource/authority/data-theme/HEAL');
    }

    g.add(s, 'dcterms:accessRights', 'http://publications.europa.eu/resource/authority/access-right/PUBLIC');

    // Set frequency and timespan from the "ar" dimension if it exists
    const years = detectYears(report);
    if (years) {
      if (hasMonthVariable(report)) {
        g.add(s, 'dcterms:accrualPeriodicity', 'http://publications.europa.eu/resource/authority/frequency/MONTHLY');
      } else if (years.length === 1 || (years.length > 1 && years[1] - years[0] === 1)) {
        g.add(s, 'dcterms:accrualPeriodicity', 'http://publications.europa.eu/resource/authority/frequency/ANNUAL');
      }
      const period = g.add(s, 'dcterms:temporal');
      const ps = period.getValue();
      g.add(ps, 'rdf:type', 'dcterms:PeriodOfTime');
      g.addD(ps, 'schema:startDate', `${years[0]}`, 'xsd:gYear');
      const currentYear = new Date().getFullYear()
      if (years.length <= 1 || currentYear-years[years.length-1] > 1) {
        g.addD(ps, 'schema:endDate', `${years[years.length - 1]}`, 'xsd:gYear');
      }
    }

    report.filter.uppdelning.forEach(uppd => {

      const f = (distURI, format) => {
        g.add(s, 'dcat:distribution', distURI);
        const distGraph = new Graph();
        distGraph.add(distURI, 'rdf:type', 'dcat:Distribution');
        distGraph.add(distURI, 'dcat:downloadURL', distURI);
        distGraph.add(distURI, 'dcat:accessURL', distURI);
        distGraph.addL(distURI, 'dcterms:title', `Uppdelning: ${uppd.label}`, 'sv');
        distGraph.addL(distURI, 'dcterms:format', format);
        distGraph.add(distURI, 'http://www.w3.org/ns/adms#status', 'http://purl.org/adms/status/Completed');

        // Or use CC0?
        distGraph.add(distURI, 'dcterms:license', 'http://creativecommons.org/licenses/by/4.0/');

        distGraph.add(distURI, 'http://data.europa.eu/r5r/availability',
          'http://data.europa.eu/r5r/availability/stable');

        extraGraphs[distURI] = distGraph;
      }

      f(`${baseURI}${key}/${uppd.table}.json`, 'application/json');
      f(`${baseURI}${key}/${uppd.table}.xlsx`, 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    });

    return {
      mainURI: s,
      mainGraph: g,
      extraGraphs,
    };
  }
};