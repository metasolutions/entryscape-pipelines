const { Graph, namespaces } = require('@entryscape/rdfjson');
const { ns = namespaces } = require('@entryscape/entrystore-js');
const md5 = require('md5');
const md5ToUuid = require('md5-to-uuid');
const logger = require('../../utils/logger/logger');
const { px } = require('../../config');
const config = px;
const toUuidUrn = (str) => `urn:uuid:${md5ToUuid(md5(str))}`;

const topicCategory2theme = {
  MI: 'ENVI',
  UF: 'EDUC',
  EN: 'ENER',
  AM: 'SOCI',
  BE: 'SOCI',
  LE: 'SOCI',
  KU: 'SOCI',
  SO: 'SOCI',
  AA: 'SOCI',
  ME: 'GOVE',
  OE: 'GOVE',
  NR: 'ECON',
  HA: 'ECON',
  HE: 'ECON',
  PR: 'ECON',
  FM: 'ECON',
  NV: 'ECON',
  TK: 'TRAN',
  BO: 'REGI',
  HS: 'HEAL',
  JO: 'AGRI',
  "Bevölkerungsstand":"SOCI",
  "Bevölkerungsbewegung": "SOCI",
  "Bevölkerungsstruktur": "SOCI",
  "Bevölkerungsszenarien": "SOCI",
  "Geburten, Todesfälle": "SOCI",
  "Ehen, Partnerschaften": "SOCI",
  "Lebenserwartung": ["SOCI","HEAL"],
  "Migration, Binnenwanderung": ["SOCI","INTR"],
  "Einbürgerungen": ["SOCI","INTR"],
  "Asylwesen": ["SOCI","INTR"],
  "Haushalte, Familien":"SOCI",
  "Beschäftigung":"ECON",
  "Erwerbstätigkeit":"ECON",
  "Erwerbsstruktur, Berufe": "ECON",
  "Arbeitslosigkeit":"ECON",
  "Löhne":"ECON",
  "Volkswirtschaftliche Gesamtrechnungen":"ECON",
  "Aussenhandel":"ECON",
  "Bankzinsen":"ECON",
  "Unternehmen, Arbeitsplätze":"ECON",
  "Landwirtschaft": ["AGRI","ECON"],
  "Tourismus":"ECON",
  "Banken":"ECON",
  "Forschung und Entwicklung":"TECH",
  "Bautätigkeit": "ECON",
  "Gebäude, Wohnungen": "SOCI",
  "Mietpreise": "ECON",
  "Vermögens- und Einkommensverteilung": "SOCI",
  "Sozialleistungen": "SOCI",
  "Gesundheitsausgaben": ["HEAL","ECON"],
  "Krankenversicherer": ["HEAL","ECON"],
  "Unfallversicherer": ["HEAL","ECON"],
  "Todesursachen":"HEAL",
  "Schulen":"EDUC",
  "Hochschulen":"EDUC",
  "Duale Ausbildung":"EDUC",
  "Weiterbildung":"EDUC",
  "Bildungsstand":"EDUC",
  "Öffentliche Finanzen":"GOVE",
  "Steuern, Abgaben":"GOVE",
  "Wahlen, Abstimmungen":"GOVE",
  "Raum, Flächennutzung":"ENVI",
  "Landschaft, Wald, Boden, Biodiversität":"ENVI",
  "Luft, Klima":"ENVI",
  "Wasser":"ENVI",
  "Abfall":"ENVI",
  "Umweltabgaben":"ENVI",
  "Energie":"ENER",
  "Energiepreise":"ENER",
  "Fahrzeug-Erstzulassungen":"TRAN",
  "Fahrzeugbestand":"TRAN",
  "Personenverkehr":"TRAN",
};


const px2json = (data) => {
  const r = {};
  (data || '').split(';').forEach(row => {

    let key = row.substr(0, row.indexOf('='));
    let value = row.substr(row.indexOf('=')+1);
    if (!key || !value) {
      return;
    }
    key = key.trim();
    if (value.indexOf('"') === 0) {
      value = value.replace(/"/g, '');
      value = value.replace(/\n/g, '');
      if (value.indexOf("##") > -1) {
        value = value.split("##");
        value = value.map(v => v.replace(/#/g, '\n'));
      } else {
        value = value.replace(/#/g, '\n');
      }
    } else {
      if (!isNaN(parseInt(value))) {
        value = parseInt(value);
      }
    }
    if (key.indexOf('("') > -1) {
      const [one, two] = key.split('("');
      key = one;
      if (!r[key]) {
        r[one] = {};
      }
      r[one][two.substr(0, two.length-2 )] = value;
    } else {
      r[key] = value;
    }
  });
  return r;
};

const contactExtract = (str) => {
  if (!str || !str.split) {
    return;
  }
  const [name, tel, email] = str.split('\n');
  if (!name || !tel || !email) {
    return;
  }

  return {
    name,
    tel: tel.replace(/ /g, ''),
    email
  }
};

const freqNS = 'http://publications.europa.eu/resource/authority/frequency/';
const thisYear = new Date().getYear()+1900;

const extractPeriodicity = (record, data) => {
  const variable = record.sv.variables.find(v => v.time);
  let freq = 'OTHER';
  let start;
  let end;
  let intervall = 1;
  const varvals = variable.values;
  if (varvals && varvals.length > 2) {
    const first = varvals[0];
    const nextToLast = varvals[varvals.length - 2];
    const last = varvals[varvals.length - 1];
    if (!isNaN(parseInt(nextToLast)) && !isNaN(parseInt(last))) {
      intervall = parseInt(last) - parseInt(nextToLast);
    }
    if (parseInt(first)) {
      start = first;
    }
    if (parseInt(last)) {
      const endAsInt = parseInt(last);
      if (thisYear - endAsInt > 1) {
        end = last;
      }
    }
  }
  if (data.TIMEVAL && variable && data.TIMEVAL[variable.text]) {
    const timeval = data.TIMEVAL[variable.text].match(/^TLIST\((.?).*\),.*/);
    if (timeval && timeval.length > 1) {
      const code = timeval[1];
      switch (code) {
        case 'A': // Årligt
          if (intervall === 1) {
            freq = 'ANNUAL';
          } else if (intervall === 2) {
            freq = 'BIENNIAL';
          }
          break;
        case 'H': // Halvår
          freq = 'ANNUAL_2';
          break;
        case 'Q': // Kvartal
          freq = 'QUARTERLY';
          break;
        case 'M': // Månatligen
          freq = 'MONTHLY';
          break;
      }
    }
  }
  return { periodicity: `${freqNS}${freq}`, start, end };
};

const addDocument = (g, s, extraGraphs, checkDuplicates, license, url) => {
  const documentURI = url;
  g.add(s, 'dcterms:license', documentURI);
  if (!checkDuplicates[documentURI]) {
    const documentGraph = new Graph();
    documentGraph.add(documentURI, 'rdf:type', 'dcterms:LicenseDocument');
    if (license.primaryLang) {
      if (license.primaryLang) {
        documentGraph.addL(documentURI, 'dcterms:title', license.primaryLang.title, license.primaryLang.lang);
        documentGraph.addL(documentURI, 'dcterms:description', license.primaryLang.desc, license.primaryLang.lang);
      }
      if (license.secondaryLang) {
        documentGraph.addL(documentURI, 'dcterms:title', license.secondaryLang.title, license.secondaryLang.lang);
        documentGraph.addL(documentURI, 'dcterms:description', license.secondaryLang.desc, license.secondaryLang.lang);
      }
    }
    extraGraphs[documentURI] = documentGraph;
    checkDuplicates[documentURI] = true;
  }
};

const addHTMLDistribution = (s, g, extraGraphs, distURI, url, format, language) => {
  let lang;
  if (language === 'DEU') lang = 'de';
  else if (language === 'ENG') lang = 'en';
  const prefix = config.html_distribution.base + 'PXWEb/api/v1/' + lang + '/eTab//';
  const newPrefix = config.html_distribution.base + 'PXWEb/pxweb/' + lang + '/eTab/etab__';

  let newurl = url.replace(prefix, '');
  const suffixIndex = url.lastIndexOf('/');
  const suffix = url.substring(suffixIndex);
  newurl = newurl.replace(suffix, '');
  newurl = newurl.replaceAll('/', '__');
  const accessURL = newPrefix + newurl + suffix;

  const dsGraph = new Graph();
  extraGraphs[distURI] = dsGraph;
  g.add(s, 'dcat:distribution', distURI);
  dsGraph.add(distURI, 'rdf:type', 'dcat:Distribution');
  if (config.html_distribution.primaryLanguageTitle1 && language === config.distribution.primaryLanguage) {
    dsGraph.addL(distURI, 'dcterms:title', config.html_distribution.primaryLanguageTitle1, config.primaryLanguageLiteral);
    dsGraph.addL(distURI, 'dcterms:title', config.html_distribution.primaryLanguageTitle2, config.secondaryLanguageLiteral);
  } else if (config.html_distribution.secondaryLanguageTitle1 && language === config.distribution.secondaryLanguage) {
    dsGraph.addL(distURI, 'dcterms:title', config.html_distribution.secondaryLanguageTitle1, config.primaryLanguageLiteral);
    dsGraph.addL(distURI, 'dcterms:title', config.html_distribution.secondaryLanguageTitle2, config.secondaryLanguageLiteral);
  }
  if (config.distribution.license) {
    dsGraph.add(distURI, 'dcterms:license', config.distribution.license);
  } else {
    dsGraph.add(distURI, 'dcterms:license', 'http://creativecommons.org/publicdomain/zero/1.0/');
  }
  dsGraph.addL(distURI, 'dcterms:format', format);
  dsGraph.add(distURI, 'dcat:accessURL', accessURL);

  dsGraph.add(distURI, 'dcterms:language',
    `http://publications.europa.eu/resource/authority/language/${language}`);
};

module.exports = {
  map(args, record, checkDuplicates) {
    const base = args.base ? args.base : 'http://example.com/resource/';
    const parent = record.path[record.path.length - 1];
    const id = parent.sv.id;
    let secondaryLanguageId = id;
    if (config.idLangSpecific) secondaryLanguageId = secondaryLanguageId.replace(config.idLangSpecific.primary, config.idLangSpecific.secondary);
    
    
    let id_without_lang = id;
    if (id.includes('.px')) {
      id_without_lang = id.replace('.px', '');
    }

    const s = base + id_without_lang;
    const g = new Graph();
    const extraGraphs = {};
    try {
      g.addL(s, 'dcterms:identifier', id_without_lang);
      const data = px2json(record.data);

      g.add(s, 'rdf:type', 'dcat:Dataset');

      if (config.removeIdFromString)  {
        g.addL(s, 'dcterms:title', parent.sv.text.replace(id.slice(0, -3), ''), config.primaryLanguageLiteral);
      } else {
        g.addL(s, 'dcterms:title', parent.sv.text, config.primaryLanguageLiteral);
      }
      if (parent.en && config.removeIdFromString && config.idLangSpecific)  {
        g.addL(s, 'dcterms:title', parent.en.text.replace(secondaryLanguageId.slice(0, -3), ''), config.secondaryLanguageLiteral);
      } else if (parent.en) {
        g.addL(s, 'dcterms:title', parent.en.text, config.secondaryLanguageLiteral);
      }

      const source = data.SOURCE && config.primaryLanguageSource ? `${config.primaryLanguageSource}${data.SOURCE}` : '';
      const sourceEN = data.SOURCE && config.secondaryLanguageSource ? `${config.secondaryLanguageSource}${data.SOURCE}` : '';
      
      if (config.removeIdFromString)  {
        g.addL(s, 'dcterms:description', record.sv.title.replace(id.slice(0, -3), '')  + source, config.primaryLanguageLiteral);
      } else {
        g.addL(s, 'dcterms:description', record.sv.title  + source, config.primaryLanguageLiteral);
      }
      if (parent.en && config.removeIdFromString && config.idLangSpecific)  {
        g.addL(s, 'dcterms:description', record.en.title.replace(secondaryLanguageId.slice(0, -3), '') + sourceEN, config.secondaryLanguageLiteral);
      } else if (parent.en) {
        g.addL(s, 'dcterms:description', record.en.title + sourceEN, config.secondaryLanguageLiteral);
      }

      g.addL(s, 'dcat:keyword', record.path[1].sv.text, config.primaryLanguageLiteral);
      if (record.path[1].en) {
        g.addL(s, 'dcat:keyword', record.path[1].en.text, config.secondaryLanguageLiteral);
      }
      const categoryId = record.path[1].sv.id;
      if (categoryId && topicCategory2theme[categoryId]) {
        const theme = topicCategory2theme[categoryId];
        if (Array.isArray(theme)) {
          theme.map((t) => {
            g.add(s, 'dcat:theme', `http://publications.europa.eu/resource/authority/data-theme/${t}`);
          });
        } else if (theme) {
          g.add(s, 'dcat:theme', `http://publications.europa.eu/resource/authority/data-theme/${theme}`);
        }
      }
      if (config.setUpdatedToIssued)  {
        g.addD(s, 'dcterms:issued', parent.sv.updated, 'xsd:dateTime');
      } else {
        g.addD(s, 'dcterms:modified', parent.sv.updated, 'xsd:dateTime');
      }
      g.add(s, 'dcterms:accessRights', 'http://publications.europa.eu/resource/authority/access-right/PUBLIC');
      if (config.primaryLanguageURI) g.add(s, 'dcterms:language', config.primaryLanguageURI);
      if (config.secondaryLanguageURI) g.add(s, 'dcterms:language', config.secondaryLanguageURI);
    

      if (config.landingpagePrimaryLanguage) g.add(s, 'dcat:landingPage', config.landingpagePrimaryLanguage);
      if (config.landingpageSecondaryLanguage) g.add(s, 'dcat:landingPage', config.landingpageSecondaryLanguage);
      
      if (config.documentation) g.add(s, 'foaf:page', config.documentation);
      
      // const { periodicity, start, end } = extractPeriodicity(record, data);
      // g.add(s, 'dcterms:accrualPeriodicity', periodicity.trim());

      // if (start || end) {
      //   const tobj = g.add(s, 'dcterms:temporal').getValue();
      //   g.add(tobj, 'rdf:type', 'dcterms:PeriodOfTime');
      //   if (start) {
      //     g.addD(tobj, 'schema:startDate', start, 'xsd:date');
      //   }
      //   if (end) {
      //     g.addD(tobj, 'schema:endDate', end, 'xsd:date');
      //   }
      // }

      const addProv = (txt) => {
        const bn = g.add(s, 'http://purl.org/dc/terms/provenance').getValue();
        g.add(bn, 'rdf:type', 'http://purl.org/dc/terms/ProvenanceStatement');
        g.addL(bn, 'dcterms:description', txt, config.primaryLanguageLiteral);
      };
      if (data.NOTEX) {
        (Array.isArray(data.NOTEX) ? data.NOTEX : [data.NOTEX]).forEach(note => {
          if (typeof note === 'string') {
            addProv(note);
          } else if (typeof note === 'object') {
            Object.keys(note).forEach((key) => {
              addProv(`${key} - ${note[key]}`);
            })
          }
        });
      }
      if (data.CONTACT) {
        Object.values(data.CONTACT).forEach(value => {
          (Array.isArray(value) ? value : [value]).forEach(v => {
            const contact = contactExtract(v);
            if (contact) {
              const curi = `${base}/contactpoint/${md5(contact.name)}`;
              // Ignore a service that will be added explicitly below.
              if (config.contact.ignore && contact.name.toLowerCase().substr(config.contact.ignore) > -1) {
                return;
              }
              // Organization email detected if there is a . in the name before the @
              const orgEmail = contact.email.match(/^.+\..+@.*$/) === null;
              g.add(s, 'dcat:contactPoint', curi);
              if (!checkDuplicates[curi]) {
                const cg = new Graph();
                if (orgEmail) {
                  cg.add(curi, 'rdf:type', 'vcard:Organization');
                } else {
                  cg.add(curi, 'rdf:type', 'vcard:Individual');
                }
                cg.addL(curi, 'vcard:fn', contact.name);
                cg.add(curi, 'vcard:hasEmail', `mailto:${contact.email.trim()}`);
                const telStmt = cg.add(curi, 'vcard:hasTelephone');
                cg.add(telStmt.getValue(), 'vcard:hasValue', `tel:${contact.tel.trim()}`);
                extraGraphs[curi] = cg;
                checkDuplicates[curi] = true;
              }
            }
          });
        });
      }
      const contactURI = config.contact.URI;
      g.add(s, 'dcat:contactPoint', contactURI);
      if (!checkDuplicates[contactURI]) {
        const cg = new Graph();
        cg.add(contactURI, 'rdf:type', 'vcard:Organization');
        cg.addL(contactURI, 'vcard:fn', config.contact.name);
        cg.add(contactURI, 'vcard:hasEmail', `mailto:${config.contact.mail}`);
        const telStmt = cg.add(contactURI, 'vcard:hasTelephone');
        cg.add(telStmt.getValue(), 'vcard:hasValue', `tel:${config.contact.telephone}`);
        const addrStmt = cg.add(contactURI, 'vcard:hasAddress');
        const addrNode = addrStmt.getValue();
        cg.add(addrNode, 'rdf:type', 'vcard:Address');
        cg.addL(addrNode, 'vcard:country-name', config.contact.country);
        cg.addL(addrNode, 'vcard:locality', config.contact.locality);
        cg.addL(addrNode, 'vcard:postal-code', config.contact.postalcode);
        cg.addL(addrNode, 'vcard:street-address', config.contact.streetaddress);
        cg.add(addrNode, 'vcard:hasURL', config.contact.hasURL);
        extraGraphs[contactURI] = cg;
        checkDuplicates[contactURI] = true;
      }

      const publisherURI = config.publisher.URI;
      g.add(s, 'dcterms:publisher', publisherURI);
      if (!checkDuplicates[publisherURI]) {
        const pg = new Graph();
        pg.add(publisherURI, 'rdf:type', 'foaf:Organization');
        pg.addL(publisherURI, 'foaf:name', config.publisher.primaryLanguageName, config.primaryLanguageLiteral);
        pg.addL(publisherURI, 'foaf:name', config.publisher.secondaryLanguageName, config.secondaryLanguageLiteral);
        pg.add(publisherURI, 'dcterms:type', 'http://purl.org/adms/publishertype/NationalAuthority');
        pg.add(publisherURI, 'foaf:homepage', config.publisher.homepage);
        pg.add(publisherURI, 'foaf:mbox', `mailto:${config.publisher.mail}`);
        extraGraphs[publisherURI] = pg;
        checkDuplicates[publisherURI] = true;
      }

      const nonMainSource = data.SOURCE && config.mainSourceString && typeof data.SOURCE === 'string' && data.SOURCE.toLowerCase() !== config.mainSourceString;
      if (nonMainSource && config.addNonMainSources) {
        const creatorURI = toUuidUrn(data.SOURCE.toLowerCase());
        g.add(s, 'dcterms:creator', creatorURI);
        if (!checkDuplicates[creatorURI]) {
          const cg = new Graph();
          cg.add(creatorURI, 'rdf:type', 'foaf:Organization');
          cg.addL(creatorURI, 'foaf:name', data.SOURCE, config.primaryLanguageLiteral);
          cg.add(creatorURI, 'dcterms:type', 'http://purl.org/adms/publishertype/NationalAuthority');
          extraGraphs[creatorURI] = cg;
          checkDuplicates[creatorURI] = true;
        }
      }


      const ssdURI = config.dataservice.URI;
      if (!checkDuplicates[ssdURI]) {
        const dsg = new Graph();
        dsg.add(ssdURI, 'rdf:type', 'dcat:DataService');
        dsg.addL(ssdURI, 'dcterms:title', config.dataservice.primaryLanguageTitle, config.primaryLanguageLiteral);
        dsg.addL(ssdURI, 'dcterms:title', config.dataservice.secondaryLanguageTitle, config.secondaryLanguageLiteral);
        dsg.add(ssdURI, 'dcat:endpointURL', config.dataservice.primaryLanguageEndpoint);
        dsg.add(ssdURI, 'dcat:endpointURL', config.dataservice.secondaryLanguageEndpoint);
        dsg.add(ssdURI, 'dcterms:publisher', publisherURI);
        dsg.add(ssdURI, 'dcterms:type', config.dataservice.type1);
        dsg.add(ssdURI, 'dcterms:type', config.dataservice.type2);
        dsg.add(ssdURI, 'dcterms:accessRights', 'http://publications.europa.eu/resource/authority/access-right/PUBLIC');
        if (config.dataservice.license) {
          dsg.add(ssdURI, 'dcterms:license', config.dataservice.license);
        } else {
          dsg.add(ssdURI,  'dcterms:license', 'http://creativecommons.org/publicdomain/zero/1.0/');
        }
        if (config.dataservice.landingpage) dsg.add(ssdURI, 'dcat:landingPage', config.dataservice.landingpage);
        if (config.dataservice.documentation) dsg.add(ssdURI, 'foaf:page', config.dataservice.documentation);
        extraGraphs[ssdURI] = dsg;
        checkDuplicates[ssdURI] = true;
      }

      const addDist = (distURI, accessURL, format, language) => {
        const dsGraph = new Graph();
        extraGraphs[distURI] = dsGraph;
        g.add(s, 'dcat:distribution', distURI);
        dsGraph.add(distURI, 'rdf:type', 'dcat:Distribution');
        if (config.distribution.primaryLanguageTitle1 && language === config.distribution.primaryLanguage) {
          dsGraph.addL(distURI, 'dcterms:title', config.distribution.primaryLanguageTitle1, config.primaryLanguageLiteral);
          dsGraph.addL(distURI, 'dcterms:title', config.distribution.primaryLanguageTitle2, config.secondaryLanguageLiteral);
        } else if (config.distribution.secondaryLanguageTitle1 && language === config.distribution.secondaryLanguage) {
          dsGraph.addL(distURI, 'dcterms:title', config.distribution.secondaryLanguageTitle1, config.primaryLanguageLiteral);
          dsGraph.addL(distURI, 'dcterms:title', config.distribution.secondaryLanguageTitle2, config.secondaryLanguageLiteral);
        }
        if (config.distribution.license) {
          dsGraph.add(distURI, 'dcterms:license', config.distribution.license);
        } else {
          dsGraph.add(distURI, 'dcterms:license', 'http://creativecommons.org/publicdomain/zero/1.0/');
        }
        dsGraph.addL(distURI, 'dcterms:format', format);
        dsGraph.add(distURI, 'dcat:accessURL', accessURL);
        dsGraph.add(distURI, 'dcat:accessService', ssdURI);
        if (config.distribution.documentation) dsGraph.add(distURI, 'foaf:page', config.distribution.documentation);

        dsGraph.add(distURI, 'dcterms:language',
          `http://publications.europa.eu/resource/authority/language/${language}`);
      };

      addDist(`${s}_json`, record.url, 'application/json', config.distribution.primaryLanguage);
      if (config.html_distribution) addHTMLDistribution(s, g, extraGraphs, `${s}html`, record.url, 'text/html', config.distribution.primaryLanguage)

      if (record.url_en) {
        addDist(`${s}_en_json`, record.url_en, 'application/json', config.distribution.secondaryLanguage);
        if (config.html_distribution) addHTMLDistribution(s, g, extraGraphs, `${s}_en_html`, record.url_en, 'text/html', config.distribution.secondaryLanguage);
      }

    } catch (err) {
      logger.error(`Problem with table ${id}`);
      logger.error(err);
    }

    return {
      mainURI: s,
      mainGraph: g,
      extraGraphs,
    };
  },
};
