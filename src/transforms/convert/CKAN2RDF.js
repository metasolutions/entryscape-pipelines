const { Graph } = require('@entryscape/rdfjson');
const config = require('../../config');
const contactPoint = require('./CKAN/contactpoint');
const publisher = require('./CKAN/publisher');
const distribution = require('./CKAN/distribution');
const dataset = require('./CKAN/dataset');

config.ckan = config.ckan || {};
const ckanDefault = config.ckan.default || config.ckan;
let ckan = ckanDefault;

module.exports = {
  init(args, passOnObj) {
    if (args.profile && config.ckan[args.profile]) {
      ckan = config.ckan[args.profile];
    } else {
      ckan = ckanDefault;
    }
  },
  map(args, record, checkDuplicates) {
    const base = args.base ? args.base : 'http://example.com/';
    const language = args.language || config.defaultLanguage || 'en';
    const extras = Array.isArray(record.extras) ? {} : record.extras || {};
    if (Array.isArray(record.extras)) {
      record.extras.forEach((extra) => {
        extras[extra.key] = extra.value;
      });
    }
    const dsGraph = new Graph();
    const extraGraphs = {};

    const dsId = record.id;
    let dsURI;
    if (ckan.constructSubject) {
      dsURI = ckan.constructSubject(record);
    } else {
      dsURI = record.uri || extras.uri || ((extras.guid) && ckan.useGUID ? `${base}/dataset/${extras.guid}` : `${base}/dataset/${dsId}`);
    }

    const params = {dsGraph, dsURI, checkDuplicates, extraGraphs, record, extras, base, language, ckan};
    dataset(params);
    distribution(params);
    publisher(params);
    contactPoint(params)

    return {
      mainURI: dsURI,
      mainGraph: dsGraph,
      extraGraphs,
    };
  },
};
