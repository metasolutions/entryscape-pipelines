const config = require('../../../config');
const { Graph } = require('@entryscape/rdfjson');
const toURN = require('./toURN');
const { recordBase, addLFactory } = require('./utils')
const select = require('../../../utils/cswSelect');


let ns = 'gmd';
let namespaceKey = '';
if (config.csw) {
  if (config.csw.namespace) ns = config.csw.namespace;
  if (config.csw.namespaceKey) namespaceKey =  (config.csw.namespaceKey + '_');
}

const telephoneVcard = (g, s, record) => {
  // Telephone
  const tel = select('string(gmd:contactInfo//gmd:voice/*)', record);
  if (/^[0-9+()-.\s]+$/.test(tel)) {
    const telstmt = g.add(s, 'vcard:hasTelephone');
    g.add(telstmt.getValue(), 'vcard:hasValue', `tel:${tel.trim().replace(/\s/g, '-')}`);
  }
};

const addressVcard = (g, s, record) => {
  const addrs = select(`.//${ns}:${namespaceKey}CI_Address`, record);
  if (addrs.length > 0) {
    addrs.forEach((addr) => {
      const addrStmt = g.add(s, 'vcard:hasAddress');
      const cpaID = addrStmt.getValue();
      g.add(cpaID, 'rdf:type', 'vcard:Address');
      const addCPL = addLFactory(g, cpaID, null, addr);
      addCPL('vcard:country-name', 'gmd:country/*');
      addCPL('vcard:locality', 'gmd:city/*');
      addCPL('vcard:postal-code', 'gmd:postalCode/*');
      if (ns === 'gmd') {
        addCPL('vcard:street-address', 'gmd:deliveryPoint/*');
      } else {
        addCPL('vcard:street-address', `${ns}:streetName/*`);
      }
    });
  }
};

const emailVcard = (g, s, record) => {
  const mail = select('string(.//gmd:electronicMailAddress/*)', record);
  if (mail !== '') {
    g.add(s, 'vcard:hasEmail', `mailto:${mail}`);
  }
};

/**
 * Adds contactpoints as a vcard by identifying suitable pointOfContacts.
 * Ignores when:
 * 1. the role is one of the publisherRoles (config)
 * 2. the method contactpoint.ignore (config) exists and returns true (signature: name, orgname, role)
 * 3. the trimmed name is empty or only contains one of the single characters -, ? or .
 *
 * A contactpoint URI is created as a URN via a combination of name and orgname.
 * If the same URN is generated for two datasets the reference is expressed in both cases but only the first is created.
 *
 * The resulting vcard is typed as an organization if there is an orgname but no name.
 * An email, telephone and address is created if they can be found.
 *
 * @param params
 */
module.exports = (params) => {
  const { g, s, record, checkDuplicates, extraGraphs } = params;

  const cps = select(`${recordBase}gmd:pointOfContact/${ns}:${namespaceKey}CI_ResponsibleParty`, record);
  cps.forEach((cp) => {
    const name = select('string(gmd:individualName/*)', cp);
    const orgname = select('string(gmd:organisationName/*)', cp);
    const role = select('string(gmd:role/gmd:CI_RoleCode/@codeListValue)', cp);
    if (role && config.csw.publisherRoles && config.csw.publisherRoles.indexOf(role) > -1) {
      return;
    }
    if (config.csw.contactpoint && config.csw.contactpoint.ignore) {
      if (config.csw.contactpoint.ignore(name, orgname, role)) {
        return;
      }
    } else if (['', '-', '?', '.'].indexOf(name.toLowerCase().trim()) !== -1 &&
      ['', '-', '?', '.'].indexOf(orgname.toLowerCase().trim()) !== -1) {
      return;
    }
    const cpURI = toURN((name + orgname).replace(' ', '').toLowerCase());
    g.add(s, 'dcat:contactPoint', cpURI);
    if (!checkDuplicates[cpURI]) {
      const cpg = new Graph();
      extraGraphs[cpURI] = cpg;
      checkDuplicates[cpURI] = true;
      if (name && name !== "") {
        cpg.add(cpURI, 'rdf:type', 'vcard:Individual');
      } else {
        cpg.add(cpURI, 'rdf:type', 'vcard:Organization');
      }
      const fn = name && name !== "" ? name : (orgname && orgname !== "" ? orgname : undefined);
      if (fn) {
        if (config.csw.contactName && fn === config.csw.contactName.name) {
          cpg.addL(cpURI, 'vcard:fn', config.csw.contactName.primary.name, config.csw.contactName.primary.lang);
          cpg.addL(cpURI, 'vcard:fn', config.csw.contactName.secondary.name, config.csw.contactName.secondary.lang);
        } else  {
          cpg.addL(cpURI, 'vcard:fn', fn);
        }
      }
      if (orgname !== '') {
        cpg.addL(cpURI, 'vcard:organization-name', orgname);
      }
      // Telephone
      telephoneVcard(cpg, cpURI, cp);
      // Email
      emailVcard(cpg, cpURI, cp);
      // Address
      addressVcard(cpg, cpURI, cp);
    }
  });
};
