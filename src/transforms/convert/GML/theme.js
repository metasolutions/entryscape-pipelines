const config = require('../../../config');
const select = require('../../../utils/cswSelect');
const { recordBase } = require('./utils')
const inspireThemeBase = 'http://inspire.ec.europa.eu/theme';

/**
 * Extract inspire themes from the element gmd:descriptiveKeywords
 * @return {object} map from inspire theme uri to the label
 */
const getInspireThemes = (descriptiveKeywords) => {
  const themes = {};
  select('gmd:MD_Keywords/gmd:keyword/gmx:Anchor', descriptiveKeywords).forEach((anchor) => {
    const href = select('string(@xlink:href)', anchor);
    if (href && href.startsWith(inspireThemeBase)) {
      themes[href] = select('string(.)', anchor);
    }
  });
  return themes;
}

/**
 *  Checks if the descriptiveKeywords element contains inspire themes
 *
 * @param descriptiveKeywords
 * @return {boolean}
 */
const isInspireTheme = (descriptiveKeywords) => {
  const anchors = select('gmd:MD_Keywords/gmd:keyword/gmx:Anchor', descriptiveKeywords);
  if (anchors.length > 0) {
    const href = select('string(@xlink:href)', anchors[0]);
    return href && href.startsWith(inspireThemeBase);
  }
  return false;
};

/**
 * Adds data-themes from mapping given TopicCategory via mapping in csw.topicCategory2Theme or from
 * keyword if given in csw.keyword2theme.
 *
 * In addition it adds a inspire theme (also given as dcat:theme) if provided as keyword with inspire theme thesaurus.
 * If the setting csw.addInspireThemeAsKeyword is set to true, the label for the inspire theme is also added as a keyword.
 *
 * @param params
 */
module.exports = (params) => {
  const { g: graph, s: subject, l: language, record } = params;
  if (config.csw.topicCategory2theme) {
    select(`${recordBase}gmd:topicCategory`, record).forEach((topic) => {
      const themeArr = config.csw.topicCategory2theme[select('string(gmd:MD_TopicCategoryCode)', topic)];
      if (themeArr) {
        themeArr.forEach((t) => {
          graph.add(subject, 'dcat:theme', `http://publications.europa.eu/resource/authority/data-theme/${t}`);
        });
      }
    });
  }
  if (config.csw.keyword2theme) {
    select(`${recordBase}gmd:descriptiveKeywords//gmd:keyword`, record).forEach((keyword) => {
      const themeArr = config.csw.keyword2theme[select('string(gco:CharacterString)', keyword)];
      if (themeArr) {
        themeArr.forEach((t) => {
          graph.add(subject, 'dcat:theme', `http://publications.europa.eu/resource/authority/data-theme/${t}`);
        });
      }
    });
  }
  select(`${recordBase}gmd:descriptiveKeywords`, record).forEach(dk => {
    if (isInspireTheme(dk)) {
      const themeuri2label = getInspireThemes(dk);
      Object.keys(themeuri2label).forEach(uri => graph.add(subject, 'dcat:theme', uri));
      if (config.csw.addInspireThemeAsKeyword) {
        Object.values(themeuri2label).forEach(val => graph.addL(subject, 'dcat:keyword', val, language));
      }
    }
  });
};