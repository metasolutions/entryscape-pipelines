const m2eu = {
  continual: 'CONT',
  daily: 'DAILY',
  weekly: 'WEEKLY',
  fortnightly: 'BIWEEKLY',
  monthly: 'MONTHLY',
  quarterly: 'QUARTERLY',
  biannually: 'ANNUAL_2',
  annually: 'ANNUAL',
  asNeeded: 'OTHER', // No perfect match found
  irregular: 'IRREG',
  notPlanned: 'UNKNOWN', // No perfect match found
  unknown: 'UNKNOWN',
};

module.exports = {
  mapMaintainance(code) {
    const val = m2eu[code];
    if (val) {
      return `http://publications.europa.eu/resource/authority/frequency/${val}`;
    }
  },
};
