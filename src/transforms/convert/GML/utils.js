const select = require('../../../utils/cswSelect');

const addLFactory = function (g, s, l, r) {
  return function (property, path, isAttr) {
    const arr = select(path, r);
    if (arr.length !== 0) {
      arr.forEach((n) => {
        if (n.firstChild != null) {
          g.addL(s, property, isAttr ? n.value : n.firstChild.nodeValue, l);
        }
      });
    }
  };
};

const createSubject = (base, uri, id) => {
  const trimmedID = id.trim();
  if (uri && uri !== '') {
    const trimmedURI = uri.replace(/\s+/g, '');
    if (trimmedURI.indexOf('http') === 0) {
      return trimmedURI.endsWith(trimmedID) ? trimmedURI : trimmedURI + trimmedID;
    }
  }
  return base + trimmedID;
};

const addUFactory = function (g, s, r) {
  return function (property, path, baseURI, isAttr) {
    const arr = select(path, r);
    if (arr.length !== 0) {
      arr.forEach((n) => {
        if (n.firstChild != null) {
          g.add(s, property, baseURI + (isAttr ? n.value : n.firstChild.nodeValue));
        }
      });
    }
  };
};

const addDate = (timenr, g, s, p) => {
  if (!Object.is(timenr, NaN)) {
    const value = new Date(timenr).toISOString();
    if (typeof timenr === 'string' && timenr.match(/^\d\d\d\d$/)) {
      g.addD(s, p, timenr, 'xsd:gYear');
    } else if (value.endsWith('T00:00:00.000Z')) {
      g.addD(s, p, value.substr(0, 10), 'xsd:date');
    } else {
      g.addD(s, p, value, 'xsd:dateTime');
    }
  }
};

module.exports = {
  recordBase: 'gmd:identificationInfo[1]/*/',
  addLFactory,
  addUFactory,
  addDate,
  createSubject,
};
