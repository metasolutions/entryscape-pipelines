const { Graph } = require('@entryscape/rdfjson');
const config = require('../../../config');
const toURN = require('./toURN');
const { recordBase } = require('./utils');
const select = require('../../../utils/cswSelect');
const { publisherMap } = require('../utils');

/**
 * Uses the publisherMapping array to find a prospective publisher where:
 * name - gmd:individualName
 * org - gmd:organisationName
 * email - gmd:electronicMailAddress
 * @see publisherMap;
 */
const publisherMapGML = (params) => publisherMap(params, config.csw.publisherMapping);


/**
 * Adds a single publisher as a foaf:Agent by identifying suitable pointOfContacts.
 *
 * First, if there is a match against the publisherMapping array, that takes precedence.
 *
 * Second, if the publisherFromOrgName (config) is true:
 * A suitable publisher is found by looking for an gmd:organisationName in all of the contactpoints.
 * The first that is found is used to create the publisher. The gmd:organisationName is used and maybe the
 * gmd:electronicEmailAddress BUT, it is only used if no gmd:individualName is found as that indicates that the
 * email is not to the organization but to the individual.
 *
 * Third, a publisher is detected from the first matching contactpoint. Contactpoint match if it has a role
 * that is listed in publisherRoles (config). If no publisherRoles are provided all contactpoints will match.
 *
 * Unless a publisherMapping is provided a URI is created for the publisher as a URN based on the name
 * (individualName or organisationName, the latter has priority). If the same URI is generated for two datasets
 * the reference is expressed in both cases but only the first is created as a graph.
 *
 * The resulting vcard is typed as an foaf:Organization if there is an organisationName but no individualName
 * otherwise typed as foaf:Person. An email is created if it can be found.
 *
 * @param params
 * @see publisherMapGML
 */
module.exports = (params) => {
  const {
    g, s, record, checkDuplicates, extraGraphs 
  } = params;
  const namespace = config.csw.namespace ? config.csw.namespace : 'gmd';
  const namespaceKey = config.csw.namespaceKey ? (config.csw.namespaceKey + '_') : '';
  if (config.csw.publisherRoles || config.csw.publisherFromOrgName) {
    const cps = select(`${recordBase}gmd:pointOfContact/${namespace}:${namespaceKey}CI_ResponsibleParty`, record);
    const publisherFound = false;
    cps.find((cp) => {
      let name = select('string(gmd:individualName/*)', cp);
      const org = select('string(gmd:organisationName/*)', cp);
      const role = select('string(gmd:role/gmd:CI_RoleCode/@codeListValue)', cp);
      const email = select('string(.//gmd:electronicMailAddress/*)', cp);
      const homepage = select(`string(.//${namespace}:URLGroup/*)`, cp);
      const individualNameExists = name !== '';

      // If there is a publisherMapping and there is a match, use it.
      if (config.csw.publisherMapping) {
        const pURI = publisherMapGML({ name, org, email, role });
        if (pURI) {
          g.add(s, 'dcterms:publisher', pURI);
          return true;
        }
      }

      // If the publisher should be created from the organization name
      if (config.csw.publisherFromOrgName) {
        if (!org) {
          return false;
        }
        name = ''; // Do not use the name
      } else if (!role || (config.csw.publisherRoles && config.csw.publisherRoles.indexOf(role) === -1)) {
        return false;
      }

      // The __publisher is to make sure we do not have a conflict with contactpoint URLs
      const fn = name && name !== "" ? name : (org && org !== "" ? org : undefined);
      const pURI = toURN((fn + "__publisher").replace(' ', '').toLowerCase());
      g.add(s, 'dcterms:publisher', pURI);
      if (!checkDuplicates[pURI]) {
        const pg = new Graph();
        extraGraphs[pURI] = pg;
        checkDuplicates[pURI] = true;
        if (name && name !== "") {
          pg.add(pURI, 'rdf:type', 'foaf:Person');
        } else {
          pg.add(pURI, 'rdf:type', 'foaf:Organization');
        }
        if (fn) {
          if (config.csw.publisherName && fn === config.csw.publisherName.name) {
            pg.addL(pURI, 'foaf:name', config.csw.publisherName.primary.name, config.csw.publisherName.primary.lang);
            pg.addL(pURI, 'foaf:name', config.csw.publisherName.secondary.name, config.csw.publisherName.secondary.lang);
          } else  {
            pg.addL(pURI, 'foaf:name', fn);
          }
        } 
        // Email
        if (email !== '') {
          if ((publisherFound && !individualNameExists) || !publisherFound) {
            pg.add(pURI, 'foaf:mbox', `mailto:${email}`);
          }
        }
        if (homepage !== '') {
          pg.add(pURI, 'foaf:homepage', homepage);
        }
      }
      return true;
    });
  }
};