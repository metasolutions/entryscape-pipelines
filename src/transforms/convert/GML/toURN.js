const md5 = require('md5');
const md5ToUuid = require('md5-to-uuid');

module.exports = (str) => `urn:uuid:${md5ToUuid(md5(str))}`;
