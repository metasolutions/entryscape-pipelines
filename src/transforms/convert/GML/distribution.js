const { Graph } = require('@entryscape/rdfjson');
const config = require('../../../config');
const { isDownloadable, getMediaType } = require('../formatUtil');
const toURN = require('./toURN');
const select = require('../../../utils/cswSelect');
const { recordBase, createSubject } = require('./utils');
const logger = require('../../../utils/logger/logger');


/*
 * Create graph for distribution and give reference to identifier
 *
 *
*/
const addDistribution = (params, identifier, url, download, increaseCounter) => {
  const { g: graph, record, extraGraphs } = params;
  const distributionURI = toURN(`${identifier}_${url}`);
  const distributionGraph = new Graph();
  increaseCounter();
  graph.add(identifier, 'dcat:distribution', distributionURI);
  distributionGraph.add(distributionURI, 'rdf:type', 'dcat:Distribution');
  if (url) {
    distributionGraph.add(distributionURI, 'dcat:accessURL', url);
  }

  if (download) {
    distributionGraph.add(distributionURI, 'dcat:downloadURL', url);
  }
  if (config.csw.addForDistribution) {
    config.csw.addForDistribution.forEach((filter) => {
      const results = filter.xpathRoot ? select(filter.xpathRoot, record)
        : select(filter.xpath, record);
      filter.onMatch(results, distributionGraph, distributionURI);
    });
  }
  extraGraphs[distributionURI] = distributionGraph;
  return { distributionGraph, distributionURI };
};

const addFormat = (distributionURI, distributionGraph, format) => {
    
  if (config.csw.mimetype2pubeu) {
    if (format in config.csw.mimetype2pubeu) {
      distributionGraph.add(distributionURI, 'dcterms:format', config.csw.mimetype2pubeu[format]);
    } else{
      logger.warn(`${format}, found no mapping to pubeu`);
    }
  } else {
    distributionGraph.addL(distributionURI, 'dcterms:format', format);
  }
};

const handleOnlineOptions = (serviceRecord, serviceSubject, params, increaseCounter, originalAccessURLs) => {
  let gmdFormat = select('string(gmd:distributionInfo/gmd:MD_Distribution/gmd:distributor/gmd:MD_Distributor/gmd:distributorFormat/gmd:MD_Format/gmd:name/gco:CharacterString)', serviceRecord);
  gmdFormat = gmdFormat.toLowerCase();

  if (gmdFormat === 'wms') {
    const gmdOnLineString = 'gmd:distributionInfo/gmd:MD_Distribution/gmd:distributor/gmd:MD_Distributor/gmd:distributorTransferOptions/gmd:MD_DigitalTransferOptions/gmd:onLine';
    const onlineOptions = select(gmdOnLineString, serviceRecord) || [];

    onlineOptions.forEach((onlineOption) => {
      const onlineOptionURL = select('string(gmd:CI_OnlineResource/gmd:linkage/gmd:URL)', onlineOption);
      const onlineOptionProtocol = select('string(gmd:CI_OnlineResource/gmd:protocol/gco:CharacterString)', onlineOption);
      const onlineOptionName = select('string(gmd:CI_OnlineResource/gmd:name/gco:CharacterString)', onlineOption);
      const onlineOptionDescription = select('string(gmd:CI_OnlineResource/gmd:description/gco:CharacterString)', onlineOption);

      if (onlineOptionURL.includes('WMS') || onlineOptionURL.includes('wms') || onlineOptionProtocol.includes('wms') || onlineOptionProtocol.includes('WMS')) {
        const download = false;
        const linkedSchemas = 'http://www.opengis.net/def/serviceType/ogc/wms';
        const format = 'application/vnd.ogc.wms_xml';

        let distributionURL = onlineOptionURL;
        originalAccessURLs.push(distributionURL);
        if (config.csw.rewriteAccessURL) {
          distributionURL = config.csw.rewriteAccessURL(distributionURL);
        }
        const { distributionGraph, distributionURI } = addDistribution(
          params, params.s, distributionURL, download, increaseCounter,
        );

        distributionGraph.addL(distributionURI, 'dcterms:title', onlineOptionName, 'sv');
        distributionGraph.addL(distributionURI, 'dcat:endpointDescription', onlineOptionDescription, 'sv');
        if (onlineOptionURL && onlineOptionURL !== '') {
          distributionGraph.add(distributionURI, 'dcat:endpointURL', onlineOptionURL);
        }
        distributionGraph.add(distributionURI, 'dcat:accessService', serviceSubject);

        if (linkedSchemas) {
          distributionGraph.add(distributionURI, 'dcterms:conformsTo', linkedSchemas);
        }
        if (format) addFormat(distributionURI, distributionGraph, format);

        if (config.csw.addDistributionTitleFromService) {
          config.csw.addDistributionTitleFromService(
            serviceRecord, select, distributionGraph, distributionURI, distributionURL, format,
          );
        }
      }
    });
  }
};

const handleOperationMetadatas = (serviceRecord, serviceSubject, operationMetadatas, params, increaseCounter, originalAccessURLs) => {
  const serviceType = select('string(gmd:identificationInfo/srv:SV_ServiceIdentification/srv:serviceType/gco:LocalName)', serviceRecord);
  const serviceTypeVersion = select('string(gmd:identificationInfo/srv:SV_ServiceIdentification/srv:serviceTypeVersion/gco:CharacterString)', serviceRecord);
  const title = select('string(gmd:identificationInfo/srv:SV_ServiceIdentification/gmd:citation/gmd:CI_Citation/gmd:title/gco:CharacterString)', serviceRecord);
  let distributionURL;

  // eslint-disable-next-line no-restricted-syntax
  for (const operationMetadata of operationMetadatas) {
    const distributionURLCandidate = select('string(srv:connectPoint/gmd:CI_OnlineResource/gmd:linkage/gmd:URL)', operationMetadata);
    if (distributionURLCandidate) {
      distributionURL = distributionURLCandidate;
      const operationsName = select('string(srv:operationName/gco:CharacterString)', operationMetadata);
      if (config.csw.includeServiceOperation
        && !config.csw.includeServiceOperation(
          distributionURL, serviceTypeVersion, operationsName,
        )) {
        return;
      }
      originalAccessURLs.push(distributionURL);
      if (config.csw.rewriteAccessURL) {
        distributionURL = config.csw.rewriteAccessURL(distributionURL);
      }
      break;
    }
  }


  let download = true;
  let linkedSchemas;
  let format;
  const typeOfService = (`${serviceType} ${serviceTypeVersion}`).toLowerCase();
  if (typeOfService.includes('wms')) {
    download = false;
    linkedSchemas = 'http://www.opengis.net/def/serviceType/ogc/wms';
    format = 'application/vnd.ogc.wms_xml';
  } else if (typeOfService.includes('wmts')) {
    download = false;
    linkedSchemas = 'http://www.opengis.net/def/serviceType/ogc/wmts';
    format = 'application/vnd.ogc.wmts_xml';
  } else if (typeOfService.includes('wfs')) {
    download = false;
    linkedSchemas = 'http://www.opengis.net/def/serviceType/ogc/wfs';
    format = 'application/vnd.ogc.wfs_xml';
  } else if (typeOfService.includes('wcs')) {
    download = false;
    format = 'application/vnd.ogc.wcs_xml';
    linkedSchemas = 'http://www.opengis.net/def/serviceType/ogc/wcs';
  } else if (typeOfService.includes('atom')) {
    format = 'application/atom+xml';
    linkedSchemas = 'https://www.w3.org/2005/Atom';
  }

  const { distributionGraph, distributionURI } = addDistribution(
    params, params.s, distributionURL, download, increaseCounter,
  );

  const containsOperations = select('gmd:identificationInfo/srv:SV_ServiceIdentification/srv:containsOperations', serviceRecord) || [];
  containsOperations.forEach((operator) => {
    const operatorURL = select('string(srv:SV_OperationMetadata/srv:connectPoint/gmd:CI_OnlineResource/gmd:linkage/gmd:URL)', operator);
    if (operatorURL && operatorURL !== '') {
      distributionGraph.add(distributionURI, 'dcat:endpointURL', operatorURL);
    }
  });

  if (linkedSchemas) {
    distributionGraph.add(distributionURI, 'dcterms:conformsTo', linkedSchemas);
  }
  if (format) addFormat(distributionURI, distributionGraph, format);

  if (config.csw.addDistributionTitleFromService) {
    config.csw.addDistributionTitleFromService(
      serviceRecord, select, distributionGraph, distributionURI, distributionURL, format,
    );
  }
  distributionGraph.addL(distributionURI, 'dcterms:title', title, params.l);
  distributionGraph.add(distributionURI, 'dcat:downloadURL', distributionURL);
  distributionGraph.add(distributionURI, 'dcat:accessService', serviceSubject);
};

const addDistributionsFromServices = (params, originalAccessURLs, increaseCounter) => {
  const {
    s: subject, id, dataset2service, base,
  } = params;

  (dataset2service[subject] || dataset2service[id] || []).forEach((serviceRecord) => {
    const serviceID = select('string(gmd:fileIdentifier/gco:CharacterString)', serviceRecord);
    const serviceURI = select(
      `string(${recordBase}gmd:citation/*/gmd:identifier/*/gmd:code/gco:CharacterString)`,
      serviceRecord,
    );
    const serviceSubject = createSubject(base, serviceURI, serviceID);
    const operationMetadatas = select('gmd:identificationInfo/srv:SV_ServiceIdentification/srv:containsOperations/srv:SV_OperationMetadata', serviceRecord) || [];
    if (operationMetadatas) {
      handleOperationMetadatas(serviceRecord, serviceSubject, operationMetadatas, params, increaseCounter, originalAccessURLs);
    } else {
      handleOnlineOptions(serviceRecord, serviceSubject, operationMetadatas, params, increaseCounter, originalAccessURLs);
    }
  });
};

const getLangSpecificTexts = (graph, subject, property, record, path) =>  {
  select(`.//${path}//gmd:LocalisedCharacterString`, record).map((locString) => {
    const lang = select('string(.//@locale)', locString);
    const sValue = select('string(.)', locString);
    const lngs = graph.find(subject, property);
    const languages = lngs.map((s) => s.getLanguage());
    if (lang === '#EN') {
      if (!(languages.includes('en'))) {
        graph.addL(subject, property, sValue, 'en');
      }
    }
  });
}

const addDistributionsFromDataset = (params, distributions, originalAccessURLs, increaseCounter) => {
  const { s: subject, l: language } = params;

  distributions.forEach((distribution) => {
    const onLineNodes = select('.//gmd:onLine', distribution);
    /* if (transOpts.length === 0) {
      transOpts = select('.//gmd:distributorTransferOptions', distribution);
    } */
    onLineNodes.forEach((option) => {
      let distributionURL = select('string(.//gmd:URL)', option);
      if (distributionURL === '' || distributionURL.indexOf(' ') !== -1) {
        return;
      }
      if (config.csw.detectAccessURLDuplicates
        && config.csw.detectAccessURLDuplicates(distributionURL, originalAccessURLs)) {
        return;
      }
      const colonIndex = distributionURL.indexOf(':');
      if (config.csw.rewriteAccessURL) {
        distributionURL = config.csw.rewriteAccessURL(distributionURL);
      }
      if (distributionURL.indexOf('http') === -1 && (colonIndex === -1 || colonIndex > 7)) { // Heuristics
        distributionURL = `http://${distributionURL}`;
      }
      const functionCode = select('string(.//gmd:CI_OnLineFunctionCode/@codeListValue)', option) || '';
      if (config.csw.restrictToTransferOptions && functionCode
        && config.csw.restrictToTransferOptions.indexOf(functionCode) === -1) {
        return;
      }
      if (config.csw.ignoreTransferOptions
        && config.csw.ignoreTransferOptions.indexOf(functionCode) > -1) {
        return;
      }

      // Detect formats.
      let distributionFormatName = ''; // select('string(gmd:distributionFormat/gmd:MD_Format/gmd:name/*)', distribution);
      const distributionProtocolName = select('string(.//gmd:protocol/*)', option);

      let mediatype = getMediaType(
        config.csw, distributionURL, distributionFormatName, distributionProtocolName,
      );

      const namecheck = select('string(.//gmd:name/*)', option);

      if (distributionURL === 'https://service.geo.llv.li/download//getfile.php?theme=Schutzwald_LI_V1')  {
        console.log()
      }
      if (config.csw.distributionHtmlAsLandingpage && mediatype === 'text/html') {
        if (namecheck && namecheck.toLowerCase().includes('zip')) {
          mediatype = 'application/zip';
        } else if (config.csw.customDistribution && distributionURL.includes(config.csw.customDistribution))  {
          // do nothing
        } else {
          const { g: graph } = params;
          graph.add(subject, 'dcat:landingPage', distributionURL);
          return;
        }
      }

      const download = config.csw.isDownloadable
        ? config.csw.isDownloadable(
          distributionURL, distributionFormatName, distributionProtocolName,
          mediatype, functionCode,
        )
        : isDownloadable(config.csw, distributionFormatName,
          distributionProtocolName, mediatype);
      
      const { distributionGraph, distributionURI } = addDistribution(
        params, subject, distributionURL, download, increaseCounter,
      );

      if (mediatype) addFormat(distributionURI, distributionGraph, mediatype);

      // Add a description if it exists
      const description = select('string(.//gmd:description/*)', option);
      if (description) {
        if (typeof config.csw.extractDistributionDescription === 'function') {
          const extractedDescription = config.csw.extractDistributionDescription(description);
          distributionGraph.addL(distributionURI, 'dcterms:description', extractedDescription, language);
        } else {
          distributionGraph.addL(distributionURI, 'dcterms:description', description, language);
        }
        if (config.csw.distribution.addNonDefaultLanguages)  {
          getLangSpecificTexts(distributionGraph, distributionURI, 'dcterms:description', option, 'gmd:description');
        }
      }

      // Add a title if a suitable exists.
      if (config.useFormatAsDistributionTitle) {
        const distributionFormatVersion = select('string(gmd:distributionFormat/gmd:version/*)', distribution);
        if (distributionFormatName !== '') {
          if (distributionFormatVersion !== '') {
            distributionFormatName += ` ${distributionFormatVersion}`;
          }
          distributionGraph.addL(distributionURI, 'dcterms:title', distributionFormatName, language);
        }
      } else {
        const distributionName = select('string(.//gmd:name/*)', option);
        if (distributionName) {
          distributionGraph.addL(distributionURI, 'dcterms:title', distributionName, language);
        } else if (config.csw.addDistributionTitle) {
          config.csw.addDistributionTitle(option, select, distributionGraph, distributionURI);
        }
        if (config.csw.distribution && config.csw.distribution.addNonDefaultLanguages)  {
          getLangSpecificTexts(distributionGraph, distributionURI, 'dcterms:title', option, 'gmd:name');
        }
      }
    });
  });
};

module.exports = (params) => {
  const {
    record,
  } = params;
  const distributions = select('gmd:distributionInfo/gmd:MD_Distribution', record);
  let distributionCounter = 0;
  const originalAccessURLs = [];
  const increaseCounter = () => {
    distributionCounter += 1;
  };

  // Add distributions from referring services of the dataset
  if (config.handleDataServices) {
    addDistributionsFromServices(params, originalAccessURLs, increaseCounter);
  }

  // Add distributions from dataset itself
  if (distributions.length > 0) {
    addDistributionsFromDataset(params, distributions, originalAccessURLs, increaseCounter);
  }
  return distributionCounter;
};
