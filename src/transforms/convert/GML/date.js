const config = require('../../../config');
const select = require('../../../utils/cswSelect');
const {addDate, recordBase} = require('./utils');

const setDatesFor = (params) => {
  const { dateTypesCodes, dateProperty, record, graph, subject, latest } = params;
  const dateNrs = [];
  const dates = select(`${recordBase}gmd:citation/*/gmd:date`, record);
  dates.forEach((d) => {
    const date = select('string(*/*/gco:Date)', d);
    const dateType = select('string(*/gmd:dateType/gmd:CI_DateTypeCode/@codeListValue)', d).toLowerCase();
    if (dateTypesCodes.indexOf(dateType) >= 0) {
      try {
        dateNrs.push(Date.parse(date));
      } catch (e) {
      }
    }
  });
  if (dateNrs.length > 0) {
    dateNrs.sort();
    if (latest) {
      addDate(dateNrs[dateNrs.length-1], graph, subject, dateProperty);
    } else {
      addDate(dateNrs[0], graph, subject, dateProperty);
    }
  }
};

module.exports = (params) => {
  // Add issued and modified dates
  const { g: graph, s: subject, record } = params;

  if (config.csw.useDateStampForModified) {
    const dateStamp = select('string(gmd:dateStamp/gco:Date)', record);
    if (dateStamp) {
      const d = Date.parse(dateStamp);
      addDate(d, graph, subject, 'dcterms:modified');
    }
  } else if (!config.csw.skipModifiedDate) {
    setDatesFor({
      dateTypesCodes: ['publication', 'creation', 'revision'],
      dateProperty: 'dcterms:modified',
      latest: true,
      graph,
      subject,
      record,
    });
  }
  setDatesFor({
    dateTypesCodes: ['publication', 'creation'],
    dateProperty: 'dcterms:issued',
    latest: false,
    graph,
    subject,
    record,
  });
};
