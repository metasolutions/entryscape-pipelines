const { Graph }  = require('@entryscape/rdfjson');
const { namespaces } = require('@entryscape/entrystore-js');
const md5 = require('md5');
const md5ToUuid = require('md5-to-uuid');
const config = require('../../config');
const JSON2RDFUtil = require('../../utils/JSON2RDFUtil');
const logger = require('../../utils/logger/logger');

const toUuidUrn = (str) => `urn:uuid:${md5ToUuid(md5(str))}`;

namespaces.add('spdx', 'http://spdx.org/rdf/terms#');
namespaces.add('dcatde', 'http://dcat-ap.de/def/dcatde/');
namespaces.add('locn', 'http://www.w3.org/ns/locn#');
namespaces.add('gsp', 'http://www.opengis.net/ont/geosparql#');

const toIso = (str) => {
  const dotArr = str.split('.');
  if (dotArr.length > 1) {
    return `${dotArr[2]}-${dotArr[1]}-${dotArr[0]}`;
  }
  return str;
};
const fr = (field, type, list) => ({
  field: typeof field === 'string' ? [field] : field,
  type,
  list,
});
const datasetMapping = {
  'dcterms:title': { field: ['title'], language: 'de' },
//  'dcterms:description': { field: ['description'], language: 'de' },
  'dcat:keyword': { field: ['keywords'], language: 'de' },
  'dcat:theme': fr(['theme_1', 'theme_2'], 'uri', true),
  'dcterms:identifier': fr('adms_identifier', 'literal'),
  'dcat:landingPage': fr('landingPage', 'uri'),
  'dcatde:politicalGeocodingURI': fr('politicalGeocodingURI', 'uri'),
  'dcatde:politicalGeocodingLevelURI': fr('politicalGeocodingLevelURI', 'uri'),
  'dcterms:issued': { dtype: 'xsd:date', field: ['issued'] },
  'dcterms:accrualPeriodicity': { type: 'uri', field: ['accrualPeriodicity'] },
};

const distributionMapping = {
  'dcterms:title': { field: ['title'], language: 'de'},
  'dcterms:description': { field: ['description'], language: 'de' },
  'dcat:accessURL': fr('accessURL', 'uri'),
  'dcterms:license': fr('license', 'uri'),
  'dcterms:format': { field: ['mediaType', 'format'], singleValued: true },
  'dcterms:language': fr('language', 'uri'),
  'dcatde:licenseAttributionByText': { field: ['licenseAttributionByText'], language: 'de' },
};

const publisherMapping = {
  'foaf:name': { field: ['publisher'], type: 'literal', language: false },
  'foaf:mbox': { field: ['publisher_mbox'],
    type: 'uri',
    toURI: JSON2RDFUtil.toMailURI },
};

const maintainerMapping = {
  'foaf:name': { field: ['maintainer'], type: 'literal', language: false },
  'foaf:mbox': { field: ['maintainer_mbox'],
    type: 'uri',
    toURI: JSON2RDFUtil.toMailURI },
};

const contactMapping = {
  'vcard:fn': { field: ['contactPoint_1_name'], type: 'literal', language: false },
  'vcard:hasEmail': { field: ['contactPoint_1_mbox'], type: 'uri', toURI: JSON2RDFUtil.toMailURI },
  'foaf:homepage': fr('contactPoint_1_homepage', 'uri'),
};

let counter = 0;
module.exports = {
  map(args, record, checkDuplicates) {
    const base = args.base ? args.base : 'http://example.com/';
    const language = args.language || config.defaultLanguage || 'de';
    counter += 1;
    const extras = {};
    const dsURI = record.adms_identifier || record.identifier;
    logger.info(`Importing dataset nr ${counter} with URI: ${dsURI}`);
    const dsGraph = new Graph();
    dsGraph.add(dsURI, 'rdf:type', 'dcat:Dataset');
    JSON2RDFUtil.mapToRDF({
      graph: dsGraph,
      subject: dsURI,
      language,
      record,
      extras,
      base,
      mapping: datasetMapping,
    });

    if (record.description) {
      const rg = /^\[<.*>\]\s(.*)$/;
      if (rg.test(record.description)) {
        dsGraph.addL(dsURI, 'dcterms:description', record.description.match(rg)[1], 'de');
      } else {
        dsGraph.addL(dsURI, 'dcterms:description', record.description, 'de');
      }
    }

    const extraGraphs = {};

    // Distributions (CKAN resources)
    (record.distributions || []).forEach((res) => {
      const rsURI = toUuidUrn(dsURI + res.accessURL);
      const rsGraph = new Graph();
      extraGraphs[rsURI] = rsGraph;
      rsGraph.add(rsURI, 'rdf:type', 'dcat:Distribution');
      dsGraph.add(dsURI, 'dcat:distribution', rsURI);
      JSON2RDFUtil.mapToRDF({
        graph: rsGraph,
        subject: rsURI,
        language,
        record: res,
        extras: {},
        base,
        mapping: distributionMapping,
      });
    });

    // Publisher
    if (record.publisher || record.publiser_mbox) {
      const pURI = toUuidUrn('publisher' + (record.publisher || '') + (record.publiser_mbox || ''));
      dsGraph.add(dsURI, 'dcterms:publisher', pURI);
      if (!checkDuplicates[pURI]) {
        const pGraph = new Graph();
        JSON2RDFUtil.mapToRDF({
          graph: pGraph,
          subject: pURI,
          language,
          record,
          extras: {},
          base,
          mapping: publisherMapping,
        });
        pGraph.add(pURI, 'rdf:type', 'foaf:Agent');
        extraGraphs[pURI] = pGraph;
        checkDuplicates[pURI] = true;
      }
    }

    // Maintainer
    if (record.maintainer_mbox) {
      const mURI = toUuidUrn('maintainer' + record.maintainer_mbox);
      dsGraph.add(dsURI, 'dcatde:maintainer', mURI);
      if (!checkDuplicates[mURI]) {
        const mGraph = new Graph();
        JSON2RDFUtil.mapToRDF({
          graph: mGraph,
          subject: mURI,
          language,
          record,
          extras: {},
          base,
          mapping: maintainerMapping,
        });
        mGraph.add(mURI, 'rdf:type', 'foaf:Agent');
        extraGraphs[mURI] = mGraph;
        checkDuplicates[mURI] = true;
      }
    }

    // Contactpoint
    if (record.contactPoint_1_mbox || record.contactPoint_1_name) {
      const cURI = toUuidUrn('contactpoint' + (record.contactPoint_1_mbox || '') + (record.contactPoint_1_name || ''));
      dsGraph.add(dsURI, 'dcat:contactPoint', cURI);
      const cGraph = new Graph();
      cGraph.add(cURI, 'rdf:type', 'vcard:Organization');
      JSON2RDFUtil.mapToRDF({
        graph: cGraph,
        subject: cURI,
        language,
        record,
        extras,
        base,
        mapping: contactMapping,
      });

      if (record.contactPoint_1_address) {
        const splitted = record.contactPoint_1_address.split(',');
        const street = splitted[0];
        const locationArr = splitted[1].trim().split(' ');
        const cblank = cGraph.add(cURI, 'vcard:hasAddress').getValue();
        cGraph.add(cblank, 'rdf:type', 'vcard:Address');
        cGraph.addL(cblank, 'vcard:street-address', street);
        cGraph.addL(cblank, 'vcard:postal-code', locationArr[0]);
        cGraph.addL(cblank, 'vcard:locality', locationArr[1]);
        cGraph.addL(cblank, 'vcard:country-name', 'Germany');
      }
      if (!checkDuplicates[cURI]) {
        extraGraphs[cURI] = cGraph;
        checkDuplicates[cURI] = true;
      }
    }

    // Time
    if (record.PeriodOfTime_startDate || record.PeriodOfTime_endDate) {
      const tblank = dsGraph.add(dsURI, 'dcterms:temporal').getValue();
      dsGraph.add(tblank, 'rdf:type', 'dcterms:PeriodOfTime');
      if (record.PeriodOfTime_startDate) {
        dsGraph.addD(tblank, 'schema:startDate', toIso(record.PeriodOfTime_startDate), 'xsd:date');
      }
      if (record.PeriodOfTime_endDate) {
        dsGraph.addD(tblank, 'schema:endDate', toIso(record.PeriodOfTime_endDate), 'xsd:date');
      }
    }

    // Spatial
    let geom;
    if (record.bbox) {
      const [E,S,W,N] = record.bbox.split(',');
      geom = `POLYGON((${W} ${N},${E} ${N},${E} ${S},${W} ${S},${W} ${N}))`;
    } else if (record['bbox-north-lat'] !== undefined) {
      const {
        'bbox-north-lat': N,
        'bbox-east-long': E,
        'bbox-south-lat': S,
        'bbox-west-long': W,
      } = record;
      geom = `POLYGON((${W} ${N},${E} ${N},${E} ${S},${W} ${S},${W} ${N}))`;
    }
    if (geom) {
      const sblank = dsGraph.add(dsURI, 'dcterms:spatial').getValue();
      dsGraph.add(sblank, 'rdf:type', 'dcterms:Location');
      dsGraph.addD(sblank, 'locn:geometry', geom, 'gsp:wktLiteral');
    }

    return {
      mainURI: dsURI,
      mainGraph: dsGraph,
      extraGraphs,
    };
  },
};
