const { promiseUtil } = require('@entryscape/entrystore-js');
const config = require('../config');
const logger = require('../utils/logger/logger');

const transform2module = {
  example: '../transforms/exampleRDFTransform',
  empty: '../transforms/noopTransform',
  check: '../transforms/checkTransform',
  validate: '../transforms/validateTransform',
  merge: '../transforms/mergeTransform',
  fetch: '../transforms/fetchTransform',
  streamFetch: '../transforms/streamFetchTransform',
  fetchCSW: '../transforms/fetch/fetchCSWTransform',
  fetchCKAN: '../transforms/fetch/fetchCKANTransform',
  fetchOGD: '../transforms/fetch/fetchOGDTransform',
  fetchPX: '../transforms/fetch/fetchPX',
  fetchNobel: '../transforms/fetch/fetchNobelTransform',
  fetchFsk: '../transforms/fetch/fetchFskTransform',
  ckan2rdf: '../transforms/convertTransform',
  gml2rdf: '../transforms/convertTransform',
  ogd2rdf: '../transforms/convertTransform',
  convert: '../transforms/convertTransform',
  copy: '../transforms/copyTransform',
};

const wrapTransform = (transform, transformType = '') => {
  let rejectedPromise;
  const nt = (jobEntry, pipeline, transformId, passOnObj, streamState) => {
    let transformPromise;
    switch (streamState) {
      case 'finish':
        if (rejectedPromise !== null) {
          return rejectedPromise;
        }
        return promiseUtil.toPromise(transform(jobEntry, pipeline, transformId, passOnObj, streamState));
      default:
        if (streamState === 'init') {
          rejectedPromise = null;
        }
        transformPromise = promiseUtil.toPromise(transform(jobEntry, pipeline, transformId, passOnObj, streamState));
        if (streamState === 'process') {
          return transformPromise.then(null, () => {
            rejectedPromise = transformPromise;
            return null;
          });
        }
        return transformPromise;
    }
  };
  nt.transformType = transformType;
  nt.stream = transform.stream;
  nt.streamTransform = transform.streamTransform;
  return nt;
};

const transforms = {};

const getTransform = (pipeline, transformId) => new Promise((resolve, reject) => {
  const tt = pipeline.getTransformType(transformId);
  if (transforms[tt]) {
    resolve(transforms[tt]);
  } else if (transform2module[tt]) {
    const transform = require(transform2module[tt]);
    const _transform = wrapTransform(transform, tt);
    if (_transform) {
      transforms[tt] = _transform;
      resolve(_transform);
    } else {
      reject(`Could not load module for transform type "${tt}"`);
    }
  } else {
    reject(`No module defined for transform type "${tt}"`);
  }
});

/**
 *
 *
 * A pipeline contains a set of transforms that are executed in order.
 * Transform will send along its output to the following transform for further processing.
 * In some situations it is better to pass a along a series of values.
 * This is useful when fetching large amounts of data, this is achieve with streamable transforms.
 * A transform is streamable if it has the value "start", "intermediate" or "stop" in the "stream" attribute.
 * A stream must be formed by a start stream followed by 0 or more intermediate transforms and ended with a
 * single stop transform.
 *
 * First, all transforms in the stream are executed with the parameter "streamState"
 * set to "init". Second, as long as the first transform gives a non-empty result when
 * executed with the "streamState" set to "process" the other transforms in the stream are
 * also executed with "streamState" set to "process" with that value passed.
 * (Consecutive transforms in the stream are executed with the output of the previous as always).
 * Third, all transforms are executed one last time with the "streamState" set to "finish".
 *
 * The result of a transform can be one of five status values:
 *  1  -  means a standalone transform succeeded
 *  0  -  means an intermediate transform succeeded
 * -1  -  means a standalone transform failed
 * -2  -  means an intermediate transform failed and the following
 *        intermediate transforms followed by a single standalone transforms
 *        will not be run. The status value for those transforms will be -3.
 * -3  -  a leading intermediate transform in the pipeline failed.
 *
 * If a transform is executed in a stream fashion the status value reflects the final call,
 * i.e. when "streamState" is set to "finish".
 *
 * On success a resultobject will be returned (in a promise):
 * {
 *    status: array_of_status_for_each_transform, //Mentioned above
 *    successCount: nr_of_successfull_standalone_transforms,
 *    allSucceeded: true_if_all_standalone_transforms_succeeded
 *    oneSucceeded: true_if_one_standalone_transform_succeeded
 *    transforms: array_of_transformtypes_in_pipeline
 * }
 *
 * @return {Promise} the promise will resolve with the resultobject described above.
 * The promise will be rejected if a transform in the pipeline cannot be resolved
 * into running code.
 */
module.exports = (jobEntry, pipeline) => {
  const transformIds = pipeline.getTransforms();
  let passOnObj;
  let intermediateFailed = false;
  return promiseUtil.forEach(transformIds, (transformId) => getTransform(pipeline, transformId))
    .then((trs) => {
      const transformIdMap = {};
      transformIds.forEach((tid, idx) => {
        transformIdMap[tid] = trs[idx];
      });

      let stream;
      const streams = [];
      let isStream = false;
      transformIds.forEach((tid, idx) => {
        const transform = trs[idx];
        if (transform.stream === 'start') {
          // New stream
          isStream = true;
          stream = [tid];
          streams.push(stream);
        } else if (isStream && transform.stream === 'intermediate') {
          stream.push(tid);
        } else if (isStream && transform.stream === 'stop') {
          // End stream
          stream.push(tid);
          isStream = false;
        } else {
          streams.push([tid]);
          isStream = false;
        }
      });

      // Run an individual transform and update pass-on-object
      const runTransform = (tid, streamState) => {
        const transform = transformIdMap[tid];
        const intermediate = transform.stream === 'start' || transform.stream === 'intermediate';
        if (intermediateFailed) {
          if (intermediate) {
            return -3;
          }
          intermediateFailed = false;
        }
        return transform(jobEntry, pipeline, tid, passOnObj, streamState)
          .then((newPassOnObj) => {
            if (newPassOnObj) {
              passOnObj = newPassOnObj;
            }
            if (transform.transformType === 'empty') {
              return 0;
            }
            return intermediate ? 0 : 1;
          }, (error) => {
            if (error && error.name && error.name === 'TooFewResultsError') {
              jobEntry.add('storepr:failReason', 'storepr:tooFewResults');
              throw error;
            } else {
              logger.error(error);
            }
            if (intermediate) {
              intermediateFailed = true;
              return -2;
            }
            return -1;
          });
      };

      // Run a stream of transforms
      const runStreamTransforms = async (streamIdArr) => {
        const initTransformId = streamIdArr[0];
        const initTransform = transformIdMap[initTransformId];
        const tailIdArr = streamIdArr.slice(1);
        let initPassOn = passOnObj;

        // Initialize stream
        await promiseUtil.forEach(streamIdArr, (tid) => transformIdMap[tid](jobEntry, pipeline, tid, initPassOn, 'init')
          .then((newInitPassOn) => {
            if (newInitPassOn) {
              initPassOn = newInitPassOn;
            }
          }));

        // Process stream
        while (true) {
          const initialProcessPassOn = await initTransform(jobEntry, pipeline,
            initTransformId, passOnObj, 'process');
          if (typeof initialProcessPassOn === 'undefined' || initialProcessPassOn === null) {
            break;
          }
          let processPassOn = initialProcessPassOn;
          for (let i = 0; i < tailIdArr.length; i++) {
            try {
              const tid = tailIdArr[i];
              const newProcessPassOn = await transformIdMap[tid](jobEntry, pipeline, tid, processPassOn, 'process');
              if (newProcessPassOn === null) {
                break;
              } else if (newProcessPassOn) {
                processPassOn = newProcessPassOn;
              }
            } catch (err) {
              logger.error('Encountered error while transforming pass on object');
              logger.error(err);
              break;
            }
          }
        }

        // Finish stream
        return await promiseUtil.forEach(streamIdArr, (tid) => runTransform(tid, 'finish'));
      };

      // Run an array of streams / individual transforms
      return promiseUtil.forEach(streams, (stream) => {
        if (stream.length === 1) {
          return runTransform(stream[0]);
        }
        return runStreamTransforms(stream);
      }).then((streamStatus) => {
        // Take care of the results
        let status = [];
        streamStatus.forEach((sStatus) => {
          status = status.concat(typeof sStatus === 'object' ? sStatus : [sStatus]);
        });
        let allT = true;
        let countT = 0;
        status.forEach((s) => {
          if (s < 0) {
            allT = false;
          }
          if (s > 0) {
            countT += 1;
          }
        });

        jobEntry.addD('storepr:successCount', `${countT}`, 'xsd:integer');
        jobEntry.addD('storepr:allSucceeded', `${allT}`, 'xsd:boolean');
        jobEntry.addD('storepr:oneSucceeded', `${countT > 0}`, 'xsd:boolean');
        return {
          status,
          successCount: countT,
          allSucceeded: allT,
          oneSucceeded: countT > 0,
          transforms: transformIds.map((transformId) => pipeline.getTransformType(transformId)),
        };
      }, (err) => {
        logger.error(err);
        if (err.name === 'TooFewResultsError') {
          jobEntry.add('storepr:failReason', 'storepr:tooFewResults');
        } else if (err.name === 'LoadFromSourceError') {
          jobEntry.add('storepr:failReason', 'storepr:loadError');
        }
        throw err;
      });
    }, (err) => {
      logger.error(err);
    });
};
