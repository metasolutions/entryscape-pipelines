const fs = require('fs');
const config = require('../config');
const local = require('../local');
const { namespaces } = require('@entryscape/entrystore-js');
const mail = require('../utils/mail');
const runJob = require('./runJob');
const { EntryStore, EntryStoreUtil, types, terms } = require('@entryscape/entrystore-js');
const globals = require('../utils/globals');
const reportPrinter = require('../utils/reportPrinter');
const { exportLinks } = require('../utils/linkchecker/linkExporter');
const logger = require('../utils/logger/logger');

/** @type {store/EntryStore} */
const es = globals.getEntryStore();
const entryStoreUtil = globals.getEntryStoreUtil();

/**
 * Array of pending pipelines
 */
const queue = [];

/**
 * A flag to let the application know if it should die (gracefully -> SIGTERM)
 * @type {boolean}
 */
let stopImmediately = false;

const customLog = (stamp, title, err) => {
  const d = new Date();
  if (err) {
    logger.error(`${d}: Error with job nr ${queue.length + 1} in queue (${title}) took ${d.getTime() - stamp} milliseconds. Message: ${err}`);
  } else {
    logger.info(`${d}: Job nr ${queue.length + 1} in queue (${title}) took ${d.getTime() - stamp} milliseconds`);
  }
};

let jobsBeforeHarvest;

const clearCache = () => es.getCache().clear();
const minsToMillis = (mins = 1) => mins * 60000;

/**
 * Get an array with all pending pipelines' URIs
 *
 * @return {Promise}
 */
const getPendingPipelineURIs = () => {
  clearCache();
  return es.newSolrQuery()
    .graphType(types.GT_PIPELINERESULT)
    .status([terms.status.Pending, terms.status.InProgress])
    .list()
    .forEach((plRes) => queue.push(plRes.getURI()))
    .then(() => {
      jobsBeforeHarvest = queue.length;
    });
};

/**
 *
 * @param jobEntry
 * @return {*}
 */
const getPipelineFromJob = (jobEntry) => {
  const pipeLineEntryURI = jobEntry.getEntryInfo().getGraph().findFirstValue(jobEntry.getURI(), 'store:pipeline', null);
  return es.getEntry(es.getEntryURIFromURI(pipeLineEntryURI), {
    forceLoad: true,
    loadResource: true,
  });
};

const fixUsernameInPipeline = (pipelineEntry) => {
  const md = pipelineEntry.getMetadata();
  const ruri = pipelineEntry.getResourceURI();
  // Ignore check for non PSI organizations.
  if (md.find(ruri, 'dcterms:subject', { type: 'literal', value: 'psi' }).length === 0) {
    return Promise.all([]);
  }
  const context = pipelineEntry.getContext();
  return context.getHomeContextOf().then((groupEntry) => {
    const group = groupEntry.getResource(true);
    return group.getEntries().then((memberArr) => {
      // user is in memberArr
      let name = '';
      return Promise.all(memberArr.map((member) =>
        // Should only be one member with real mailaddress as name
        member.getResource().then((user) => {
          const _name = user.getName();
          if (_name.indexOf('@') > 0) {
            name = _name;
          }
        }))).then(() => {
        if (name !== '' && md.findFirstValue(ruri, 'foaf:mbox') !== (`mailto:${name}`)) {
          md.findAndRemove(ruri, 'foaf:mbox');
          md.add(ruri, 'foaf:mbox', `mailto:${name}`);
          pipelineEntry.commitMetadata();
        }
      });
    });
  });
};

const sendValidateReport = (pipeline, currentResult) => {
  const toResourceURI = (uri) => {
    if (uri) {
      const es = pipeline.getEntryStore();
      return es.getResourceURI(es.getContextId(uri), es.getEntryId(uri));
    }
  };

  const md = currentResult.getMetadata();
  const struct = reportPrinter.extractSummaryStruct(currentResult);
  const generatedReport = toResourceURI(md.findFirstValue(null, 'prov:generated'));
  const sourceRDF = toResourceURI(md.findFirstValue(null, 'dcterms:source'));
  const resourceURI = currentResult.getResourceURI();
  const contextId = es.getContextId(resourceURI);
  const baseURI = es.getBaseURI();
  
  const report = (type) => {
    const msgs = ['The following report is sent to you automatically because of issues found in the harvested information.<br>'];

    msgs.push('Here is a summary of the report:');
    msgs.push(reportPrinter.summary(struct).replace(/(\r\n|\n|\r)/gm, '<br>'));

    if (config.shortValidateMail) {
      const validatePage = config.shortValidateMail.page(baseURI, contextId);
      msgs.push(`<br>The latest validation report is available <a href="${validatePage}" target="_blank">here</a>.`);
    } else {
      if (sourceRDF) {
        msgs.push(`<br>A snapshot of the harvested information is available <a href="${sourceRDF}" target="_blank">here</a>.`);
        msgs.push(`Note that the snapshot is deleted after three days, save it before then if you need to keep it longer.<br>`);
      }
      if (generatedReport) {
        msgs.push(`<br>A full validation report has been generated and is available <a href="${generatedReport}" target="_blank">here</a>.`);
        msgs.push(`Note that the full report is deleted after three days, save it before then if you need to keep it longer.<br>`);
      }
  }

    mail.sendByType('validationReport', pipeline, msgs.join('<br>'), type);
    // mail.validationReport(pipeline, msgs.join('\n'));
  };
  if ((struct.nrOfMandatoryFieldsMissing > 0 || struct.nrOfMandatoryTypesMissing > 0)) {
    report('mandatory');
  } else if ((struct.nrOfRecommendedFieldsMissing > 0 || struct.nrOfDeprecatedFieldsPresent > 0)) {
    report('recommended');
  }
};

const cleanUpOldResults = (pipeline, currentResult) => {
  const otherresults = pipeline.getReferrers('store:pipeline');
  const currentMergeSuccess = currentResult.getMetadata().find(null, 'storepr:merge').length > 0;
  let mergeSuccessDiscovered = currentMergeSuccess;
  const error = currentResult.getMetadata().findFirstValue(null, 'storepr:failReason');

  const defs = [];
  return es.newSolrQuery().uri(otherresults).sort('created+desc').list()
    .forEach((or, nth) => {
      if (or.getURI() === currentResult.getURI()) {
        return;
      }
      let keep = nth < 3;
      const md = or.getMetadata();
      const nthMergeSuccess = md.find(null, 'storepr:merge').length > 0;

      // Mail, first merge success after failure
      if (nth === 1 && currentMergeSuccess && !nthMergeSuccess) {
        mail.sendByType('mergeFirstSuccess', pipeline);
      }

      // Mail, first time merge failure after success
      if (nth === 1 && !currentMergeSuccess && nthMergeSuccess) {
        mail.sendByType('mergeFirstFailure', pipeline, error);
      }

      // Mail, third time merge failure after success
      if (nth === 3 && !currentMergeSuccess && nthMergeSuccess && !mergeSuccessDiscovered) {
        mail.sendByType('mergeThirdFailure', pipeline, error);
      }

      // If first three results are merge failures, keep latest successful
      if (!mergeSuccessDiscovered && nthMergeSuccess) {
        mergeSuccessDiscovered = true;
        keep = true;
      }

      if (keep) {
        if (md.find(null, 'dcterms:subject', { type: 'literal', value: 'latest' }).length > 0) {
          md.findAndRemove(null, 'dcterms:subject', { type: 'literal', value: 'latest' });
          md.add(or.getResourceURI(), 'dcterms:subject', { type: 'literal', value: 'previous' });
          defs.push(or.commitMetadata());
        } else if (md.find(null, 'dcterms:subject', {
          type: 'literal',
          value: 'previous',
        }).length > 0) {
          md.findAndRemove(null, 'dcterms:subject', { type: 'literal', value: 'previous' });
          defs.push(or.commitMetadata());
        }
      } else {
        const sourceEntryURI = md.findFirstValue(null, 'dcterms:source');
        if (sourceEntryURI) {
          defs.push(es.getREST().del(sourceEntryURI));
        }
        const validationReportURI = md.findFirstValue(null, 'prov:generated');
        if (validationReportURI) {
          defs.push(es.getREST().del(validationReportURI));
        }
        defs.push(or.del());
      }
    })
    .then(() => Promise.all(defs));
};

/**
 * @param {store/EntryInfo} entryInfo
 */
const setJobStatusInProgress = (entryInfo) => {
  entryInfo.setStatus(terms.status.InProgress);
  return entryInfo.commit().then(() => {
    const entry = entryInfo.getEntry();
    entry.setRefreshNeeded(true);
    return entry.refresh();
  });
};

const harvestJob = async (jobEntryURI) => {
  const jobEntry = await es.getEntry(jobEntryURI);
  const stamp = new Date().getTime();
  const entryInfo = jobEntry.getEntryInfo();
  const title = jobEntry.getMetadata().findFirstValue(jobEntry.getResourceURI(),
    'dcterms:title');

  try {
    if (entryInfo.getStatus() === terms.status.InProgress) {
      // TODO potentially delete all job related results (if you can identify them)
      // given that the process did not go through
    } else if (entryInfo.getStatus() !== terms.status.Pending) {
      logger.info(`Skipping job that is already done (solr index slow to update): ${title}`);
      return true;
    }
    logger.info(`Processing harvesting job: ${title}`);
    await setJobStatusInProgress(entryInfo);
    // Get the pipeline
    const pipelineEntry = await getPipelineFromJob(jobEntry);
    // Get the pipeline resource
    const jobResource = await pipelineEntry.getResource();
    // Run job
    let res;
    try {
      res = await runJob(jobEntry, jobResource);
    } catch (e) {
      // Do nothing since check below for res exists, and set status to failure
    }

    sendValidateReport(pipelineEntry, jobEntry);

    if (config.extractLinksOnHarvest) {
      const context = pipelineEntry.getContext().getId();
      exportLinks(context);
    }

    // Make this job entry the latest
    jobEntry.addL('dcterms:subject', 'latest');
    await jobEntry.commitMetadata();
    // Now update the status.

    // Change job status, in entry info graph, based on runJob result
    // (We do this after metadata update because we can ignore unmodifiedSince check for entry info
    if (res && res.oneSucceeded) {
      entryInfo.setStatus(terms.status.Succeeded);
    } else {
      entryInfo.setStatus(terms.status.Failed);
    }
    await entryInfo.commit(true);
    await cleanUpOldResults(pipelineEntry, jobEntry);
    await fixUsernameInPipeline(pipelineEntry);
    customLog(stamp, title);
  } catch (err) {
    customLog(stamp, title, err);
  }
};

const ignore = (job) => {
  const cid = es.getContextId(job);
  let ignore = false;
  if (config.blacklist && config.blacklist.indexOf(cid) !== -1) ignore = true;
  else if (config.denylist && config.denylist.indexOf(cid) !== -1) ignore = true;
  if (ignore) logger.info(`Context ${cid} ignored due to deny listed`);
  return ignore;
}

/**
 * Recursively calls harvestJob for each pending pipeline in (pending) queue
 * @return {Promise}
 */
const harvestJobs = () => new Promise((resolve) => {
  clearCache();
  if (!stopImmediately && queue.length > 0) {
    const job = queue.pop();
    if (ignore(job)) {
      return resolve(harvestJobs());
    }
    return harvestJob(job)
      .then(() => {
        clearCache();
        return resolve(harvestJobs());
      });
  }
  // stamp = null;
  resolve(true);
});

const dir = 'output/harvester/';
if (!fs.existsSync(dir)) {
  fs.mkdirSync(dir);
}
const statusFile = `${dir}status.json`;
const startedDate = new Date().toISOString();

const writeHarvesterStatus = () => {
  const status = {
    date: new Date().toISOString(),
    startedAt: startedDate,
    latestHarvestCount: jobsBeforeHarvest,
    stopSignalRecieved: stopImmediately
  };
  fs.writeSync(fs.openSync(statusFile, 'w'), JSON.stringify(status, null, '  '), 0, 'utf8');
};

let authtime = new Date().getTime();
const authenticate = () => {
  const auth = es.getAuth();
  return auth.getUserInfo(true).then((ui) => {
    if (ui.id === '_guest') {
      return auth.login(local.user, local.password);
    }
    const newtime = new Date().getTime();
    if (newtime - authtime > 86400000) { // If last sign in was over 24 hours ago, sign in again.
      return auth.logout().then(() => {
        authtime = newtime;
        return auth.login(local.user, local.password);
      });
    }
    return undefined;
  });
};




let intervalLoop = null;
if (process.argv.length > 2) {
  config.runOnlyOnce = process.argv[2].toLowerCase() === 'runonlyonce';
}

/**
 * Main daemon tasks
 */
const mainLoop = () => {
  intervalLoop = null;
  return authenticate().then(getPendingPipelineURIs)
    .then(harvestJobs)
    .then(writeHarvesterStatus)
    .then(() => {
      if (stopImmediately) {
        logger.info(`HARVESTER STOPPING! Total of ${queue.length} items left in the queue`);
        process.exit(0);
      }
      es.getCache().clear();
      if (!config.runOnlyOnce) {
        intervalLoop = setTimeout(mainLoop,
          minsToMillis(config.harvesterTimeout ? config.harvesterTimeout : 10));
      }
    }, (err) => {
      logger.error(err);
    });
};

mainLoop();

/**
 * Listen for SIGTERM
 *  - set stopImmediately to true
 *  - stop loop
 */
process.on('SIGTERM', () => {
  logger.info('HARVESTER STOP SIGNAL RECEIVED, WAITING FOR ONGOING JOBS TO FINISH FIRST');
  stopImmediately = true;
  if (intervalLoop != null) {
    logger.info(`HARVESTER STOPPING! Total of ${queue.length} items left in the queue`);
    clearInterval(intervalLoop);
    process.exit(0);
  }
});
