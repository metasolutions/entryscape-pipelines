const globals = require('../../utils/globals');
const { types } = require('@entryscape/entrystore-js');
const authenticate = require('../../utils/authenticate');

/** CLI output */
const Table = require('cli-table');
const table = new Table({
  head: ['Transform Type', 'Priority', 'Arguments'],
});
const logger = require('../../utils/logger/logger');

let contextId;
if (process.argv.length > 5 && process.argv[5]) {
  contextId = process.argv[5];
} else {
  logger.warn('Missing parameter. Use `pl ls --help` exiting...');
  process.exit(0);
}

const es = globals.getEntryStore();
authenticate().then(() => {
  const context = es.getContextById(contextId);
  const query = es.newSolrQuery().graphType(types.GT_PIPELINE).context(context)
    .list()
    .forEach(entry => entry.getResource().then((plRes) => {
      const md = entry.getMetadata();
      const transformIds = plRes.getTransforms();

      transformIds.forEach((id) => {
        /** get transform info **/
        const type = plRes.getTransformType(id);
        const priority = plRes.getPriority(id);
        const args = plRes.getTransformArguments(id);
        table.push([
          type || '',
          priority !== undefined ? priority : '',
          JSON.stringify(args || {}, null, 4),
        ]);
      });

    }, (err) => {
      // Pipeline without resource, ignore, not neccessarily an error
    })).then(() => {
      logger.info(table.toString());
      process.exit(0); // Success
    }, (err) => {
      logger.error(err);
      process.exit(1); // Error
    });
});
