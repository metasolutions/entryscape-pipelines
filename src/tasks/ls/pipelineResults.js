const globals = require('../../utils/globals');
const authenticate = require('../../utils/authenticate');
const { types, terms } = require('@entryscape/entrystore-js');

/** CLI output */
const ProgressBar = require('progress');
const colors = require('colors');
const Table = require('cli-table');
const logger = require('../../utils/logger/logger');

const table = new Table({
  head: ['Pipeline Result', 'Status', 'Context ID', 'Entry ID'],
});

/** @type {store/EntryStore} */
const es = globals.getEntryStore();

/** @see {store/terms} */
const mapStatusToTerm = {
  pending: terms.status.Pending,
  success: terms.status.Succeeded,
  failed: terms.status.Failed,
  inprogress: terms.status.InProgress,
};

/**
 * Get proeprty URI from status string
 * @param status
 * @return {String}
 */
const getStatusProperty = status => mapStatusToTerm[status];

/**
 * Applies coloring ot a text according to the status
 * @param status
 * @return {String}
 */
const colorStatusProperty = (status) => {
  switch (status) {
    case 'pending':
    case terms.status.Pending:
      return colors.magenta('Pending');
    case 'inprogress':
    case terms.status.InProgress:
      return colors.blue('InProgress');
    case 'success':
    case terms.status.Succeeded:
      return colors.green('Succeeded');
    case 'failed':
    case terms.status.Failed:
      return colors.red('Failed');
    default:
      return status;
  }
};

/**
 * Parse status argument if exists
  */
  let status;
if (process.argv.length > 4 && process.argv[4]) {
  status = process.argv[4];
}

/*
 * Parse context argument if it exists
 */
let contextId;
if (process.argv.length > 5 && process.argv[5]) {
  contextId = process.argv[5];
}
/**
 * Main logic
 */
authenticate().then(() => {
  const statusProp = getStatusProperty(status);
  const query = es.newSolrQuery().graphType(types.GT_PIPELINERESULT);

  if (status) query.status(statusProp);
  if (contextId) query.context(contextId);

  const sList = query.list();
  let bar = null;
  sList.getEntries().then(() => {
    const size = sList.getSize();
    if (size) {
      bar = new ProgressBar(':bar', { total: size });
    }
  });

  sList.forEach((entry) => {
    bar && bar.tick(); // update progress bar

    const md = entry.getMetadata();
    const context = entry.getContext();

    table.push([
      md.findFirstValue(entry.getResourceURI(), 'dcterms:title') || 'N/A', /** @see https://github.com/Automattic/cli-table/issues/65 */
      colorStatusProperty(entry.getEntryInfo().getStatus() || ''),
      context.getId(),
      entry.getId(),
    ]);
  }).then(() => logger.info(table.toString()));
});
