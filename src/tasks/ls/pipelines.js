const globals = require('../../utils/globals');
const { types } = require('@entryscape/entrystore-js');
const authenticate = require('../../utils/authenticate');

/** CLI output */
const ProgressBar = require('progress');
const bar = new ProgressBar(':bar', { total: 640 });
const Table = require('cli-table');
const table = new Table({
  head: ['Name', 'RESOURCE URI', 'Context ID', 'Entry ID'],
});
const logger = require('../../utils/logger/logger');


let tag;
if (process.argv.length > 3 && process.argv[3]) {
  tag = process.argv[3];
}
/*
 * Parse context argument if it exists
 */
let contextId;
if (process.argv.length > 5 && process.argv[5]) {
  contextId = process.argv[5];
}

const es = globals.getEntryStore();
authenticate().then(() => {
  const query = es.newSolrQuery().graphType(types.GT_PIPELINE);
  if (tag != null) {
    query.tagLiteral(tag);
  }
  if (contextId) query.context(contextId);

  query.list().forEach(entry => entry.getResource().then((plRes) => {
    bar.tick();
    const md = entry.getMetadata();
    const context = entry.getContext();
    const title = md.findFirstValue(entry.getResourceURI(), 'dcterms:title');
    table.push([
      title || '',
      entry.getResourceURI(),
      context.getId(),
      entry.getId(),
    ]);
  }, (err) => {
    // Pipeline without resource, ignore, not neccessarily an error
  })).then(() => {
    logger.info(table.toString());
    process.exit(0); // Success
  }, (err) => {
    logger.error(err);
    process.exit(1); // Error
  });
})
  .catch((e) => logger.error('There was an Error: ', e));
