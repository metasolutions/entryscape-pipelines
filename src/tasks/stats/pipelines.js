const globals = require('../../utils/globals');
const authenticate = require('../../utils/authenticate');
const { namespaces, types, terms } = require('@entryscape/entrystore-js');
const logger = require('../../utils/logger/logger');

/** @type {store/EntryStore} */
const es = globals.getEntryStore();

const ensureStatisticsContextExists = () => es.getContextById('catalogstatistics').getEntry()
  .then(null, () => {
    const cpe = es.newContext('catalogstatistics', 'catalogstatistics');
    const ei = cpe.getEntryInfo();
    const acl = ei.getACL(true);
    acl.rread.push('_guest');
    acl.mread.push('_guest');
    ei.setACL(acl);
    return cpe.commit();
  });

namespaces.add('pr', 'http://entrystore.org/terms/pipelineresult#');
namespaces.add('st', 'http://entrystore.org/terms/statistics#');

const doStatistics = async (contextEntry) => {
  let psidatasets = 0;
  let otherdatasets = 0;
  let onlyPsidatapage = 0;
  let onlyDcat = 0;
  let psiAndDcat = 0;
  let other = 0;
  const ne = es.getContextById(contextEntry.getId()).newNamedEntry();
  const md = ne.getMetadata();
  const s = ne.getResourceURI();
  try {
    // Calculate amount of public datasets in the repository right now, not how many got harvested.
    const residentPublicList = es.newSolrQuery().publicRead(true).rdfType('dcat:Dataset').list();
    await residentPublicList.getEntries();

    // Calculate amount of total datasets in the repository right now, not how many got harvested.
    const residentList = es.newSolrQuery().rdfType('dcat:Dataset').list();
    await residentList.getEntries();

    const failedPSIList = es.newSolrQuery().tagLiteral(['latest', 'psi'], 'and')
      .status(terms.status.Failed).limit(1)
      .list();
    await failedPSIList.getEntries();
    es.newSolrQuery().graphType(types.GT_PIPELINERESULT)
      .tagLiteral('latest').status(terms.status.Succeeded)
      .list()
      .forEach((pr) => {
        let nds = 0;
        const prmd = pr.getMetadata();
        const prs = pr.getResourceURI();
        let dcat = false;
        let psidatapage = false;
        const dc = prmd.findFirstValue(prs, 'pr:mergeResourceCount');
        if (dc) {
          dcat = true;
          if (parseInt(dc, 10) == dc) {
            nds = parseInt(dc);
          }
        }
        if (prmd.findFirstValue(prs, 'pr:check') === 'true') {
          psidatapage = true;
        }
        if (prmd.find(prs, 'dcterms:subject', { type: 'literal', value: 'psi' }).length > 0) {
          onlyDcat += dcat && !psidatapage ? 1 : 0;
          onlyPsidatapage += !dcat && psidatapage ? 1 : 0;
          psiAndDcat += dcat && psidatapage ? 1 : 0;
          psidatasets += nds;
        } else {
          other += 1;
          otherdatasets += nds;
        }
        // Report contexts that have more than 0 datasets, or alternatively where we have
        // 0 datasets for those where we have a successfull harvesting but with an empty RDF file
        // (as this is how we clear an organization currenty).
        if (nds > 0 || prmd.findFirstValue(prs, 'pr:fetchRDF') === 'true') {
          md.addD(s, `st:datasets_in_context_${pr.getContext().getId()}`, `${nds}`, 'xsd:integer');
        }
      })
      .then(() => {
        const d = new Date();
        md.addD(s, 'dcterms:date', d.toISOString().substr(0, 10), 'xsd:date');
        if (d.getDate() === 1) {
          md.addL(s, 'dcterms:subject', 'newMonth');
          if (d.getMonth() === 0) {
            md.addL(s, 'dcterms:subject', 'newYear');
          }
        }
        md.add(s, 'rdf:type', 'st:CatalogStatistics');
        md.addD(s, 'st:psiDatasetCount', `${psidatasets}`, 'xsd:integer');
        md.addD(s, 'st:otherDatasetCount', `${otherdatasets}`, 'xsd:integer');
        md.addD(s, 'st:psiPage', `${onlyPsidatapage}`, 'xsd:integer');
        md.addD(s, 'st:psiDcat', `${onlyDcat}`, 'xsd:integer');
        md.addD(s, 'st:otherDcat', `${other}`, 'xsd:integer');
        md.addD(s, 'st:psiPageAndDcat', `${psiAndDcat}`, 'xsd:integer');
        md.addD(s, 'st:psiFailed', `${failedPSIList.getSize()}`, 'xsd:integer');
        md.addD(s, 'st:residentDatasetCount', `${residentList.getSize()}`, 'xsd:integer');
        md.addD(s, 'st:residentPublicDatasetCount', `${residentPublicList.getSize()}`, 'xsd:integer');
        ne.commit().then((e) => {
          logger.info(`Current statistics calculated and stored as entry ${e.getId()} in context catalogstatistics`); // Where does rmStatus come from?
          process.exit();
        });
      });
  } catch (e) {
    logger.error(e);
  }
};

authenticate()
  .then(ensureStatisticsContextExists)
  .then(doStatistics)
  .catch((err) => {
    logger.error(err.message);
  });
