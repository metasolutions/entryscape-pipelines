const globals = require('../../utils/globals');
const authenticate = require('../../utils/authenticate');
const {
  types,
  namespaces,
  promiseUtil ,
  EntryStore,
  EntryStoreUtil
} = require('@entryscape/entrystore-js');
const config = require('../../config');
const logger = require('../../utils/logger/logger');

const es = globals.getEntryStore();
const esu = globals.getEntryStoreUtil();
namespaces.add('dcat', 'http://www.w3.org/ns/dcat#');

const kind = process.argv[2];
const kindConfig = config?.sync[kind];

const logError = (err) => {
  logger.error(err.message);
  throw err;
};

const constructId = (dataset) => {
  const scopeId = dataset.getContext().getId();
  const specificId = dataset.getMetadata().findFirstValue(dataset.getResourceURI(), 'dcterms:identifier')
    || dataset.getId();
  return `${scopeId}_${kind}_${specificId}`;
}

const findExisting = async (tag) => {
  const arr = [];
  await es.newSolrQuery().tagLiteral(tag).graphType(types.GT_PIPELINE).forEach(e => arr.push(e));
  return arr;
}

const findURL = async (dataset) => {
  // Check for right distribution given format in kindconfig or assume RDF/XML
  const distributions = await esu.loadEntriesByResourceURIs(dataset.getMetadata().find(dataset.getResourceURI(), 'dcat:distribution')
    .map(stmt => stmt.getValue()));
  const dist = distributions.find((di) =>
    di.getMetadata().findFirstValue(di.getResourceURI(), 'dcterms:format') === 'application/rdf+xml');
  return dist ? dist.getMetadata().findFirstValue(dist.getResourceURI(), 'dcat:downloadURL')
    || dist.getMetadata().findFirstValue(dist.getResourceURI(), 'dcat:accessURL') : null;
};

const getContextName = (dataset) => dataset.getMetadata().findFirstValue(dataset.getResourceURI(), 'dcterms:title');


const datasetSource = async () => {
  const datasets = [];
  await es.newSolrQuery().uriProperty('dcterms:conformsTo', kindConfig.conformsTo)
    .forEach(e => datasets.push(e));
  const existing = await findExisting(kind);
  const existingIdx = {};
  existing.forEach(p => (existingIdx[p.getContext().getId()] = p));
  await promiseUtil.forEach(datasets, async (dataset) => {
    const id = constructId(dataset);
    const existing = existingIdx[id];
    if (existing) {
      // Found a pipeline, so don't keep it around as something to remove later
      delete existingIdx[id];
    } else {
      // Need to create a pipeline (in a context) for this dataset
      const datasetName = getContextName(dataset);
      const pipelineName = `Sync (${kind}) for "${datasetName}" in context ${dataset.getContext().getId()}`
      const sourceURL = await findURL(dataset);
      if (sourceURL) {
        const contextEntry = es.newContext(undefined, id)
          .addL('dcterms:title', pipelineName);
        const ei = contextEntry.getEntryInfo();
        const acl = ei.getACL(true);
        acl.rread.push('_guest');
        ei.setACL(acl);
        await contextEntry.commit();
        const context = es.getContextById(contextEntry.getId());
        const protoPipeline = context.newPipeline(kind)
          .addL('dcterms:subject', kind)
          .addL('dcterms:title', pipelineName)
          .addL('dcterms:description', `Created based on dataset "${datasetName}"`)
          .add('dcterms:source', dataset.getURI());

        const pipelineRes = protoPipeline.getResource();
        pipelineRes.addTransform('empty', {});
        pipelineRes.addTransform('fetch', { source: sourceURL });
        pipelineRes.addTransform('merge', kindConfig.merge);
        await protoPipeline.commit();
      }
    }
  });
}

const getIdentifier = (e) => {
  let id = e.getMetadata().findFirstValue(e.getResourceURI(), 'dcterms:identifier');
  if (id) {
    id = id.toLowerCase().replace(/[_-]/g, '');
    return id;
  }
}

/**
 * Synchronize all public contexts in another store based on their ids with existing pipelines, example config:
 *
 * sync: {
 *    opendata: {
 *      type: "externalContexts",
 *      externalSource: "https://sandbox.editera.dataportal.se/store",
 *      pipelineTag: ["opendata", "psi"]
 *    }
 * }
 *
 * @return {Promise<void>}
 */
const externalContextSource = async () => {
  const existing = await findExisting(kindConfig.pipelineTag || kind);
  const existingIdx = {};
  existing.forEach(p => (existingIdx[getIdentifier(p)] = p));
  const externalES = new EntryStore(kindConfig.externalSource);
  const externalESU = new EntryStoreUtil(externalES);
  const base = externalES.getBaseURI();
  await externalES.newSolrQuery()
    .graphType(types.GT_CONTEXT)
    .rdfType('http://entryscape.com/terms/CatalogContext')
    .literalProperty('dcterms:identifier', '*')
    .forEach(async contextEntry => {
      const id = getIdentifier(contextEntry);
      if (id) {
        try {
          const catalogEntry = await externalESU.getEntryByType('dcat:Catalog', contextEntry);
          const harvestURL = `${base}${contextEntry.getId()}/metadata/${catalogEntry.getId()}?recursive=dcat`;
          const existingPipelineEntry = existingIdx[id];
          if (existingPipelineEntry) {
            /** @type Pipeline */
            const pipeline = await existingPipelineEntry.getResource();
            const tid = pipeline.getTransformForType('fetch');
            const args = pipeline.getTransformArguments(tid);
            if (args.source !== harvestURL) {
              args.source = harvestURL;
              pipeline.setTransformArguments(tid, args);
              await pipeline.commit();
            }
          }
        } catch(e) {
          console.warn(`Context with id ${contextEntry.getId()} ("${contextEntry.getMetadata()
            .findFirstValue(null, 'dcterms:title')}") is not a catalog.`);
        }
      }
    });
};


const terminology2Dataset = async () => {

  const catalog = await esu.getEntryByType('dcat:Catalog', kindConfig.context);
  const datasets = [];
  await es.newSolrQuery().context(kindConfig.context).rdfType('dcat:Dataset').forEach(e => datasets.push(e));

  await es.newSolrQuery()
    .publicRead(true)
    .rdfType('skos:ConceptScheme')
    //    .literalProperty('dcterms:identifier', '*')
    .forEach(async skosEntry => {
      const harvestURL = `${base}/${skosEntry.getId()}/metadata/${catalogEntry.getId()}?recursive=dcat`;
      // TODO add dataset per terminology
    });
};

/** Main logic */
authenticate().then(async () => {
  if (!kindConfig) return;

  switch (kindConfig.type) {
    case 'externalContexts':
      await externalContextSource();
      break;
    case 'datasets':
    default:
      await datasetSource();
  }
}).then(() => {
  logger.info('Pipeline(s) synchronized from datasets');
}, logError);
