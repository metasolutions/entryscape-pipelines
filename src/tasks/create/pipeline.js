const globals = require('../../utils/globals');
const authenticate = require('../../utils/authenticate');
const config = require('../../config');
const { promiseUtil, namespaces } = require('@entryscape/entrystore-js');
const logger = require('../../utils/logger/logger');

const dcat = require('../../transforms/recipes/dcat');
const inspire = require('../../transforms/recipes/inspire');
const ckan = require('../../transforms/recipes/ckan');
const custom = require('../../transforms/recipes/custom');

const fs = require('fs');

const es = globals.getEntryStore();
namespaces.add('dcat', 'http://www.w3.org/ns/dcat#');
namespaces.add('esterms', 'http://entryscape.com/terms/');

const filePath = process.argv[3];
const data = process.argv[4];
const type = process.argv[5];
const keywords = process.argv[6].split(';');
const pipelineFile = process.argv[7];

if (filePath === '' && data === '') {
  logger.error('No filepath or data provided. User `pl create --help`, exiting...');
  process.exit(0);
}

const logError = (err) => {
  logger.error(err);
  throw err;
};

/**
 * @param row
 * @return {null|Object}
 */
const parseRow = (row) => {
  const arr = row.split(';');
  const name = arr[0];
  const email = arr[1];
  const id = arr[2];
  return { name, email, id };
};

const getHomeContext = (groupEntry) => {
  const ctxId = groupEntry.getResource(true).getHomeContext();
  return es.getContextById(ctxId);
};

/**
 * Create a common pipeline
 *
 * @param groupEntry
 * @param row
 * @return {*}
 */
const createPipeline = (groupEntry, row) => {
  const { name, email, id } = parseRow(row);
  const ctx = getHomeContext(groupEntry);
  const pp = ctx.newPipeline();
  pp.addL('dcterms:identifier', id);
  pp.addL('dcterms:title', name);
  keywords.forEach(key => pp.addL('dcterms:subject', key));
  const isEmail = require('isemail');
  if (email != null && isEmail.validate(email)) {
    pp.add('foaf:mbox', `mailto:${email}`);
  }
  switch (type) {
    case 'inspire':
      return inspire(pp, row);
    case 'ckan':
      return ckan(pp, row);
    case 'custom':
      return custom(pp, pipelineFile);
    case 'dcat':
    default:
      return dcat(pp, row);
  }
};

/**
 * Create a new user with
 */
const createUser = async (groupEntry, email, name) => {
  const giveUserAclFromGroup = (userEntry) => {
    const ei = groupEntry.getEntryInfo();
    const acl = ei.getACL(true);
    acl.rwrite.push(userEntry.getId());
    acl.mread.push('_guest');
    ei.setACL(acl);
    return Promise.all([
      groupEntry.getResource(true).addEntry(userEntry),
      ei.commit(true)]);
  };

  let userprom;
  const userCheckData = await es.getREST().get(`${es.getBaseURI()}_principals?entryname=${email}`);
  if (userCheckData.length > 0) {
    // return existing userEntry
    logger.info(`User with email ${email} already existed, reusing it`)
    const userEntryId = userCheckData[0];
    userprom = es.getContextById('_principals').getEntryById(userEntryId);
  } else {
    // Create new userEntry
    userprom = es.newUser(email).addL('foaf:givenName', name).commit().then((ue) => {
      logger.info(`created user with email ${email}`);
      return ue;
    }, (err) => {
      logger.error(`Failed to create user for ${email}, it probably already exists`);
      throw err;
    });
  }
  return userprom.then(giveUserAclFromGroup, logError);
};

const addContextType = (ctxEntry) => {
  const ei = ctxEntry.getEntryInfo();
  switch (type) {
    case 'custom':
      ei.getGraph().add(ctxEntry.getResourceURI(), 'rdf:type', 'esterms:WorkbenchContext');
      break;
    case 'inspire':
    case 'ckan':
    case 'dcat':
    default:
      ei.getGraph().add(ctxEntry.getResourceURI(), 'rdf:type', 'esterms:CatalogContext');
  }
};

const setContextAclAndTitle = async (groupEntry, name, id) => {
  const ctx = getHomeContext(groupEntry);
  const ctxEntryURI = ctx.getEntryURI();
  const ctxEntry = await ctx.getEntryStore().getEntry(ctxEntryURI, {forceLoad: true});
  addContextType(ctxEntry);
  const ei = ctxEntry.getEntryInfo();
  const acl = ei.getACL(true);
  acl.rread.push('_guest');
  acl.mread.push('_guest');
  acl.mread.push(groupEntry.getId());
  ei.setACL(acl);
  try {
    await ei.commit(true);
    ctxEntry.addL('dcterms:title', name);
    if (id != null && id !== '') {
      ctxEntry.addL('dcterms:identifier', id);
    }
    try {
      await ctxEntry.commitMetadata(true);
    } catch (err2) {
      logger.error('Failed update context title');
    }
  } catch (err) {
    logger.error('Failed update context acl and context type');
    throw err;
  }
};

/**
 * Main logic, creates group, context, users, catalog and pipeline
 *
 * @param {String} CSV like row
 * @return {function()}
 */
const createAll = async (row) => {
  const parsedRow = parseRow(row);
  if (parsedRow) {
    const { name, email, id } = parseRow(row);

    const groupEntry = await es.createGroupAndContext();
    groupEntry.getMetadata().addL(groupEntry.getResourceURI(), 'foaf:name', name);
    try {
      await groupEntry.commitMetadata(true);
    } catch (err) {
      logger.warn(`Could not update metadata for group: ${err}`);
    }
    if (email !== '') {
      /** create user */
      try {
        await createUser(groupEntry, email, name);
      } catch (err) {
        logger.error(`Could not create user: ${err}`);
      }
    }

    /** set info for context */
    try {
      await setContextAclAndTitle(groupEntry, name, id);
    } catch (err) {
      logger.error(`Could not update contextacl and title${err}`);
    }

    /** create a common pipeline */
    try {
      await createPipeline(groupEntry, row);
    } catch (err) {
      logger.error(`Could not create pipeline${err}`);
    }

    logger.info(`Created new pipeline. DATA:\n${JSON.stringify(parseRow(row), null, 2)}`);
  }
};

authenticate().then(() => {
  if (filePath) {
    fs.readFile(filePath, (err, rows) => {
      const dataStr = String(rows);
      const allTextLines = dataStr.split(/\r\n|\n/);

      let counter = 0;
      promiseUtil.forEach(allTextLines, (line) => {
        if (!(line === undefined || line === '' || line.trim() === '')) {
          counter += 1;
          return createAll(line).then(null, (e) => {
            logger.error(`Something went wrong with line ${counter}`);
            logger.error(e.message);
            throw e;
          });
        }
        return undefined;
      });
    });
  } else {
    createAll(data);
  }
});
