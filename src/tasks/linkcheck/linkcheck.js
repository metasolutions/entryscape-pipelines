const { main: linkCheck } = require('../../utils/linkchecker/linkChecker');

linkCheck().then(() => {
  process.exit();
}, (err) => {
  process.exit(1);
});
