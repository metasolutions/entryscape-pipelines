const { exportLinks } = require('../../utils/linkchecker/linkExporter');

const arg = process.argv[2];
const context = arg !== 'undefined' ? arg : null; // Fork has passed string args
exportLinks(context).then(() => {
  process.exit();
}, (err) => {
  process.exit(1);
});