const { report  } = require('../../utils/linkchecker/linkReporter');

report().then(() => {
  process.exit();
}, (err) => {
  process.exit(1);
});
