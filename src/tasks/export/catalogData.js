const globals = require('../../utils/globals');
const fs = require('fs');

const es = globals.getEntryStore();

const catalogs = [];
const query = es.newSolrQuery()
  .rdfType('dcat:Catalog')
  .publicRead(true)
  .uriProperty('dcat:dataset', '*')
  .forEach((catalogEntry) => {
    catalogs.push({
      entry: catalogEntry,
      label: catalogEntry.getMetadata().findFirstValue(null, 'dcterms:title'),
      nr: catalogEntry.getMetadata().find(catalogEntry.getResourceURI(), 'dcat:dataset').length,
    });
  })
  .then(() => {
    catalogs.sort((c1, c2) => (c1.nr < c2.nr ? 1 : -1));
    let datasetCount = 0;
    catalogs.forEach(c => datasetCount += c.nr);
    const data = {
      labels: catalogs.map(o => o.label),
      series: [catalogs.map(o => o.nr)],
      datasetCount
    };

    const dir = '../output/catalog/';
    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir);
    }
    const datetime = new Date().toISOString();
    const jsonPathDate = `${dir}data${datetime}.json`;
    const jsonPath = `${dir}data.json`;
    const jsonPathDateFd = fs.openSync(jsonPathDate, 'w');
    fs.writeSync(jsonPathDateFd, JSON.stringify(data, null, '  '), 0, 'utf8');
    fs.copyFileSync(jsonPathDate, jsonPath);

    process.exit(0);
  });
