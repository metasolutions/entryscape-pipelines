const globals = require('../../utils/globals');
const fs = require('fs');

const es = globals.getEntryStore();

const themes = [
  {
    "value": "http://publications.europa.eu/resource/authority/data-theme/AGRI",
    "label": {
      "en": "Agriculture, fisheries, forestry and food",
      "sv": "Jordbruk, fiske, skogsbruk och livsmedel",
      "nb": "Jordbruk, fiskeri, skogbruk og mat",
      "da": "Landbrug, fiskeri, skovbrug og mad",
      "de": "Landwirtschaft, Fischerei, Forstwirtschaft und Nahrungsmittel"
    }
  },
  {
    "value": "http://publications.europa.eu/resource/authority/data-theme/ECON",
    "label": {
      "en": "Economy and finance",
      "sv": "Ekonomi och finans",
      "nb": "Økonomi og finans",
      "da": "Økonomi og finans",
      "de": "Wirtschaft und Finanzen"
    }
  },
  {
    "value": "http://publications.europa.eu/resource/authority/data-theme/EDUC",
    "label": {
      "en": "Education, culture and sport",
      "sv": "Utbildning, kultur och sport",
      "nb": "Utdanning, kultur og sport",
      "da": "Uddannelse, kultur og sport",
      "de": "Bildung, Kultur und Sport"
    }
  },
  {
    "value": "http://publications.europa.eu/resource/authority/data-theme/ENER",
    "label": {
      "en": "Energy",
      "sv": "Energi",
      "nb": "Energi",
      "da": "Energi",
      "de": "Energie"
    }
  },
  {
    "value": "http://publications.europa.eu/resource/authority/data-theme/ENVI",
    "label": {
      "en": "Environment",
      "sv": "Miljö",
      "nb": "Miljø",
      "da": "Miljø",
      "de": "Umwelt"
    }
  },
  {
    "value": "http://publications.europa.eu/resource/authority/data-theme/GOVE",
    "label": {
      "en": "Government and public sector",
      "sv": "Regeringen och den offentliga sektorn",
      "nb": "Forvaltning og offentlig sektor",
      "da": "Forvaltning og offentlig sektor",
      "de": "Regierung und öffentlicher Sektor"
    }
  },
  {
    "value": "http://publications.europa.eu/resource/authority/data-theme/HEAL",
    "label": {
      "en": "Health",
      "sv": "Hälsa",
      "nb": "Helse",
      "da": "Helbred",
      "de": "Gesundheit"
    }
  },
  {
    "value": "http://publications.europa.eu/resource/authority/data-theme/INTR",
    "label": {
      "en": "International issues",
      "sv": "Internationella frågor",
      "nb": "Internasjonale temaer",
      "da": "Internationale forhold",
      "de": "Internationale Angelegenheiten"
    }
  },
  {
    "value": "http://publications.europa.eu/resource/authority/data-theme/JUST",
    "label": {
      "en": "Justice, legal system and public safety",
      "sv": "Rättvisa, rättsliga system och allmän säkerhet",
      "nb": "Justis, rettssystem og allmenn sikkerhet",
      "da": "Justits, rettssystem og almen sikkerhed",
      "de": "Justiz, Rechtssystem und öffentliche Sicherheit"
    }
  },
  {
    "value": "http://publications.europa.eu/resource/authority/data-theme/SOCI",
    "label": {
      "en": "Population and society",
      "sv": "Befolkning och samhälle",
      "nb": "Befolkning og samfunn",
      "da": "Befolkning og samfund",
      "de": "Bevölkerung und Gesellschaft"
    }
  },
  {
    "value": "http://publications.europa.eu/resource/authority/data-theme/REGI",
    "label": {
      "en": "Regions and cities",
      "sv": "Regioner och städer",
      "nb": "Regioner og byer",
      "da": "Regioner og byer",
      "de": "Regionen und Städte"
    }
  },
  {
    "value": "http://publications.europa.eu/resource/authority/data-theme/TECH",
    "label": {
      "en": "Science and technology",
      "sv": "Vetenskap och teknik",
      "nb": "Vitenskap og teknologi",
      "da": "Videnskab og teknologi",
      "de": "Wissenschaft und Technologie"
    }
  },
  {
    "value": "http://publications.europa.eu/resource/authority/data-theme/TRAN",
    "label": {
      "en": "Transport",
      "sv": "Transport",
      "nb": "Transport",
      "da": "Transport",
      "de": "Verkehr"
    }
  }
];
const _tm = {
  100142: 'GOVE', // 04 POLITICS
  100143: 'INTR', // 08 INTERNATIONAL RELATIONS
  100144: 'GOVE', // 10 EUROPEAN UNION
  100145: 'JUST', // 12 LAW
  100146: 'ECON', // 16 ECONOMICS
  100147: 'ECON', // 20 TRADE
  100148: 'ECON', // 24 FINANCE
  100149: 'SOCI', // 28 SOCIAL QUESTIONS
  100150: 'EDUC', // 32 EDUCATION AND COMMUNICATIONS
  100151: 'TECH', // 36 SCIENCE
  100152: 'ECON', // 40 BUSINESS AND COMPETITION
  100153: 'SOCI', // 44 EMPLOYMENT AND WORKING CONDITIONS
  100154: 'TRAN', // 48 TRANSPORT
  100155: 'ENVI', // 52 ENVIRONMENT
  100156: 'AGRI', // 56 AGRICULTURE, FORESTRY AND FISHERIES
  100157: 'AGRI', // 60 AGRI-FOODSTUFFS
  100158: 'TECH', // 64 PRODUCTION, TECHNOLOGY AND RESEARCH
  100159: 'ENER', // 66 ENERGY
  100160: 'ECON', // 68 INDUSTRY   -- not a good match
  100161: 'REGI', // 72 GEOGRAPHY  -- not a good match
  100162: 'INTR',  // 76 INTERNATIONAL ORGANISATIONS
};

const tc = {};
themes.forEach((t) => {
  tc[t.value] = 0;
});

const tm = {};
Object.keys(_tm).forEach((key) => {
  tm[`http://eurovoc.europa.eu/${key}`] = `http://publications.europa.eu/resource/authority/data-theme/${_tm[key]}`;
});

const getTheme = (stmt) => {
  const theme = stmt.getValue();
  if (tc[theme] != null) {
    return theme;
  } else if (tm[theme]) {
    return tm[theme];
  }
  return undefined;
};

const language = process.argv[2];

const themeLabels = {};
themes.forEach((t) => {
  themeLabels[t.value] = t.label[language];
});

es.newSolrQuery()
  .rdfType(['http://entryscape.com/terms/IndependentDataService', 'dcat:Dataset']) // Dataservices and datasets
  .publicRead(true)
  .forEach((d) => {
    d.getMetadata().find(null, 'dcat:theme').forEach((stmt) => {
      const theme = getTheme(stmt);
      if (theme) {
        tc[theme] += 1;
      }
    });
  })
  .then(() => {
    const ts = Object.keys(tc);
//    ts.sort((l1, l2) => (tc[l1] < tc[l2] ? 1 : -1));
    const data = {
      labels: ts.map(t => themeLabels[t]),
      series: ts.map(t => tc[t]),
      values: ts,
    };


    const dir = `../output/theme/${language}/`;
    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir);
    }
    const datetime = new Date().toISOString();
    const jsonPathDate = `${dir}data${datetime}.json`;
    const jsonPath = `${dir}data.json`;
    const jsonPathDateFd = fs.openSync(jsonPathDate, 'w');
    fs.writeSync(jsonPathDateFd, JSON.stringify(data, null, '  '), 0, 'utf8');
    fs.copyFileSync(jsonPathDate, jsonPath);
    process.exit(0);
  });
