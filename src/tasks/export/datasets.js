const globals = require('../../utils/globals');
const authenticate = require('../../utils/authenticate');
const { Graph, converters } = require('@entryscape/rdfjson');
const { namespaces } = require('@entryscape/entrystore-js');
const config = require('../../config');
const fs = require('fs');

namespaces.add('dcat', 'http://www.w3.org/ns/dcat#');
const es = globals.getEntryStore();
const totGraph = new Graph();
const dir = '../output/datasets/';
if (!fs.existsSync(dir)) {
  fs.mkdirSync(dir);
}
const datetime = new Date().toISOString();
const jsonPathDate = `${dir}data${datetime}.json`;
const jsonPath = `${dir}data.json`;
const rdfPathDate = `${dir}data${datetime}.rdf`;
const rdfPath = `${dir}data.rdf`;

authenticate().then(() => {
  es.newSolrQuery().publicRead(true).rdfType([
    'dcat:Dataset',
    'dcat:DataService',
    'dcat:Distribution',
    'vcard:Kind',
    'vcard:Individual',
    'vcard:Organization',
    'foaf:Agent',
    'foaf:Person',
    'foaf:Organization',
  ]).list().forEach((entry, idx) => {
    if (Array.isArray(config.exportExclude)
      && config.exportExclude.indexOf(entry.getContext().getId()) !== -1) {
      return;
    }

    // In case dataset and formats and licenses have been duplicated on the dataset level,
    // remove them before exporting!
    const md = entry.getMetadata();
    const ruri = entry.getResourceURI();
    if (md.find(ruri, 'rdf:type', 'dcat:Dataset').length > 0) {
      md.findAndRemove(ruri, 'dcterms:format');
      md.findAndRemove(ruri, 'dcterms:license');
    }
    totGraph.addAll(md);
  })
    .then(() => {
      const jsonPathDateFd = fs.openSync(jsonPathDate, 'w');
      fs.writeSync(jsonPathDateFd, JSON.stringify(totGraph.exportRDFJSON(), null, '  '), 0, 'utf8');
      fs.copyFileSync(jsonPathDate, jsonPath);
      const rdfPathDateFd = fs.openSync(rdfPathDate, 'w');
      fs.writeSync(rdfPathDateFd, converters.rdfjson2rdfxml(totGraph), 0, 'utf8');
      fs.copyFileSync(rdfPathDate, rdfPath);
    });
});
