const globals = require('../../utils/globals');
const config = require('../../config');
const fs = require('fs');
const { namespaces, types, terms } = require('@entryscape/entrystore-js');
const es = globals.getEntryStore();
namespaces.add('st', 'http://entrystore.org/terms/statistics#');

const query = es.newSolrQuery()
  .rdfType('st:CatalogStatistics')
  .tagLiteral('newMonth')
  .sort('created+desc')
  .limit(30)
  .getEntries(0).then((entries) => {
    const data = [];
    entries.forEach((statsEntry) => {
      const md = statsEntry.getMetadata();
      const s = statsEntry.getResourceURI();
      let amount;
      const residentDatasetCount = md.findFirstValue(s, 'st:residentPublicDatasetCount');
      if (residentDatasetCount) {
        amount = parseInt(residentDatasetCount, 10);
      } else {
        const psiCount = parseInt(md.findFirstValue(s, 'st:psiDatasetCount'), 10);
        const otherCount = parseInt(md.findFirstValue(s, 'st:otherDatasetCount'), 10);
        amount = psiCount + otherCount + (config.datasetHistoryHarvestOffset || 0);
      }
      data.push({x: statsEntry.getEntryInfo().getCreationDate().toISOString(), y: amount});
    });
    const dir = '../output/datasetHistory/';
    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir);
    }
    const datetime = new Date().toISOString();
    const jsonPathDate = `${dir}data${datetime}.json`;
    const jsonPath = `${dir}data.json`;
    const jsonPathDateFd = fs.openSync(jsonPathDate, 'w');
    fs.writeSync(jsonPathDateFd, JSON.stringify(data, null, '  '), 0, 'utf8');
    fs.copyFileSync(jsonPathDate, jsonPath);
    process.exit(0);
  });
