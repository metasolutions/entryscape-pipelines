const globals = require('../../utils/globals');
const config = require('../../config');
const fs = require('fs');
const es = globals.getEntryStore();

const f = (types) => {
  let size = 0;
  const list = es.newSolrQuery().publicRead(true).rdfType(types).list();
  return list.getEntries(0).then(() => list.getSize());
};

const entities = Object.keys(config.entityExport);
Promise.all(entities.map((entity => f(config.entityExport[entity])))).then((numbers) => {
  const data = {};
  entities.forEach((entity, idx) => {
    data[entity] = numbers[idx];
  });
  const dir = '../output/entity/';
  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir);
  }
  const datetime = new Date().toISOString();
  const jsonPathDate = `${dir}data${datetime}.json`;
  const jsonPath = `${dir}data.json`;
  const jsonPathDateFd = fs.openSync(jsonPathDate, 'w');
  fs.writeSync(jsonPathDateFd, JSON.stringify(data, null, '  '), 0, 'utf8');
  fs.copyFileSync(jsonPathDate, jsonPath);
  process.exit(0);
});
