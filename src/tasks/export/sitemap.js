const globals = require('../../utils/globals');
const authenticate = require('../../utils/authenticate');
const config = require('../../config');
const fs = require('fs');

const es = globals.getEntryStore();
const dir = '../output/sitemap/';
if (!fs.existsSync(dir)) {
  fs.mkdirSync(dir);
}
const datetime = new Date().toISOString();
const jsonPathDate = `${dir}data${datetime}.json`;
const jsonPath = `${dir}data.json`;
const xmlPathDate = `${dir}data${datetime}.xml`;
const xmlPath = `${dir}data.xml`;

authenticate().then(() => {
  const json = [];
  const sitemap = [];
  es.newSolrQuery().publicRead(true).rdfType(config.sitemapType).forEach((entry, idx) => {
    if (Array.isArray(config.exportExclude)
      && config.exportExclude.indexOf(entry.getContext().getId()) !== -1) {
      return;
    }
    const type = entry.getMetadata().findFirstValue(entry.getResourceURI(), 'rdf:type');
    const ei = entry.getEntryInfo();
    const eid = entry.getId();
    const cid = entry.getContext().getId();
    const modified = ei.getModificationDate();
    const created = ei.getCreationDate();
    json.push({ eid, cid, modified, created, type, resource: entry.getResourceURI() });
    if (config.toSitemapURL) {
      sitemap.push(`<url><loc>${config.toSitemapURL(cid, eid, type)}</loc><lastmod>${modified.toISOString()}</lastmod></url>`);
    }
  }).then(() => {
      const jsonPathDateFd = fs.openSync(jsonPathDate, 'w');
      fs.writeSync(jsonPathDateFd, JSON.stringify(json, null, '  '), 0, 'utf8');
      fs.copyFileSync(jsonPathDate, jsonPath);
    if (config.toSitemapURL) {
      const xmlPathDateFd = fs.openSync(xmlPathDate, 'w');
      fs.writeSync(xmlPathDateFd, `<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
${sitemap.join('\n')}
</urlset>`, 0, 'utf8');
      fs.copyFileSync(xmlPathDate, xmlPath);
    }
  });
});
