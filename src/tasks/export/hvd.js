const globals = require('../../utils/globals');
const fs = require('fs');
const { traverseExport } = require('@entryscape/entrysync');
const { Graph } = require('@entryscape/rdfjson');
const logger = require('../../utils/logger/logger');
const { prune, report, saveToFile } = require('../../utils/traverse');
const es = globals.getEntryStore();

const dir = '../output/hvd/';
if (!fs.existsSync(dir)) {
  fs.mkdirSync(dir);
}

const allowedTypes = [
    'http://www.w3.org/ns/dcat#Dataset',
    'http://www.w3.org/ns/dcat#DatasetSeries',
    'http://www.w3.org/ns/dcat#DataService'
];
const types = [
    'dcat:Catalog', 
    'dcat:Dataset',
    'dcat:DatasetSeries', 
    'dcat:DataService', 
    'dcat:Distribution', 
    'foaf:Agent', 
    'vcard:Organization'
];
const properties = [
    'dcat:dataset',
    'dcat:inSeries',
    'dcat:service',
    'dcat:DataService', 
    'dcat:accessService',
    'dcat:distribution', 
    'dcterms:publisher', 
    'dcterms:creator', 
    'dcat:contactPoint', 
    'prov:agent'
];

const query = es.newSolrQuery().rdfType(['dcat:Catalog']);
//const query = es.newSolrQuery().context(['1494','158']).rdfType(['dcat:Catalog']);

const preSync = (entry, metadata) => {
    const resourceURI = entry.getResourceURI();
    const types = metadata.find(resourceURI, 'rdf:type').map(t=>t.getValue());
    if (types.find((t) => allowedTypes.includes(t)))    {
        const hvd = metadata.findFirstValue(resourceURI, 'http://data.europa.eu/r5r/applicableLegislation');
        if (!hvd) return false;
    }
    return metadata;
};


const main = async () => {
    logger.info('Start of hvd export');
    const destinationGraph = new Graph();
    const traverseReport = await traverseExport({ query, types, destinationGraph, properties, preSync});
    logger.info(traverseReport);

    prune(destinationGraph, types);
    report(destinationGraph, types, 'Pruning results:');
    
    saveToFile(destinationGraph, dir+'hvd');
    logger.info('End of hvd export');
};


main();