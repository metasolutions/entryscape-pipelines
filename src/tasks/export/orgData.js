const globals = require('../../utils/globals');
const fs = require('fs');
const logger = require('../../utils/logger/logger');

const es = globals.getEntryStore();

const findFoafName = (graph) => {
  let name;
  graph.find(null, 'foaf:name').forEach(stmt => {
    if (!name && stmt.getValue() !== '') {
      name = stmt.getValue().trim();
    }
  });
  return name;
};

const agentNames = {};
const agentCount = {};
let datasetCount = 0;
let independentDataserviceCount = 0;
const query = es.newSolrQuery()
  .rdfType(['foaf:Agent', 'foaf:Person', 'foaf:Organization'])
  .publicRead(true)
  .forEach((agentEntry) => {
    agentNames[agentEntry.getResourceURI()] = findFoafName(agentEntry.getMetadata()) || '';
  })
  .then(() => es.newSolrQuery()
    .rdfType('http://entryscape.com/terms/IndependentDataService')
    .publicRead(true)
    .forEach((dataserviceEntry) => {
      independentDataserviceCount += 1;
      const title = dataserviceEntry.getMetadata().findFirstValue(dataserviceEntry.getResourceURI(), 'dcterms:title');
      const stmts = dataserviceEntry.getMetadata().find(dataserviceEntry.getResourceURI(), 'dcterms:publisher');
      if (stmts.length != 1) {
        logger.info(`Dataservice ${title} has ${stmts.length} publishers!`);
      }
      stmts.forEach((stmt, idx) => {
        const puri = stmt.getValue();
        agentCount[puri] = (agentCount[puri] || 0) + 1;
        if (!agentNames[puri]) {
          logger.info(`Publisher name can not be found for dataset ${title}`);
        }
      });
    }))
  .then(() => es.newSolrQuery()
    .rdfType('dcat:Dataset')
    .publicRead(true)
    .forEach((datasetEntry) => {
      datasetCount += 1;
      const title = datasetEntry.getMetadata().findFirstValue(datasetEntry.getResourceURI(), 'dcterms:title');
      const stmts = datasetEntry.getMetadata().find(datasetEntry.getResourceURI(), 'dcterms:publisher');
      if (stmts.length != 1) {
        logger.info(`Dataset ${title} has ${stmts.length} publishers!`);
      }
      stmts.forEach((stmt, idx) => {
        const puri = stmt.getValue();
        agentCount[puri] = (agentCount[puri] || 0) + 1;
        if (!agentNames[puri]) {
          logger.info(`Publisher name can not be found for dataset ${title}`);
        }
      });
    }))
  .then(() => {
    const agents = Object.keys(agentCount).map(puri => (
      {
        label: agentNames[puri] || '',
        nr: agentCount[puri],
        value: puri
      }));
    agents.sort((c1, c2) => (c1.nr < c2.nr ? 1 : -1));
    let publisherCount = agents.length;
    const data = {
      labels: agents.map(o => o.label),
      series: [agents.map(o => o.nr)],
      values: agents.map(o => o.value),
      datasetCount,
      independentDataserviceCount,
      publisherCount
    };
    const dir = '../output/org/';
    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir);
    }
    const datetime = new Date().toISOString();
    const jsonPathDate = `${dir}data${datetime}.json`;
    const jsonPath = `${dir}data.json`;
    const jsonPathDateFd = fs.openSync(jsonPathDate, 'w');
    fs.writeSync(jsonPathDateFd, JSON.stringify(data, null, '  '), 0, 'utf8');
    fs.copyFileSync(jsonPathDate, jsonPath);
    process.exit(0);
  });
