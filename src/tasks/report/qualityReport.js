const globals = require('../../utils/globals');
const authenticate = require('../../utils/authenticate');
const { namespaces, types, terms} = require('@entryscape/entrystore-js');


const checkLanguage = (md, s, prop) => {
  const langs = new Set();
  let noLang = false;
  const stmts = md.find(s, prop);
  stmts.forEach((stmt) => {
    const lang = stmt.getLanguage();
    langs.add(lang);
    if (!lang) {
      noLang = true;
    }
  });
  return [langs.size, noLang, stmts.length];
};

/**
 *
 * @param {store/EntryStore} entrystore
 */
module.exports = async (entrystore, id2ContextLabel) => {
  const publishers = {};
  const contactpoints = {};
  const formats = {};
  const licenses = {};
  let totalDatasetCount = 0;
  let hasTitle = 0;
  let multilingualTitle = 0;
  let missingLanguageOnTitle = 0;
  let hasDescription = 0;
  let multilingualDescription = 0;
  let missingLanguageOnDescription = 0;
  let hasPublisher = 0;
  let hasContactPoint = 0;
  let hasTheme = 0;
  let hasKeyword = 0;
  let mandatoryDatasets = 0;
  let recommendedDatasets = 0;
  const nrOfDistributionsPerDataset = {};

  let totalDistributionCount = 0;
  let hasDistributionFormat = 0;
  let hasDistributionLicense = 0;
  let hasDownloadURL = 0;
  let hasAccessURL = 0;
  let hasDistributionTitle = 0;
  let multilingualDistributionTitle = 0;
  let missingLanguageOnDistributionTitle = 0;
  let hasDistributionDescription = 0;
  let multilingualDistributionDescription = 0;
  let missingLanguageOnDistributionDescription = 0;
  let mandatoryDistributions = 0;
  let recommendedDistributions = 0;

  await entrystore.newSolrQuery().rdfType('dcat:Dataset').publicRead(true).forEach((de) => {
    const cid = de.getContext().getId();
    if (!id2ContextLabel.hasOwnProperty(cid)) {
      return;
    }
    totalDatasetCount += 1;
    const resURI = de.getResourceURI();
    const md = de.getMetadata();

    // Check title
    const [ titleLangCount, titleNoLang, amountTitle ] = checkLanguage(md, resURI, 'dcterms:title');
    if (amountTitle > 0) {
      hasTitle += 1;
    }
    if (titleLangCount > 1) {
      multilingualTitle += 1;
    }
    if (titleNoLang) {
      missingLanguageOnTitle += 1;
    }
    // Check Description
    const [ descLangCount, descNoLang, amountDesc] = checkLanguage(md, resURI, 'dcterms:description');
    if (amountDesc > 0) {
      hasDescription += 1;
    }
    if (descLangCount > 1) {
      multilingualDescription += 1;
    }
    if (descNoLang) {
      missingLanguageOnDescription += 1;
    }

    // Publishers
    const dPublishers = md.find(resURI, 'dcterms:publisher');
    if (dPublishers.length > 0) {
      hasPublisher += 1;
      dPublishers.forEach(p => (publishers[p.getValue()] = cid));
    }

    // ContactPoints
    const dContactPoints = md.find(resURI, 'dcat:contactPoint');
    if (dContactPoints.length > 0) {
      hasContactPoint += 1;
      dContactPoints.forEach(c => (contactpoints[c.getValue()] = cid));
    }

    // Theme
    const dThemes = md.find(resURI, 'dcat:theme');
    if (dThemes.length > 0) {
      hasTheme += 1;
    }

    // Keyword
    const dKeywords = md.find(resURI, 'dcat:keyword');
    if (dKeywords.length > 1) {
      hasKeyword += 1;
    }

    // Mandatory
    if (amountTitle > 0 && amountDesc > 0) {
      mandatoryDatasets += 1;
      // Recommended
      if (dThemes.length > 0 && dPublishers.length > 0 && dContactPoints.length > 0 && dThemes.length > 0 && dKeywords.length > 1) {
        recommendedDatasets += 1;
      }
    }

    const dists = md.find(resURI, 'dcat:distribution');
    nrOfDistributionsPerDataset[dists.length] = (nrOfDistributionsPerDataset[dists.length] || 0) + 1;
  });

  await entrystore.newSolrQuery().rdfType('dcat:Distribution').publicRead(true).forEach((de) => {
    const cid = de.getContext().getId();
    if (!id2ContextLabel.hasOwnProperty(cid)) {
      return;
    }
    totalDistributionCount += 1;
    const resURI = de.getResourceURI();
    const md = de.getMetadata();

    // Licenses
    const licenseStmts = md.find(resURI, 'dcterms:license');
    if (licenseStmts.length > 0) {
      hasDistributionLicense += 1;
      const fURI = licenseStmts[0].getValue();
      licenses[fURI] = (licenses[fURI] || 0) + 1;
    }

    // downloadURL
    const downloadURL = md.find(resURI, 'dcat:downloadURL');
    if (downloadURL.length > 0) {
      hasDownloadURL += 1;
    }

    // accessURL
    const accessURL = md.find(resURI, 'dcat:accessURL');
    if (accessURL.length > 0) {
      hasAccessURL += 1;
    }

    // Formats
    const formatStmts = md.find(resURI, 'dcterms:format');
    if (formatStmts.length > 0) {
      hasDistributionFormat += 1;
      const mt = formatStmts[0].getValue();
      formats[mt] = (formats[mt] || 0) + 1;
    }

    // Check title
    const [ titleLangCount, titleNoLang, amountTitle ] = checkLanguage(md, resURI, 'dcterms:title');
    if (amountTitle > 1) {
      hasDistributionTitle += 1;
    }
    if (titleLangCount > 1) {
      multilingualDistributionTitle += 1;
    }
    if (titleNoLang) {
      missingLanguageOnDistributionTitle += 1;
    }
    // Check Description
    const [ descLangCount, descNoLang, amountDesc] = checkLanguage(md, resURI, 'dcterms:description');
    if (amountDesc > 0) {
      hasDistributionDescription += 1;
    }
    if (descLangCount > 1) {
      multilingualDistributionDescription += 1;
    }
    if (descNoLang) {
      missingLanguageOnDistributionDescription += 1;
    }

    // Mandatory
    if (accessURL.length > 0) {
      mandatoryDistributions += 1;
      // Recommended
      if (amountDesc > 0 && formatStmts.length > 0 && licenseStmts.length > 0) {
        recommendedDistributions += 1;
      }
    }
  });

  return {
    totalDatasetCount,
    hasTitle,
    multilingualTitle,
    missingLanguageOnTitle,
    hasDescription,
    multilingualDescription,
    missingLanguageOnDescription,
    hasPublisher,
    hasContactPoint,
    hasTheme,
    hasKeyword,
    mandatoryDatasets,
    recommendedDatasets,
    totalDistributionCount,
    hasDistributionTitle,
    multilingualDistributionTitle,
    missingLanguageOnDistributionTitle,
    hasDistributionDescription,
    multilingualDistributionDescription,
    missingLanguageOnDistributionDescription,
    hasDownloadURL,
    hasAccessURL,
    hasDistributionFormat,
    hasDistributionLicense,
    mandatoryDistributions,
    recommendedDistributions,
    nrOfContactPoints: Object.keys(contactpoints).length,
    nrOfPublishers: Object.keys(publishers).length,
    nrOfDistributionsPerDataset,
    formats,
    licenses
  };
};
