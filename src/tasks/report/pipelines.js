const globals = require('../../utils/globals');
const authenticate = require('../../utils/authenticate');
const { namespaces, types, terms} = require('@entryscape/entrystore-js');
const qualityReport = require('./qualityReport');
const fs = require('fs');
const logger = require('../../utils/logger/logger');

/** @type {store/EntryStore} */
const es = globals.getEntryStore();

const ensureReportContextExists = () => es.getContextById('catalogsreport').getEntry()
  .then(null, () => {
    const cpe = es.newContext('catalogsreport', 'catalogsreport');
    const ei = cpe.getEntryInfo();
    const acl = ei.getACL(true);
    acl.rread.push('_guest');
    acl.mread.push('_guest');
    ei.setACL(acl);
    return cpe.commit();
  });

namespaces.add('pr', 'http://entrystore.org/terms/pipelineresult#');
namespaces.add('st', 'http://entrystore.org/terms/statistics#');

const doReport = async (contextEntry) => {

  const id2ContextLabel = {};
  await es.newSolrQuery().graphType(types.GT_CONTEXT).forEach(ce => {
    if (ce.isPublic()) {
      id2ContextLabel[ce.getId()] = ce.getMetadata().findFirstValue(ce.getResourceURI(), 'dcterms:title');
    }
  });

  const quality = await qualityReport(es, id2ContextLabel);

  const allStatistics = [];
  await es.newSolrQuery().context('catalogstatistics')
    .rdfType('st:CatalogStatistics').sort('created+desc').forEach((e) => allStatistics.push(e));

  const statistics = allStatistics.slice(0, 90);
/*  const allStatistics = await es.newSolrQuery().context('catalogstatistics')
    .rdfType('st:CatalogStatistics').limit(100).list().getEntries();
  const statistics = allStatistics.slice(9);*/
  const datasetGrowth = [
    statistics.map((entry) => entry.getMetadata().findFirstValue(entry.getResourceURI(), 'st:psiDatasetCount')),
    statistics.map((entry) => entry.getMetadata().findFirstValue(entry.getResourceURI(), 'st:otherDatasetCount'))
  ];

  const md = statistics[0].getMetadata();
  const psiPage = parseInt(md.findFirstValue(null, 'st:psiPage'), 10);
  const psiPageAndDcat = parseInt(md.findFirstValue(null, 'st:psiPageAndDcat'), 10);
  const psiDcat = parseInt(md.findFirstValue(null, 'st:psiDcat'), 10);
  const psiFailed = parseInt(md.findFirstValue(null, 'st:psiFailed'), 10);

  const inContextPath = namespaces.expand('st:datasets_in_context_');
  const catalogPresence = statistics.map((entry) => {
    const md = entry.getMetadata();
    const s = entry.getResourceURI();
    const stmts = md.find(s).filter((stmt) => stmt.getPredicate().startsWith(inContextPath));
    const c2d = {};
    stmts.forEach((stmt) => {
      const cat = stmt.getPredicate().substr(inContextPath.length);
      if (id2ContextLabel.hasOwnProperty(cat)) {
        c2d[cat] = parseInt(stmt.getValue());
      }
    });
    return c2d;
  });
  const catalogGrowth = [
    statistics.map((entry) => {
      const md = entry.getMetadata();
      const s = entry.getResourceURI();
      return parseInt(md.findFirstValue(s, 'st:psiPageAndDcat'), 10) +
        parseInt(md.findFirstValue(s, 'st:psiDcat'), 10)
    }),
    statistics.map((entry) => parseInt(entry.getMetadata().findFirstValue(entry.getResourceURI(),
      'st:otherDcat'), 10))
  ];
  statistics.map((entry) => {
    const md = entry.getMetadata();
    const s = entry.getResourceURI();
    const stmts = md.find(s).filter((stmt) => stmt.getPredicate().startsWith(inContextPath));
    const c2d = {};
    stmts.forEach((stmt) => {
      const cat = stmt.getPredicate().substr(inContextPath.length);
      if (id2ContextLabel.hasOwnProperty(cat)) {
        c2d[cat] = parseInt(stmt.getValue());
      }
    });
    return c2d;
  });
  const currentCatalogs = catalogPresence[0];
  const pastCatalogs = catalogPresence[catalogPresence.length - 1];
  const orderedCatalogs = Object.keys(currentCatalogs).sort((c1, c2) => {
    return currentCatalogs[c1] > currentCatalogs[c2] ? 1 : -1;
  });
  const catalogSeries = [
    orderedCatalogs.map(c => pastCatalogs[c] || 0),
    orderedCatalogs.map(c => currentCatalogs[c]),
  ];
  const addedCatalogs = orderedCatalogs.filter(c => pastCatalogs[c] === undefined);
  const removedCatalogs = Object.keys(pastCatalogs).filter(c => currentCatalogs[c] === undefined);

  logger.info("Missing labels: "+ orderedCatalogs.filter(c => !id2ContextLabel[c]));

  return {
    datasets: {
      growth: datasetGrowth,
      perCatalog: {
        labels: orderedCatalogs.map(c => id2ContextLabel[c]),
        data: catalogSeries,
      },
    },
    catalogs: {
      growth: catalogGrowth,
      latest: orderedCatalogs,
      added: addedCatalogs,
      removed: removedCatalogs,
      unchanged: orderedCatalogs.filter(c => (addedCatalogs.indexOf(c) === -1 && removedCatalogs.indexOf(c) === -1)),
      psiPage,
      psiPageAndDcat,
      psiDcat,
      psiFailed
    },
    quality,
    contextId2label: id2ContextLabel
  };
};

authenticate()
  //.then(ensureReportContextExists)
  .then(doReport)
  .then((report) => {
    fs.writeFileSync('report.js', "document.dcatReport = "+JSON.stringify(report, null, '  '));
  }, (err) => {
    logger.error(err.message);
  });
