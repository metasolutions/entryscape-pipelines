const globals = require('../../utils/globals');
const authenticate = require('../../utils/authenticate');
const { namespaces, types, terms} = require('@entryscape/entrystore-js');
const logger = require('../../utils/logger/logger');

/** @type {store/EntryStore} */
const es = globals.getEntryStore();

namespaces.add('pr', 'http://entrystore.org/terms/pipelineresult#');
namespaces.add('st', 'http://entrystore.org/terms/statistics#');

const doReport = async () => {

  const id2ContextLabel = {};
  await es.newSolrQuery().graphType(types.GT_CONTEXT).forEach(ce => {
    if (ce.isPublic()) {
      id2ContextLabel[ce.getId()] = ce.getMetadata().findFirstValue(ce.getResourceURI(), 'dcterms:title');
    }
  });

  const statistics = [];
  await es.newSolrQuery().tagLiteral('newMonth').context('catalogstatistics')
    .rdfType('st:CatalogStatistics').limit(90).forEach(s => statistics.push(s));


  const inContextPath = namespaces.expand('st:datasets_in_context_');
  const catalogPresence = statistics.map((entry) => {
    const md = entry.getMetadata();
    const s = entry.getResourceURI();
    const stmts = md.find(s).filter((stmt) => stmt.getPredicate().startsWith(inContextPath));
    const c2d = { __date: entry.getEntryInfo().getCreationDate()};
    stmts.forEach((stmt) => {
      const cat = stmt.getPredicate().substr(inContextPath.length);
      if (id2ContextLabel.hasOwnProperty(cat)) {
        c2d[cat] = parseInt(stmt.getValue());
      }
    });
    return c2d;
  });

  const id2pub = {};
  catalogPresence.forEach((r) => {
    const d = `${r.__date.getFullYear()}-${r.__date.getMonth()+1}`;
    Object.keys(r).forEach((key) => {
      if (key !== '__date') {
        if (!id2pub[key]) {
          id2pub[key] = {
            label: id2ContextLabel[key],
            months: {}
          };
        }
        id2pub[key].months[d] = r[key];
      }
    });
  });

  const months = catalogPresence.map((r) => {
    return `${r.__date.getFullYear()}-${r.__date.getMonth()+1}`;
  });

  const rows = Object.keys(id2pub).map((id) => {
    const obj = id2pub[id];
    const row = [obj.label, id];
    let first = true;
    months.forEach((m) => {
      row.push(obj.months[m] || '');
      if (obj.months[m] && first) {
        row.splice(2, 0, m);
        first = false;
      }
    });
    return row.join(', ');
  });

  const header = `name, id, first, ${months.join(', ')}\n`;

  return header + rows.join('\n');
};

authenticate()
  .then(doReport)
  .then((report) => {
    logger.info(report);
  }, (err) => {
    logger.error(err.message);
  });
