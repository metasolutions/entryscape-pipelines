const globals = require('../../utils/globals');
const authenticate = require('../../utils/authenticate');
const { types, terms } = require('@entryscape/entrystore-js');
const config = require('../../config');
const logger = require('../../utils/logger/logger');

const rmStatus = process.argv[3];

if (rmStatus === 'true') {
  const colors = require('colors');
  logger.info(colors.red('Invalid pipeline result status given. Filter must be either pending, inprogress, success, failed or all'));
  process.exit(-1);
}

/** @type {store/EntryStore} */
const es = globals.getEntryStore();

/** @type {store/EntryStoreUtil} */
const esUtil = globals.getEntryStoreUtil();

const deletePipelineResults = (status = null) => {
  /** construct query */
  const query = es.newSolrQuery().graphType(types.GT_PIPELINERESULT);
  switch (status) {
    case 'pending': {
      query.status(terms.status.Pending);
      break;
    }
    case 'success': {
      query.status(terms.status.Succeeded);
      break;
    }
    case 'failed': {
      query.status(terms.status.Failed);
      break;
    }
    case 'inprogress': {
      query.status(terms.status.InProgress);
      break;
    }
    default: {
      // noop
    }
  }

  /** remove all entries in the the search list */
  return esUtil.removeAll(query.list());
};

authenticate()
  .then(() => deletePipelineResults(rmStatus))
  .then(() => {
    const colors = require('colors');
    logger.info(`All pipelines results ${rmStatus !== 'all' ? ` with status=${colors.blue(rmStatus)},` : ''} were deleted!`);
  }, (err) => {
    logger.error(err.message);
  });
