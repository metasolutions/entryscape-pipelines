const globals = require('../../utils/globals');
const authenticate = require('../../utils/authenticate');
const { types, namespaces } = require('@entryscape/entrystore-js');
const config = require('../../config');
const fs = require('fs');
const logger = require('../../utils/logger/logger');

const es = globals.getEntryStore();
namespaces.add('dcat', 'http://www.w3.org/ns/dcat#');

const execAll = process.argv[3];
const execRURI = process.argv[4];
const execCtxId = process.argv[5];
const tags = process.argv[6] === '' ? [] : process.argv[6].split(',');

if (!(execAll || execRURI || execCtxId || tags.length >= 0)) {
  logger.warn('Missing parameter. Use `pl exec --help` exiting...');
  process.exit(0);
}

const logError = (err) => {
  logger.error(err.message);
  throw err;
};

/**
 * Get all pipelines in a context.
 * If no context given, search is not filtered by context.
 *
 * @param {int} ctxId
 * @return {Promise}
 */
const getPipelineEntriesByContext = (ctxId = 0) => {
  const query = es.newSolrQuery();
  if (ctxId) {
    const ctx = es.getContextById(ctxId);
    query.context(ctx);
  }
  if (tags.length > 0) {
    query.tagLiteral(tags);
  }
  return query.graphType(types.GT_PIPELINE);
};

/**
 * @param {String} ruri
 * @return {Promise}
 */
const getPipelineEntryByRURI = ruri => globals.getEntryStoreUtil().getEntryByResourceURI(ruri);

const ignore = (job) => {
  const cid = job.getContext().getId();
  let ignore = false;
  if (config.blacklist && config.blacklist.indexOf(cid) !== -1) ignore = true;
  else if (config.denylist && config.denylist.indexOf(cid) !== -1) ignore = true;
  if (ignore) logger.info(`Context ${cid} ignored due to deny listed`);
  return ignore;
}


/**
 * @param {store/Entry} entry
 * @return {Promise}
 */
const executePipeline = (entry) => {
  if (ignore(entry)) {
    return Promise.resolve(true);
  }
  logger.info(`Executing pipeline: ${entry.getMetadata().findFirstValue(entry.getResourceURI(),
    'dcterms:title')}`);
  return entry.getResource().then(pipeline => pipeline.execute()).then(null, (e) => {
    logger.info(`Failed to execute pipeline ${entry.getURI()}`);
  });
};

/** Main logic */
authenticate().then(() => {
  if (execAll) {
    return getPipelineEntriesByContext().forEach(executePipeline);
  } else if (execRURI) {
    return getPipelineEntryByRURI(execRURI).then(executePipeline);
  }
  // execCtxId
  return getPipelineEntriesByContext(execCtxId).forEach(executePipeline);
}).then(() => {
  logger.info('Pipeline(s) executed succesfully');
}, logError);
