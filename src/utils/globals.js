const config = require('../config');
const local = require('../local');
const { ItemStore, bundleLoader } = require('@entryscape/rdforms');
const { EntryStore, EntryStoreUtil } = require('@entryscape/entrystore-js');
const entryStore = config.entrystore = new EntryStore(local.repository || config.repository);
const entryStoreUtil = config.entrystoreutil = new EntryStoreUtil(entryStore);
const itemStore = new ItemStore();

const itemStorePromise = new Promise((resolve, reject) => {
  if (config.itemstore) {
    if (config.itemstore.bundles) {
      bundleLoader(itemStore, config.itemstore.bundles).then(() => {
        resolve(itemStore);
      });
    }
  }
});

/**
 * @module {dcat/utils/globals}
 */
const globals = {
  /**
   * @returns {store/EntryStore}
   */
  getEntryStore() {
    return entryStore;
  },
  /**
   * @returns {rdforms/template/ItemStore}
   */
  getItemStore() {
    return itemStorePromise;
  },
  /**
   * @returns {store/EntryStoreUtil}
   */
  getEntryStoreUtil() {
    return entryStoreUtil;
  },
};

module.exports = globals;
