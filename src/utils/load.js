const fs = require('fs');
const loadFromURL = require('./loadFromURL');
const logger = require('./logger/logger');

const loadFromFile = (source) => new Promise((resolve, reject) => {
  const path = `${__dirname}/../../input/sources/`;
  const file = path + source;
  try {
    const content = fs.readFileSync(file).toString();
    resolve(content);
  } catch (err) {
    logger.info(`File ${file} not found`);
    reject(err);
  }
});

const load = (source, urlParams) => {
  if (source.startsWith('http')) {
    if (urlParams) return loadFromURL(source, ...urlParams);
    return loadFromURL(source);
  }
  return loadFromFile(source);
};

module.exports = {
  load,
};
