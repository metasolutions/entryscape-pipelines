const { utils, Graph } = require('@entryscape/rdfjson');
const { promiseUtil, namespaces } = require('@entryscape/entrystore-js');
const dcatFix = require('./dcatFix');
const config = require('../config');
const md5 = require('md5');
const logger = require('./logger/logger');
const { EntrySync } = require('@entryscape/entrysync');

namespaces.add('prov', 'http://www.w3.org/ns/prov#');


module.exports = class DcatIndex {
  constructor(graph, base, context, dedup) {
    this._graph = graph;
    this._base = base;
    this._context = context;
    this._dedup = dedup;

    dcatFix.to11(graph);
    dcatFix.to2(graph);
    dcatFix.publisherAsKeyword(graph);
    dcatFix.accessRights(graph);
    dcatFix.removeNonURILicenses(graph);

    this._contacts = {};
    this._publishers = {};
    this._datasets = {};
    this._datasetSeries = {};
    this._dataservices = {};
    this._distributions = {};

    this.renameBlankDatasets();
    this.renameBlankDatasetSeries();
    this.renameBlankServices();
    this.renameBlankDistributions();
    this.extractPublishers();
    this.extractContacts();
    this.extractDataServices();
    this.extractDatasets();
    this.extractDatasetSeries();
    this.extractDistributions();
    this.extractCatalog();

    if (config.copyLicenseToDataset) {
      Object.keys(this._datasets).forEach((uri) => dcatFix.licenseOnDataset(uri, this._datasets[uri], this._distributions));
    }
    if (config.copyFormatToDataset) {
      Object.keys(this._datasets).forEach((uri) => dcatFix.formatOnDataset(uri, this._datasets[uri], this._distributions));
    }
  }

  async sync() {
    const csync = new EntrySync({context: this._context});
    const dsync = new EntrySync({context: this._context, dedup: this._dedup});
    const dssync = new EntrySync({context: this._context, dedup: this._dedup});
    const esync = new EntrySync({context: this._context});
    const ssync = new EntrySync({context: this._context});

    const extratypes = [
      'dcat:Distribution',
      'foaf:Agent',
      'foaf:Person',
      'foaf:Organization',
      'vcard:Individual',
      'vcard:Organization',
      'vcard:Kind'];
    const es = this._context.getEntryStore();

    // Sync Catalog
    await csync.calculateIndexFromList(es.newSolrQuery().rdfType('dcat:Catalog')
      .context(this._context).list());
    await csync.harvestMetadata(this._catalogURI, this._catalogGraph);
    await csync.removeMissing(); // Just in case the catalog URI changed

    // Sync all publishers, contactpoints, distributions
    await esync.calculateIndexFromList(es.newSolrQuery().rdfType(extratypes)
      .context(this._context).list());
    await promiseUtil.forEach(Object.keys(this._contacts), (cURI) => esync.harvestMetadata(cURI, this._contacts[cURI]));
    await promiseUtil.forEach(Object.keys(this._publishers), (pURI) => {
      const publisherGraph = this._publishers[pURI];
      return esync.harvestMetadata(pURI, this._publishers[pURI]);
    });
    await promiseUtil.forEach(Object.keys(this._distributions), (dURI) => esync.harvestMetadata(dURI, this._distributions[dURI]));
    await esync.removeMissing();

    // Sync all datasets
    await dsync.calculateIndexFromList(es.newSolrQuery().rdfType('dcat:Dataset')
      .context(this._context).list());
    await promiseUtil.forEach(Object.keys(this._datasets), (daURI) => {
      const dg = this._datasets[daURI];
      let forceUpdate = false;
      dg.find(daURI, 'dcat:distribution').forEach((stmt) => {
        if (!esync.isUnchanged(stmt.getValue())) {
          forceUpdate = true;
        }
      });
      return dsync.harvestMetadata(daURI, dg, undefined, forceUpdate);
    });
    await dsync.removeMissing();

    // Sync all datasetseries
    await dssync.calculateIndexFromList(es.newSolrQuery().rdfType('dcat:DatasetSeries')
      .context(this._context).list());
    await promiseUtil.forEach(Object.keys(this._datasetSeries), (daURI) => {
      const dg = this._datasetSeries[daURI];
      return dssync.harvestMetadata(daURI, dg);
    });
    await dssync.removeMissing();


    // Sync all dataservices
    await ssync.calculateIndexFromList(es.newSolrQuery().rdfType('dcat:DataService')
      .context(this._context).list());
    await promiseUtil.forEach(Object.keys(this._dataservices), (dsURI) => {
      const dataserviceGraph = this._dataservices[dsURI];
      return ssync.harvestMetadata(dsURI, dataserviceGraph);
    });
    await ssync.removeMissing();
    this._dedup.save();
    return [
      { ...dsync.getReport(), type: 'dcat:Dataset', title: 'Dataset' },
      { ...ssync.getReport(), type: 'dcat:DataService', title: 'DataService' },
      { ...dssync.getReport(), type: 'dcat:DatasetSeries', title: 'DatasetSeries' },
    ];
  }

  renameBlankDatasets() {
    this.renameBlankFromTitle('dcat:Dataset');
  }

  renameBlankDatasetSeries() {
    this.renameBlankFromTitle('dcat:DatasetSeries');
  }

  renameBlankServices() {
    this.renameBlankFromTitle('dcat:DataService');
  }

  renameBlankFromTitle(rdfType) {
    this._graph.find(null, 'rdf:type', rdfType).forEach(
      (stmt) => {
        if (stmt.isSubjectBlank()) {
          let counter = 0;
          let todo = true;
          while (todo) {
            try {
              const value = stmt.getSubject();
              const name = this._graph.findFirstValue(value, 'dcterms:title');
              const newURI = this._base + md5(name.toLocaleLowerCase().trim() + counter);
              this._graph.replaceBlankWithURI(value, newURI, true);
              todo = false;
            } catch (e) {
              counter += 1;
            }
          }
        }
      });
  }

  renameBlankDistributions() {
    this._graph.find(null, 'dcat:distribution').forEach(
      (stmt) => {
        if (stmt.isObjectBlank()) {
          const value = stmt.getValue();
          const access = this._graph.findFirstValue(value, 'dcat:accessURL');
          const format = this._graph.findFirstValue(value, 'dcterms:format') || '';
          const newURI = this._base + md5(`${access}${format}${stmt.getSubject()}`);
          try {
            this._graph.replaceBlankWithURI(value, newURI, true);
          } catch (e) {
            logger.error(`Failed to rename distribution based on: ${access}, ${format} and ${stmt.getSubject()}`);
            const newURI2 = this._base + md5(`${access}${format}${stmt.getSubject()}${new Date().getTime()}`);
            this._graph.replaceBlankWithURI(value, newURI2, true);
          }
        }
      });
  }

  extractPublishers() {
    const name2URI = {};
    const ctxId = this._context.getId();
    const createPublisher = (uri) => {
      const newGraph = utils.extract(this._graph, new Graph(), uri);
      if (newGraph.find(uri, 'rdf:type').length === 0) newGraph.add(uri, 'rdf:type', 'foaf:Agent');
      if (!this._dedup.isDuplica(uri, newGraph, ctxId)) this._publishers[uri] = newGraph;
    };
    this._graph.find(null, 'rdf:type', 'foaf:Agent').concat(
      this._graph.find(null, 'rdf:type', 'foaf:Person'),
      this._graph.find(null, 'rdf:type', 'foaf:Organization'),
    ).forEach(
      (stmt) => {
        const value = stmt.getSubject();
        if (stmt.isSubjectBlank()) {
          const name = this._graph.findFirstValue(value, 'foaf:name');
          const mail = this._graph.findFirstValue(value, 'foaf:mbox');
          const sourceId = name || mail || value;
          const existingURI = name2URI[sourceId];
          if (!existingURI) {
            const newURI = this._base + md5(sourceId);
            name2URI[sourceId] = newURI;
            this._graph.replaceBlankWithURI(value, newURI, true);
            createPublisher(newURI);
          } else {
            // Triples pointing to this bnode needs to point to the existingURI
            // Ignore all other outgoing triples, they will not be extracted
            this._graph.forEach((s, p, o) => {
              if (o.type === 'bnode' && o.value === value) {
                o.value = existingURI;
                o.type = 'uri';
              }
            });
          }
        } else createPublisher(value);
      },
    );
  }

  extractContacts() {
    const name2URI = {};
    this._graph.find(null, 'dcat:contactPoint').forEach((stmt) => {
      const value = stmt.getValue();
      if (stmt.isObjectBlank()) {
        const name = this._graph.findFirstValue(value, 'vcard:fn');
        const mail = this._graph.findFirstValue(value, 'vcard:hasEmail');
        const sourceId = mail || name || value;
        const existingURI = name2URI[sourceId];
        if (!existingURI) {
          const newURI = this._base + md5(sourceId);
          name2URI[sourceId] = newURI;
          this._graph.replaceBlankWithURI(value, newURI, true);
          const ng = utils.extract(this._graph, new Graph(), newURI);
          if (ng.find(newURI, 'rdf:type').length === 0) {
            ng.add(newURI, 'rdf:type', 'vcard:Kind');
          }
          this._contacts[newURI] = ng;
        } else {
          // Triples pointing to this bnode needs to point to the existingURI
          // Ignore all other outgoing triples, they will not be extracted
          this._graph.forEach((s, p, o) => {
            if (o.type === 'bnode' && o.value === value) {
              o.value = existingURI;
              o.type = 'uri';
            }
          });
        }
      } else {
        const ng = utils.extract(this._graph, new Graph(), value);
        if (ng.size() !== 0) {
          if (ng.find(value, 'rdf:type').length === 0) {
            ng.add(value, 'rdf:type', 'vcard:Kind');
          }
          this._contacts[value] = ng;
        } else {
          stmt.setAsserted(false);
        }
      }
    });
  }

  extractDistributions() {
    const diIgnore = {};
    diIgnore[namespaces.expand('odrs:copyrightHolder')] = true;
    diIgnore[namespaces.expand('dcat:accessService')] = true;
    this._graph.find(null, 'dcat:distribution').forEach((stmt) => {
      const diURI = stmt.getValue();
      // Only add distributions that are included (e.g. not filtered through deduplication etc.)
      if (this._datasets[stmt.getSubject()]) {
        const newGraph = new Graph();
        this._distributions[diURI] = newGraph;
        // Copy over main metadata of the distribution.
        utils.extract(this._graph, newGraph, diURI, diIgnore, true);
        // for each downloadURL, try copy over metadata for that downloadUrl if it exists
        const hasDistributionType = newGraph.find(diURI, 'rdf:type', 'dcat:Distribution').length > 0;
        if (!hasDistributionType) {
          newGraph.add(diURI, 'rdf:type', 'dcat:Distribution');
        }
        newGraph.find(null, 'dcat:downloadURL').forEach((statement) => {
          utils.extract(this._graph, newGraph, statement.getValue(), {}, true);
        });
      }
    });
  }

  extractDatasets() {
    const daIgnore = {};
    daIgnore[namespaces.expand('dcat:distribution')] = true;
    daIgnore[namespaces.expand('dcat:contactPoint')] = true;
    daIgnore[namespaces.expand('dcterms:publisher')] = true;
    daIgnore[namespaces.expand('dcterms:creator')] = true;
    daIgnore[namespaces.expand('prov:agent')] = true;
    const ctxId = this._context.getId();
    this._graph.find(null, 'rdf:type', 'dcat:Dataset').forEach((statement) => {
      const daURI = statement.getSubject();
      const metadata = utils.extract(this._graph, new Graph(), daURI, daIgnore, true);
      
      
      if (!this._dedup.isDuplica(daURI, metadata, ctxId)) {
        this._datasets[daURI] = metadata;
        // for each foaf:page, try copy over metadata if it exists
        metadata.find(null, 'foaf:page').forEach((stmt) => {
          utils.extract(this._graph, metadata, stmt.getValue(), {}, true);
        });
        // for each conformsTo, try copy over metadata if it exists
        metadata.find(null, 'dcterms:conformsTo').forEach((stmt) => {
          // Exclude publishers as they may cause duplicates in search index on portals for the dataset.
          // Solr index of entrystore does not care about the subject.
          utils.extract(this._graph, metadata, stmt.getValue(), {'http://purl.org/dc/terms/publisher': true}, false);
        });
      }
    });
  }

  extractDatasetSeries() {
    const daIgnore = {};
    daIgnore[namespaces.expand('dcat:distribution')] = true;
    daIgnore[namespaces.expand('dcat:contactPoint')] = true;
    daIgnore[namespaces.expand('dcterms:publisher')] = true;
    daIgnore[namespaces.expand('dcterms:creator')] = true;
    daIgnore[namespaces.expand('prov:agent')] = true;
    const ctxId = this._context.getId();
    this._graph.find(null, 'rdf:type', 'dcat:DatasetSeries').forEach((statement) => {
      const daURI = statement.getSubject();
      const metadata = utils.extract(this._graph, new Graph(), daURI, daIgnore, true);
      
      
      if (!this._dedup.isDuplica(daURI, metadata, ctxId)) {
        this._datasetSeries[daURI] = metadata;
        // for each foaf:page, try copy over metadata if it exists
        metadata.find(null, 'foaf:page').forEach((stmt) => {
          utils.extract(this._graph, metadata, stmt.getValue(), {}, true);
        });
        // for each conformsTo, try copy over metadata if it exists
        metadata.find(null, 'dcterms:conformsTo').forEach((stmt) => {
          // Exclude publishers as they may cause duplicates in search index on portals for the dataset.
          // Solr index of entrystore does not care about the subject.
          utils.extract(this._graph, metadata, stmt.getValue(), {'http://purl.org/dc/terms/publisher': true}, false);
        });
      }
    });
  }

  extractDataServices() {
    const dsIgnore = {};
    dsIgnore[namespaces.expand('dcat:servesDataset')] = true;
    dsIgnore[namespaces.expand('dcat:contactPoint')] = true;
    dsIgnore[namespaces.expand('dcterms:publisher')] = true;
    const ctxId = this._context.getId();
    this._graph.find(null, 'rdf:type', 'dcat:DataService').forEach((stmt) => {

      const dataserviceURI = stmt.getSubject();
      const metadata = utils.extract(this._graph, new Graph(), dataserviceURI, dsIgnore, true);

      if (!this._dedup.isDuplica(dataserviceURI, metadata, ctxId)) {
        this._dataservices[dataserviceURI] = metadata;
      }

    });
  }

  extractCatalog() {
    const caIgnore = {};
    caIgnore[namespaces.expand('dcat:dataset')] = true;
    caIgnore[namespaces.expand('dcat:service')] = true;
    caIgnore[namespaces.expand('dcat:contactPoint')] = true;
    caIgnore[namespaces.expand('dcterms:publisher')] = true;
    caIgnore[namespaces.expand('odrs:copyrightHolder')] = true;
    const stmts = this._graph.find(null, 'rdf:type', 'dcat:Catalog');
    if (stmts.length === 1) {
      if (stmts[0].isSubjectBlank()) {
        const cblank = stmts[0].getSubject();
        const ctitle = this._graph.findFirstValue(cblank, 'dcterms:title');
        this._catalogURI = this._base + md5(`catalog${ctitle}`);
        this._graph.replaceBlankWithURI(cblank, this._catalogURI, true);
      } else {
        this._catalogURI = stmts[0].getSubject();
      }
      this._catalogGraph = utils.extract(this._graph, new Graph(),
        this._catalogURI, caIgnore, true);
      this._catalogGraph.findAndRemove(this._catalogURI, 'dcat:dataset');
      Object.keys(this._datasets).forEach((ruri) => {
        this._catalogGraph.add(this._catalogURI, 'dcat:dataset', ruri);
      });
      Object.keys(this._datasetSeries).forEach((ruri) => {
        this._catalogGraph.add(this._catalogURI, 'dcat:dataset', ruri);
      });
    }
  }
};
