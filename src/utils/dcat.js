const { Graph } = require('@entryscape/rdfjson');
const { namespaces } = require('@entryscape/entrystore-js');
const entry = require('./entry');
const rdf = require('./rdf');
const responsibles = require('./responsibles');
const { promiseUtil } = require('@entryscape/entrystore-js');
const globals = require('./globals');
const logger = require('./logger/logger');

module.exports = {

  // context & group exists
  // create & import catalog ,if catalog does not exists
  // import B
  /**
   * It creates catalog-entry in entrystore if it does not exists.
   * Then it imports rdf and updates catalog metadata.
   *
   * @param ctxid - context id of entry store
   * @param input - file path of rdf data or rdf graph to be imported
   * @param entrystore -entry object
   * @returns { promise } - upon success promise returns updated catalog entry
   */
  importCatalog(ctxid, input, entrystore) {
    const context = entrystore.getContextById(ctxid);
    return globals.getEntryStoreUtil().getEntryByType('dcat:Catalog', context).then(null, () => {
      const pe = context.newEntry();
      pe.getMetadata().add(pe.getResourceURI(), namespaces.expand('rdf:type'), namespaces.expand('dcat:Catalog'));
      return pe.commit();
    }).then(catalogEntry => this.importRDF(catalogEntry, input));
  },

  // import C
  // create context & group
  /**
   * It creates context and group and imports catalog from rdf data file
   * @param entrystore - entry store object
   * @param filePath - file path of rdf data to be imported
   * @returns { promise } - upon success promise returns id of newly created context
   */
  createContextAndGroupAndImport(entrystore, filePath) {
    return entrystore.createGroupAndContext().then((entry) => {
      const group = entry;
      var homeContext = entry.getResource(true).getHomeContext();
      var homeContext = entrystore.getContextById(homeContext);
      // returning context id of newly created context
      return this.importCatalog(homeContext.getId(), filePath, entrystore).then(() => homeContext.getId());
    });
  },

  // import A
  /**
   * It reads rdf data from file and imports into catalog
   * creates and updates meta data of datasets,distributions,contact points,publishers
   * and also updates catalog metadata
   * @param {store/Entry} catalogEntry - the catalog entry to which all datasets should be attached
   * @param filePath -file path of rdf data to be imported
   * @returns {entryPromise} - upon success returns updated catalog entry
   */
  importRDF(catalogEntry, input) {
    const contextId = catalogEntry.getContext().getId();
    const entrystore = catalogEntry.getEntryStore();
    if (typeof input === 'string') { // if input is filePath
      const filePath = input;
      return rdf.loadRDF(filePath).then(function (inGraph) {
        return responsibles.getOrCreate(inGraph, entrystore, contextId).then(responsiblesObj => this._createAllDatasetsWithDistributions(inGraph, responsiblesObj, entrystore, contextId).then(id2datasetEntries => this._updateCatalog(catalogEntry, inGraph, id2datasetEntries, responsiblesObj)));
      });
    } // input is graph object
    const inGraph = input;
    return responsibles.getOrCreate(inGraph, entrystore, contextId).then(responsiblesObj => this._createAllDatasetsWithDistributions(inGraph, responsiblesObj, entrystore, contextId).then(id2datasetEntries => this._updateCatalog(catalogEntry, inGraph, id2datasetEntries, responsiblesObj)));
  },

  /**
   * reads each dataset statement from graph ,creates distributions for it
   * creates dataset entry and updates meta data of dataset with distributions ,publishers and contact points and commits
   * @param inGraph - input graph object ,which holds rdf data to be imported
   * @param responsiblesObj - object which holds publiser and contact point entries
   * @param entrystore - entry store object
   * @param ctxid - id of catalog conext
   * @returns {array} - upon success ,returns an array of newly created dataset entries
   * @private
   */
  _createAllDatasetsWithDistributions(inGraph, responsiblesObj, entrystore, ctxid) {
    const datasetStmts = inGraph.find(null, 'rdf:type', 'dcat:Dataset');

    return promiseUtil.forEach(datasetStmts, (datasetStmt) => {
      const origURI = datasetStmt.getSubject();
      return this._createDistributionsForDataset(inGraph,
        origURI, responsiblesObj, entrystore, ctxid).then(id2distEntries => this._createDataset(inGraph, origURI, id2distEntries, responsiblesObj, entrystore, ctxid));
    });
  },
  /**
   * reads each distribution data from ingraph for given dataset uri and creates entry in store
   * @param inGraph - input graph object ,which holds rdf data to be imported
   * @param originalDatasetURI - dataset uri ,for which distributions will be created
   * @param responsiblesObj - object which holds publiser and contact point entries ** can be removed ,not using
   * @param entrystore - entry store object
   * @param ctxid - id of catalog conext
   * @returns {array} - upon success returns an array of distribution entries
   * @private
   */
  _createDistributionsForDataset(inGraph, originalDatasetURI, responsiblesObj, entrystore, ctxid) {
    const distributionEntries = [];
    const ignore = {};
    ignore[namespaces.expand('dcat:distribution')] = true;
    ignore[namespaces.expand('adms:contactPoint')] = true;
    ignore[namespaces.expand('dcterms:publisher')] = true;
    const distributionStmts = inGraph.find(originalDatasetURI, 'dcat:distribution', null);
    distributionStmts.forEach((distributionStmt) => {
      const value = distributionStmt.getValue();
      distributionEntries.push(entry.extractAndCreate(
        entrystore.getContextById(ctxid), value, inGraph, ignore).commit().then(null, (err) => {
        logger.error(err.stack);
      }));
    });
    return Promise.all(distributionEntries);
    // return promise with id2distributionentries (new entries).
  },
  /**
   * creates dataset entry
   * reads publisher statements for given dataset uri and its corresponding entries from responsibles, add to dataset metadata
   * reads contact points statements for given dataset uri and its corresponding entries from responsibles,add to dataset metadata
   * add distributions to  dataset metadata
   * save/commits dataset entry in entry store
   * @param inGraph - input graph object ,which holds rdf data to be imported
   * @param originalDatasetURI - dataset uri to be replaced by newly created dataset uri
   * @param distributionEntries -an array of distribution entries corresponding for given dataset to update metadata
   * @param responsiblesObj - an object of all responsibles
   * @param entrystore - an entry store object
   * @param ctxid - id of context of catalog
   * @returns {entryPromise} - on success returns promise with new datasetentry
   * @private
   */
  _createDataset(inGraph, originalDatasetURI, distributionEntries, responsiblesObj, entrystore, ctxid) {
    const ignore = {};
    ignore[namespaces.expand('dcat:distribution')] = true;
    ignore[namespaces.expand('dcat:contactPoint')] = true;
    ignore[namespaces.expand('dcterms:publisher')] = true;
    const datasetMetadataGraph = rdf.closure(inGraph, originalDatasetURI, ignore);
    const newDatasetEntry = entry.create(entrystore.getContextById(ctxid),
      originalDatasetURI, datasetMetadataGraph);
    const newDatasetResourceURI = newDatasetEntry.getResourceURI();

    // publishers
    const publisherStmts = inGraph.find(originalDatasetURI, 'dcterms:publisher', null);
    const id2PEntries = responsiblesObj.id2PEntries;
    publisherStmts.forEach((publisherStmt) => {
      if (id2PEntries[publisherStmt.getValue()]) {
        datasetMetadataGraph.add(newDatasetResourceURI, 'dcterms:publisher', id2PEntries[publisherStmt.getValue()].getResourceURI());
      }
    });
    // contactpoints
    // changed old version(adms:contactPoint)  to new version (dcat:contactPoint)
    const cpStmts = inGraph.find(originalDatasetURI, 'dcat:contactPoint', null);
    const id2CPEntries = responsiblesObj.id2CPEntries;
    cpStmts.forEach((cpStmt) => {
      datasetMetadataGraph.add(newDatasetResourceURI, 'dcat:contactPoint', id2CPEntries[cpStmt.getValue()].getResourceURI());
    });
    // distributions
    const cache = entrystore.getCache();
    distributionEntries.forEach((distributionEntry) => {
      datasetMetadataGraph.add(newDatasetResourceURI, 'dcat:distribution', distributionEntry.getResourceURI());
      cache.unCache(distributionEntry); // Remove from cache so we conserve memory.
    });
    newDatasetEntry.setMetadata(datasetMetadataGraph);
    return newDatasetEntry.commit();
    // return promise with new datasetentry
  },

  /**
   * updates dcat catalog with publishers and datasets
   * @param {store/Entry} catalogEntry
   * @param inGraph - input graph object ,which holds rdf data to be imported
   * @param datasetEntries - an array of dataset entries to update catalog metadata
   * @param responsiblesObj - an object which holds all publisher entries and contact point entries
   * @returns {entryPromise} - upon success promise returns updated dcat catalog entry
   * @private
   */
  _updateCatalog(catalogEntry, inGraph, datasetEntries, responsiblesObj, entrystore, ctxid) {
    var entrystore = catalogEntry.getEntryStore(),
      contextId = catalogEntry.getContext().getId();
    let catalogMetadataGraph;
    const objectEntries = {};
    const pubEntries = [];
    let publisherStmts;
    const ignoreCatalog = {};
    ignoreCatalog[namespaces.expand('dcterms:publisher')] = true;
    ignoreCatalog[namespaces.expand('dcat:dataset')] = true;
    const catalogStmts = inGraph.find(null, 'rdf:type', 'dcat:Catalog');
    const catalogEntryResourceURI = catalogEntry.getResourceURI();
    if (catalogStmts.length > 0) {
      catalogStmts.forEach((catalogStmt) => {
        catalogMetadataGraph = rdf.closure(inGraph, catalogStmt.getSubject(), ignoreCatalog);
        // var dcatEntryResourceURI = dcatEntry.getResourceURI();
        catalogMetadataGraph.replaceSubject(catalogStmt.getSubject(), catalogEntryResourceURI);
        // datasets
        datasetEntries.forEach((datasetEntry) => {
          catalogMetadataGraph.add(catalogEntryResourceURI, 'dcat:dataset', datasetEntry.getResourceURI());
        });
        // publisher
        publisherStmts = inGraph.find(catalogStmt.getSubject(), 'dcterms:publisher', null);
        const id2PEntries = responsiblesObj.id2PEntries;
        publisherStmts.forEach((publisherStmt) => {
          catalogMetadataGraph.add(catalogEntryResourceURI, 'dcterms:publisher', id2PEntries[publisherStmt.getValue()].getResourceURI());
        });
      });
    } else {
      catalogMetadataGraph = catalogEntry.getMetadata();
      // datasets
      datasetEntries.forEach((datasetEntry) => {
        catalogMetadataGraph.add(catalogEntryResourceURI, 'dcat:dataset', datasetEntry.getResourceURI());
      });
      // publisher
      // publisherStmts = inGraph.find(catalogStmt.getSubject(), "dcterms:publisher", null);
      const publisherEntries = responsiblesObj.id2PEntries;
      for (const publisherEntry in publisherEntries) {
        if (publisherEntries.hasOwnProperty(publisherEntry)) {
          catalogMetadataGraph.add(catalogEntryResourceURI, 'dcterms:publisher', publisherEntries[publisherEntry].getResourceURI());
        }
      }
    }

    // Lets clear datasets, publishers and contactpoints from cache to conserve memory
    // Distributions where uncached previously after dataset creation.
    const cache = entrystore.getCache();
    datasetEntries.forEach((de) => {
      cache.unCache(de);
    });
    const pes = responsiblesObj.id2PEntries;
    for (const peKey in pes) {
      if (pes.hasOwnProperty(peKey)) {
        cache.unCache(pes[peKey]);
      }
    }
    const ces = responsiblesObj.id2CPEntries;
    for (const ceKey in ces) {
      if (ces.hasOwnProperty(ceKey)) {
        cache.unCache(ces[ceKey]);
      }
    }

    catalogEntry.setMetadata(catalogMetadataGraph);
    return catalogEntry.commitMetadata();
    // return a promise with updated catalogentry.
  },

  /**
   * deletes all entries in catalog
   * @param entrystore - entry store object
   * @param contextId - id of catalog context
   * @returns {promises} - an array of promises
   */
  clearCatalog(entrystore, contextId) {
    const properties = [
      'dcat:Dataset',
      'dcat:Distribution',
      'foaf:Agent',
      'foaf:Person',
      'foaf:Organization',
      'vcard:Individual',
      'vcard:Organization',
      'vcard:Kind'];
    const context = entrystore.getContextById(contextId);
    const promises = [];
    const esu = globals.getEntryStoreUtil();
    const list = entrystore.newSolrQuery().rdfType(properties).context(context).limit(100)
      .list();
    return esu.removeAll(list);
  },
  /**
   * adds namespaces for further use
   */
  prepareNamespaces() {
    namespaces.add('dcat', 'http://www.w3.org/ns/dcat#');
    namespaces.add('adms', 'http://www.w3.org/ns/adms#');
    namespaces.add('foaf', 'http://xmlns.com/foaf/0.1/');
    namespaces.add('odrs', 'http://schema.theodi.org/odrs#');
    // addded for sample test data
    namespaces.add('dct', 'http://purl.org/dc/terms/');
  },

  /**
   * A count of all (DCAT) resource types in a graph
   * @param graph {rdfjson/Graph}
   *
   * TODO add more resource types
   */
  getResourceCount(graph) {
    const datasetStmts = graph.subjects('rdf:type', 'dcat:Dataset');
    const distributionStmts = graph.subjects('rdf:type', 'dcat:Distribution');

    const datasetCount = datasetStmts.values().length;
    const distributionCount = distributionStmts.values().length;

    return {
      total: datasetCount + distributionCount,
      datasetCount,
      distributionCount,
    };
  },
};
