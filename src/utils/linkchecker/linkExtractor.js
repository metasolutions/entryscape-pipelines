const { namespaces } = require('@entryscape/entrystore-js');
const authenticate = require('../authenticate');
const globals = require('../globals');
const config = require('../../config');
const logger = require('../logger/logger');

/**
 * Extract configuration based links from a context.
 *
 * @param {string} context
 */
const extractLinks = async (context = null) => {
  namespaces.add('dcat', 'http://www.w3.org/ns/dcat#');

  const linkInquiries = [];
  let extractLinksEntryTypes;
  if (!config.extractLinksProps) {
    logger.error('Missing configuration. Check the extractLinks* properties on config.js_example.')
    throw new Error(
      'Missing configuration. Check the extractLinks* properties on config.js_example.'
    );
  } else {
    extractLinksEntryTypes = Object.keys(config.extractLinksProps);
  }
  const extractLinksEntryLabel = config.extractLinksEntryLabel ? config.extractLinksEntryLabel : 'dcterms:title';
  const extractLinksEntryFormat = config.extractLinksEntryFormat ? config.extractLinksEntryFormat : 'dcterms:format';
  await authenticate();

  const es = globals.getEntryStore();
  const cache = es.getCache();
  const [contexts, exclude] = getContextQueryParams(context);


  for (const rdfType of extractLinksEntryTypes) {
    await es
      .newSolrQuery()
      .context(contexts, exclude)
      .rdfType(rdfType)
      .forEach((extractLinksEntry) => {

        const md = extractLinksEntry.getMetadata();
        const ruri = extractLinksEntry.getResourceURI();
        const entryURI = extractLinksEntry.getURI();
        const context = extractLinksEntry.getContext().getId();
        const entryLabel = md.findFirstValue(ruri, extractLinksEntryLabel);
        const entryFormat = md.findFirstValue(ruri, extractLinksEntryFormat);
        const extractLinksEntryProps = config.extractLinksProps[rdfType] || [];

        if (extractLinksEntryProps.length === 0) {
          throw new Error('An entry type is missing configuration properties.');
        }

        extractLinksEntryProps.forEach((prop) => {
          md.find(null, prop).forEach((stmt) => {
            if (stmt.getType() === 'uri') {
              const inquiry = {
                entryType: rdfType,
                entryURI,
                entryLabel,
                entryFormat,
                context,
              };
              inquiry.uri = stmt.getValue();
              inquiry.property = prop;
              inquiry.createdAt = new Date().toISOString();
              linkInquiries.push(inquiry);
            }
          });
        });
        cache.unCache(extractLinksEntry);
      });
  }
  return linkInquiries;
};

const getContextQueryParams = (context) => {
  if (context) {
    return [context, false];
  }

  if (config.extractLinksExclude && config.extractLinksExclude.length > 0) {
    return [config.extractLinksExclude, true];
  }

  return [null, false];
};

module.exports = { extractLinks };
