const fileSystem = require('fs');
const path = require('path');
const { extractLinks } = require('./linkExtractor');
const logger = require('../logger/logger');

/**
 * Exports link inquiries on a json file. Can run for either a specific context or all.
 *
 * @param {string=} context
 */
const exportLinks = async (context = null) => {
  // ! needs to add output/links folders if they don't exists otherwise throws
  const dir = path.join(__dirname, '..', '..', '..', 'output', 'links', '/');

  if (!fileSystem.existsSync(dir)) {
    fileSystem.mkdirSync(dir);
  }

  const dateTime = new Date().toISOString();
  const contextStr = context || 'all';
  const fileName = `link-inquiries-${contextStr}-${dateTime}`;
  const filePath = `${dir}${fileName}.json`;
  const linkInquiries = await extractLinks(context);

  if (linkInquiries.length === 0) {
    logger.info(
      `Link Extractor - No link inquiries exported for context ${contextStr}.`
    );
    return;
  }

  const fileDescriptor = fileSystem.openSync(filePath, 'w');
  fileSystem.writeSync(
    fileDescriptor,
    JSON.stringify(linkInquiries, null, '  '),
    0,
    'utf8'
  );
  logger.info(
    `Link extractor - Exported ${linkInquiries.length} link inquiries for context ${contextStr} on file ${fileName}.`
  );
};

module.exports = { exportLinks };
