/**
 * A link check inquiry
 * @typedef {Object} Inquiry
 * @property {string} uri - The link to check
 * @property {string} property - The property pointing to the URI
 * @property {string} entryType - The class on which the property is expressed
 * @property {string} entryURI - The URI for the instance of the class where the link failed
 * @property {string} entryLabel - A label for the main URI
 * @property {string} context - The entry's context
 * @property {string} createdAt - Date and time when link was added to the queue
 */

/**
 * A pending link check inquiry - essentially an inquiry merged with a check
 * @typedef {Object} PendingInquiry
 * @property {string} uri - The link to check
 * @property {string} property - The property pointing to the URI
 * @property {string} entryType - The class on which the property is expressed
 * @property {string} entryURI - The URI for the instance of the class where the link failed
 * ! @property {string} entryLabel - A label for the main URI
 * @property {string} context - The entry's context
 * @property {string} createdAt - Date and time when link was added to the queue
 * @property {number} statusCode
 * @property {string} checkedAt
 * @property {string} status
 * @property {string} statusMessage
 * @property {number} attempts
 */

/**
 * A check, used to store recently checked URIs
 * @typedef {Object} Check
 * @property {string} uri
 * @property {string} status
 * @property {number} statusCode
 * @property {string} statusMessage
 * @property {string} checkedAt
 */

const path = require('path');
const fileSystem = require('fs');
const { namespaces } = require('@entryscape/entrystore-js');
const { getSkipTimeframe, inquireMap, inquireMapToArray } = require('./util');
const logger = require('../logger/logger');

namespaces.add('esterms', 'http://entryscape.com/terms/');

const dir = path.join(__dirname, '..', '..', '..', 'output', 'links', '/');
let inquiryFiles = [];

/**
 * Removes files whose inquiries have been fed to the link checker.
 */
const cleanup = () => {
  logger.info(`\nCleanup (removed files) (${inquiryFiles.length})`);
  logger.info('------------------------------------------------');
  inquiryFiles.forEach((filename, i) => {
    try {
      fileSystem.unlinkSync(dir + filename);
      logger.info(`${i}) ${filename}`);
    } catch (err) {
      logger.error(err);
    }
  });
};

/**
 * Returns pending inquiries stored in the link-checks-pending.json file. Pending
 * inquiries are inquiries that failed during a previous link checker run and have
 * had less than 3 checking attempts.
 *
 * @return {Map<String, Inquiry>}
 */
const addPendingInquiriesToMap = (map) => {
  const _map = map || new Map();
  try {
    const jsonData = fileSystem.readFileSync(
      `${dir}link-checks-pending.json`,
      'utf8'
    );
    const pending = JSON.parse(jsonData);
    inquireMap(map, pending);
  } catch (err) {
    if (err.code == 'ENOENT') {
      logger.warn('No pending inquiries file found');
    } else {
      logger.error(err);
    }
  }

  return _map;
};

/**
 * Retrieves inquiries stored in link-inquiries-{contextID}-{date}.json files,
 * exported by the link extractor.
 *
 * @return {Map<String,Inquiry>}
 */
const addFileInquiriesToMap = (map) => {
  const _map = map || new Map();
  const files = fileSystem.readdirSync(dir);
  inquiryFiles = files.filter((f) => f.indexOf('link-inquiries') !== -1);
  inquiryFiles.forEach((filename) => {
    const jsonData = fileSystem.readFileSync(dir + filename, 'utf8');
    try {
      const data = JSON.parse(jsonData);
      inquireMap(_map, data);
    } catch (e) {
      logger.error(`Inquires file found but could not be parsed: ${filename}`);
    }
  });

  if (inquiryFiles.length === 0) {
    logger.info('No extracted inquiries found');
  }

  return _map;
};

/**
 * Returns the inquiries stored in json files. These include inquiries produced
 * by the extraction process, as well as any pending inquiries from
 * a previous link checker run.
 *
 * @return {Inquiry[]}
 */
const getInquiries = () => {
  const map = addFileInquiriesToMap();
  const extractedCount = map.size;
  logger.info(`Extracted inquiries (${extractedCount})`);
  addPendingInquiriesToMap(map);
  logger.info(`Pending inquiries (${map.size - extractedCount})`);

  // Important: pending have to be first to keep the attempts data when checking later
  return inquireMapToArray(map);
};

/**
 * Retrieves the checks stored in the link-checks-recent.json file,
 * and filters them down according to a timeframe set by the
 * `linkCheckSkipRetryHours` config variable.
 *
 * @return {Map<String, Check>}
 */
const getRecent = () => {
  const recent = new Map();
  const timeframe = getSkipTimeframe();

  try {
    const jsonData = fileSystem.readFileSync(
      `${dir}link-checks-recent.json`,
      'utf8'
    );
    const data = JSON.parse(jsonData);
    data.forEach((recentCheck) => {
      if (new Date() - new Date(recentCheck.checkedAt) < timeframe) {
        recent.set(recentCheck.uri, recentCheck);
      }
    });
  } catch (err) {
    if (err.code == 'ENOENT') {
      logger.warn('No recent checks file found')
    } else {
      logger.error(err);
    }
  }

  if (recent.size > 0) {
    logger.info(`Recent inquiries (${recent.size})`);
  }

  return recent;
};

/**
 * Creates or updates the `recent` file with the latest.
 *
 * @param {Check[]} recent
 */
const updateRecent = (recent) => {
  const recentChecksFile = `${dir}link-checks-recent.json`;
  const timeframe = getSkipTimeframe();
  const recentInTimeframe = recent.filter(
    (check) => new Date() - new Date(check.checkedAt) < timeframe
  );
  fileSystem.writeSync(
    fileSystem.openSync(recentChecksFile, 'w'),
    JSON.stringify(recentInTimeframe, null, '  '),
    0,
    'utf8'
  );

  logger.info(`Updated recent inquiries (${recentInTimeframe.length})`);
};

/**
 * Creates or updates the `pending` file with the latest.
 *
 * @param {PendingInquiry[]}
 */
const updatePending = (pending, attempts) => {
  const pendingChecksFile = `${dir}link-checks-pending.json`;
  const filteredPending = pending.filter((check) => check.attempts < attempts);
  fileSystem.writeSync(
    fileSystem.openSync(pendingChecksFile, 'w'),
    JSON.stringify(filteredPending, null, '  '),
    0,
    'utf8'
  );

  logger.info(`Updated pending inquiries (${filteredPending.length})`);
};

const toReportFile = `${dir}link-checks-to-report.json`;

/**
 * Returns the inquires to report.
 *
 * @return {Inquiry[]}
 */
const getToBeReported = () => {
  let toReport = [];
  try {
    const jsonData = fileSystem.readFileSync(toReportFile, 'utf8');
    toReport = JSON.parse(jsonData);
  } catch (err) {
    if (err.code == 'ENOENT') {
      logger.error('No file found for inquires to report');
    } else {
      logger.error(err);
    }
  }

  return toReport;
};

/**
 * Creates or updates the `checks` file with the latest checks.
 *
 * @param {PendingInquiry[]}
 */
const updateToBeReported = (toReport) => {
  fileSystem.writeSync(
    fileSystem.openSync(toReportFile, 'w'),
    JSON.stringify(toReport, null, '  '),
    0,
    'utf8'
  );

  logger.info(`Updated inquiries to report (${toReport.length})`);
};

module.exports = {
  getInquiries,
  getRecent,
  updatePending,
  updateRecent,
  getToBeReported,
  updateToBeReported,
  cleanup,
};
