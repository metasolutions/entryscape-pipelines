const { linkCheck, linkCheckExclude } = require('../../config');
const readline = require('readline');
const { getRequestData, inquiryInArray } = require('./util');
// const { report: createReport } = require('./reporter');
const {
  getInquiries,
  getRecent,
  updatePending,
  updateRecent,
  cleanup,
  getToBeReported,
  updateToBeReported
} = require('./io');
const { mergeInquires } = require('./util');
const logger = require('../logger/logger');

const attempts = linkCheck.attempts || 3;

const wrapUp = async (report, recentMap, pending) => {
  // await createReport(report);
  const oldToReport = await getToBeReported();
  const toReport = mergeInquires(oldToReport, report);
  await updateToBeReported(toReport);

  const recentArr = Array.from(recentMap.values());

  logger.info(`\nLink Checker Output`);
  logger.info('----------------------------------------------------');
  updateRecent(recentArr);
  updatePending(pending, attempts);

  cleanup();
};

const logResults = (
  inbox,
  report,
  avoidedThroughRecent,
  avoidedThroughConfig,
  enqueued,
  cached,
  broken
) => {
  logger.info('\nLink Checker stats');
  logger.info('----------------------------------------------------');
  logger.info(`inbox: ${inbox.length}`);
  logger.info(`report: ${report.length}`);
  logger.info(`avoided due to recent: ${avoidedThroughRecent}`);
  logger.info(`avoid through config: ${avoidedThroughConfig}`);
  logger.info(`enqueued: ${enqueued}`);
  logger.info(`cached: ${cached}`);
  logger.info(`broken: ${broken}`);
};

if (!linkCheck) {
  logger.error('Missing configuration. Check the linkCheck* properties on config.js_example.')
  throw new Error('Missing configuration. Check the linkCheck* properties on config.js_example.');
}

const main = () => {
  return new Promise((onSuccess, onError) => {
    import('linkstatus').then(processor => {
      logger.info(`\nLink Checker Input`);
      logger.info('----------------------------------------------------');

      const inbox = getInquiries(); // inquiries from files - extracted and pending
      const pending = [];
      const recentMap = getRecent();
      let enqueued = 0;
      let cached = 0;
      let avoidedThroughRecent = 0;
      let avoidedThroughConfig = 0;
      let broken = 0;
      let brokenLink = 0;
      const report = [];

      let reportCount = 0;
      let initialRecentCount;
      const linksToCheck = new Set();
      const reporter = (linkReport, inquiry) => {
        inquiry.checkedAt = new Date().toISOString();
        if (linkReport.requestStatus === "success" && linkReport.status >= 200 && linkReport.status <= 300) {
          inquiry.status = 'success';
          inquiry.statusCode = linkReport.status;
          inquiry.statusMessage = linkReport.statusText;
        } else {
          inquiry.status = 'broken';
          inquiry.statusMessage = linkReport.statusText;
          broken += 1;
          inquiry.attempts = inquiry.attempts ? inquiry.attempts + 1 : 1;
          if (inquiry.attempts < attempts && !inquiryInArray(pending, inquiry)) {
            pending.push(inquiry);
          }
        }
        report.push(inquiry);
        const inquiryRequestData = getRequestData(inquiry);
        if (!recentMap.get(inquiry.uri)) {
          recentMap.set(inquiry.uri, inquiryRequestData);
          if (linkReport.requestStatus !== 'success') brokenLink += 1;
        }

        if (linkCheck.verboseOutput) {
          const linkCheckCount = recentMap.size - initialRecentCount;
          readline.clearLine(process.stdout, 0);
          readline.cursorTo(process.stdout, 0, null);
          process.stdout.write(`Links checked: ${linkCheckCount}  Links remaining: ${linksToCheck.size - linkCheckCount} Broken: ${(brokenLink * 100 / linkCheckCount).toPrecision(4)}%  Inquiries: ${reportCount} Link: ${inquiry.uri}`);
        }
        reportCount += 1;
      };

      const checker = processor.default({
        reporter,
        maxSockets: linkCheck.maxSockets || 10,
        hostDelay: linkCheck.hostDelay || 500,
        dryRun: linkCheck.dryRun === undefined ? false : linkCheck.dryRun
      });

      for (const inquiry of inbox) {
        // recentMap only handles checks from `recents` file
        // newly recent, aka duplicates, get a *cached* response from blc
        if (recentMap.has(inquiry.uri)) {
          const recentInquiry = recentMap.get(inquiry.uri);
          const recentResponse = getRequestData(recentInquiry);
          Object.assign(inquiry, recentResponse); // inquiry might have the same uri but diff props
          report.push(inquiry);
          if (inquiry.status === 'broken') {
            pending.push(inquiry);
          }
          avoidedThroughRecent += 1;

          // Edge case with only pending that are also recent, wouldn't fire BLC's `end` event
          if (avoidedThroughRecent === inbox.length) {
            wrapUp(report, recentMap, pending).then(() => {
              logResults(
                inbox,
                report,
                avoidedThroughRecent,
                enqueued,
                cached,
                broken
              )
              onSuccess();
            });
          }
        } else if (
          linkCheckExclude &&
          linkCheckExclude.some((regex) => regex.test(inquiry.uri))
        ) {
          // If the inquiry URI matches any of the regular expressions in the configuration
          // parameter `linkCheckExclude` mark it as `excluded`
          Object.assign(inquiry, {
            status: 'excluded',
            statusCode: null,
            statusMessage: 'Ignored due to configuration setting',
            checkedAt: new Date().toISOString(),
            attempts: 0,
          });
          report.push(inquiry);
          avoidedThroughConfig += 1;
        } else {
          try {
            checker.enqueue(inquiry.uri, inquiry);
            linksToCheck.add(inquiry.uri);
            enqueued += 1;
          } catch (err) {
            logger.error(`Failed to enqueue link ${inquiry.uri}`);
          }
        }
      }
      initialRecentCount = recentMap.size;
      logger.info(`Starting link check at: ${new Date()}`);
      checker.run().then(async () => {
        logger.info(`\nEnded link check at: ${new Date()}`);
        await wrapUp(report, recentMap, pending);
        logResults(
          inbox,
          report,
          avoidedThroughRecent,
          avoidedThroughConfig,
          enqueued,
          cached,
          broken
        );
        onSuccess();
      });
    });
  });
};

module.exports = { main };
