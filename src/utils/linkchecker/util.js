const md5 = require('md5');

/**
 * @typedef {import('./io.js').Inquiry} Inquiry
 * @typedef {import('./io.js').PendingInquiry} PendingInquiry
 */

const { linkCheckSkipRetryHours } = require('../../config');

/**
 * Isolates and returns the request data from an inquiry.
 *
 * @return {Object}
 */
const getRequestData = (inquiry) => ({
  uri: inquiry.uri,
  status: inquiry.status,
  statusCode: inquiry.statusCode,
  statusMessage: inquiry.statusMessage,
  checkedAt: inquiry.checkedAt,
  attempts: inquiry.attempts,
});

const mapInquiriesToContext = (inquiries) => {
  const contextInquiriesMap = new Map(
    Array.from(inquiries, (inquiry) => [inquiry.context, []])
  );
  inquiries.forEach((inquiry) =>
    contextInquiriesMap.get(inquiry.context).push(inquiry)
  );
  return contextInquiriesMap;
};

/**
 * Checks whether two inquiries are similar (can still have different `createdAt`).
 *
 * @param {Inquiry} inquiryA
 * @param {Inquiry} inquiryB 
 * @return {boolean}
 */
const inquiriesLooselyEqual = (inquiryA, inquiryB) =>
  inquiryA.uri === inquiryB.uri &&
  inquiryA.context === inquiryB.context &&
  inquiryA.propery === inquiryB.propery &&
  inquiryA.entryURI === inquiryB.entryURI &&
  inquiryA.entryLabel === inquiryB.entryLabel &&
  inquiryA.entryType === inquiryB.entryType;

const inquiryHash = (inquiry) => md5(`${inquiry.uri}_${inquiry.entryURI}_${inquiry.property}`);

/**
 * Checks whether an array includes an inquiry.
 *
 * @param {PendingInquiry[]} array !inqarray
 * @param {PendingInquiry} inquiry
 * @return {boolean}
 */
const inquiryInArray = (array, inquiry) =>
  array.some((arrayInquiry) => inquiriesLooselyEqual(arrayInquiry, inquiry));

/**
 * Return the timeframe (in milliseconds) according to which a check is
 * considered recent.
 *
 * @return {number}
 */
const getSkipTimeframe = () => {
  return (linkCheckSkipRetryHours || 6) * 60 * 60 * 1000;
};


const inquireMap = (map, inquiries) => {
  const _map = map || new Map();
  inquiries.forEach(i => _map.set(inquiryHash(i), i));
  return _map;
}

const inquireMapToArray = (map) => Array.from(map.values());

/**
 * Merge two arrays of inquiries, assuming all inquiries in the 'newer' array is more up to date
 * and will replace any inquires in the 'older' array if they are the same.
 * Two inquires are assumed to be the same if they have the same entryURI, uri and property.
 *
 * @param older
 * @param newer
 * @return {unknown[]}
 */
const mergeInquires = (older, newer) =>
  inquireMapToArray(inquireMap(inquireMap(null, older), newer));

module.exports = {
  getRequestData,
  mapInquiriesToContext,
  inquiryInArray,
  getSkipTimeframe,
  inquireMap,
  inquireMapToArray,
  mergeInquires
};