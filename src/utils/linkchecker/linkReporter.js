/**
 * @typedef {import('./io.js').PendingInquiry} PendingInquiry
 */

const { namespaces, promiseUtil } = require('@entryscape/entrystore-js');
const { linkReportCleanup } = require('../../config');
const authenticate = require('../authenticate');
const globals = require('../globals');
const { mapInquiriesToContext } = require('./util');
const { getToBeReported, updateToBeReported } = require('./io');
const {
  exportLinkChecks: exportApiLinkChecks,
} = require('../apiChecker/apiQueue');
const { exportDataLinks } = require('../../datacache/dataQueue');
const logger = require('../logger/logger');

const es = globals.getEntryStore();
const esu = globals.getEntryStoreUtil();
namespaces.add('esterms', 'http://entryscape.com/terms/');

/**
 * Creates a report entry for each context.
 *
 * @param {PendingInquiry[]} recent
 */
const updateReportsInEntryStore = async (reportInquiries) => {
  await authenticate();

  let contextInquiriesMap = mapInquiriesToContext(reportInquiries);
  const contexts = Array.from(contextInquiriesMap.keys());

  // Save one report per context. Wait for each report before continuing to the next.
  await promiseUtil.forEach(contexts, async (context) => {
    const inquiries = contextInquiriesMap.get(context);
    const pe = es.getContextById(context)
      .newEntry()
      .add('rdf:type', 'esterms:LinkCheckReport')
      .addD( 'dcterms:created', new Date().toISOString(), 'xsd:dateTime') // Maybe not neccessary
      .addD( 'esterms:linkChecks', `${inquiries.length}`, 'xsd:integer');
    let failedChecksCounter = 0;
    let excludedChecksCounter = 0;
    inquiries.forEach((check) => {
      if (check.status === 'broken') {
        failedChecksCounter += 1;
// No need to store this information in the metadata as we have it in the detailed report.
// pe.add('esterms:failedLinkCheck', check.uri);
      } else if (check.status === 'excluded') {
        excludedChecksCounter += 1;
// No need to store this information in the metadata as we have it in the detailed report.
// pe.add('esterms:excludedLinkCheck', check.uri);
      }
    });
    pe.addD('esterms:failedLinkChecks',`${failedChecksCounter}`, 'xsd:integer')
      .addD( 'esterms:excludedLinkChecks',`${excludedChecksCounter}`,'xsd:integer');

    try {
      const entry = await pe.commit();
      const ruri = entry.getResourceURI();
      const resource = entry.getResource(true);
      try {
        await resource.putJSON(inquiries);
        logger.info(`> Context ${context} report resource: ${ruri}`);
      } catch (err2) {
        logger.error(`Failed to save detailed linkreport as json: ${err2}`);
      }
    } catch (err) {
      logger.error(`Failed to save linkreport metadata for context ${context}. Error is: ${err}`);
    }
  });

  // We loose the references to allow them to be garbage collected if need be.
  contextInquiriesMap = null;
  reportInquiries = null;

  // Remove all link reports except three latest in each context
  if (linkReportCleanup) {
    await promiseUtil.forEach(contexts, async (context) => {
      es.getCache().clear();
      const threeDaysAgo = new Date();
      threeDaysAgo.setDate(threeDaysAgo.getDate()-3);
      const removedURIs = await esu.removeAll(es.newSolrQuery().context(context)
        .rdfType('esterms:LinkCheckReport').limit(2).createdRange(null, threeDaysAgo));
      logger.info(`Removed ${(removedURIs || []).length} linkcheck reports in context ${context}`);
    });
  }
};

/**
 * Should be run first after link export and link check has been exectued.
 * The link check can be run multiple times until pending queue is empty.
 *
 * @return {Promise<void>}
 */
const report = async () => {
  const inquiries = await getToBeReported();
  exportApiLinkChecks(inquiries);
  exportDataLinks(inquiries);
  await updateReportsInEntryStore(inquiries);
  // Clear the inquiries to be reported on disk, i.e. start over with a new report
  updateToBeReported([]);
};

module.exports = { report };
