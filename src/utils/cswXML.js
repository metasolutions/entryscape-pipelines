const xpath = require('xpath');
const { DOMParser, XMLSerializer } = require('xmldom');

const NAMESPACE_CSW = 'http://www.opengis.net/cat/csw/2.0.2';
const NAMESPACE_OGC = 'http://www.opengis.net/ogc';

const getSearchResultCounts = (select, doc) => {
  const searchResultsNode = select('/csw:GetRecordsResponse/csw:SearchResults', doc)[0];
  if (searchResultsNode) {
    return {
      matched: searchResultsNode.getAttribute('numberOfRecordsMatched'),
      returned: searchResultsNode.getAttribute('numberOfRecordsReturned'),
    };
  }

  const locator = select('/ows:ExceptionReport/ows:Exception/@locator', doc)[0];
  const execptionText = select('/ows:ExceptionReport/ows:Exception/ows:ExceptionText', doc)[0];
  if ((execptionText && execptionText.firstChild && execptionText.firstChild.data.includes('locator=startPosition'))
      || (locator && locator.value === 'startPosition')) {
    return { matched: 0, returned: 0 };
  }
  throw new Error('Problem parsing response.');
};

const getRecords = (select, doc, recordXPath) => select(recordXPath, doc);

const getRecordsWithFilter = (select, records = [], filters) => records.filter((rec) => filters.every((filter) => {
  // if the value to check exists then do a value check (size check if it's array)
  // otherwise just check if there's a value returned
  if (typeof filter.value === 'function') {
    const params = select(filter.xpath, rec);
    return filter.value(params);
  }
  if (Array.isArray(filter.value)) {
    const res = select(filter.xpath, rec);
    return filter.value.indexOf(res) !== -1;
  }
  if (filter.value) {
    const res = select(filter.xpath, rec);
    return ((Array.isArray(res) && (res.length === filter.value)) || (res === filter.value));
  }
  return select(filter.xpath, rec)[0];
}));

const recordPassesFilters = (record, namespaces, filters) => {
  const select = xpath.useNamespaces(namespaces);
  return getRecordsWithFilter(select, [record], filters).length === 1;
};

/**
 * @exports csw
 */
const csw = {
  recordPassesFilters,
  getRecordsQueryXML(info) {
    const {
      service,
      constraintLanguage,
      startPosition,
      elementSetName,
      resultType,
      typeNames,
      outputSchema,
      maxRecords,
      limit,
      propertyFilter,
    } = info;

    // create document
    const doc = new DOMParser().parseFromString(
        `<csw:GetRecords 
            xmlns:csw="${NAMESPACE_CSW}" 
            xmlns:ogc="${NAMESPACE_OGC}" 
            service="${service}"
            resultType="${resultType}" 
            startPosition="${startPosition}" 
            maxRecords="${maxRecords}"
            outputSchema="${outputSchema}"  
            outputFormat="application/xml" 
            version="2.0.2" 
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
            xsi:schemaLocation="http://www.opengis.net/cat/csw/2.0.2 
            http://schemas.opengis.net/csw/2.0.2/CSW-discovery.xsd"></csw:GetRecords>`, 'text/xml');

    // get root node
    const getRecordsNode = doc.getElementsByTagName('csw:GetRecords')[0];

    // create query
    const queryNode = doc.createElement('csw:Query');
    queryNode.setAttribute('typeNames', 'csw:Record');
    getRecordsNode.appendChild(queryNode);

    // create query params
    const elSetNameNode = doc.createElement('csw:ElementSetName');
    elSetNameNode.appendChild(doc.createTextNode(elementSetName));
    queryNode.appendChild(elSetNameNode);

    return doc;
  },

  parseRecord(recordXML, info) {
    return new DOMParser().parseFromString(recordXML);
  },
  parseRecords(recordsResultsXML, info) {
    const { namespaces, filters, recordXPath } = info;
    const select = xpath.useNamespaces(namespaces);

    const doc = new DOMParser().parseFromString(recordsResultsXML);
    const { matched, returned } = getSearchResultCounts(select, doc);
    const unfilteredRecords = getRecords(select, doc, recordXPath);
    const records = getRecordsWithFilter(select, unfilteredRecords, filters); // filter
    // records

    return {
      records,
      matched,
      returned,
    };
  },
  xmlToString(node) {
    return new XMLSerializer().serializeToString(node);
  },
};

module.exports = csw;
