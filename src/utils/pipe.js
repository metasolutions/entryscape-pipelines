function pipe() {
  if (!arguments.length) throw new Error('pipe requires one or more arguments');
  const args = Array.isArray(arguments[0]) ? arguments[0] : [].slice.apply(arguments);

  return reduce(kestrel, args[0], rest(args));
}

function rest(array) {
  return array.slice(1);
}

function reduce(fn, acc, list) {
  return list.reduce(fn, acc);
}

function kestrel(a, b) {
  return function () {
    const self = this;
    return a.apply(self, arguments)
      .then(result => b.call(self, result));
  };
}

module.exports = pipe;

