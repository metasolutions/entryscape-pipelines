const globals = require('../../utils/globals');
const { namespaces, terms } = require('@entryscape/entrystore-js');

namespaces.add('esterms', 'http://entryscape.com/terms/');

/**
 *
 * @param {store/Entry} pipelineEntry
 * @return {string}
 */
const getNotificationSettingsDefaultEmail = (pipelineEntry) => {
  const md = pipelineEntry.getMetadata();
  const email = md.findFirstValue(pipelineEntry.getResourceURI(), 'foaf:mbox');

  return email
    ? email.replace('mailto:', '')
    : config.mail && config.to;
};

/**
 * Get saved email notification settings or a default one
 * @param pipelineEntry
 * @return {Promise<{}|Object>}
 */
const getNotificationsSettings = async (pipelineEntry) => {
  const pipelineResource = pipelineEntry.getResource(true);
  const settings = pipelineResource.getPipelineArguments(terms.argumentTypes.configuration);

  if (settings.email) {
    settings.isEnabled = (settings.isEnabled === 'true'); // cheap coercion
    settings.email = settings.email || getNotificationSettingsDefaultEmail(pipelineEntry);
    return settings;
  }

  return {
    isEnabled: false,
  };
};


module.exports = {
  getNotificationsSettings,
};
