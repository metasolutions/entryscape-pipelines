const { template } = require('lodash');
const config = require('../config');
const rdf = require('./rdf');

const newURLPrototypeEntry = function (context) {
  if (config.newURLPattern) {
    return context.newLink(template(config.newURLPattern)({ contextId: context.getId(), entryId: '_newId' }));
  }
  return context.newEntry();
};


var entry = {

  /**
   * Same as create, although this one extracts a suitable metadata
   * from the bigger graph via the closure algorithm.
   *
   * @param {store/Context} context
   * @param {string} origURL
   * @param {rdfjson/Graph} graph
   * @param {object=} ignore is an object with predicates as attributes,
   * which are to be ignored (excluded)         * @returns {store/PrototypeEntry}
   */
  extractAndCreate(context, origURL, graph, ignore) {
    return entry.create(context, origURL, rdf.closure(graph, origURL, ignore));
  },

  /**
   * Create a PrototypeEntry with the given metadata and url.
   * If the origURL is a blank node it is replaced with:
   *  - a URL created from the newURLPattern when provided in the config
   *  - a repository URL otherwise
   *
   * If the origURL is a URI and it matches replaceOldURLPattern:
   *  - a new URL according to the newURLPattern if provided in the config
   *  - a repository URL otherwise
   *
   * If the origURL is a URI and it does not match replaceOldURLPattern (or not specified):
   *  - the origURL is kept
   *
   * @param {store/Context} context
   * @param {string} origURL
   * @param {rdfjson/Graph} metadata
   * @returns {store/PrototypeEntry}
   */
  create(context, origURL, metadata) {
    let pe;
    if (origURL.indexOf('http') !== 0) { // Is blank node?
      pe = newURLPrototypeEntry(context);
    } else if ((config.replaceOldURLPattern
      && origURL.match(config.replaceOldURLPattern) !== null)) {
      pe = newURLPrototypeEntry(context);
    } else {
      pe = context.newLink(origURL);
    }

    if (metadata) {
      if (origURL !== pe.getResourceURI()) {
        metadata.replaceSubject(origURL, pe.getResourceURI());
      }
      pe.setMetadata(metadata);
    }
    return pe;
  },
};

module.exports = entry;
