const config = require('../config');
const rdf = require('./rdf');
const entry = require('./entry');
const { promiseUtil } = require('@entryscape/entrystore-js');

let agentId2prom = {};
let vcardmail2prom = {};

const getORCreateAgent = function (graph, value, entrystore, context) {
  let mail = graph.findFirstValue(value, 'foaf:mbox'),
    new_search = entrystore.newSolrQuery()
    .rdfType(['foaf:Agent', 'foaf:Person', 'foaf:Organization']);
  if (agentId2prom[mail || value]) {
    return agentId2prom[mail || value];
  }
  if (mail) {
    new_search.objectUri(mail);
  } else {
    new_search.resource(value);
  }
  if (context) {
    new_search.context(context);
  }
  const agentPromise = new_search.list().getEntries(0).then((agents) => {
    if (agents.length > 0) {
      return agents[0];
    }
    return entry.create(context, value,
      rdf.closure(graph, value)).commit();
  });
  agentId2prom[mail || value] = agentPromise;
  return agentPromise;
};

const getORCreateVCard = function (graph, value, entrystore, context) {
  let mail = graph.findFirstValue(value, 'vcard:hasEmail'),
    new_search = entrystore.newSolrQuery()
    .rdfType(['vcard:Kind', 'vcard:Individual', 'vcard:Organization']);
  if (vcardmail2prom[mail || value]) {
    return vcardmail2prom[mail || value];
  }
  if (mail) {
    new_search.objectUri(mail);
  } else {
    new_search.resource(value);
  }

  if (context) {
    new_search.context(context);
  }
  const vcardprom = new_search.list().getEntries(0).then((agents) => {
    if (agents.length > 0) {
      return agents[0];
    }
    return entry.create(context, value,
      rdf.closure(graph, value)).commit();
  });
  vcardmail2prom[mail || value] = vcardprom;
  return vcardprom;
};

module.exports = {
  /**
   * Creates entries for all responsibles, i.e. instances of foaf:Agent or vcard:Kind
   * (or of subclasses of those) which are available in ingraph.
   *
   * @param {rdfjson/Graph} inGraph the graph containing the metadata containing responsibles
   * @param {store/EntryStore} entrystore
   * @param {string} ctxid id of the context where referring datasets are imported, this value is ignored if the config provides an explicit context for where all responsibles should be managed (attribute 'responsibleContext').
   * @returns {object} upon successful creation an object is returned having two mappings of responsibles, 'id2PEntries' and 'id2CPEntries', from the subject uri used in the inGraph and its corresponding created(or found) entry
   */
  getOrCreate(inGraph, entrystore, ctxid) {
    let id2Publisher = {},
      id2Contact = {},
      context;

    if (config.responsibleContext) {
      context = entrystore.getContextById(config.responsibleContext);
    } else {
      agentId2prom = {};
      vcardmail2prom = {};
      context = entrystore.getContextById(ctxid);
    }

    // Find all agents via types.
    const foafAgentStmts = inGraph.find(null, 'rdf:type', 'foaf:Agent');
    const foafPersonStmts = inGraph.find(null, 'rdf:type', 'foaf:Person');
    const foafOrgStmts = inGraph.find(null, 'rdf:type', 'foaf:Organization');
    const foafPubStmts = foafAgentStmts.concat(foafPersonStmts, foafOrgStmts);
    foafPubStmts.forEach((publisherStmt) => {
      const uri = publisherStmt.getSubject();
      id2Publisher[uri] = uri;
    });

    // Find all vcards via dcat:contactPoint relation.
    const contPtStmts = inGraph.find(null, 'dcat:contactPoint', null);
    contPtStmts.forEach((contactStmt) => {
      const uri = contactStmt.getValue();
      id2Contact[uri] = uri;
    });

    // Now make sure all vcards and agents are loaded / created before returning
    return promiseUtil.forEach(id2Publisher, id => getORCreateAgent(inGraph, id, entrystore, context)).then(() => promiseUtil.forEach(id2Contact, id => getORCreateVCard(inGraph, id, entrystore, context))).then(() => ({ id2PEntries: id2Publisher, id2CPEntries: id2Contact }));
  },
};
