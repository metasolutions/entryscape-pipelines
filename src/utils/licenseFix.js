const config = require('../config');


const convertLicense = (license) => {
  if (config.licenses) {
    const mapped = config.licenses.map[license];
    if (mapped) {
      return mapped;
    }
    const accept = Array.isArray(config.licenses.accept) ? config.licenses.accept : [config.licenses.accept];
    if (accept.find(check => {
      if (typeof check === 'string') {
        return check === license;
      } else if (check instanceof RegExp) {
        return check.test(license);
      }
      return false;
    })) {
      return license;
    }
    return undefined;
  }
  return license;
};


module.exports = (licenses) => {
  let converted = new Set();
  if (config.licenses) {
    licenses.forEach(l => {
      const cl = convertLicense(l);
      if (cl) {
        converted.add(cl);
      }
    });
    if (converted.size === 0) {
      if (licenses.size > 0) {
        if (config.licenses.otherlicense) {
          converted.add(config.licenses.otherlicense);
        }
      } else if (config.licenses.nolicense) {
        converted.add(config.licenses.nolicense);
      }
    }
  }
  return converted;
}