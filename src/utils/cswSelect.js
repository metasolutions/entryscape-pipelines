const xpath = require('xpath');
module.exports = xpath.useNamespaces({
  gmd: 'http://www.isotc211.org/2005/gmd',
  gco: 'http://www.isotc211.org/2005/gco',
  gmx: 'http://www.isotc211.org/2005/gmx',
  srv: 'http://www.isotc211.org/2005/srv',
  xlink: 'http://www.w3.org/1999/xlink',
  gml: 'http://www.opengis.net/gml',
  "gml3.2": 'http://www.opengis.net/gml/3.2',
  che: 'http://www.geocat.ch/2008/che',
});
