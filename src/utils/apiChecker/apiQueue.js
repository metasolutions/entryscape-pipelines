const fileSystem = require('fs');
const path = require('path');
const { extractLinksApiProps = {}} = require('../../config');
const logger = require('../logger/logger');

const dir = path.join(__dirname, '..', '..', '..', 'output', 'links', '/');

module.exports = {
  /**
   * Filters link checks that correspond to APIs according to the 'extractLinksApiProps'
   * configuration and writes them on a JSON file.
   *
   * @param {Object[]} report - Non-filtered link checks
   */
  exportLinkChecks(report) {
    if (!fileSystem.existsSync(dir)) {
      fileSystem.mkdirSync(dir);
    }

    // Filter out the checks according to the provided configuration
    const apiLinkChecks = report.filter(
      ({ entryType, property }) =>
        Object.keys(extractLinksApiProps).includes(entryType) &&
        extractLinksApiProps[entryType].includes(property)
    );

    const fileName = `api-links.json`;
    const filePath = `${dir}${fileName}`;

    if (apiLinkChecks.length === 0) {
      logger.info(
        `API Queue - No API links found for exporting. Check 'extractLinksApiProps' in your configuration.`
      );
      return;
    }

    const fileDescriptor = fileSystem.openSync(filePath, 'w');
    fileSystem.writeSync(
      fileDescriptor,
      JSON.stringify(apiLinkChecks, null, '  '),
      0,
      'utf8'
    );
    logger.info(
      `API Queue - Exported ${apiLinkChecks.length} API link(s) on file ${fileName}.`
    );
  },

  /**
   * Retrieves the checks stored in the api-links.json file,
   *
   * @return {Array}
   */
  importChecks() {
    try {
      const jsonData = fileSystem.readFileSync(`${dir}api-links.json`, 'utf8');
      return JSON.parse(jsonData);
    } catch (err) {
      if (err.code == 'ENOENT') {
        logger.error('No API links file found');
      } else {
        logger.error(err);
      }
    }
    return [];
  },
};
