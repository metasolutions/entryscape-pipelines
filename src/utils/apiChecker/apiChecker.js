const { EntryStore } = require('@entryscape/entrystore-js');
const path = require('path');
const fileSystem = require('fs');
const fetch = require('node-fetch');
const yaml = require('yaml');
const config = require('../../config');
const authenticate = require('../authenticate');
const { importChecks } = require('./apiQueue');
const globals = require('../globals');
const logger = require('../logger/logger');

// Load from apiQueue  (filtered so only relevant potiential API checks remains, link checker has run already on these)
// Load content, ignore if larger than 2 Mb.
// Parse as JSON
// Simple detect if openapi / swagger via attributes in json ("openapi" / "swagger")
// Group by dataset if distribution, will require a query to entryStore per distribution.
// Export the following (where "43369" is the dataset/dataservice level):
// If we have a DataService, the detections will be directly on the service level,
// and the detections will repeat the same entryId.

/*
{
    "data": [
        {
            "contextId" : "43",
            "entryId" : "43369", // Dataset
            "detections" : [
                {
                    "entryId" : "43323",   // Distribution
                    "apiDefinition": "http://urltillswagger",
                    "type": "swagger",
                    "version": "2.0"
                },
                {
                    "entryId" : "43324", // Distribution
                    "apiDefinition": "http://urltillswagger",
                    "type": "swagger",
                    "version": "3.0"
                }
            ]
        },
        {
            "contextId" : "43",
            "entryId" : "43370", // Data service
            "detections" : [
                {
                    "entryId" : "43370",   // Same as above
                    "apiDefinition": "http://urltillswagger",
                    "type": "swagger",
                    "version": "2.0"
                }
            ]
        }
    ]
}
*/

// TODO
// ! PIP-15 - file history management - don't delete/override old files
// ! PIP-14 - take file size into account, only up to 2Mb

const dir = path.join(__dirname, '..', '..', '..', 'output', 'links', '/');

/**
 * Returns the entry under which the api links will be grouped. Name is
 * misleading since we only use the parent entry in case of a check corresponding
 * to a distribution. In case of a data service we actually return the entry itself.
 *
 * @param {EntryStore} entryStore
 * @param {string} entryType
 * @param {string} resourceURI
 * @param {string} entryURI
 * @returns {Entry}
 */
const getParentEntry = async (
  entryStore,
  entryType,
  entryURI,
  resourceURI,
  context
) => {
  if (entryType === 'dcat:DataService') {
    const dataServiceEntry = await entryStore.getEntry(entryURI);
    return dataServiceEntry;
  }

  // If the check corresponds to a distribution we need to find its resource URI
  // to then query for the parent dataset
  if (!resourceURI) {
    const entry = await entryStore.getEntry(entryURI);
    resourceURI = entry.getResourceURI();
  }

  const parentEntries = await entryStore
    .newSolrQuery()
    .context(context)
    .uriProperty('dcat:distribution', resourceURI)
    .limit(1)
    .list()
    .getEntries();

  return parentEntries[0];
};

/**
 * Converts map data to json.
 *
 * @param {EntryStore} entryStore
 * @param {Map} parentEntryMap
 * @returns {Object}
 */
const getExportData = (entryStore, parentEntryMap) => {
  const exportData = { data: [] };
  [...parentEntryMap.keys()].map((key) => {
    const parentEntryObject = {
      contextId: parentEntryMap.get(key)[0].parentEntryContextId,
      entryId: parentEntryMap.get(key)[0].parentEntryId,
      detections: [],
    };

    parentEntryMap.get(key).map((apiLink) => {
      parentEntryObject.detections.push({
        entryId: entryStore.getEntryId(apiLink.entryURI),
        apiDefinition: apiLink.uri,
        type: apiLink.apiType,
        version: apiLink.apiVersion,
        format: apiLink.format,
        allowOrigin: apiLink.allowOrigin
      });
    });
    exportData.data.push(parentEntryObject);
  });

  return exportData;
};

const formats = {
  json: ['application/json'],
  yaml: ['text/vnd.yaml', 'application/yaml'],
};

module.exports = {
  async main() {
    await authenticate();
    const checks = importChecks();
    logger.info(`Imported links from api-links.json: ${checks.length}`);

    // Filter can't be used asynchronously so using map wrapped with Promise.all
    await Promise.all(
      checks.map(async (check) => {
        if (check.status !== 'success') {
          check.api = false;
          return check;
        }

        try {
          const options = config.apiCheckOrigin ? { headers: {Origin: config.apiCheckOrigin}} : {};
          const response = await fetch(check.uri, options);
          const contentType = response.headers.get('Content-Type');
          check.allowOrigin = response.headers.get('Access-Control-Allow-Origin');
          let data;

          if (contentType.indexOf('application/json') > -1) {
            // Retrieve JSON if media type is correct and body can be parsed
            // An exception will be thrown if it cannot be parsed as JSON
            data = await response.json();
            check.format = 'json';
          } else {
            // If not JSON, try to interpret body as Yaml
            // An exception will be thrown if it cannot be parsed as yaml
            const body = await response.text();
            data = yaml.parse(body,undefined, {loglevel: 'silent'});
            check.format = 'yaml';
          }

          // Mark as API if JSON/Yaml response contains either a 'swagger' or 'openapi' property
          check.api = data.swagger || data.openapi;
          check.apiType =
            (data.swagger && 'swagger') || (data.openapi && 'openapi');
          check.apiVersion = data.swagger || data.openapi;
          return check;
        } catch (error) {
          logger.error(error);
          check.api = false;
          return check;
        }
      })
    );

    const apiLinks = checks.filter((check) => check.api);
    logger.info(`Swagger or OpenAPI links: ${apiLinks.length}`);

    const parentEntryMap = new Map();
    const entryStore = globals.getEntryStore();
    await Promise.all(
      apiLinks.map(async (apiLink) => {
        const { entryType, entryURI, resourceURI, context } = apiLink;
        const parentEntry = await getParentEntry(
          entryStore,
          entryType,
          entryURI,
          resourceURI,
          context
        );
        apiLink.parentEntryId = parentEntry.getId();
        apiLink.parentEntryContextId = parentEntry.getContext().getId();

        const identifier = `${apiLink.parentEntryId}-${apiLink.parentEntryContextId}`;
        parentEntryMap.has(identifier)
          ? parentEntryMap.get(identifier).push(apiLink)
          : parentEntryMap.set(identifier, [apiLink]);
      })
    );

    const exportData = getExportData(entryStore, parentEntryMap);
    const exportFile = `${dir}api-link-check-results.json`;
    fileSystem.writeSync(
      fileSystem.openSync(exportFile, 'w'),
      JSON.stringify(exportData, null, '  '),
      0,
      'utf8'
    );
  },
};
