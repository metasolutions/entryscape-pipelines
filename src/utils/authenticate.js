const globals = require('./globals');
const local = require('../local');
const logger = require('./logger/logger');

module.exports = () =>  globals.getEntryStore().getAuth()
  .login(local.user, local.password).catch((err) => {
    logger.info(`Could not authenticate user ${local.user}`);
    throw err;
  });
