
// Fetch transform logging info

const loadFromUrl = (url) => `Loaded records from: ${url}`;

const fetch = { loadFromUrl };

module.exports = { fetch };
