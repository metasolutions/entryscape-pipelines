const log4js = require('log4js');
const config = require('../../config.js');


const setLevel = (logger) => {
  let level = 'info';
  if (config.logger && config.logger.level) level = config.logger.level;
  logger.level = level;
};

const logger = log4js.getLogger();
setLevel(logger);


module.exports = logger;
