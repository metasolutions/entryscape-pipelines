
const fs = require('fs');
const { converters } = require('@entryscape/rdfjson');
const logger = require('./logger/logger');

const saveToFile = (destinationGraph, file) => {

    const xml = converters.rdfjson2rdfxml(destinationGraph);

    const datetime = new Date().toISOString();
    const xmlPathDate = `${file}-${datetime}.rdf`;
    const xmlPath = `${file}.rdf`;
    const xmlPathDateFd = fs.openSync(xmlPathDate, 'w');
    fs.writeSync(xmlPathDateFd, xml, 0, 'utf8');
    fs.copyFileSync(xmlPathDate, xmlPath);
};

const report = (destinationGraph, types, comment) => {
    const stats = {};
    types.map((t) => {
        stats[t] = destinationGraph.find(null, 'rdf:type', t).length;
    })
    if (comment) logger.info(comment);
    logger.info(stats);
};

const prune = (destinationGraph, types) => {

    const catalogs = destinationGraph.find(null, 'rdf:type', 'dcat:Catalog');
    catalogs.forEach((c) => {
        const catalogSubject = c.getSubject();

        // Find all data item pointers of the catalog
        let dataset = destinationGraph.find(catalogSubject, 'dcat:dataset').map(t=>t.getValue());
        let inSeries = destinationGraph.find(catalogSubject, 'dcat:inSeries').map(t=>t.getValue());
        let service = destinationGraph.find(catalogSubject, 'dcat:service').map(t=>t.getValue());
        let dataService = destinationGraph.find(catalogSubject, 'dcat:DataService').map(t=>t.getValue());

        let datasets = dataset.concat(inSeries)
        let services = service.concat(dataService);

        // Check if the data items is a dangling pointer, if so, remove pointer from catalog
        datasets.forEach((r) => {
            const type = destinationGraph.findFirstValue(r, 'rdf:type');
            if (!type) {
                destinationGraph.findAndRemove(catalogSubject, 'dcat:dataset', r);
                destinationGraph.findAndRemove(catalogSubject, 'dcat:inSeries', r);
            }
        });
        services.forEach((r) => {
            const type = destinationGraph.findFirstValue(r, 'rdf:type');
            if (!type) {
                destinationGraph.findAndRemove(catalogSubject, 'dcat:service', r);
                destinationGraph.findAndRemove(catalogSubject, 'dcat:DataService', r);
            }
        });

        // Check contents of catalog again, after removal of dangling pointers
        dataset = destinationGraph.find(catalogSubject, 'dcat:dataset').map(t=>t.getValue());
        inSeries = destinationGraph.find(catalogSubject, 'dcat:inSeries').map(t=>t.getValue());
        service = destinationGraph.find(catalogSubject, 'dcat:service').map(t=>t.getValue());
        dataService = destinationGraph.find(catalogSubject, 'dcat:DataService').map(t=>t.getValue());

        // If no pointers to data items, then remove catalog and its publisher
        const dataitems = dataset.concat(inSeries).concat(service).concat(dataService);
        if (dataitems.length === 0) {
            const publisher = destinationGraph.findFirstValue(catalogSubject, 'dcterms:publisher');
            if (publisher) {
                destinationGraph.findAndRemove(catalogSubject, 'dcterms:publisher');
                
                // If no other catalogs points to this publisher, remove it
                const publisherPointers = destinationGraph.find(null, 'dcterms:publisher', publisher);
                if (publisherPointers.length === 0) {
                    destinationGraph.findAndRemove(publisher);
                }
            }
            // Finally, remove the catalog due to no existing pointers to data items
            destinationGraph.findAndRemove(catalogSubject);
        }
    });
};
  
module.exports = {
  prune,
  report,
  saveToFile
};
  