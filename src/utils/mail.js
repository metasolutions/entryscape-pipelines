const nodemailer = require('nodemailer');
const Mailgen = require('mailgen');
const fs = require('fs');
const config = require('../config');
const mailconfig = require('../mailconfig');
const { getNotificationsSettings } = require('./mail/store');
const logger = require('./logger/logger');

let transporter;
if (mailconfig.smtp && mailconfig.smtp.host && mailconfig.smtp.port && mailconfig.smtp.tls && mailconfig.smtp.tls.secure && mailconfig.smtp.tls.servername) {
  transporter = nodemailer.createTransport(mailconfig.smtp);
} else if (mailconfig.smtp) {
  transporter = nodemailer.createTransport({
    host: mailconfig.smtp.host,
    port: mailconfig.smtp.port,
    auth: mailconfig.smtp.auth,
  });
} else {
  transporter = nodemailer.createTransport({
    sendmail: true,
    newline: 'unix',
    path: '/usr/sbin/sendmail',
  });
}

// Configure mailgen
const mailGenerator = new Mailgen({
  theme: 'salted',
  product: config.mailProduct || {
    name: 'EntryScape',
    link: 'https://entryscape.com',
    logo: 'https://entryscape.com/logo.png'
  }
});

const send = (to, subject, html, forceUpdate) => {
  if ((config.mail && config.mail === true) || forceUpdate) {
    logger.info(`Sending mail:
To: ${to}
Subject: ${subject}`);
    const sendMailArgs = {
      subject,
      html,
    };
    if (config.from) {
      sendMailArgs.from = config.from;
    }
    if (config.replyTo) {
      sendMailArgs.replyTo = config.replyTo;
    }
    if (config.bcc) {
      sendMailArgs.bcc = config.bcc;
    }
    to.split(',').forEach((splittedTo) => {
      sendMailArgs.to = splittedTo.trim();
      transporter.sendMail(sendMailArgs, (err, info) => {
        if (err) {
          logger.error(`Mail not sent: ${err}`);
        } else {
          logger.info(`Mail sent: ${info.response}`);
        }
      });
    });
  } else {
    logger.info(`The following mail was not sent due to mail being disabled (or not configured):
To: ${to}
Subject: ${subject}`);
  }
};

const replaceAndSend = (to, subject, text, emailParams) => {
  const { title } = emailParams;
  try {
    let body;
    if (config.body) {
      body = {};
      config.body.title += title;
      config.body.intro = text;
      body.body = config.body;
    } else {
      body = {
        body: {
          name: title,
          intro: text.replace('__TITLE__', title),
          outro: config.mailOutro || 'Need help, or have questions? Just reply to this email, we\'d love to help.'
        },
      };
    }

    // Generate an HTML email with the provided contents
    const emailBodyHTML = mailGenerator.generate(body);

    // Generates a file to check the email content locally
    // require('fs').writeFileSync('preview.html', emailBodyHTML, 'utf8');

    send(to, subject.replace('__TITLE__', title), emailBodyHTML);
  } catch (e) {
    logger.error('Error:', e.stack);
  }
};

const mergeFailureMessages = {
  subject: {
    firstFailure: 'Harvest job for catalog __TITLE__ failed',
    thirdFailure: 'Harvest job for catalog __TITLE__ failed (third time in a row)',
  },
  message: {
    firstFailure: 'Harvest job for catalog __TITLE__ failed after previous success, first warning!',
    thirdFailure: 'Harvest job for catalog __TITLE__ failed third time in a row after previous success.',
  },
};

/**
 *
 * @param emailParams
 * @param failureType
 */
const mergeFailure = (emailParams, failureType, error) => {
  let to;
  if (config.to && config.to.split(',').length > 1) {
    to = config.to;
  } else {
    to = emailParams.email || (config[failureType] && config[failureType].to);
  }
  if (to) {
    const subject = (config[failureType] && config[failureType].subject) || mergeFailureMessages.subject[failureType];
    let message = (config[failureType] && config[failureType].message) || mergeFailureMessages.message[failureType];
    if (error && error === 'http://entrystore.org/terms/pipelineresult#tooFewResults') message += config.harvestErrorMessage.fewResults;
    if (error && error === 'http://entrystore.org/terms/pipelineresult#loadError') message += config.harvestErrorMessage.loadError;

    replaceAndSend(to, subject, message, emailParams);

    // filterOutFailureRecipients(args, args.toOnFirstFailure).forEach((to) => {
    // replaceAndSend(to, subject, message, emailParams);
    // });

    // filterOutFailureRecipients(args, config.firstFailure && config.firstFailure.to).forEach((to) => {
    //   replaceAndSend(to, subject, message, emailParams);
    // });
  }
};


/**
 *
 * @param emailParams
 */
const validationReport = (emailParams) => {
  const { email, report } = emailParams;
  const to = email || (config.failure && config.failure.to);
  if (to) {
    const subject = (config.validate && config.validate.subject) || 'Validation report for __TITLE__';
    replaceAndSend(to, subject, report, emailParams);
  }
};

/**
 *
 * @param emailParams
 */
const mergeFirstSuccess = (emailParams) => {
  const to = emailParams.email || (config.restored && config.restored.to);
  if (to) {
    const subject = (config.restored && config.restored.subject) || 'Harvest job for __TITLE__ succeeded after previous failure';
    const message = (config.restored && config.restored.message) || 'Harvest job for __TITLE__ succeeded after previous failure.';
    replaceAndSend(to, subject, message, emailParams);
  }
};

const sendByType = async (type, ...args) => {
  // get notification preference settings
  const [pipeline, error] = args;
  const {
    /** @type boolean */
    isEnabled,
    /** @type string */
    email,
    /** @type string 'off|lax|strict' */
    harvesting,
    /** @type string 'off|lax|strict' */
    validation
  } = await getNotificationsSettings(pipeline);

  if (isEnabled) {
    const title = pipeline.getMetadata().findFirstValue(null, 'dcterms:title');
    const emailParams = { email, title };
    switch (type) {
      case 'validationReport':
        const [, report, validationFailureType] = args;
        if (validation !== 'off') {
          emailParams.report = report;
          // if there was a mandatory missing or the user selected to receive
          // email on recommended missing also
          if (validationFailureType === 'mandatory' || validation === 'strict') {
            validationReport(emailParams);
          }
        }
        break;
      case 'mergeFirstSuccess':
        mergeFirstSuccess(emailParams);
        break;
      case 'mergeFailure':
        if (harvesting) {
          mergeFailure(emailParams);
        }
        break;
      case 'mergeFirstFailure':
        if (harvesting === 'strict') {
          mergeFailure(emailParams, 'firstFailure', error);
        }
        break;
      case 'mergeThirdFailure':
        if (harvesting === 'lax') {
          mergeFailure(emailParams, 'thirdFailure', error);
        }
        break;
      default:
        break;
    }
  }
};


// /**
//  *
//  * @param args
//  * @param recipients
//  * @returns {Array}
//  */
// const filterOutFailureRecipients = (args, recipients) => {
//   if (!recipients) {
//     return [];
//   }
//   const failureRecipients = new Set();
//   if (args.toOnFailure) {
//     args.toOnFailure.split(',').forEach((to) => {
//       failureRecipients.add(to.trim());
//     });
//   }
//   if (config.failure && config.failure.to) {
//     config.failure.to.split(',').forEach((to) => {
//       failureRecipients.add(to.trim());
//     });
//   }
//   const newTo = [];
//   recipients.split(',').forEach((to) => {
//     if (!failureRecipients.has(to.trim())) {
//       newTo.push(to.trim());
//     }
//   });
//   return newTo;
// };

module.exports = {
  send,
  sendByType,
};
