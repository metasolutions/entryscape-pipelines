const { converters, Graph } = require('@entryscape/rdfjson');
const fs = require('fs');
const logger = require('./logger/logger');

var rdf = {

  /**
   * It copies over statements from one a metadata graph to another.
   * The statements copied are those with the provided uri in subject position, or
   * statements reachable via intermediate blank nodes from that uri.
   * Statements are not copied over if the predicate is listed in the ignore array.
   *
   * @param {rdfjson/Graph} inGraph graph which holds rdf data in graph format
   * @param {string} uri a starting point to find all statements to include
   * @param {object=} ignore is an object with predicates as attributes,
   * which are to be ignored (excluded)
   * @param {rdfjson/Graph=} outGraph optional graph which holds copied statements
   * when constructing new outGraph, if no outGraph is provided a new will be created.
   * @return {rdfjson/Graph} the graph to which the statements has been copied
   */
  closure(inGraph, uri, ignore, outGraph) {
    outGraph = outGraph || new Graph();
    ignore = ignore || {};
    const stmts = inGraph.find(uri, null, null);
    for (let i = 0; i < stmts.length; i++) {
      const stmt = stmts[i];
      if (!ignore[stmt.getPredicate()]) {
        outGraph.add(stmts[i]);
        if (stmt.getType() === 'bnode') {
          rdf.closure(inGraph, stmt.getValue(), ignore, outGraph);
        }
      }
    }
    return outGraph;
  },

  /**
   * Loads a file and converts into a graph
   * @param filePath - file path of rdf data to be imported
   * @returns {Promise} - returns a promise with a Graph.
   */
  loadRDF(filePath) {
    try {
      return new Promise((resolve, reject) => {
        fs.readFile(filePath, 'utf8', async (err, data) => {
          resolve(await converters.rdfxml2graph(data));
        });
      });
    } catch (error) {
      logger.error('Found error while reading file, check path');
    }
  },

  /**
   * Deletes all entries fo the specified types in a context.
   * (I.e. an entry is deleted if there is an rdf:type pointing to one of the
   * specified types in the metadata graph of the entry.)
   *
   * @param {store/Context} contextId
   * @param {string[]} types
   * @returns {promise} a promise indicating when the clearing is complete
   */
  clearContext(entrystore, contextId, types) {
    const context = entrystore.getContextById(contextId);
    const promises = [];
    return entrystore.newSolrQuery().rdfType(types)
      .context(context).limit(100)
      .list()
      .forEach((entry) => {
        promises.push(entry.del(true));
      });
    return Promise.all(promises);
  }
};

module.exports = rdf;
