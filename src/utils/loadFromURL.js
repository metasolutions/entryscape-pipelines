const Iconv  = require('iconv-lite');
const originalFetch = require('node-fetch');
const fetch = require('fetch-retry')(originalFetch);
const logger = require('./logger/logger');

const config = require('../config');

const retries = config.loadFromURL && config.loadFromURL.retries ? config.loadFromURL.retries : null;
const retryDelay = config.loadFromURL && config.loadFromURL.retryDelay ? config.loadFromURL.retryDelay : null;
const timeoutTime = config.loadFromURL && config.loadFromURL.timeoutTime ? config.loadFromURL.timeoutTime : 30000;
const timeoutDeactivate = config.loadFromURL && config.loadFromURL.timeoutDeactivate ? config.loadFromURL.timeoutDeactivate : false;
const sleep = config.loadFromURL && config.loadFromURL.sleep ? config.loadFromURL.sleep : 0;

const request = (uri, timeoutTime, reqParams) =>  new Promise(async (resolve, reject) => {
  const AbortController = globalThis.AbortController || await require('abort-controller');
  const controller = new AbortController();
  const timeout = setTimeout(() => {
    controller.abort();
  }, timeoutTime);

  try {
    if (!timeoutDeactivate) reqParams.signal = controller.signal;
    const response = await fetch( uri, reqParams);
    if (response.status !== 200) {
      if (response.status) logger.error(`Status response for ${uri} is ${response.status}`);
      throw new Error('Status not 200');
    }
    else {
      let body;
      const contentType = response.headers.get('content-type');
      if (contentType.indexOf('Windows-1252') > -1) {
        const buffer = await response.arrayBuffer();
        body = Iconv.decode(Buffer.from(buffer), 'win1252');
      } else {
        body = await response.text();
      }
      resolve(body);
    }
  } catch (error) {
    reject(error);
  } finally {
    clearTimeout(timeout);
  }
});

module.exports = async (url, head, method = 'GET', params = [], mimeType = null, extraHeaders = {}) => {
  if (url !== null) {
    const reqParams = {};
    reqParams.headers = {'User-Agent': 'Harvesting daemon (node.js)'};
    if (retries) reqParams.retries = retries;
    if (retryDelay) reqParams.retryDelay = retryDelay;
    if (head) {
      return request(url, timeoutTime, reqParams);
    } else {
      reqParams.method = method;
      reqParams.headers['Accept'] = mimeType ? `${mimeType}, */*` : '*/*';
      if (Object.keys(extraHeaders).length !== 0) reqParams.headers = { ...reqParams.headers, ...extraHeaders };
      if(params.length > 0) reqParams['body'] = params;
      logger.debug(`Loading ${url} with method ${method}`);
      if (sleep) await new Promise(r => setTimeout(r, sleep));
      return request(url,  timeoutTime * 2, reqParams);
    }
  }
};
