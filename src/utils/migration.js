const config = require('../config');
const { namespaces } = require('@entryscape/entrystore-js');
const { types } = require('@entryscape/entrystore-js');
const globals = require('./globals');
const pipe = require('./pipe');

/**
 * Migrate from old and specific harvesting mechanism to a more generic one.
 * In practise, replace some properties with more generic names
 */

const specificToGenericProperties = {
  'http://entrystore.org/terms/pipelineresult#psidata': ['storepr:check'],
  'http://entrystore.org/terms/pipelineresult#dcatRDF': ['storepr:fetchSource', 'storepr:fetchRDF'],
  'http://entrystore.org/terms/pipelineresult#dcatsource': ['storepr:fetchSource'],
  'http://entrystore.org/terms/pipelineresult#dcatMerge': ['storepr:merge'],
  'http://entrystore.org/terms/pipelineresult#dcatDatasetCount': ['storepr:mergeMainResourceCount'],
  'http://entrystore.org/terms/pipelineresult#dcatValidationWarnings': ['storepr:validateWarnings'],
  'http://entrystore.org/terms/pipelineresult#dcatValidationErrors': ['storepr:validateErrors'],
  'http://entrystore.org/terms/pipelineresult#dcatValidationDeprecated': ['storepr:validateDeprecated'],
  'http://entrystore.org/terms/pipelineresult#dcatRDFError': ['storepr:fetchRDFError'],
};

const es = globals.getEntryStore();
namespaces.add('store', 'http://entrystore.org/terms/');
namespaces.add('storepr', 'http://entrystore.org/terms/pipelineresult#');
namespaces.add('dcat', 'http://www.w3.org/ns/dcat#');

/**
 * @return {Promise}
 */
const authenticate = () => es.getAuth().login(config.adminuser, config.adminpassword);

/**
 * A solr query to obtain a list of GT_PIPELINERESULT graph types
 */
const getPipelineResults = () => es.newSolrQuery().graphType(types.GT_PIPELINERESULT).list();

/**
 * @return {Promise}
 */
const migratePipelineResults = list => list.forEach((entry) => { // pipeline results entries
  /** @type {rdfjson/Graph} */
  const md = entry.getMetadata();
  let hasChanged = false;
  // for each triple check if it contains an old property and add the required new one(s).
  md.forEach((s, p, o) => {
    if (p in specificToGenericProperties) {
      addNewStmts(entry, s, p, o);
      md.findAndRemove(s, p, o);
      hasChanged = true;
    }
  });

  if (hasChanged) {
    entry.commitMetadata();
  }
});

/**
 * Create new statements for the metadata graph of the entry.
 * s, o remains the same. p is new.
 *
 * @param {store/Entry}
 * @param s
 * @param p
 * @param o
 */
const addNewStmts = (entry, s, p, o) => {
  const md = entry.getMetadata();
  specificToGenericProperties[p].forEach(newPredicate => md.create(s, newPredicate, o));
};

const migrate = pipe(
  authenticate,
  getPipelineResults,
  migratePipelineResults,
);

migrate();
