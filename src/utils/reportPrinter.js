const { namespaces } = require('@entryscape/entrystore-js');
const config = require('../config');

const summary = (report) => {
  const {
    nrOfMandatoryTypesMissing,
    nrOfMandatoryFieldsMissing,
    nrOfRecommendedFieldsMissing,
    nrOfDeprecatedFieldsPresent,
    blockedResources,
    nrOfMainResources
  } = report;
  const msgs = [];

  if (nrOfMainResources || blockedResources.length > 0) {
    const firstMandatoryTypeName = config.validate.firstMandatoryTypeName || 'main resource(s)';
    msgs.push(`${parseInt(nrOfMainResources) + blockedResources.length} ${firstMandatoryTypeName} found`);
  }
  if (blockedResources.length > 0) {
    msgs.push(`of these where ${blockedResources.length} not imported due to lacking mandatory fields, their URIs are:`);
    blockedResources.forEach((br) => msgs.push(br));
    msgs.push('\n');
  }
  if (nrOfMandatoryTypesMissing) {
    msgs.push(`${nrOfMandatoryTypesMissing} mandatory type(s) were not provided`);
  }
  if (nrOfMandatoryFieldsMissing) {
    msgs.push(`${nrOfMandatoryFieldsMissing} mandatory field(s) were not provided`);
  }
  if (nrOfRecommendedFieldsMissing) {
    msgs.push(`${nrOfRecommendedFieldsMissing} recommended field(s) were not provided`);
  }
  if (nrOfDeprecatedFieldsPresent) {
    msgs.push(`${nrOfDeprecatedFieldsPresent} deprecated field(s) were present`);
  }
  return msgs.join('\n');
};

const summaryFromReport = (report) => {
  return summary({
    nrOfMandatoryTypesMissing: (report.mandatoryError || []).length,
    nrOfMandatoryFieldsMissing: report.errors,
    nrOfRecommendedFieldsMissing: report.warnings,
    nrOfDeprecatedFieldsPresent: report.deprecated,
    blockedResources: report.blocked,
    nrOfMainResources: report.mainResources.length - report.blocked.length,
  });
};

const print = (res) => {
  const ms = [];
  if (res.errors.length > 0 || res.warnings.length > 0 || res.deprecated.length > 0) {
    ms.push(`--- Resource with URI "${res.uri}" of type ${namespaces.shortenKnown(res.type)} ---`);
    if (res.errors.length > 0) {
      ms.push(`  ${res.errors.length} mandatory field(s) missing: ${res.errors.map(err => err.path).join(', ')}`);
    }
    if (res.warnings.length > 0) {
      ms.push(`  ${res.warnings.length} recommended field(s) missing: ${res.warnings.map(warn => warn.path).join(', ')}`);
    }
    if (res.deprecated.length > 0) {
      ms.push(`  ${res.deprecated.length} deprecated field(s) present: ${res.deprecated.join(', ')}`);
    }
  }
  return ms.join('\n');
};

const details = (report) => {
  const errors = [];
  const rest = [];
  report.resources.forEach((res) => {
    if (res.errors.length > 0) {
      errors.push(res);
    } else {
      rest.push(res);
    }
  });
  const reps = [];
  const doPrint = rep => {
    const str = print(rep);
    if (str && str.length > 0) {
      reps.push(str);
    }
  };
  errors.forEach(doPrint);
  rest.forEach(doPrint);
  return reps.join('\n\n');
};

const full = (report) => `${summaryFromReport(report)}\n\n----- Detailed report -----\n${details(report)}`;

const extractSummaryStruct = (pipelineEntry) => {
  const md = pipelineEntry.getMetadata();
  return {
    nrOfMandatoryTypesMissing: md.findFirstValue(null, 'storepr:validateMandatoryMissing') || 0,
    nrOfMandatoryFieldsMissing: md.findFirstValue(null, 'storepr:validateErrors') || 0,
    nrOfRecommendedFieldsMissing: md.findFirstValue(null, 'storepr:validateWarnings') || 0,
    nrOfDeprecatedFieldsPresent: md.findFirstValue(null, 'storepr:validateDeprecated') || 0,
    blockedResources: md.find(null, 'storepr:validateBlocked').map((stmt) => stmt.getValue()),
    nrOfMainResources: md.findFirstValue(null, 'storepr:validateMainResourceCount') || 0
  }
};


module.exports = {
  summary,
  summaryFromReport,
  extractSummaryStruct,
  details,
  full,
};
