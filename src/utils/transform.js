const config = require('../config');
const logger = require('./logger/logger');

const extractArray = (str) => {
  if (typeof str === 'string' && str.startsWith('[')) {
    try {
      return JSON.parse(str);
    } catch (e) {
    }
  } else if (Array.isArray(str))  {
    return str;
  } else if (str === undefined) {
    return [];
  }
  return [str];
};


const profileTypes = (args) => {
  let primaryTypes;
  let supportiveTypes;
  if (args.profile && config.profiles && config.profiles[args.profile]) {
    primaryTypes = config.profiles[args.profile].primaryTypes;
    supportiveTypes = config.profiles[args.profile].supportiveTypes;
  } else if (args.primaryTypes && args.supportiveTypes) {
    primaryTypes = args.primaryTypes;
    supportiveTypes = args.supportiveTypes;
  } else if (args.masterType && args.slaveTypes) {
    primaryTypes = args.masterType
    supportiveTypes = args.slaveTypes;
  } else {
    logger.error('No profile types found!');
    throw Error();
  }
  const stypes = extractArray(supportiveTypes);
  const mtypes = extractArray(primaryTypes);
  return [mtypes, stypes];
};

const getCustomTransform = (tranformClass, type) => {
  const path = `${__dirname}/../transforms/custom/transforms/${tranformClass}/${type}`;
  try {
    const result = require(path);
    return result;
  } catch (error) {
    logger.info(`Custom transform for type: ${type} at path ${path} does not exist`);
  }
};
  
module.exports = {
  profileTypes,
  getCustomTransform,
};
  