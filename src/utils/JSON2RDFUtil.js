const iso639 = require('./iso639');

const notURI = (str) => {
  const colonIdx = str.indexOf(':');
  return str.indexOf('http') === -1 && (colonIdx === -1 || colonIdx > 7);
};
const toURI = (base, str) => {
  if (str.indexOf(' ') >= 0 || str.length > 50) {
    return '';
  }
  return base + (encodeURIComponent(str.replace(' ', '').toLowerCase()));
};
const toLangURI = (lang) => {
  switch (lang.length) {
    case 2:
      return iso639.toEU(iso639.to3(lang));
    case 3:
      return iso639.toEU(lag);
    default:
      return '';
  }
};
const toMailURI = (base, mail) => {
  if (mail.indexOf(':') === -1) {
    return `mailto:${mail}`;
  }
  return mail;
};
const mapToRDF = (params) => {
  const { graph, subject, language, parent, record, extras, base, mapping } = params;
  Object.keys(mapping).forEach((property) => {
    const conf = mapping[property];
    const excluded = typeof conf.exclude === 'function' ? conf.exclude :
      (!Array.isArray(conf.exclude) ? () => false : (value) => {
        const _v = typeof value === 'string' ? value.toLowerCase().trim() : value;
        return conf.exclude.some((v) => v.toLowerCase().trim() === _v);
      });
    if (typeof conf === 'function') {
      conf(params);
      return;
    }
    const _addURI = (v) => {
      const _v = notURI(v) ? toURI(base, v) : v;
      if (_v && _v != '') {
        graph.add(subject, property, _v);
      }
    };
    const addURI = (value) => {
      const v = conf.toURI ? conf.toURI(base, value) : value;
      if (Array.isArray(v)) {
        v.forEach(_addURI);
      } else if (typeof v === 'string' && v !== '') {
        _addURI(v);
      }
    };

    const addLiteral = (value) => {
      if (conf.dtype) {
        graph.addD(subject, property, value, conf.dtype);
      } else {
        graph.addL(subject, property, value, conf.lang !== false ? language : undefined);
      }
    };

    // We iterate through all fields, we use array.some instead of array.forEach to be able to stop the iteration
    // in case the conf.singleValued is set to true.
    conf.field.some((field) => {
      let value;
      let blankStmt;
      if (field.indexOf('extra:') === 0) {
        value = extras[field.substr(6)];
      } else if (field.indexOf('parent:') === 0) {
        value = parent[field.substr(7)];
      } else {
        value = record[field];
      }
      if (value === null || typeof value === 'undefined' || value === '') {
        return;
      }
      switch (conf.type || 'literal') {
        case 'uri':
          if (Array.isArray(value)) {
            if (value.forEach((val) => {
              if (excluded(val)) {
                return;
              }
              addURI(conf.objectAttribute ? val[conf.objectAttribute] :
                (typeof val === 'object' && val.display_name ? val.display_name : val));
              return conf.singleValued;
            })) {
              return conf.singleValued;
            }
          } else {
            if (excluded(value)) {
              return false;
            }
            addURI(value);
            return conf.singleValued;
          }
          break;
        case 'blank':
          if (excluded(value)) {
            return false;
          }
          blankStmt = graph.add(subject, property);
          graph.add(blankStmt.getValue(), 'rdf:type', conf.blankType);
          graph.addL(blankStmt.getValue(), conf.property, `${value}`, language);
          return conf.singleValued === true;
        default: // Literal
          if (Array.isArray(value)) {
            if (value.some((val) => {
              if (excluded(val)) {
                return false;
              }
              addLiteral(conf.objectAttribute ? val[conf.objectAttribute] : val);
              return conf.singleValued;
            })) {
              return conf.singleValued;
            }
          } else {
            if (excluded(value)) {
              return false;
            }
            addLiteral(value);
            return conf.singleValued;
          }
      }
    });
  });
};

module.exports = {
  notURI,
  toURI,
  toLangURI,
  toMailURI,
  mapToRDF,
};
