const logger = require('./logger/logger');

let ckan2dcatconverter;
const ckan2dcatmapping = [];

ckan2dcatmapping.language = 'dcterms:language';
ckan2dcatmapping.dcat_publisher_name = 'foaf:name';
ckan2dcatmapping.dcat_publisher_email = 'foaf:mbox';
ckan2dcatmapping.dcat_modified = 'dcterms:modified';
ckan2dcatmapping.dcat_issued = 'dcterms:issued';
// ckan2dcatmapping["guid"] = "dcterms:identifier";
ckan2dcatmapping.maintainer = '';// contact point
ckan2dcatmapping.maintainer_email = '';
ckan2dcatmapping.spatial = 'dcterms:spatial';
ckan2dcatmapping.contact_email = 'vcard:hasEmail';
ckan2dcatmapping.contact_name = 'vcard:fn';
ckan2dcatmapping.publisher_type = 'dcterms:type';
ckan2dcatmapping.spatial_text = 'dcterms:spatial';
ckan2dcatmapping.theme = 'dcat:theme';
ckan2dcatmapping.version_notes = 'adms:versionNotes';
ckan2dcatmapping.publisher_email = 'foaf:mbox';
ckan2dcatmapping.publisher_name = 'foaf:name';

ckan2dcatconverter = {
  /**
   * converts ckanJSON to dcatJSON
   * @param ckanJSON
   * @returns {Promise}
   */
  convert(ckanJSON) {
    const dcatJSON = {};
    let dataset = {};
    const datasets = [];
    let metadata,
      datasetMetadata = {};
    let distributions = [];
    let distribution;
    let results;
    // check result had results or not
    if (ckanJSON.result.hasOwnProperty('results')) {
      results = ckanJSON.result.results;
    } else {
      results = ckanJSON.result;
    }
    let publisherObj = {};
    let publisherMetadata = {};
    let contactPtObj;
    let contactPtMetadata;
    results.forEach((result) => {
      dataset = {};
      datasetMetadata = {};
      distributions = [];
      publisherObj = {};
      publisherMetadata = {};
      contactPtObj = {};
      contactPtMetadata = {};

      ckan2dcatconverter.setLiteral(datasetMetadata, 'dcterms:title', result.title);
      ckan2dcatconverter.setLiteral(datasetMetadata, 'dcterms:description', result.notes);
      ckan2dcatconverter.setURL(datasetMetadata, 'dcat:landingPage', result.url);
      ckan2dcatconverter.setLiteral(datasetMetadata, 'dcterms:identifier', result.id);
      // extras
      // identifier
      // issued
      // modified
      // language

      const extras = result.extras || [];
      extras.forEach((extra) => {
        // logger.info("extra.key = "+ckan2dcatmapping[extra.key]);
        if (ckan2dcatmapping[extra.key] != undefined) {
          if (extra.key.indexOf('publisher_email') != -1) {
            ckan2dcatconverter.setURL(publisherMetadata, ckan2dcatmapping[extra.key], extra.value);
            publisherObj.id = extra.value;// using mail id  as unique key for further process
          } else if (extra.key.indexOf('publisher_name') != -1) {
            ckan2dcatconverter.setLiteral(publisherMetadata, ckan2dcatmapping[extra.key], extra.value);
          } else if (extra.key.indexOf('contact_email') != -1) {
            ckan2dcatconverter.setURL(contactPtMetadata, 'vcard:hasEmail', extra.value);
            contactPtObj.id = extra.value;// using mail is as unique key for further process
          } else if (extra.key.indexOf('contact_name') != -1) {
            ckan2dcatconverter.setLiteral(contactPtMetadata, 'vcard:fn', extra.value);
          } else {
            ckan2dcatconverter.setLiteral(datasetMetadata, ckan2dcatmapping[extra.key], extra.value);
          }
        }
      });
      // tags - keyword
      const tags = result.tags;
      let keyword = '';
      tags.forEach((tag) => {
        keyword += `${tag.name} `;
      });
      ckan2dcatconverter.setLiteral(datasetMetadata, 'dcat:keyword', keyword.trim());

      // maintainer -contact point
      // maintainer_email

      if (result.maintainer != undefined) {
        ckan2dcatconverter.setLiteral(contactPtMetadata, 'vcard:fn', result.maintainer);
      }
      if (result.maintainer_email != undefined) {
        ckan2dcatconverter.setLiteral(contactPtMetadata, 'vcard:hasEmail', result.maintainer_email);
        contactPtObj.id = result.maintainer_email;// using mail is as unique key for further process
      }
      // identifier

      const resources = result.resources;
      // logger.info("number of distributions "+ resources.length);
      resources.forEach((resource) => {
        distribution = {};
        metadata = {};
        if (Object.keys(resource).length != 0) {
          ckan2dcatconverter.setLiteral(metadata, 'dcterms:title', resource.name);
          ckan2dcatconverter.setLiteral(metadata, 'dcterms:description', resource.description);
          // ckan2dcatconverter.setURL(metadata,'dcat:accessURL',resource.download_url);//check parameter which is to be mapped
          ckan2dcatconverter.setLiteral(metadata, 'dcterms:format', resource.format);
          distribution.metadata = metadata;
          distributions.push(distribution);
        }
      });
      const publisher = [];
      if (Object.keys(publisherMetadata).length != 0) {
        publisherObj.metadata = publisherMetadata;
        publisher.push(publisherObj);
      }

      const contactPt = [];
      if (Object.keys(contactPtMetadata).length != 0) {
        contactPtObj.metadata = contactPtMetadata;
        contactPt.push(contactPtObj);
      }

      dataset.metadata = datasetMetadata;
      dataset.distributions = distributions;
      dataset.publisher = publisher;
      dataset.contactPoint = contactPt;
      datasets.push(dataset);
    });
    dcatJSON.datasets = datasets;
    // logger.info( JSON.stringify(dcatJSON, null, 2));

    return Promise.resolve(dcatJSON);
  },
  setLiteral(metadata, property, value) {
    if (value != undefined) {
      // if property is format add text/
      if (property.indexOf('format') != -1) {
        metadata[property] = [{ value: `text/${value}`, type: 'literal', lang: 'en' }];
      } else {
        metadata[property] = [{ value, type: 'literal', lang: 'en' }];
      }
    }
  },
  setURL(metadata, property, value) {
    if (value != undefined) {
      metadata[property] = [{ value, type: 'uri' }];
    }
  },
};

module.exports = ckan2dcatconverter;
