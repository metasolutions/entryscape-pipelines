const addLFactory = function ({graph, subject, language, record, select}) {
  return function ({property, path, isAttr, datatype, check}) {
    const arr = select(path, record);
    if (arr.length !== 0) {
      arr.forEach((n) => {
        if (n.firstChild != null) {
          const val = n.firstChild.nodeValue;
          if (check && !check(val)) {
            return;
          }
          if (datatype) {
            graph.addD(subject, property, isAttr ? n.value : val, datatype);
          } else {
            graph.addL(subject, property, isAttr ? n.value : val, language);
          }
        }
      });
    }
  };
};

const isURI = (str) => {
  if (str !== undefined && str !== '') {
    let v = str;
    if (v.indexOf(':') > 0) {
      v = namespaces.expand(v);
    }
    return v.indexOf('http') === 0 || v.indexOf('mailto') === 0;
  }
  return false;
};

const addUFactory = function ({graph, subject, record, select}) {
  return function ({property, path, map, isAttr, separator}) {
    let _map;
    let oneValue;
    if (typeof map === 'string') {
      _map = v => `${map}${v}`;
    } else if (typeof map === 'object') {
      _map = v => map[v];
    } else {
      _map = v => v;
    }
    const arr = select(path, record);
    if (arr.length !== 0) {
      arr.forEach((n) => {
        if (n.firstChild != null) {
          oneValue = isAttr ? n.value : n.firstChild.nodeValue;
          if (separator) {
            oneValue.split(separator).forEach((v) => {
              const va = _map(v.trim());
              if (isURI(va)) {
                graph.add(subject, property, va);
              }
            });
          } else {
            const va = _map(oneValue.trim());
            if (isURI(va)) {
              graph.add(subject, property, va);
            }
          }
        }
      });
    }
    return oneValue;
  };
};

const createBanCheck = (banned) => {
  if (!banned || banned === '' || (Array.isArray(banned) && banned.length === 0)) {
    return () => true;
  }
  if (typeof banned === 'string') {
    return (val) => val.trim().toLowerCase() !== banned;
  } else if (banned instanceof RegExp) {
    return (val) => !banned.test(val);
  } else if (typeof banned === 'function') {
    return banned;
  } else if (Array.isArray(banned)) {
    return (val) => !banned.some((c) => {
      const v = val.trim().toLowerCase();
      if (typeof c === 'string') {
        return v === c;
      } else if (c instanceof RegExp) {
        return c.test(val);
      } else if (typeof c === 'function') {
        return c(val);
      }
    });
  }
};


module.exports = {
  addLFactory,
  addUFactory,
  isURI,
  createBanCheck
};
