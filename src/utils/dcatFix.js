const { namespaces } = require('@entryscape/entrystore-js');
const licenseFix = require('./licenseFix');


namespaces.add('adms', 'http://www.w3.org/ns/adms#');

const _tm = {
  100142: 'GOVE', // 04 POLITICS
  100143: 'INTR', // 08 INTERNATIONAL RELATIONS
  100144: 'GOVE', // 10 EUROPEAN UNION
  100145: 'JUST', // 12 LAW
  100146: 'ECON', // 16 ECONOMICS
  100147: 'ECON', // 20 TRADE
  100148: 'ECON', // 24 FINANCE
  100149: 'SOCI', // 28 SOCIAL QUESTIONS
  100150: 'EDUC', // 32 EDUCATION AND COMMUNICATIONS
  100151: 'TECH', // 36 SCIENCE
  100152: 'ECON', // 40 BUSINESS AND COMPETITION
  100153: 'SOCI', // 44 EMPLOYMENT AND WORKING CONDITIONS
  100154: 'TRAN', // 48 TRANSPORT
  100155: 'ENVI', // 52 ENVIRONMENT
  100156: 'AGRI', // 56 AGRICULTURE, FORESTRY AND FISHERIES
  100157: 'AGRI', // 60 AGRI-FOODSTUFFS
  100158: 'TECH', // 64 PRODUCTION, TECHNOLOGY AND RESEARCH
  100159: 'ENER', // 66 ENERGY
  100160: 'ECON', // 68 INDUSTRY   -- not a good match
  100161: 'REGI', // 72 GEOGRAPHY  -- not a good match
  100162: 'INTR',  // 76 INTERNATIONAL ORGANISATIONS
};

const tm = {};
Object.keys(_tm).forEach((key) => {
  tm[`http://eurovoc.europa.eu/${key}`] = `http://publications.europa.eu/resource/authority/data-theme/${_tm[key]}`;
});

const _pm = {
  annual: 'ANNUAL',
  biennial: 'BIENNIAL',
  bimonthly: 'BIMONTHLY',
  biweekly: 'BIWEEKLY',
  continuous: 'CONT',
  daily: 'DAILY',
  irregular: 'IRREG',
  monthly: 'MONTHLY',
  quarterly: 'QUARTERLY',
  semiannual: 'ANNUAL_2',
  semimonthly: 'MONTHLY_2',
  semiweekly: 'WEEKLY_2',
  threeTimesAMonth: 'MONTHLY_3',
  threeTimesAWeek: 'WEEKLY_3',
  threeTimesAYear: 'ANNUAL_3',
  triennial: 'TRIENNIAL',
  weekly: 'WEEKLY',
};

const pm = {};
Object.keys(_pm).forEach((key) => {
  pm[`http://purl.org/cld/freq/${key}`] = `http://publications.europa.eu/resource/authority/frequency/${_pm[key]}`;
});

const am = {
  'http://publications.europa.eu/resource/authority/dataset-access/PUBLIC':
  'http://publications.europa.eu/resource/authority/access-right/PUBLIC',
  'http://publications.europa.eu/resource/authority/dataset-access/RESTRICTED':
  'http://publications.europa.eu/resource/authority/access-right/RESTRICTED',
  'http://publications.europa.eu/resource/authority/dataset-access/NON-PUBLIC':
  'http://publications.europa.eu/resource/authority/access-right/NON-PUBLIC',
};
const dt = namespaces.expand('xsd:date');

module.exports = {
  to2(graph) {
    graph.find(null, 'schema:startDate').forEach((stmt) => {
      stmt.setPredicate('dcat:startDate');
    });
    graph.find(null, 'schema:endDate').forEach((stmt) => {
      stmt.setPredicate('dcat:endDate');
    });
  },
  to11(graph) {
    graph.find(null, 'dcat:theme').forEach((stmt) => {
      const match = tm[stmt.getValue()];
      if (match) {
        stmt.setValue(match);
      }
    });
    graph.find(null, 'dcterms:accessRights').forEach((stmt) => {
      const match = am[stmt.getValue()];
      if (match) {
        stmt.setValue(match);
      }
    });

    graph.find(null, 'dcterms:accrualPeriodicity').forEach((stmt) => {
      const match = pm[stmt.getValue()];
      if (match) {
        stmt.setValue(match);
      }
    });

    graph.find(null, 'adms:contactPoint').forEach((stmt) => {
      stmt.setPredicate('dcat:contactPoint');
    });

    graph.find(null, 'adms:version').forEach((stmt) => {
      stmt.setPredicate('owl:versionInfo');
    });

    graph.find().forEach((stmt) => {
      const v = stmt.getValue().trim();
      if (stmt.getDatatype() === dt && (v === '' || isNaN(Date.parse(v)) == null)) {
        stmt.setAsserted(false);
      }
    });
  },
  spelling(graph) {
    graph.find(null, 'vcard:Organisation').forEach((stmt) => {
      stmt.setPredicate('vcard:Organization');
    });
  },
  accessRights(graph) {
    const values = ['PUBLIC', 'RESTRICTED', 'NON_PUBLIC'].map((ln) =>
      `http://publications.europa.eu/resource/authority/access-right/${ln}`);
    graph.find(null, 'dcterms:accessRights').forEach((stmt) => {
      if (stmt.getType() !== 'uri' || values.indexOf(stmt.getValue()) === -1) {
        stmt.setAsserted(false);
      }
    });
  },
  publisherAsKeyword(graph) {
    const pub2name = {};
    graph.find(null, 'rdf:type', 'dcat:Dataset').forEach((stmt) => {
      const pub = graph.findFirstValue(stmt.getSubject(), 'dcterms:publisher');
      if (pub) {
        let pubname = pub2name[pub];
        if (!pubname) {
          pubname = graph.findFirstValue(pub, 'foaf:name');
          pub2name[pub] = pubname;
        }
        if (pubname) {
          graph.addL(stmt.getSubject(), 'dcat:keyword', pubname);
        }
      }
    });
  },
  fiveStars(graph) {
    // Todo
  },
  removeNonURILicenses(graph) {
    graph.find(null, 'dcterms:license').forEach((stmt) => {
      if (stmt.getType() !== 'uri') {
        graph.remove(stmt);
      }
    });
  },
  licenseOnDataset(uri, datasetGraph, dist2graph) {
    const licenses = new Set();
    datasetGraph.find(uri, 'dcat:distribution').forEach((stmt) => {
      const g = dist2graph[stmt.getValue()];
      if (g) {
        g.find(stmt.getValue(), 'dcterms:license').forEach(lStmt => licenses.add(lStmt.getValue()));
      }
    });
    datasetGraph.find(uri, 'dcterms:license').forEach((stmt) => {
      if (stmt.getType() === 'uri') {
        licenses.add(stmt.getValue());
      }
      stmt.setAsserted(false);
    });
    licenseFix(licenses).forEach(l => datasetGraph.add(uri, 'dcterms:license', l));
  },
  formatOnDataset(uri, datasetGraph, dist2graph) {
    const formats = new Set();
    datasetGraph.find(uri, 'dcat:distribution').forEach((stmt) => {
      const g = dist2graph[stmt.getValue()];
      if (g) {
        g.find(stmt.getValue(), 'dcterms:format').forEach((lStmt) => {
          lStmt.setValue((lStmt.getValue() || '').trim()); // trim away blanks on some formats
          formats.add(lStmt.getValue())
        });
      }
    });
    formats.forEach(l => datasetGraph.addL(uri, 'dcterms:format', l));
  },
};
