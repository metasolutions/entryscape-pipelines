const ckan2dcatconverter = require('./ckan2dcatconverter');
const fs = require('fs');
const http = require('http');
const https = require('https');
const logger = require('./logger/logger');

const ckan = {
  /**
   * It imports datasets for given orgnization into entry store
   * And it also updates catalog metadata
   * @param urlOrgInfo - string ,url which is used to read catalog metadata
   * @param urlDatasets - string,url which is used to get datasets
   * @param dcatEntry - dcatEntry
   * @returns {entryPromise} - upon success promise returns catalog entry
   */
  importCKAN(urlOrgInfo, urlDatasets, dcatEntry) {
    const context = dcatEntry.getContext();
    return ckan.loadJSONFromURL(urlDatasets).then(ckanJSON =>
      ckan2dcatconverter.convert(ckanJSON).then(datasets =>
        // need to modify following
        ckan._createAllResponsibles(datasets, context).then(responsibles =>
          ckan._createAllDatasetsWithDistributions_ckan(dcatJSON, context, responsibles)
          .then(datasetEntries => ckan._updateCatalog_ckan(datasetEntries, dcatEntry,
            responsibles, urlOrgInfo)))));
  },
  /**
   * Updates dcat catalog with meta data and datasets
   * @param datasetEntries - an array of dataset entries to update catalog
   * @param dcatEntry - dcat entry
   * @param responsibles - an object,which holds all publisher entries and contact point entries
   * @param urlOrgInfo - url to get catalog's metadata
   * @returns {promise} - upon success promise returns updated dcat catalog entry
   * @private
   */
  _updateCatalog_ckan(datasetEntries, dcatEntry, responsibles, urlOrgInfo) {
    return ckan.loadJSONFromURL(urlOrgInfo).then((catalogJSON) => {
      dcatEntry.addL('dcterms:title', catalogJSON.result.title, 'en');
      dcatEntry.addL('dcterms:description',
        catalogJSON.result.description, 'en');
      datasetEntries.forEach((datasetEntry) => {
        dcatEntry.add('dcat:dataset', datasetEntry.getResourceURI());
      });
      return dcatEntry.commitMetadata();
    });
  },
  /**
   * Reads each dataset statement from JSON, creates distributions for it
   * creates dataset entry and updates meta data of dataset with distributions,
   * publishers and contact points and commits
   * @param dcatJSON - input json object, which holds ckan data to be imported
   * @param context
   * @param responsibles
   * @returns {array} - upon success, returns an array of newly created dataset entries
   * @private
   */
  _createAllDatasetsWithDistributions_ckan(dcatJSON, context, responsibles) {
    const datasets = dcatJSON.datasets;
    const datasetEntries = [];
    datasets.forEach((dataset) => {
      datasetEntries.push(ckan._createDistributionsForDataset_ckan(dataset, context)
        .then(distEntries => ckan._createDataset_ckan(dataset, distEntries,
          context, responsibles)));
    });
    return Promise.all(datasetEntries);
  },

  /**
   * Reads each distribution data from ingraph for given dataset and creates entry in store
   * @param dataset - json object, for which distributions will be created
   * @param context - where to create distributions
   * @returns {array} - upon success returns an array of distribution entries
   * @private
   */
  _createDistributionsForDataset_ckan(dataset, context) {
    const distributionEntries = [];
    const distributions = dataset.distributions;
    distributions.forEach((distribution) => {
      distributionEntries.push(ckan._createEntry(context, distribution.metadata, 'dcat:Distribution'));
    });
    return Promise.all(distributionEntries);
  },

  /**
   * Creates dataset entry
   * read publisher for given dataset and its corresponding entries from responsibles,
   * add to dataset metadata read contact points  for given dataset and its corresponding
   * entries from responsibles,add to dataset metadata
   * updates newly created dataset metadata with distribution entries
   * save/commits dataset entry in entry store
   * @param dataset input json object,which holds dataset values
   * @param distributionEntries,an array of distribution entries
   * @param context where to create datasets
   * @param responsibles, an object of all responsibles
   * @returns {entryPromise} - on success returns promise with new datasetentry
   * @private
   */
  _createDataset_ckan(dataset, distributionEntries, context, responsibles) {
    const pDatasetEntry = context.newEntry();
    const pDatasetResourceURI = pDatasetEntry.getResourceURI();
    const pDatasetMetadata = pDatasetEntry.getMetadata();
    const datasetMetaData = dataset.metadata; // json object
    for (const property in datasetMetaData) {
      if (datasetMetaData.hasOwnProperty(property)) {
        const objectArray = datasetMetaData[property];
        objectArray.forEach((valObj) => {
          pDatasetMetadata.add(pDatasetResourceURI, property, valObj);
        });
      }
    }
    // Distributions
    distributionEntries.forEach((distributionEntry) => {
      pDatasetMetadata.add(pDatasetResourceURI, 'dcat:distribution', distributionEntry.getResourceURI());
    });

    // publishers
    const publisherEntries = responsibles.id2PEntries;
    const publishers = dataset.publisher;
    publishers.forEach((publisher) => {
      pDatasetMetadata.add(pDatasetResourceURI, 'dcterms:publisher', publisherEntries[publisher.id].getResourceURI());
    });

    // contact points
    const contactPtEntries = responsibles.id2CPEntries;
    const contactPts = dataset.contactPoint;
    contactPts.forEach((contactPt) => {
      pDatasetMetadata.add(pDatasetResourceURI, 'dcat:contactPoint', contactPtEntries[contactPt.id].getResourceURI());
    });

    pDatasetMetadata.add(pDatasetResourceURI, 'rdf:type', 'dcat:Dataset');
    return pDatasetEntry.commit().then(null, (err) => {
      logger.info(`Found an error in _createDataset_ckan: ${err}`);
    });
  },
  /**
   * creates entry in entry store
   * returns promise of newly created entry
   * @param context - where to create entry
   * @param metadata - json object ,metadata of entry
   * @param rdfType - rdf type of entry
   * @returns {entryPromise} - upon success promise returns newly created entry
   * @private
   */
  _createEntry(context, metadata, rdfType) {
    const pEntry = context.newEntry();
    const pMetadata = pEntry.getMetadata();
    const pResourceURI = pEntry.getResourceURI();
    for (const property in metadata) {
      if (metadata.hasOwnProperty(property) !== 'id') {
        const objectArray = metadata[property];
        objectArray.forEach((valObj) => {
          pMetadata.add(pResourceURI, property, valObj);
        });
      }
    }
    pMetadata.add(pResourceURI, 'rdf:type', rdfType);
    return pEntry.commit().then(null, (err) => {
      logger.info(`Found an error in _createEntry: ${err}`);
    });
  },

  /**
   * creates entries for all resposibles which are available in dcatJSON like
   * publishers, contactpoints
   * @param datasets - json object,which holds ckan data
   * @param context - where to create responsibles
   * @returns {object} - upon successful creation of responisbles returns an object
   * having 2 keys like id2PEntries,id2CPEntries
   * and value ,and value is key -value array holding mail id as key and value is its entry
   * @private
   */
  _createAllResponsibles(datasets, context) {
    // using mail id as unique id and add to id2publishers
    const createResposibleEntries = (responsible, id2ResponsiblePs, rdfType) => {
      if (!id2ResponsiblePs[responsible.id]) {
        // create new entry and add to id2ResponsiblePs
        id2ResponsiblePs[responsible.id] = ckan._createEntry(context,
          responsible.metadata, rdfType);
      }
    };
    const id2PublisherPs = {};
    const id2ContactPtPs = {};
    // get all publishers from all datasets
    // get all contact point from all datasets
    const responsibles = ckan._getResposiblesFromDatasets(datasets);
    const publishers = responsibles.publishers;
    const contactPts = responsibles.contactPts;
    // check each publisher if exists in id2publishers,if not create and add
    // check each contact point if exists in id2publishers,if not create and add
    publishers.forEach((publisher) => {
      createResposibleEntries(publisher, id2PublisherPs, 'dcterms:publisher');
    });
    contactPts.forEach((contactPt) => {
      createResposibleEntries(contactPt, id2ContactPtPs, 'dcat:contactPoint');
    });

    return Promise.all(id2ContactPtPs).then(cntPs => Promise.all(id2PublisherPs)
      .then(publisherPs => ({ id2PEntries: publisherPs, id2CPEntries: cntPs })));
  },

  /**
   * to get all resposibles from input json
   * @param datasets - array of dataset objects, which holds ckan data
   * @returns {publishers: Array, contactPts: Array}
   * @private
   */
  _getResposiblesFromDatasets(datasets) {
    let publisherPs = [];
    let cntPs = [];
    datasets.forEach((dataset) => {
      publisherPs = publisherPs.concat(dataset.publisher);
      cntPs = cntPs.concat(dataset.contactPoint);
    });
    return { publishers: publisherPs, contactPts: cntPs };
  },

  /**
   * utility to load json from filepath
   * @param filePath - string filpath
   * @returns {Promise} a promise with a ckan json
   */
  loadJSONFromFile(filePath) {
    return new Promise((resolve, reject) => {
      fs.readFile(filePath, (err, data) => {
        if(!error) {
          resolve(JSON.parse(data));
        }
        else {
          reject(`Could not read file ${filePath}`);
        }
      });
    });
  },
  /**
   * utility to load json from URL
   * @param url
   * @returns {Promise}
   */
  loadJSONFromURL(url) {
    return new Promise((resolve, reject) => {
      https.get(url, (response) => {
        // Continuously update stream with data
        let body = '';
        response.on('data', (txt) => {
          body += txt;
        });
        response.on('end', () => {
          // Data reception is done, do whatever with it!
          const parsed = JSON.parse(body);
          resolve(parsed);
        });
      });
    });
  },
};

module.exports = ckan;
