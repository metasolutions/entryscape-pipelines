const ArrayList = require('./ArrayList');
const config = require('../config');

/**
 * A Group-Context-Entry list (GCEList) provides a list of entries corresponding to objects
 * (like data-catalogs and conceptschemes) maintained in separate contexts.
 * Keeping the objects (and all belonging entries) in separate context provides
 * good maintainability and access control. A separate group is also defined per context
 * to allow collaboration. The current user may have access to zero or more of
 * these objects which is detected by checking memberships of groups, checking
 * their home-contexts and then for a corresponding entry in each context.
 */
module.exports = class extends ArrayList {
  constructor(params) {
    this.userEntry = params.userEntry;
    this.entryType = params.entryType;
    this.contextId2groupId = {};
  }

  setGroupIdForContext(contextId, groupId) {
    this.contextId2groupId[contextId] = groupId;
  }

  getGroupId(contextId) {
    return this.contextId2groupId[contextId];
  }

  loadEntries() {
    let es = config.entrystore,
      groupURIs = this.userEntry.getParentGroups();
    return Promise.all(groupURIs.map(guri => es.getEntry(guri)))
      .then(this.extractEntriesFromGroups.bind(this))
      .then(function (entries) {
        this.entries = entries;
        return entries;
      }.bind(this));
  }

  extractEntriesFromGroups(groupEntryArr) {
    let es = config.entrystore,
      esu = config.entrystoreutil,
      hcArr = groupEntryArr.map(ge => ge.getResource(true).getHomeContext());

    let entries = [],
      entryDefs = [];
    hcArr.forEach(function (hc, idx) {
      if (hc != null) {
        this.setGroupIdForContext(hc, groupEntryArr[idx].getId());

        entryDefs.push(esu.getEntryByType(this.entryType, es.getContextById(hc)).then((entry) => {
          entries[idx] = entry;
        }, () => {
          entries[idx] = null;
        }));
      } else {
        entries[idx] = null;
      }
    }, this);
    return Promise.all(entryDefs).then(() => entries.filter(entry => entry !== null));
  }
};
